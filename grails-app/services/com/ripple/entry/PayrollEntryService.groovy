package com.ripple.entry

import com.ripple.deductions.PhilHealthContribution
import com.ripple.deductions.SssContribution
import com.ripple.deductions.WithholdingTaxPeriod
import com.ripple.deductions.WithholdingTaxStatus
import com.ripple.loans.LoanType
import com.ripple.master.Employee
import com.ripple.master.Rate
import org.joda.time.LocalDate

import java.math.RoundingMode

class PayrollEntryService {

    boolean transactional = true
    def workingHoursService
    def payPeriodService
    def loanService
    def rateService
    def deductionsService

    def recompute(pe) {

        def grossPay = pe.getBasicPay() +
                pe.getRegularOvertimePay() +
                pe.getSundayPay() +
                pe.getLegalHolidayPay() +
                pe.getSpecialHolidayPay() +
                pe.getCola() -
                pe.getLateValue() + pe.getAnnualLeaveCredits()

        def netPay = (grossPay - pe.getSssContribution()
                - pe.getPhilHealthContribution()
                - pe.getPagIbigContribution()
                - pe.getWithholdingTax()
                - pe.getAdvances()
                + pe.getAnnualLeaveCredits()
//            +pe.getBonuses()
                - loanService.computeTotalLoanPayments(pe))

        populateLoanPayments(pe)
        pe.setGrossPay(grossPay)
        pe.setNetPay(netPay)
        pe.save()
    }

    def populateLoanPayments(pe) {
        pe.setCashAdvance(0.00)
        pe.setSssLoan(0.00)
        pe.setHdmfLoan(0.00)
        pe.setPagIbigLoan(0.00)

        pe?.payments.each {it ->
            switch (it?.loan?.loanType?.code) {
                case LoanType.CASH_ADVANCE:
                    pe.setCashAdvance(it?.amount)
                    break;
                case LoanType.SSS_LOAN:
                    pe.setSssLoan(it?.amount)
                    break;
                case LoanType.HDMF_LOAN:
                    pe.setHdmfLoan(it?.amount)
                    break;
                case LoanType.HOUSING_LOAN:
                    pe.setPagIbigLoan(it?.amount)
                    break;
                default:
                    break;
            }
        }
        pe.save()
    }

    def calculatePay(workingHours) {
        Employee employee = workingHours.getEmployee()
        def currentPayPeriod = workingHours.getPayPeriod()

        PayrollEntry payrollEntry = PayrollEntry.findByPayPeriodAndEmployee(currentPayPeriod, employee)
        if (payrollEntry == null)
            payrollEntry = new PayrollEntry()

        payrollEntry.setEmployee(employee)
        payrollEntry.setPayPeriod(currentPayPeriod)

        payrollEntry = computeOvertimeAndAbsences(payrollEntry, workingHours, currentPayPeriod, employee)

        //only compute deductions for 2nd payperiod. Loan Payments are made on the first pay period.
        if (currentPayPeriod.getFirstPayPeriod()) {
            loanService.addLoanPayments(payrollEntry, employee)
            payrollEntry.setNetPay(payrollEntry.getGrossPay() - loanService.computeTotalLoanPayments(payrollEntry))
            populateLoanPayments(payrollEntry)
        } else {
            payrollEntry = computeDeductions(employee, currentPayPeriod, payrollEntry)
        }
        payrollEntry.save()
        compute13thMonthPay(payrollEntry)
        getYearlyLeaveCredits(payrollEntry)

    }

    BigDecimal getGrossPay(Employee employee, PayPeriod... payPeriods) {
        def total = BigDecimal.ZERO
        payPeriods?.each {payPeriod ->
            if (payPeriod == null)
                return

            def payrollEntry = PayrollEntry.findByEmployeeAndPayPeriod(employee, payPeriod)
            if (payrollEntry == null)
                return

            total = total.add(payrollEntry.getGrossPay() - payrollEntry.getCola())
        }
        return total.setScale(2, RoundingMode.HALF_EVEN)
    }

    BigDecimal getGrossPayWithCola(Employee employee, PayPeriod... payPeriods) {
        def total = BigDecimal.ZERO
        payPeriods?.each {payPeriod ->
            if (payPeriod == null)
                return

            def payrollEntry = PayrollEntry.findByEmployeeAndPayPeriod(employee, payPeriod)
            if (payrollEntry == null)
                return

            total = total.add(payrollEntry.getGrossPay())
        }
        return total.setScale(2, RoundingMode.HALF_EVEN)
    }

    def getPreviousPayrollEntries(employee, pp) {
        def c = PayrollEntry.createCriteria()
        def cal = new LocalDate(pp?.monthYear?.year, 1, 1)
        def periodEnd = pp.periodEndDate
        def employmentDate = employee.employmentDate
        def payrollEntries = c.list {
            and {
                payPeriod {
                    and {
                        ge('monthYear', cal)
                        ge('monthYear', employmentDate)
                        le('monthYear', periodEnd)
                    }
                }
                eq('employee.id', employee.id)
            }

        }
        return payrollEntries
    }

    def compute13thMonthPay(payrollEntry) {
        def payrollEntries = getPreviousPayrollEntries(payrollEntry.employee, payrollEntry.payPeriod)
        def bonus = 0.00
        if (payrollEntries?.basicPay?.sum())
            bonus = ((payrollEntries?.basicPay?.sum() - payrollEntries.lateValue.sum()) / 12)
        payrollEntry.setBonuses(bonus)
        payrollEntry.save()
    }

    def getYearlyLeaveCredits(payrollEntry) {
        def employee = payrollEntry.employee
        def dailyRate = rateService.getDailyRate(employee)
        def payPeriod = payrollEntry.payPeriod
        LocalDate aDate = employee.employmentDate
        if (aDate.year == (payPeriod?.monthYear?.getYear()))
            return;
        def anniversary = new org.joda.time.LocalDate(payPeriod?.monthYear?.getYear(), aDate.monthOfYear, aDate.dayOfMonth)
        if (anniversary >= payPeriod.getPeriodStartDate() &&
                anniversary <= payPeriod.getPeriodEndDate()) {
            payrollEntry.setAnnualLeaveCredits(dailyRate * 5)
            payrollEntry.setNetPay(payrollEntry.getNetPay() + payrollEntry.getAnnualLeaveCredits())
            payrollEntry.setGrossPay(payrollEntry.getGrossPay() + payrollEntry.getAnnualLeaveCredits())
            payrollEntry.save()
        }
    }


    def computeDeductions(employee, currentPayPeriod, payrollEntry) {
        if (employee?.getEmergencyLaborer()) {
            payrollEntry.setNetPay(payrollEntry.getGrossPay())
            payrollEntry.save()
            return payrollEntry
        }

        if (!employee.getRetired()) {
            def previousPayPeriod = payPeriodService.getPreviousPayPeriod(currentPayPeriod)
            def regularPay = deductionsService.getBasicPay(employee, currentPayPeriod, previousPayPeriod)
            def totalGrossPay = getGrossPay(employee, currentPayPeriod, previousPayPeriod)
            def previousPayrollEntry = getPayrollEntryByPayPeriod(previousPayPeriod)
            def previousValue = 0.00
            if (previousPayrollEntry != null)
                previousValue = previousPayrollEntry?.getLateValue()
            def basicPay = regularPay - payrollEntry?.getLateValue() - previousValue

            // actual value computations
            // basicPay - absences/undertime - cola
            def philHealth = deductionsService.getEmployeePhilHealthContribution(basicPay)
            def philHealthER = deductionsService.getEmployerPhilHealthContribution(basicPay)


            def sss = deductionsService.getEmployeeSssContribution(basicPay)
            def sssECC = deductionsService.getEmployeeSssCompensation(basicPay)
            def sssER = deductionsService.getEmployerSssContribution(basicPay)

            def previousPayrollEntryLateValue = 0.00
            if (previousPayrollEntry)
                previousPayrollEntryLateValue = previousPayrollEntry?.getLateValue()


            def pagIbig = deductionsService.getEmployeePagIbigContribution(basicPay - payrollEntry?.getLateValue() - previousPayrollEntryLateValue)
            def pagIbigER = deductionsService.getEmployerPagIbigContribution(basicPay - payrollEntry?.getLateValue() - previousPayrollEntryLateValue)
            // basic pay - overtime - cola, - absences
            def tax = deductionsService.getWithholdingTaxDeduction(employee, (basicPay - payrollEntry?.getLateValue() - previousPayrollEntryLateValue))

            if (currentPayPeriod.toLocalDate().monthOfYear == 12 && !currentPayPeriod.getFirstPayPeriod() && !employee.getRetired()) {
                // compute YTD tax.
                def ppe = getPreviousPayrollEntries(employee, currentPayPeriod)
                def grossTaxable = ppe.basicPay.sum() - ppe.lateValue.sum() - ppe.sssContribution.sum() - ppe.philHealthContribution.sum() - ppe.pagIbigContribution.sum()

                def wts = WithholdingTaxStatus.findByPeriodAndName(WithholdingTaxPeriod.findByName(WithholdingTaxPeriod.MONTHLY), employee.taxStatus)
                def exemption = deductionsService.getWithholdingTaxStatusExemption(wts) * 1000
                grossTaxable = grossTaxable - exemption;

                def map = [
                        ['low': 0.00, 'high': 10000.00, 'value': 0.00, 'percent': 0.05],
                        ['low': 10000.00, 'high': 30000.00, 'value': 500.00, 'percent': 0.10],
                        ['low': 30000.00, 'high': 70000.00, 'value': 2500.00, 'percent': 0.15],
                        ['low': 70000.00, 'high': 140000.00, 'value': 8500.00, 'percent': 0.20],
                        ['low': 140000.00, 'high': 250000.00, 'value': 22500.00, 'percent': 0.25],
                        ['low': 250000.00, 'high': 500000.00, 'value': 50000.00, 'percent': 0.30],
                        ['low': 500000.00, 'high': 10000000.00, 'value': 125000.00, 'percent': 0.34]
                ]
                def taxValue = 0.00
                def totalTaxPaid = ppe.withholdingTax.sum() - payrollEntry.getWithholdingTax()
                if (grossTaxable < 0.00) {
                    taxValue = -totalTaxPaid
                } else {
                    def taxBracket = map.findAll {
                        it.low <= grossTaxable && it.high > grossTaxable || grossTaxable <= 0 && it.low == 0.00
                    }
                    grossTaxable = grossTaxable - taxBracket["low"]
                    taxValue = grossTaxable * taxBracket["percent"]
                    taxValue = taxValue + taxBracket["value"]
                    taxValue = -totalTaxPaid + taxValue
                }
                tax = taxValue.setScale(0, RoundingMode.HALF_EVEN)
            }

            if (employee.philHealthContribution) {
                def philHealthEntry = PhilHealthContribution.findByEmployeeShare(employee.philHealthContribution, [cache: true])
                philHealth = philHealthEntry.employeeShare
                philHealthER = philHealthEntry.employerShare
            }

            if (employee.sssContribution) {
                def sssEntry = SssContribution.findByEmployeeContribution(employee.sssContribution, [cache: true])
                sss = sssEntry?.employeeContribution
                sssECC = sssEntry?.employeeCompensation
                sssER = sssEntry?.employerContribution
            }

            payrollEntry.setSssEmployeeCompensation(sssECC)
            payrollEntry.setSssContributionEmployer(sssER)
            payrollEntry.setPhilHealthContributionEmployer(philHealthER)
            payrollEntry.setPagIbigContributionEmployer(pagIbigER)

            payrollEntry.setPhilHealthContribution(philHealth)
            payrollEntry.setSssContribution(sss)
            payrollEntry.setPagIbigContribution(pagIbig)
            payrollEntry.setWithholdingTax(tax)
            payrollEntry.setNetPay(payrollEntry.getGrossPay() - philHealth - sss - pagIbig - tax - loanService.computeTotalLoanPayments(payrollEntry))
        } else {
            payrollEntry.setSssEmployeeCompensation(0.00)
            payrollEntry.setSssContributionEmployer(0.00)
            payrollEntry.setPhilHealthContributionEmployer(0.00)
            payrollEntry.setPagIbigContributionEmployer(0.00)
            payrollEntry.setPhilHealthContribution(0.00)
            payrollEntry.setSssContribution(0.00)
            payrollEntry.setPagIbigContribution(0.00)
            payrollEntry.setWithholdingTax(0.00)

            payrollEntry.setNetPay(payrollEntry.getGrossPay())
        }
        return payrollEntry
    }

    def computeOvertimeAndAbsences(payrollEntry, workingHours, currentPayPeriod, employee) {
        def dailyRate = rateService.getDailyRate(employee, currentPayPeriod.getPeriodStartDate())
        def currentRate = rateService.getRate(employee, currentPayPeriod.getPeriodStartDate())
        def lateHoursDeduction = 0.00
        def regularOvertimePay = 0.00
        def legalHolidayPay = 0.00
        def specialHolidayPay = 0.00
        def sundayPay = 0.00
        def finalCola = 0.00

        def (totalRegularOvertimeHours, totalRegularWorkingHours, totalLateHours, totalSundayHours, totalRegularHolidayHours, totalLegalHolidayHours, totalDaysWorked) = workingHoursService.computeDerivedHours(workingHours)

        //hours
        payrollEntry.setRegularHoursWorked(totalRegularWorkingHours)    // actual hours worked
        payrollEntry.setLateHours(totalLateHours)                       // hours late, including absences
        payrollEntry.setTotalRegularHours(totalLateHours + totalRegularWorkingHours)  //regular work hours in pay period
        payrollEntry.setRegularOvertimeHours(totalRegularOvertimeHours)             //overtime hours
        payrollEntry.setSundayHoursWorked(totalSundayHours)                         //sunday hours
        payrollEntry.setSpecialHolidayHours(totalRegularHolidayHours)               //regular holiday hours
        payrollEntry.setLegalHolidayHours(totalLegalHolidayHours)                   // legal holiday hours. x 2 credit for work rendered on that day.
        payrollEntry.setTotalDaysWorked(totalDaysWorked)                            //total actual days worked


        def basicPay = deductionsService.getBasicPay(employee, currentPayPeriod)    // basic pay for current pay period (without any deductions)
        payrollEntry.setBasicPay(basicPay)

        def regularHoursPay = rateService.computeBasicPay(totalRegularWorkingHours, dailyRate)// actual hours worked pay.
        payrollEntry.setRegularHoursPay(regularHoursPay)

        if (employee?.getEmergencyLaborer()) {
            def hourlyRate = dailyRate / 8
            lateHoursDeduction = rateService.computeLateDeduction(totalLateHours, dailyRate)
            regularOvertimePay = hourlyRate * totalRegularOvertimeHours
            legalHolidayPay = hourlyRate * totalLegalHolidayHours
            specialHolidayPay = totalRegularHolidayHours * hourlyRate
            sundayPay = totalSundayHours * hourlyRate

        } else if (!currentRate?.getExemptFromDeductions()) {
            lateHoursDeduction = rateService.computeLateDeduction(totalLateHours, dailyRate)
            regularOvertimePay = rateService.computeRegularOvertimePay(totalRegularOvertimeHours, dailyRate)
            if (currentRate?.getType() == Rate.MONTHLY && !currentRate?.getExemptFromDeductions()) {
                legalHolidayPay = rateService.computeSpecialHolidayPay(totalLegalHolidayHours / 2, dailyRate)
            } else {
                legalHolidayPay = rateService.computeLegalHolidayPay(totalLegalHolidayHours, dailyRate)
            }
            specialHolidayPay = rateService.computeSpecialHolidayPay(totalRegularHolidayHours, dailyRate)
            sundayPay = rateService.computeSpecialHolidayPay(totalSundayHours, dailyRate)
        }else if (currentRate?.getExemptFromDeductions()){
            lateHoursDeduction = regularOvertimePay = legalHolidayPay = specialHolidayPay = sundayPay = 0.00
        }

        if (!employee?.getEmergencyLaborer())
            finalCola = deductionsService.getFinalCola(employee, currentPayPeriod, totalDaysWorked)

        payrollEntry.setRegularOvertimePay(regularOvertimePay)
        payrollEntry.setLegalHolidayPay(legalHolidayPay)
        payrollEntry.setSpecialHolidayPay(specialHolidayPay)
        payrollEntry.setSundayPay(sundayPay)
        payrollEntry.setLateValue(lateHoursDeduction)
        payrollEntry.setCola(finalCola)

        payrollEntry.setGrossPay(basicPay + regularOvertimePay + sundayPay + legalHolidayPay + specialHolidayPay + finalCola - lateHoursDeduction)

        payrollEntry.setPayPeriodLabel(currentPayPeriod.toString())
        payrollEntry.save(flush: true)
        return payrollEntry
    }



    def getPayrollEntryByPayPeriod(payPeriod) {
        return PayrollEntry.findByPayPeriod(payPeriod)
    }
}
