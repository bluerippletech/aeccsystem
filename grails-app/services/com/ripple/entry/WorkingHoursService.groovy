package com.ripple.entry

import com.ripple.master.Holiday
import com.ripple.master.Rate

class WorkingHoursService {

    boolean transactional = true

    def rateService

    def serviceMethod() {

    }
    // processes workinghours entry and extracts the number of hours.

    def computeDerivedHours(workingHours) {
        def totalRegularOvertimeHours = 0.00
        def totalRegularWorkingHours = 0.00
        def totalLateHours = 0.00
        def totalSundayHours = 0.00
        def totalRegularHolidayHours = 0.00
        def totalLegalHolidayHours = 0.00
        def currentDayValue = 0.00
        def totalDaysWorked = 0.00
        def pp = workingHours.getPayPeriod()
        def currentDate = pp.getPeriodStartDate()
        def endDate = pp.getPeriodEndDate()
        def employee = workingHours.getEmployee()
        def currentRate = rateService.getRate(employee, pp.getPeriodStartDate())

        while (currentDate != endDate.plusDays(1)) {
            currentDayValue = workingHours.getDayValue(currentDate.getDayOfMonth())
            def holiday = Holiday.findByHolidayDate(currentDate, [cache: true])

            if (currentDate.getDayOfWeek() == 7) {
                totalSundayHours += currentDayValue
                totalDaysWorked += getDayWorkRatio(currentDayValue)
            }
            else if (holiday != null
                    && (holiday?.applicableSites*.id?.contains(employee?.getCurrentAssignment()?.getWorkLocation()?.id) || holiday?.applicableSites.size() == 0)
            ) {
                if (holiday.legalHoliday) {
//                    def hasWorked = findWorkingHoursForEmployee(workingHours.getEmployee(), pp, currentDate)
                    def hasWorked = checkIfEmployeeWorkedPriorToDate(workingHours.getEmployee(), pp, currentDate)
                    if (hasWorked && currentDayValue == 0) {
                        if (!(currentRate?.getType() == Rate.MONTHLY && !currentRate?.getExemptFromDeductions())) {
                            totalLegalHolidayHours += 8

                        }
                        totalDaysWorked += 1
                    } else if (currentDayValue > 0) {
                        if (currentRate.getType() == Rate.MONTHLY)
                            totalDaysWorked += 1
                        else
                            totalDaysWorked += getDayWorkRatio(currentDayValue)
                        totalLegalHolidayHours += (workingHours.getDayValue(currentDate.getDayOfMonth()) * 2)
                    }
                } else {
                    totalRegularHolidayHours += currentDayValue
                    totalDaysWorked += getDayWorkRatio(currentDayValue)
                }
            } else if (currentDayValue >= 8) {
                totalRegularOvertimeHours += currentDayValue - 8
                totalRegularWorkingHours += 8
                totalDaysWorked += getDayWorkRatio(currentDayValue)
            } else if (currentDayValue < 8) {
                totalLateHours += 8 - currentDayValue
                totalRegularWorkingHours += currentDayValue
                totalDaysWorked += getDayWorkRatio(currentDayValue)
            } else {
                println "Warning, exception here. or at least a log file. to indicate that no conditions matched for the computation of the hours. log the day here."
            }
            currentDate = currentDate.plusDays(1)
        }

        [totalRegularOvertimeHours, totalRegularWorkingHours, totalLateHours, totalSundayHours, totalRegularHolidayHours, totalLegalHolidayHours, totalDaysWorked]
    }

    BigDecimal getTotalHoursRegularWork(workingHours) {

    }

    BigDecimal getDayWorkRatio(hours) {
        if (hours < new BigDecimal("8.00")) {
            hours.divide(new BigDecimal("8.00"))
        } else {
            new BigDecimal("1.00")
        }
    }

    /*Given an employee, the current PayPeriod, and the currentDate,
    *this method will return true if the employee has worked on the previous working day, or on holidays and/or sundays in between it.
    *False if otherwise.
    */

    Boolean checkIfEmployeeWorkedPriorToDate(employee, currentPayPeriod, dateToFind) {
        def payperiod = currentPayPeriod
        def currentDate = dateToFind.minusDays(1)
        def validWorkDayfound = false;
        def hasWorked = false;
        while (!validWorkDayfound) {
            if (currentDate.getDayOfWeek() == 7 || ((Holiday.findByHolidayDate(currentDate)) != null)) {
                currentDate = currentDate.minusDays(1)
            } else {
                validWorkDayfound = true;
                break;
            }


        }
        if (payperiod.getPeriodStartDate() > currentDate) {
            payperiod = PayPeriod.executeQuery('from PayPeriod where id < :currentid order by id desc', [currentid: currentPayPeriod.id, max: 1])
        }

        if (payperiod.size()>0) {
            def workingHours = WorkingHours.findByPayPeriodAndEmployee(payperiod[0], employee)
            if (workingHours) {
                hasWorked = (workingHours.getDayValue(currentDate.getDayOfMonth()) > 0)
            }

        }
        return hasWorked;
    }




    Boolean findWorkingHoursForEmployee(employee, currentPayPeriod, dateToFind) {
        def hasWorked = false
        def previousWorkDay = dateToFind.minusDays(1)
        if (currentPayPeriod.getPeriodStartDate() == dateToFind) {
            //start searching from previous pay period.
            def previousPayPeriod = PayPeriod.executeQuery('from PayPeriod where id < :currentid order by id desc', [currentid: currentPayPeriod.id, max: 1])
            if (previousPayPeriod[0] != null) {
                def ppTemp = PayPeriod.get(previousPayPeriod[0].id)
                hasWorked = findWorkingHoursForEmployee(employee, ppTemp, currentPayPeriod.getPeriodStartDate())
            } else {
                hasWorked = false
            }
        } else {
            def whi = WorkingHours.findByPayPeriodAndEmployee(currentPayPeriod, employee)
            if (whi != null) {
                while (previousWorkDay != currentPayPeriod.getPeriodStartDate()) {
                    if (previousWorkDay.getDayOfWeek() == 7 && whi.getDayValue(previousWorkDay.getDayOfMonth()) > 0) {
                        hasWorked = true
                        break
                    } else if (Holiday.findByHolidayDate(previousWorkDay, [cache: true]) != null || previousWorkDay.getDayOfWeek() == 7) {
                        previousWorkDay = previousWorkDay.minusDays(1)
                    } else {
                        if (whi.getDayValue(previousWorkDay.getDayOfMonth()) > 0)
                            hasWorked = true
                        else
                            hasWorked = false
                        break
                    }
                }
            } else {
                hasWorked = false
            }
        }
        return hasWorked
    }

    /*
    * Sets the default values for a new entry of working hours.
    * For Regular days, 8.00 Hours
    * For Holidays and Sundays, 0.00 Hours.
    *
    */

    void setWorkingHoursDefaultValues(workingHours, payPeriod) {
        PayPeriod pp = payPeriod
        def startDate = pp.getPeriodStartDate()
        def endDate = pp.getPeriodEndDate()
        while (startDate != endDate.plusDays(1)) {
            if (Holiday.findByHolidayDate(startDate, [cache: true]) == null && startDate.getDayOfWeek() != 7) {
                workingHours.setDayValue(startDate.getDayOfMonth(), 8.00)
            } else {
                workingHours.setDayValue(startDate.getDayOfMonth(), 0.00)
            }
            startDate = startDate.plusDays(1)
        }
    }

}
