package com.ripple.entry

import com.ripple.master.Holiday
import org.joda.time.LocalDate

class PayPeriodService {

    boolean transactional = true

    List<LocalDate> getDays(PayPeriod payPeriod) {
        def startDate = payPeriod.getPeriodStartDate()
        def endDate = payPeriod.getPeriodEndDate()
        def days = new ArrayList<LocalDate>()
        for (def date = startDate; date.compareTo(endDate) <= 0; date = date.plusDays(1)) {
            days.add(new LocalDate(date))
        }
        return days
    }

    Integer getNumberOfDays(PayPeriod payPeriod) {
        getDays(payPeriod).size()
    }

    List<LocalDate> getRegularWorkingDays(PayPeriod payPeriod) {
        def startDate = payPeriod.getPeriodStartDate()
        def endDate = payPeriod.getPeriodEndDate()
        def days = new ArrayList<LocalDate>()
        for (def date = startDate; date.compareTo(endDate) <= 0; date = date.plusDays(1)) {
            if (date.getDayOfWeek() == 7)
                continue // Sunday, continue
            if (Holiday.findByHolidayDate(date,[cache:true]))
                continue // Holiday, continue
            days.add(new LocalDate(date))
        }
        return days
    }

    Integer getNumberOfRegularWorkingDays(PayPeriod payPeriod) {
        getRegularWorkingDays(payPeriod).size()
    }

    List<LocalDate> getHolidays(PayPeriod payPeriod) {
        def startDate = payPeriod.getPeriodStartDate()
        def endDate = payPeriod.getPeriodEndDate()
        def days = new ArrayList<LocalDate>()
        for (def date = startDate; date.compareTo(endDate) <= 0; date = date.plusDays(1)) {
            if (Holiday.findByHolidayDate(date,[cache:true]))
                days.add(new LocalDate(date))
        }
        return days
    }

    Integer getNumberOfHolidays(PayPeriod payPeriod) {
        getHolidays(payPeriod).size()
    }

    List<LocalDate> getLegalHolidays(PayPeriod payPeriod) {
        def startDate = payPeriod.getPeriodStartDate()
        def endDate = payPeriod.getPeriodEndDate()
        def days = new ArrayList<LocalDate>()
        for (def date = startDate; date.compareTo(endDate) <= 0; date = date.plusDays(1)) {
            def holiday = Holiday.findByHolidayDate(date,[cache:true])
            if (holiday == null)
                continue // Regular Working or Sunday, continue
            if (holiday.legalHoliday)
                days.add(new LocalDate(date))
        }
        return days
    }

    Integer getNumberOfLegalHolidays(PayPeriod payPeriod) {
        getLegalHolidays(payPeriod).size()
    }

    List<LocalDate> getSpecialHolidays(PayPeriod payPeriod) {
        def startDate = payPeriod.getPeriodStartDate()
        def endDate = payPeriod.getPeriodEndDate()
        def days = new ArrayList<LocalDate>()
        for (def date = startDate; date.compareTo(endDate) <= 0; date = date.plusDays(1)) {
            def holiday = Holiday.findByHolidayDate(date,[cache:true])
            if (holiday == null)
                continue // Regular Working or Sunday, continue
            if (holiday.legalHoliday)
                continue // Legal Holiday, continue
            days.add(new LocalDate(date))
        }
        return days
    }

    Integer getNumberOfSpecialHolidays(PayPeriod payPeriod) {
        getSpecialHolidays(payPeriod).size()
    }

    PayPeriod getPreviousPayPeriod(PayPeriod payPeriod) {
        def previous = PayPeriod.list().findAll { entry ->
            entry.compareTo(payPeriod) < 0
        } 
        previous ? previous.last() : null
    }
}
