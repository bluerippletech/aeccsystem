package com.ripple.report

import org.codehaus.groovy.grails.plugins.jasper.JasperService
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JRExporterParameter
import net.sf.jasperreports.engine.JasperPrint
import java.lang.reflect.Field
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

class ReportService extends JasperService{

    @Override
    public ByteArrayOutputStream generateReport(JasperReportDef reportDef) {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream()
        JRExporter exporter = generateExporter(reportDef)

        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArray)
        exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8")

        if (reportDef.fileFormat==JasperExportFormat.XLS_FORMAT){
            exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
        }

        JasperPrint jasperPrint = generatePrinter(reportDef)
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint)
        exporter.exportReport()

        return byteArray
    }



    def serviceMethod() {

    }
}
