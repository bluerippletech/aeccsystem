package com.ripple.master

class AssignmentService {

    boolean transactional = true

    def serviceMethod() {

    }

    def getProjectsJSON(params) {
        def sortIndex = params.sidx ?: 'projectName'
        def sortOrder = params.sord ?: 'asc'
        def maxRows = Integer.valueOf(params.maxRows ?: 10)

        def projects = Assignment.createCriteria().list(max: maxRows) {
            ilike('projectName', "%${params.term}%")
            order(sortIndex, sortOrder).ignoreCase()
        }

        def totalRows = projects.totalCount
        def numberOfPages = Math.ceil(totalRows / maxRows)
        def results = projects?.collect {
            [
                    id: it.id,
                    label:it.projectName,
                    value:it.projectName
            ]
        }
        results

    }
}
