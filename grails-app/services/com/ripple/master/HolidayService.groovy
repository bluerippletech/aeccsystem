package com.ripple.master

class HolidayService {

    boolean transactional = true

    def serviceMethod() {

    }

    def BigDecimal getOvertimeMultiplier(aDate){
        if (aDate==null)
          return 1.25/8
        else{
            def holiday = Holiday.findByHolidayDate(aDate,[cache:true])
            if (holiday.getLegalHoliday()){
                return (2*1.3)/8
            }else{
                return 1.3/8
            }
        }
    }


    String getStyle(aDate,aLocation){
        
        //sunday style
        if(aDate.getDayOfWeek()==7)
          return "color:gray"

        def hol=Holiday.findByHolidayDate(aDate,[cache:true])
        if (hol&&(hol?.applicableSites*.id.contains(aLocation?.id) ||hol?.applicableSites.size()==0)){
          if (hol.getLegalHoliday())
            return "color:green"
          else
            return "color:gold"
        }
          
    }
}
