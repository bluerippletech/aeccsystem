package com.ripple.master

import static com.ripple.master.DocumentNumbers.*

class DocumentNumbersService {

    static transactional = true

/*
    def generateDocumentNumber(transactionCode){
        return generateDocumentNumber(transactionCode, 7) // default Document Number Length if not defined
    }
*/

//	def generateDocumentNumber(transactionCode, documentNumberSize) {
    def generateDocumentNumber(transactionCode,documentNumberSize){
        generateDocumentNumber(transactionCode)
    }

    def generateDocumentNumber(transactionCode) {
        def documentNumbersInstance = DocumentNumbers?.findOrSaveWhere(documentType:transactionCode)
        def newDocNumber= (documentNumbersInstance.currentDocNumber.toInteger() + 1).toString().padLeft(7, '0')
        documentNumbersInstance.currentDocNumber = newDocNumber
        documentNumbersInstance.save()
        return newDocNumber
    }

    def generateDocumentNumber(transactionCode, status, value){
        if(status == "beforeInsert"){
            def documentNumbersInstance = DocumentNumbers?.findOrSaveWhere(documentType:transactionCode)
            def newDocNumber= value?.padLeft(7, '0')
            documentNumbersInstance.currentDocNumber = newDocNumber
            documentNumbersInstance.save()
            return newDocNumber
        }else if(status == "beforeRender"){
            def documentNumbersInstance = DocumentNumbers?.findOrSaveWhere(documentType:transactionCode)
            def newDocNumber= (documentNumbersInstance.currentDocNumber.toInteger()+1).toString().padLeft(7, '0')
            documentNumbersInstance.currentDocNumber = newDocNumber
            return newDocNumber
        }

    }
}
