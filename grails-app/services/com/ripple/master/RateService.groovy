package com.ripple.master

import org.joda.time.LocalDate

import java.math.RoundingMode

class RateService {

    boolean transactional = true

    def serviceMethod() {

    }

    def getDailyRate(Employee employee){
        Rate rate = employee.getRate()

        if (rate.type==Rate.MONTHLY){
            convertMonthlySalaryToDailySalary(rate.amount)
        }else{
            rate.amount
        }
    }

    Rate getRate(Employee employee, LocalDate date) {
        def rates = employee?.getRates()?.findAll { entry ->
            date?.compareTo(entry.getEffectivityDate()) >= 0
        }
        
        rates ? rates.last() : null
    }

    BigDecimal getDailyRate(Employee employee, LocalDate date) {
        def rate = getRate(employee, date)
        switch (rate?.getType()) {
            case Rate.DAILY:
            return rate?.getAmount()
            case Rate.MONTHLY:
            default:
            return convertMonthlySalaryToDailySalary(rate?.getAmount())
        }
    }

    BigDecimal getMonthlyRate(Employee employee, LocalDate date) {
        def rate = getRate(employee, date)
        switch (rate?.getType()) {
            case Rate.DAILY:
            return new BigDecimal("0.00") // Monthly Rate is not computed for daily rate types
            case Rate.MONTHLY:
            default:
            return rate.getAmount()
        }
    }
    
    BigDecimal convertMonthlySalaryToDailySalary(BigDecimal monthly) {
        (monthly.multiply(new BigDecimal("12")).divide(new BigDecimal("314"), RoundingMode.HALF_EVEN)).setScale(2, RoundingMode.HALF_EVEN)
    }

    def computeBasicPay(regularWorkHours,dailyRate){
        ((regularWorkHours/8)*dailyRate).setScale(2, RoundingMode.HALF_EVEN)
    }

    def computeRegularOvertimePay(overtimeHours,dailyRate){
        (overtimeHours*(dailyRate*1.25/8)).setScale(2, RoundingMode.HALF_EVEN)
    }

    def computeLateDeduction(lateHours,dailyRate){
        (lateHours*(dailyRate/8)).setScale(2, RoundingMode.HALF_EVEN)
    }

    def computeLegalHolidayPay(legalHolidayHours,dailyRate){
        (legalHolidayHours*(dailyRate/8)).setScale(2, RoundingMode.HALF_EVEN)
    }

    def computeSpecialHolidayPay(specialHolidayHours,dailyRate){
        (specialHolidayHours*(dailyRate*1.30/8)).setScale(2, RoundingMode.HALF_EVEN)
    }



}
