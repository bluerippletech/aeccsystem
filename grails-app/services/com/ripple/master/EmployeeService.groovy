package com.ripple.master

import com.ripple.deductions.WithholdingTaxStatus
import org.joda.time.LocalDate

class EmployeeService {

    boolean transactional = true

    List retreiveEmployeeList(project,location){
        def employeeList
        def list = []
        def c = Employee.createCriteria()
        employeeList = c {
            currentAssignment {
                if (project){
                    and {
                        eq('project.id', Long.parseLong(project))
                        if (location){
                            eq('workLocation.id', Long.parseLong(location))
                        }
                    }
                }
            }
         order("lastName", "asc")
         order("firstName", "asc")
        }
        employeeList.each(){
            list<<[
                id:it.id,
                firstName:it.firstName,
                lastName:it.lastName,
                middleInitials:it.middleInitials,
                employeeNumber:it.employeeNumber,
                wholeName:it.lastName+', '+it.firstName+' '+it.middleInitials
            ]
        }
        return list
    }

    List retreiveTaxStatus(){
        def criteria = WithholdingTaxStatus.createCriteria()
        def status = criteria.list {
             projections {
                     distinct("name")
             }
       }
        
    }

    def getEmployeesCurrentAnniversary(nextMonth){
        def currentDate = (new LocalDate()).minusMonths(1)
        if (nextMonth)
            currentDate.plusMonths(1)
        def startDate = currentDate.withDayOfMonth(1)
        def endDate = startDate.plusMonths(1).withDayOfMonth(1).minusDays(1)

        def e = Employee.createCriteria()

        def plus = nextMonth?1:0
        def thisMonth = Employee.findAll("from Employee as e where month(e.employmentDate) = ? and e.isActive = true order by day(e.employmentDate) asc,e.lastName asc",[((new LocalDate()).plusMonths(plus).getMonthOfYear())])
//        StringBuilder sb = new StringBuilder()
//        thisMonth.each{
//            if (it.employmentDate.yearOfCentury<currentDate.yearOfCentury&& it.grpNumber<4){
//               sb.append(it.employmentDate.format('MMMMM dd, yyyy') + " - " + it.toString() +" - " + it.currentPosition).append("<br>")
//            }
//
//
//        }
        return thisMonth;
    }

    def getBirthdays(){
        def currentDate = (new LocalDate()).minusMonths(1)
        def startDate = currentDate.withDayOfMonth(1)
        def endDate = startDate.plusMonths(1).withDayOfMonth(1).minusDays(1)

        def e = Employee.createCriteria()

        def thisMonth = Employee.findAll("from Employee as e where month(e.birthDate) = ? and e.isActive = true order by day(e.birthDate) asc,e.lastName asc",[((new LocalDate()).getMonthOfYear())])
//        StringBuilder sb = new StringBuilder()
//        thisMonth.each{
//               sb.append(it.birthDate.format('MMMMM dd, yyyy') + " - " + it.toString() +" - " + it.currentPosition).append("<br>")
//        }
        return thisMonth;
    }

    def findEmployees(params) {
        def sortIndex = params.sidx ?: 'lastName'
        def sortOrder = params.sord ?: 'asc'
        def maxRows = Integer.valueOf(params.rows ?: 10)
        def currentPage = Integer.valueOf(params.page ?: 1) ?: 1
        def rowOffset = currentPage == 1 ? 0 : (currentPage - 1) * maxRows


        def employees = Employee.createCriteria().list(max: maxRows, offset: rowOffset) {
            if (params.employeeName)
            or{
                ilike('firstName', "%${params.employeeName}%")
                ilike('lastName', "%${params.employeeName}%")
                ilike('middleInitials', "%${params.employeeName}%")
            }

            if (params.employeeNumber)
                ilike('employeeNumber', "%${params.employeeNumber}%")
            if (params.project)
                currentAssignment{
                    project{
                        ilike('projectName', "%${params.project}%")
                    }
                }
            if (params.workLocation)
                currentAssignment{
                    workLocation{
                        ilike('location', "%${params.workLocation}%")
                    }
                }
            order(sortIndex, sortOrder).ignoreCase()
        }

        def totalRows = employees.totalCount
        def numberOfPages = Math.ceil(totalRows / maxRows)
        def results = employees?.collect {
            [
                    cell: [
                            it.toString(),
                            it.employeeNumber,
                            it.currentAssignment?.project?.toString(),
                            it.currentAssignment?.workLocation?.toString(),
                            it.picture?.length?"Yes":"No"
                    ],
                    id: it.id
            ]
        }
        [rows: results, page: currentPage, records: totalRows, total: numberOfPages]

    }


}
