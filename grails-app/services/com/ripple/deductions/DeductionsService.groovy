
package com.ripple.deductions

import com.ripple.entry.PayPeriod
import com.ripple.master.Employee
import com.ripple.master.Rate

import java.math.RoundingMode

class DeductionsService {

    def workingHoursService
    def payPeriodService
    def rateService

    boolean transactional = false

    /**
     * Rules for basic pay.
     *  1. An employee can only change its rate type at the start of the pay period.
     *  2. Daily rates are computed daily. i.e.,
     *      Rate increases immediately reflect in total basic pay.
     *  3. Monthly rates are computed at the start of the pay period. i.e.,
     *      Rate increases only reflect if its effectivity date is on or before
     *      the start of the pay period.
     **/
    BigDecimal getBasicPay(Employee employee, PayPeriod... payPeriods) {
        def total = BigDecimal.ZERO
        payPeriods?.each { payPeriod ->
            if (employee == null || payPeriod == null)
            return
            
            def rate = rateService.getRate(employee, payPeriod.getPeriodStartDate())
            
            if (rate == null)
            return

            switch (rate?.getType()) {
                case Rate.DAILY:
                def dates = payPeriodService.getRegularWorkingDays(payPeriod)
                dates.each { date ->
                    def daily = rateService.getDailyRate(employee, date)
                    total = total.add(daily)
                }
                break
                case Rate.MONTHLY:
                default:
                def monthly = rateService.getMonthlyRate(employee, payPeriod.getPeriodStartDate())
                def perPeriod = monthly.divide(new BigDecimal("2"), RoundingMode.HALF_EVEN)
                total = total.add(perPeriod)
                break
            }
        }
        return total.setScale(2, RoundingMode.HALF_EVEN)
    }

    BigDecimal getCostOfLivingAllowance(Employee employee, PayPeriod... payPeriods) {
        def total = BigDecimal.ZERO
        payPeriods?.each { payPeriod ->
            if (employee == null || payPeriod == null)
            return
                
            def rate = rateService.getRate(employee, payPeriod.getPeriodStartDate())
            if (rate == null)
            return

            switch (rate?.getType()) {
                case Rate.DAILY:
                def cola = rate.getDailyCola()
                def days = payPeriodService.getNumberOfDays(payPeriod)
                total = total.add(cola.multiply(days))
                break
                case Rate.MONTHLY:
                default:
                def cola = rate.getMonthlyCola().divide(new BigDecimal("2"), RoundingMode.HALF_EVEN)
                total = total.add(cola)
                break
            }
        }
        return total.setScale(2, RoundingMode.HALF_EVEN)
    }

    BigDecimal getFinalCola(Employee employee,  PayPeriod payPeriod, BigDecimal totalDaysWorked) {
        def total = BigDecimal.ZERO

        if (employee == null || payPeriod == null)
        return total

        def rate = rateService.getRate(employee, payPeriod.getPeriodStartDate())
        if (rate == null)
        return total

        if (rate?.getExemptFromDeductions()){
            def cola = rate.getMonthlyCola().divide(new BigDecimal("2"), RoundingMode.HALF_EVEN)
            total = total.add(cola)
        }else{
            def cola = rate.getDailyCola()
            total = total.add(cola.multiply(totalDaysWorked))
        }
        return total.setScale(2, RoundingMode.HALF_EVEN)
    }

    BigDecimal getEmployeePhilHealthContribution(BigDecimal basic) {
        def philHealth = PhilHealthContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(basic)
        }
        if (philHealth)
            return philHealth?.getEmployeeShare()
        else
            return 0.00
    }

    BigDecimal getEmployerPhilHealthContribution(BigDecimal basic) {
        def philHealth = PhilHealthContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(basic)
        }
        if (philHealth)
            return philHealth?.getEmployerShare()
        else
            return 0.00
    }
    // SSS Contributions
    BigDecimal getEmployeeSssContribution(BigDecimal basic) {
        def sss = SssContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(basic)
        }
        if (sss)
            return sss?.getTotalEmployeeContribution()
        else
            return 0.00

    }

    BigDecimal getEmployeeSssCompensation(BigDecimal basic) {
        def sss = SssContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(basic)
        }
        if (sss)
            return sss?.getEmployeeCompensation()
        else
            return 0.00
    }

    BigDecimal getEmployerSssContribution(BigDecimal basic) {
        def sss = SssContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(basic)
        }
        if (sss)
            return sss?.getEmployerContribution()
        else
            return 0.00
    }

    
    // SSS Contributions

    BigDecimal getEmployeePagIbigContribution(BigDecimal basic, BigDecimal cola) {
        def total = basic.add(cola)
        return getEmployeePagIbigContribution(total)
    }

    BigDecimal getEmployeePagIbigContribution(BigDecimal total){

        def pagibig = PagIbigContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(total)
        }
        total = total.multiply(pagibig?.getEmployeeMultiplier())
        return total.setScale(1, RoundingMode.HALF_EVEN)
    }

    BigDecimal getEmployerPagIbigContribution(BigDecimal basic, BigDecimal cola) {
//        def limit = new BigDecimal("100.00")
        def total = basic.add(cola)
        def pagibig = PagIbigContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(total)
        }
        total = total.multiply(pagibig?.getEmployerMultiplier())
        total = total.setScale(1, RoundingMode.HALF_EVEN)
        return total
//        return (total.compareTo(limit) > 0) ? limit : total
    }

    BigDecimal getEmployerPagIbigContribution(BigDecimal total){

        def pagibig = PagIbigContribution.list().find { entry ->
            entry.metaClass.inRange { value ->
                value >= entry.getLowRange() && value <= entry.getHighRange()
            }
            entry.inRange(total)
        }
        total = total.multiply(pagibig?.getEmployerMultiplier())
        return total.setScale(1, RoundingMode.HALF_EVEN)
    }

    BigDecimal getWithholdingTaxDeduction(Employee employee, BigDecimal basic) {
        def period = WithholdingTaxPeriod.findByName(WithholdingTaxPeriod.MONTHLY,[cache:true])
        return this.getWithholdingTaxDeduction(period, employee, basic)
    }
    
    BigDecimal getWithholdingTaxDeduction(WithholdingTaxPeriod period,
        Employee employee, BigDecimal basic) {

        def status = period?.statuses.find { entry ->
            entry?.name?.equals(employee?.getTaxStatus() ?: "Z")
        }
        def exemptions = status?.exemptions.findAll { entry ->
            entry?.getValue() < basic
        }        
        def exemption = exemptions ? exemptions.last() : null

        def baseTax = exemption?.getBracket()?.getBaseTax()
        def multiplier = exemption?.getBracket()?.getPercentOverMultiplier()
        def shield = exemption?.getValue()
        def total = ((basic.minus(shield)).multiply(multiplier)).add(baseTax)
        return total.setScale(0, RoundingMode.HALF_EVEN)
    }

    BigDecimal getWithholdingTaxStatusExemption(WithholdingTaxStatus status)
    {
        def dependents = status.dependents ?: 0
        def total = BigDecimal.ZERO
        def baseExemption = new BigDecimal("50.0")
        def additionalExemption = new BigDecimal("25.0").multiply(dependents)
        
        switch (status.name)
        {
            case "Z":
            break
            default:
            total = baseExemption.add(additionalExemption)
            break
        }
        return total.setScale(1, RoundingMode.HALF_EVEN)
    }

}
