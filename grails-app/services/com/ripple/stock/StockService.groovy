package com.ripple.stock

import com.ripple.receiving.ReceivingReport
import com.ripple.receiving.ReceivingReportLineItem
import com.ripple.materialIssuance.MaterialIssuance
import com.ripple.purchasing.PurchaseOrder
import com.ripple.materialIssuance.MaterialIssuanceLineItem
import com.ripple.master.Warehouse
import com.ripple.master.item.Item

class StockService {

    def documentNumbersService

    def stockActionSelector(modelInstance, String movement, String transactionMovementType){
        try{
            switch (transactionMovementType){
                case 'RR':
                    postReceivingReport(modelInstance, movement, transactionMovementType)
                    break;
            }
        }catch (Exception e){
            throw e;
        }
    }

    protected void postReceivingReport(ReceivingReport rrInstance, String movement, String transactionMovementType){
        rrInstance.lineItems.each {
            def lineItem = it;
            def stockCardParams = [:];
            stockCardParams.put('transactionCode', transactionMovementType);
            stockCardParams.put('transactionReferenceId', lineItem?.id);
            stockCardParams.put('rrReference', rrInstance);
            stockCardParams.put('quantity', lineItem?.quantity);
            stockCardParams.put('item', lineItem?.item);
            stockCardParams.put('remainingBalance', lineItem?.quantity);
            stockCardParams.put('allocated', 0);
            stockCardParams.put('served', 0);
            stockCardParams.put('movement', movement);
            stockCardParams.put('unitPrice', lineItem?.unitPrice);
            stockCardParams.put('warehouse', rrInstance?.purchaseOrder?.warehouse);
            stockCardParams.put('unitOfMeasure', lineItem?.unitOfMeasure);
            def stockCardInstance = new StockCard(stockCardParams);
            if(!stockCardInstance.save(flush: true))
                println stockCardInstance.errors;
        }
    }

    def materialIssuancePerItem(MaterialIssuanceLineItem lineItem, String movement, String transactionMovementType){
        def misInstance = lineItem.issuance;

        def referenceStockList = StockCard.createCriteria().listDistinct {
            order('createdDate', 'asc')
            warehouse{
                eq("id", misInstance?.procurementSlip?.warehouse?.id)
            }
            and{
                item{
                    eq("id", lineItem?.item?.id)
                }
            }
            and{
                eq("movement", "IN")
            }
        }

        def referenceStock

        if(lineItem.item.stockMovement=="FIFO")
            referenceStock = referenceStockList.first();
        else if(lineItem.item.stockMovement=="LIFO")
            referenceStock = referenceStockList.last();
        else if(lineItem.item.stockMovement=="Yes")
            throw new Exception("Invalid Stock Movement");

        def remainder = lineItem?.quantity

        while(remainder>0){
            def totalAllocation = 0

            if((referenceStock?.remainingBalance-remainder)>=0)
                totalAllocation = remainder
            else{
                totalAllocation = remainder - referenceStock?.remainingBalance
            }

            try{
                if(misInstance.status == misInstance.ForApproval)
                    allocateMaterialIssuancePerItemAndStock(lineItem, movement, transactionMovementType, referenceStock, totalAllocation)
                else if(misInstance.status == misInstance.Posted)
                    postMaterialIssuancePerItemAndStock(lineItem, movement, transactionMovementType, referenceStock, totalAllocation)
            }catch (Exception e){
                throw e
            }

            remainder = remainder % totalAllocation;

            if(remainder>0){
                def tempEmp=referenceStockList.findIndexOf{it.id==referenceStock.id};
                if (lineItem.item.stockMovement=="FIFO"){
                    if ((tempEmp+1)>=referenceStockList.size())
                        referenceStock = referenceStockList.getAt(0)
                    else
                        referenceStock = referenceStockList.getAt(tempEmp+1);
                }else if (lineItem.item.stockMovement=="LIFO"){
                    if ((tempEmp-1)<=0)
                        referenceStock = referenceStockList.getAt(0)
                    else
                        referenceStock = referenceStockList.getAt(tempEmp-1);
                }
            }
        }
    }

    def allocateMaterialIssuancePerItemAndStock(MaterialIssuanceLineItem lineItem, String movement, String transactionMovementType, StockCard referenceStock, Long quantity){
        def misInstance = lineItem.issuance;
            if(referenceStock){
                def rrInstance = referenceStock?.rrReference;

                referenceStock?.allocated = (referenceStock.allocated + quantity)

                def lineItemReference = new LineItemReference(stockCard:referenceStock, lineItem: lineItem, allocated: lineItem.quantity);

                if(!lineItemReference.save(flush: true)){
                    println lineItemReference.errors;
                }else if(referenceStock){
                    if(!referenceStock?.save(flush: true))
                        println referenceStock.errors;
                }

            }else{
                throw new Exception("No Stock Available");
            }
    }

    def postMaterialIssuancePerItemAndStock(MaterialIssuanceLineItem lineItem, String movement, String transactionMovementType, StockCard referenceStock, Long quantity){
        def misInstance = lineItem.issuance;
        if(referenceStock){
            def rrInstance = referenceStock?.rrReference;

            def stockCardParams = [:]

            if(rrInstance){
                stockCardParams.put('transactionCode', transactionMovementType);
                stockCardParams.put('transactionReferenceId', lineItem?.id);
                stockCardParams.put('rrReference', rrInstance);
                stockCardParams.put('referenceStock', referenceStock);
                stockCardParams.put('movement', movement);
                stockCardParams.put('quantity', (quantity*-1));
                stockCardParams.put('item', lineItem?.item);
                stockCardParams.put('warehouse', rrInstance?.purchaseOrder?.warehouse);
                stockCardParams.put('unitOfMeasure', lineItem?.unitOfMeasure);
            }

            def stockCardInstance = new StockCard(stockCardParams);

            referenceStock?.remainingBalance = (referenceStock.remainingBalance - quantity)
            referenceStock?.allocated = (referenceStock.allocated - quantity)
            referenceStock?.served = (referenceStock.served + quantity)

            def lineItemReference = new LineItemReference(stockCard:referenceStock, lineItem: lineItem, allocated: lineItem.quantity);

            if(!lineItemReference?.save(flush: true)){
                println lineItemReference.errors;
            }else if(referenceStock){
                if(!referenceStock?.save(flush: true)){
                    println referenceStock.errors;
                }else if(stockCardInstance){
                    if(!stockCardInstance?.save(flush: true))
                        println stockCardInstance.errors;
                }
            }
        }else{
            throw new Exception("No Stock Available");
        }
    }

    Long stockCount(Item item, Warehouse warehouse){
        def stockList =  StockCard.findAllByItemAndWarehouseAndMovement(item, warehouse, "IN")
        return stockList.sum {it.baseQuantity};
    }

//    protected Set computeReturnAll(String type, ReceivingReport rrInstance, ReceivingReportLineItem lineItem){
//        def relatedEntry = StockCard.findAllByItemAndWarehouse()
//        switch (type){
//            case 'remainingBalance':
//                break;
//        }
//    }

    def serviceMethod() {

    }
}
