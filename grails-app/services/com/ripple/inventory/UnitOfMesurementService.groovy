package com.ripple.inventory

import com.ripple.master.item.Item
import com.ripple.materialIssuance.MaterialIssuanceLineItem
import com.ripple.procurement.Procurement

class UnitOfMesurementService {

    boolean transactional = false

    //levelUnitInstance == levelUnitSource (levelUnit->baseUnit)
    BigDecimal convertToBase(qty, levelUnitInstance, itemId){
        def itemInstance = Item.get(itemId)
        def value
        if(levelUnitInstance==itemInstance?.baseLevel){
            value = (qty as BigDecimal)
        }else if (levelUnitInstance==itemInstance?.secondLevel){
            value = (qty as BigDecimal)*itemInstance?.secondLevel.value;
        }else if (levelUnitInstance==itemInstance?.thirdLevel){
            value = (qty as BigDecimal)*(itemInstance?.thirdLevel?.value)*(itemInstance.secondLevel.value);
        }
        return value
    }

    BigDecimal convertToLevel(qty, toLevelUnitInstance, fromLevelUnitInstance, itemId){
        def itemInstance = Item.get(itemId)
        def baseValue = convertToBase(qty, fromLevelUnitInstance, itemId)
        def value
        if(toLevelUnitInstance==itemInstance.baseLevel){
            value = (baseValue)
        }else if (toLevelUnitInstance==itemInstance?.secondLevel){
            value = (baseValue)/itemInstance?.secondLevel.value;
        }else if (toLevelUnitInstance==itemInstance?.thirdLevel){
            value = (baseValue)/(itemInstance.thirdLevel?.value)/(itemInstance.secondLevel?.value);
        }
        return value
    }

    BigDecimal getRemainderFromLevel(qty, levelUnitInstance, itemId){
        def itemInstance = Item.get(itemId)
        def value
        if(levelUnitInstance == itemInstance.baseLevel){
            value = 0;
        }else if (levelUnitInstance==itemInstance?.secondLevel){
            value = (qty as BigDecimal)%itemInstance?.secondLevel.value;
        }else if (levelUnitInstance==itemInstance.thirdLevel){
            value = (qty as BigDecimal)/(itemInstance?.thirdLevel?.value)%(itemInstance?.secondLevel?.value);
        }
        return value
    }

    //levelUnitInstance == levelUnitSource (levelUnit->baseUnit)
    BigDecimal getRemainingQtyToBase(itemInstance, levelUnitInstance, procurementInstanceId){
        def procurementInstance = Procurement.get(procurementInstanceId);
        def lineItemInstance = procurementInstance?.lineItems.find {it.item == itemInstance}
        def value
        if (levelUnitInstance==itemInstance?.secondLevel){
            value = (lineItemInstance?.servedQuantity as BigDecimal)*itemInstance?.secondLevel.value;
        }else if (levelUnitInstance==itemInstance.thirdLevel){
            value = (lineItemInstance?.servedQuantity as BigDecimal)*(itemInstance?.thirdLevel.value)*(itemInstance?.secondLevel?.value);
        }
        return value
    }

    //levelUnitInstance == levelUnitOutput (baseUnit->levelUnit)
    BigDecimal getRemainingQtyToLevel(itemInstance, levelUnitInstance, procurementInstanceId){
        def procurementInstance = Procurement.get(procurementInstanceId);
        def lineItemInstance = procurementInstance?.lineItems?.find {it.item == itemInstance}
        def value
        if (levelUnitInstance==itemInstance?.secondLevel){
            value = (lineItemInstance?.servedQuantity as BigDecimal)/itemInstance?.secondLevel?.value;
        }else if (levelUnitInstance==itemInstance.thirdLevel){
            value = (lineItemInstance?.servedQuantity as BigDecimal)/(itemInstance?.thirdLevel?.value)/(itemInstance?.secondLevel?.value);
        }
        return value
    }

    def serviceMethod() {

    }
}
