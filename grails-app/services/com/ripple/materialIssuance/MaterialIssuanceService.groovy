package com.ripple.materialIssuance

class MaterialIssuanceService {

    def unitOfMesurementService;

    def postServedQuantity(MaterialIssuance misInstance){
        def lineItems = misInstance.lineItems
        lineItems.each{
            def lineItem = it
            def procurementLineItemInstance = lineItem.issuance.procurementSlip.lineItems.find {it.item == lineItem.item}
            if(procurementLineItemInstance){
                if(procurementLineItemInstance.servedQuantity >= 0){
                    def converted = unitOfMesurementService.convertToLevel(lineItem?.quantity, procurementLineItemInstance.unitOfMeasure, lineItem?.unitOfMeasure, lineItem?.item?.id)
                    procurementLineItemInstance?.servedQuantity = procurementLineItemInstance?.servedQuantity+converted;
                    if(!procurementLineItemInstance.save(flush: true)){
                        throw new Exception("Unable to save procurement count.");
                    }
                }
            }
        }
    }

    def serviceMethod() {

    }
}
