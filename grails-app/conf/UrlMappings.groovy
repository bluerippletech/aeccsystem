class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
        "/forms"(view:"/forms")
        "/sample"(view: "/sample")
		"/"(controller: "dashboard", action: "index")

        //error handlers
        "500"(controller: 'error', action:'handle')
        "404"(view:'/404')

        "/login/$action?"(controller: "login")
        "/logout/$action?"(controller: "logout")
    }
}
