package aeccsystem

class RippleTagLib {

    static namespace = "ripple"

    def springSecurityService

    def modal={ attrs, body ->
        def className = attrs.className;
        Class clazz = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz
        def modelInstance = clazz.newInstance();
        if (attrs.parentId){
            def tempMap = [:]
            def parentInstance =  grailsApplication.domainClasses.find { it.clazz.simpleName == attrs.parentClass }.clazz
            tempMap.put(attrs.parentProperty, parentInstance.findById(attrs.parentId));
            modelInstance.properties = tempMap;
        }
        out << render(template:"/templates/modal/instanceModal",  model:[modelInstance:modelInstance, 'entity':attrs.entity , 'idHolder':attrs.elementId, 'className':className, 'parentProperty':attrs.parentProperty,  'parentId':attrs.parentId]);
    }

    def modalScript ={ attrs, body ->
        out << render(template:"/templates/modal/instanceModalScript", model:[attrs:attrs]);
    }

    def getSecurityRole ={ attrs, body ->
        def userInstance = springSecurityService.getCurrentUser()
        out << userInstance.getAuthorities()*.roleName
    }

    def selectSubCat ={ attrs, body ->
        out << render(template:"/templates/select/subItemJs", model:[attrs:attrs]);
    }

    def autoComplete ={ attrs, body ->
        out << render(template:"/templates/autocomplete/textField", model:[attrs:attrs]);
    }

    def itemPicker ={ attrs, body ->
        out << render(template: "/templates/itempicker/dataTable", model: [attrs:attrs]);
    }
}
