package com.ripple.purchasing



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.item.Item
import com.ripple.ModalizerController
import com.ripple.util.Formatter as FM
import com.ripple.procurement.Procurement

class PurchaseOrderLineItemController extends ModalizerController {

    //setter for Domain className for inherited class
    String getDomainClazz() {return 'PurchaseOrderLineItem'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [purchaseOrderLineItemInstanceList: PurchaseOrderLineItem.list(params), purchaseOrderLineItemInstanceTotal: PurchaseOrderLineItem.count()]
    }

    def create() {
        [purchaseOrderLineItemInstance: new PurchaseOrderLineItem(params)]
    }

    def save() {
        def purchaseOrderLineItemInstance = new PurchaseOrderLineItem(params)
        if (!purchaseOrderLineItemInstance.save(flush: true)) {
            render(view: "create", model: [purchaseOrderLineItemInstance: purchaseOrderLineItemInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), purchaseOrderLineItemInstance.id])
        redirect(action: "show", id: purchaseOrderLineItemInstance.id)
    }

    def show() {
        def purchaseOrderLineItemInstance = PurchaseOrderLineItem.get(params.id)
        if (!purchaseOrderLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [purchaseOrderLineItemInstance: purchaseOrderLineItemInstance]
    }

    def edit() {
        def purchaseOrderLineItemInstance = PurchaseOrderLineItem.get(params.id)
        if (!purchaseOrderLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [purchaseOrderLineItemInstance: purchaseOrderLineItemInstance]
    }

    def update() {
        def purchaseOrderLineItemInstance = PurchaseOrderLineItem.get(params.id)
        if (!purchaseOrderLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (purchaseOrderLineItemInstance.version > version) {
                purchaseOrderLineItemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem')] as Object[],
                        "Another user has updated this PurchaseOrderLineItem while you were editing")
                render(view: "edit", model: [purchaseOrderLineItemInstance: purchaseOrderLineItemInstance])
                return
            }
        }

        purchaseOrderLineItemInstance.properties = params

        if (!purchaseOrderLineItemInstance.save(flush: true)) {
            render(view: "edit", model: [purchaseOrderLineItemInstance: purchaseOrderLineItemInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), purchaseOrderLineItemInstance.id])
        redirect(action: "show", id: purchaseOrderLineItemInstance.id)
    }

    def delete() {
        def purchaseOrderLineItemInstance = PurchaseOrderLineItem.get(params.id)
        if (!purchaseOrderLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), params.id])
            redirect(action: "list")
            return
        }

        try {
            purchaseOrderLineItemInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'purchaseOrderLineItem.label', default: 'PurchaseOrderLineItem'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = PurchaseOrderLineItem.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(unit_price,char) like '%${sSearch}%'")

                    item{
                        or{

                            ilike('itemCode', "%${sSearch}%")

                            ilike('itemName', "%${sSearch}%")

                        }
                    }

                    unitOfMeasure{

                        unitOfMeasure{

                        ilike('name', "%${sSearch}%")

                        }

                    }

                }
            }
            and{eq('purchaseOrder', PurchaseOrder.get(params?.id))}
        }
        def totalRecords = PurchaseOrderLineItem.count();
        def totalDisplayRecords = PurchaseOrderLineItem.createCriteria().count {
            if (sSearch) {
                or {

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(unit_price,char) like '%${sSearch}%'")

                    item{
                        or{

                            ilike('itemCode', "%${sSearch}%")

                            ilike('itemName', "%${sSearch}%")

                        }
                    }

                    unitOfMeasure{

                        unitOfMeasure{

                            ilike('name', "%${sSearch}%")

                        }

                    }

                }
            }
            and{eq('purchaseOrder', PurchaseOrder.get(params?.id))}
        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.item?.itemCode,

                    1: it.item?.itemName,

                    2: it.quantity,

                    3: it.unitOfMeasure?.unitOfMeasure?.toString(),

                    4: FM?.formatCurrency(it.unitPrice),

                    5: FM?.formatCurrency(it.getTotalItemAmount()),

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def categorized(){
        def itemInstance = Item.get(params.category)
        def levelUnitList = [];
        if (itemInstance?.baseLevel)
            levelUnitList.add(itemInstance?.baseLevel)
        if (itemInstance?.secondLevel)
            levelUnitList.add(itemInstance?.secondLevel)
        if (itemInstance?.thirdLevel)
            levelUnitList.add(itemInstance?.thirdLevel)
        def data = []
        def tempMap = [:]
        levelUnitList.each {
            tempMap.put('optionKey',it?.id);
            tempMap.put('optionValue',it?.unitOfMeasure);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }

    def getUnitMeasure(){
        def data = PurchaseOrderLineItem.get(params?.id)?.unitOfMeasure?.id;
        render data
    }

    @Override
    def getInstance(){
        def className = getDomainClazz()
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)

        def propertyMap = [:]

        propertyMap.put('id', modelInstance?.id);
        modelInstance?.properties?.each {
            def perValue = it;
            if(perValue?.value?.hasProperty("id")){
                def instanceVal = perValue.value;
                def tempMap = [:];
                tempMap.put('id', instanceVal.id);
                instanceVal?.properties?.each {
                    if(!(it.key in ['beforeInsert', 'beforeDelete', 'beforeUpdate', 'documentNumbersService', 'afterInsert', 'afterDelete', 'afterUpdate']))
                        tempMap.put(it.key, it.value)
                }
                propertyMap.put(perValue.key, tempMap);
                tempMap = [:];
            }else
                propertyMap.put(perValue.key, perValue.value)
        }
        render propertyMap as JSON;
    }
}
