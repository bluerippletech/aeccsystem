package com.ripple.purchasing



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.procurement.Procurement
import com.ripple.master.Warehouse
import com.ripple.procurement.ProcurementLineItem
import com.ripple.master.item.LevelUnit

class PurchaseOrderController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [purchaseOrderInstanceList: PurchaseOrder.list(params), purchaseOrderInstanceTotal: PurchaseOrder.count()]
    }

    def create() {
        [purchaseOrderInstance: new PurchaseOrder()]
    }

    def save() {
        def purchaseOrderInstance = new PurchaseOrder(params)
        if (!purchaseOrderInstance.save(flush: true)) {
            render(view: "create", model: [purchaseOrderInstance: purchaseOrderInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), purchaseOrderInstance.id])
        redirect(action: "show", id: purchaseOrderInstance.id)
    }

    def show() {
        def purchaseOrderInstance = PurchaseOrder.get(params.id)
        if (!purchaseOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), params.id])
            redirect(action: "list")
            return
        }

        [purchaseOrderInstance: purchaseOrderInstance]
    }

    def edit() {
        def purchaseOrderInstance = PurchaseOrder.get(params.id)
        if (!purchaseOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), params.id])
            redirect(action: "list")
            return
        }

        sec.ifNotGranted(roles: 'ROLE_ADMIN') {
            if (purchaseOrderInstance?.status == 'Cancelled' || purchaseOrderInstance?.status == 'For Approval') {
                purchaseOrderInstance?.setStatus('Draft')
            }
        }

        [purchaseOrderInstance: purchaseOrderInstance]
    }

    def update() {
        def purchaseOrderInstance = PurchaseOrder.get(params.id)
        if (!purchaseOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (purchaseOrderInstance.version > version) {
                purchaseOrderInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'purchaseOrder.label', default: 'PurchaseOrder')] as Object[],
                        "Another user has updated this PurchaseOrder while you were editing")
                render(view: "edit", model: [purchaseOrderInstance: purchaseOrderInstance])
                return
            }
        }

        purchaseOrderInstance.properties = params

        if (!purchaseOrderInstance.save(flush: true)) {
            render(view: "edit", model: [purchaseOrderInstance: purchaseOrderInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), purchaseOrderInstance.id])
        redirect(action: "show", id: purchaseOrderInstance.id)
    }

    def delete() {
        def purchaseOrderInstance = PurchaseOrder.get(params.id)
        if (!purchaseOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), params.id])
            redirect(action: "list")
            return
        }

        try {
            purchaseOrderInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = PurchaseOrder.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('purchaseOrderNumber', "%${sSearch}%")

                    procurement{
                        ilike('procurementNumber', "%${sSearch}%")
                    }

                    supplier{
                        ilike('supplierName', "%${sSearch}%")
                    }

                    ilike('status', "%${sSearch}%")

                }
            }
        }
        def totalRecords = PurchaseOrder.count();
        def totalDisplayRecords = PurchaseOrder.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('purchaseOrderNumber', "%${sSearch}%")

                    procurement{
                        ilike('procurementNumber', "%${sSearch}%")
                    }

                    supplier{
                        ilike('supplierName', "%${sSearch}%")
                    }

                    ilike('status', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.purchaseOrderNumber,

                    1: it.procurement?.toString(),

                    2: it.supplier.toString(),

                    3: it.dateStarted,

                    4: it.status,

                    5: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def newFromProcurement(){
        def newParams = params;
        def procurementInstance = Procurement.get(params['procurement.id'])
        def warehouseInstance = Warehouse.get(params['warehouse.id'])
        def purchaseOrderInstance = new PurchaseOrder(procurement:procurementInstance, warehouse:  warehouseInstance)
        def selectedItems = ProcurementLineItem.getAll(params.list("selectedItemLine"))

        if (!purchaseOrderInstance.save(flush: true)) {
            render(view: "create", model: [purchaseOrderInstance: purchaseOrderInstance])
            return
        }

        selectedItems.each {
            def lineItem = it;
            if(lineItem.item){
                def purchaseOrderLineItem = new PurchaseOrderLineItem(item: lineItem?.item, unitOfMeasure: LevelUnit.get(newParams.unitOfMeasure?.get(it?.id)), quantity: params[it.id])
                purchaseOrderInstance.addToLineItems(purchaseOrderLineItem)
            }
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), purchaseOrderInstance.id])
        redirect(action: "show", id: purchaseOrderInstance.id)
    }

    def formStatus(){
        def purchaseOrderInstance = PurchaseOrder.get(params.id)
        if (!purchaseOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (purchaseOrderInstance.version > version) {
                purchaseOrderInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'purchaseOrder.label', default: 'PurchaseOrder')] as Object[],
                        "Another user has updated this PurchaseOrder while you were editing")
                render(view: "edit", model: [purchaseOrderInstance: purchaseOrderInstance])
                return
            }
        }

        def newStatus = params._action_formStatus

        if (newStatus){
            switch (newStatus){
                case 'For Approval':
                    newStatus = 'For Approval'
                    break

                case 'Cancel':
                    newStatus = 'Cancelled'
                    break

                case 'Approve':
                    newStatus = 'Approved'
                    break

                case 'Disapprove':
                    newStatus = 'Disapproved'
                    break

                default:
                    newStatus = 'Draft'
            }
        }

        if (newStatus=='For Approval' ){
            if (purchaseOrderInstance.supplier==null){
                purchaseOrderInstance.errors.rejectValue("supplier", "",
                        [message(code: 'purchaseOrder.label', default: 'PurchaseOrder')] as Object[],
                        "Supplier cannot be blank")
                render(view: "edit", model: [purchaseOrderInstance: purchaseOrderInstance])
                return
            }
            else if (purchaseOrderInstance.warehouse==null){
                purchaseOrderInstance.errors.rejectValue("warehouse", "",
                        [message(code: 'purchaseOrder.label', default: 'PurchaseOrder')] as Object[],
                        "Warehouse cannot be blank")
                render(view: "edit", model: [purchaseOrderInstance: purchaseOrderInstance])
                return
            }
        }


        purchaseOrderInstance.status = newStatus


        if (!purchaseOrderInstance.save(flush: true)) {
            render(view: "edit", model: [purchaseOrderInstance: purchaseOrderInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'purchaseOrder.label', default: 'PurchaseOrder'), purchaseOrderInstance.id])
        redirect(action: "show", id: purchaseOrderInstance.id)
    }

}
