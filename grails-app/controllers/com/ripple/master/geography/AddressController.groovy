package com.ripple.master.geography



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.ModalizerController

class AddressController extends ModalizerController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [addressInstanceList: Address.list(params), addressInstanceTotal: Address.count()]
}

def create() {
    [addressInstance: new Address(params)]
}

def save() {
    def addressInstance = new Address(params)
    if (!addressInstance.save(flush: true)) {
        render(view: "create", model: [addressInstance: addressInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'address.label', default: 'Address'), addressInstance.id])
    redirect(action: "show", id: addressInstance.id)
}

def show() {
    def addressInstance = Address.get(params.id)
    if (!addressInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Address'), params.id])
        redirect(action: "list")
        return
    }

    [addressInstance: addressInstance]
}

def edit() {
    def addressInstance = Address.get(params.id)
    if (!addressInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Address'), params.id])
        redirect(action: "list")
        return
    }

    [addressInstance: addressInstance]
}

def update() {
    def addressInstance = Address.get(params.id)
    if (!addressInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Address'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (addressInstance.version > version) {
                addressInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'address.label', default: 'Address')] as Object[],
                        "Another user has updated this Address while you were editing")
            render(view: "edit", model: [addressInstance: addressInstance])
            return
        }
    }

    addressInstance.properties = params

    if (!addressInstance.save(flush: true)) {
        render(view: "edit", model: [addressInstance: addressInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'address.label', default: 'Address'), addressInstance.id])
    redirect(action: "show", id: addressInstance.id)
}

def delete() {
    def addressInstance = Address.get(params.id)
    if (!addressInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'address.label', default: 'Address'), params.id])
        redirect(action: "list")
        return
    }

    try {
        addressInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'address.label', default: 'Address'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'address.label', default: 'Address'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = Address.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('isDeleted',"%${sSearch}%")
                        
                        ilike('createdDate',"%${sSearch}%")
                        
                        ilike('editedDate',"%${sSearch}%")
                        
                        ilike('editedBy',"%${sSearch}%")
                        
                        ilike('createdBy',"%${sSearch}%")
                        
                        ilike('address1',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = Address.count();
    def totalDisplayRecords = Address.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('isDeleted',"%${sSearch}%")
                        
                        ilike('createdDate',"%${sSearch}%")
                        
                        ilike('editedDate',"%${sSearch}%")
                        
                        ilike('editedBy',"%${sSearch}%")
                        
                        ilike('createdBy',"%${sSearch}%")
                        
                        ilike('address1',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.isDeleted,
                        
                        1:it.createdDate,
                        
                        2:it.editedDate,
                        
                        3:it.editedBy,
                        
                        4:it.createdBy,
                        
                        5:it.address1,
                        
                        6:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
}
