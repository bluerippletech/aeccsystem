package com.ripple.master.geography



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class ProvinceController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [provinceInstanceList: Province.list(params), provinceInstanceTotal: Province.count()]
    }

    def create() {
        [provinceInstance: new Province(params)]
    }

    def save() {
        def provinceInstance = new Province(params)
        if (!provinceInstance.save(flush: true)) {
            render(view: "create", model: [provinceInstance: provinceInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'province.label', default: 'Province'), provinceInstance.id])
        redirect(action: "show", id: provinceInstance.id)
    }

    def show() {
        def provinceInstance = Province.get(params.id)
        if (!provinceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'province.label', default: 'Province'), params.id])
            redirect(action: "list")
            return
        }

        [provinceInstance: provinceInstance]
    }

    def edit() {
        def provinceInstance = Province.get(params.id)
        if (!provinceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'province.label', default: 'Province'), params.id])
            redirect(action: "list")
            return
        }

        [provinceInstance: provinceInstance]
    }

    def update() {
        def provinceInstance = Province.get(params.id)
        if (!provinceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'province.label', default: 'Province'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (provinceInstance.version > version) {
                provinceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'province.label', default: 'Province')] as Object[],
                        "Another user has updated this Province while you were editing")
                render(view: "edit", model: [provinceInstance: provinceInstance])
                return
            }
        }

        provinceInstance.properties = params

        if (!provinceInstance.save(flush: true)) {
            render(view: "edit", model: [provinceInstance: provinceInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'province.label', default: 'Province'), provinceInstance.id])
        redirect(action: "show", id: provinceInstance.id)
    }

    def delete() {
        def provinceInstance = Province.get(params.id)
        if (!provinceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'province.label', default: 'Province'), params.id])
            redirect(action: "list")
            return
        }

        try {
            provinceInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'province.label', default: 'Province'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'province.label', default: 'Province'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Province.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('name', "%${sSearch}%")

                }
            }
        }
        def totalRecords = Province.count();
        def totalDisplayRecords = Province.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('name', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.isDeleted,

                    1: it.createdDate,

                    2: it.editedDate,

                    3: it.editedBy,

                    4: it.createdBy,

                    5: it.name,

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def listing(){
        def dataList = Province?.findAllByNameLike("%${params.q}%")*.name;
        render dataList as JSON;
    }
}
