package com.ripple.master.geography



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class CityController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [cityInstanceList: City.list(params), cityInstanceTotal: City.count()]
    }

    def create() {
        [cityInstance: new City(params)]
    }

    def save() {
        def cityInstance = new City(params)
        if (!cityInstance.save(flush: true)) {
            render(view: "create", model: [cityInstance: cityInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'city.label', default: 'City'), cityInstance.id])
        redirect(action: "show", id: cityInstance.id)
    }

    def show() {
        def cityInstance = City.get(params.id)
        if (!cityInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'city.label', default: 'City'), params.id])
            redirect(action: "list")
            return
        }

        [cityInstance: cityInstance]
    }

    def edit() {
        def cityInstance = City.get(params.id)
        if (!cityInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'city.label', default: 'City'), params.id])
            redirect(action: "list")
            return
        }

        [cityInstance: cityInstance]
    }

    def update() {
        def cityInstance = City.get(params.id)
        if (!cityInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'city.label', default: 'City'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (cityInstance.version > version) {
                cityInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'city.label', default: 'City')] as Object[],
                        "Another user has updated this City while you were editing")
                render(view: "edit", model: [cityInstance: cityInstance])
                return
            }
        }

        cityInstance.properties = params

        if (!cityInstance.save(flush: true)) {
            render(view: "edit", model: [cityInstance: cityInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'city.label', default: 'City'), cityInstance.id])
        redirect(action: "show", id: cityInstance.id)
    }

    def delete() {
        def cityInstance = City.get(params.id)
        if (!cityInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'city.label', default: 'City'), params.id])
            redirect(action: "list")
            return
        }

        try {
            cityInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'city.label', default: 'City'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'city.label', default: 'City'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = City.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('name', "%${sSearch}%")

                }
            }
        }
        def totalRecords = City.count();
        def totalDisplayRecords = City.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('name', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.isDeleted,

                    1: it.createdDate,

                    2: it.editedDate,

                    3: it.editedBy,

                    4: it.createdBy,

                    5: it.name,

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def listing(){
        def dataList = City?.findAllByNameLike("%${params.q}%")*.name;
        render dataList as JSON;
    }
}
