package com.ripple.master



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.project.EstimateItem
import com.ripple.project.ExpenseType
import org.joda.time.LocalDate

class AssignmentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [assignmentInstanceList: Assignment.list(params), assignmentInstanceTotal: Assignment.count()]
    }

    def create() {
        [assignmentInstance: new Assignment(params)]
    }

    def save() {
        def assignmentInstance = new Assignment(params)
        if (!assignmentInstance.save(flush: true)) {
            render(view: "create", model: [assignmentInstance: assignmentInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'assignment.label', default: 'Assignment'), assignmentInstance.id])
        redirect(action: "show", id: assignmentInstance.id)
    }

    def show() {
        def assignmentInstance = Assignment.get(params.id)
        if (!assignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignment.label', default: 'Assignment'), params.id])
            redirect(action: "list")
            return
        }

        [assignmentInstance: assignmentInstance]
    }

    def edit() {
        def assignmentInstance = Assignment.get(params.id)
        if (!assignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignment.label', default: 'Assignment'), params.id])
            redirect(action: "list")
            return
        }

        [assignmentInstance: assignmentInstance]
    }

    def update() {
        def assignmentInstance = Assignment.get(params.id)
        if (!assignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignment.label', default: 'Assignment'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (assignmentInstance.version > version) {
                assignmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'assignment.label', default: 'Assignment')] as Object[],
                        "Another user has updated this Assignment while you were editing")
                render(view: "edit", model: [assignmentInstance: assignmentInstance])
                return
            }
        }

        assignmentInstance.properties = params

        if (!assignmentInstance.save(flush: true)) {
            render(view: "edit", model: [assignmentInstance: assignmentInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'assignment.label', default: 'Assignment'), assignmentInstance.id])
        redirect(action: "show", id: assignmentInstance.id)
    }

    def delete() {
        def assignmentInstance = Assignment.get(params.id)
        if (!assignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignment.label', default: 'Assignment'), params.id])
            redirect(action: "list")
            return
        }

        try {
            assignmentInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'assignment.label', default: 'Assignment'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'assignment.label', default: 'Assignment'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Assignment.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('projectName', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    switch (sSearch){
                        case 'true':
                            sqlRestriction("active=1")
                            break
                        case 'false':
                            sqlRestriction("active=0")
                            break
                    }

                    ilike('projectAddress', "%${sSearch}%")

                }
            }
        }
        def totalRecords = Assignment.count();
        def totalDisplayRecords = Assignment.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('projectName', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    switch (sSearch){
                        case 'true':
                            sqlRestriction("active=1")
                            break
                        case 'false':
                            sqlRestriction("active=0")
                            break
                    }

                    ilike('projectAddress', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.projectName,

                    1: it.description,

                    2: it.active,

                    3: it.projectAddress,

                    4: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def showProject(){
        def assignmentInstance = Assignment.get(params.id)
        if (!assignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignment.label', default: 'Assignment'), params.id])
            redirect(action: "list")
            return
        }

        [assignmentInstance: assignmentInstance]
    }

    def generateCostSummary = {
//        println params
        def projectId = params.id
        def project = Assignment.get(projectId)
        def dateVals = params.date.split("/")
        def generateDate = new Date()

        //Collection Generation
        Calendar lastDateOfMonth = Calendar.getInstance()
        lastDateOfMonth.set(Integer.parseInt(dateVals[2]).intValue(), Integer.parseInt(dateVals[0]).intValue() - 1, Integer.parseInt(dateVals[1]).intValue())
        def lastDay = lastDateOfMonth.getActualMaximum(Calendar.DATE)

        lastDateOfMonth.set(Calendar.DATE, lastDay)

        Calendar firstDateOfMonth = Calendar.getInstance()
        firstDateOfMonth.set(Integer.parseInt(dateVals[2]).intValue(), Integer.parseInt(dateVals[0]).intValue() - 1, Integer.parseInt(dateVals[1]).intValue())
        firstDateOfMonth.set(Calendar.DATE, 1)


        LocalDate newLastDateOfMonth = new LocalDate(lastDateOfMonth.getTime())
        LocalDate newFirstDateOfMonth = new LocalDate(firstDateOfMonth.getTime())

//        println firstDateOfMonth.getTime()
//        println lastDateOfMonth.getTime()

        def monthToDateCollections = com.ripple.project.Collection.findAllByProjectAndCollectionDateLessThanEquals(project, newLastDateOfMonth)
        def thisMonthCollections = com.ripple.project.Collection.findAllByProjectAndCollectionDateBetween(project, newFirstDateOfMonth, newLastDateOfMonth)
        def monthToDateCollectionAmount = monthToDateCollections*.amount.sum()?:0.00
        def thisMonthCollectionAmount = thisMonthCollections*.amount.sum()?:0.00

        def thisMonthCollectionRetentionAmount = thisMonthCollections*.retention.sum()?:0.00
        def thisMonthCollectionFwtAmount = thisMonthCollections*.fwt.sum()?:0.00
        def thisMonthCollectionWtAmount = thisMonthCollections*.wt.sum()?:0.00
        def thisMonthCollectionRecoupmentAmount = thisMonthCollections*.recoupment.sum()?:0.00
        def thisMonthCollectionNetAmount = thisMonthCollections*.netAmount.sum()?:0.00

        def monthToDateCollectionRetentionAmount = monthToDateCollections*.retention.sum()?:0.00
        def monthToDateCollectionFwtAmount = monthToDateCollections*.fwt.sum()?:0.00
        def monthToDateCollectionWtAmount = monthToDateCollections*.wt.sum()?:0.00
        def monthToDateCollectionRecoupmentAmount = monthToDateCollections*.recoupment.sum()?:0.00
        def monthToDateCollectionNetAmount = monthToDateCollections*.netAmount.sum()?:0.00



        def estimates = EstimateItem.findAllByProject(project)
        def estimateTotal = 0.00
        if (estimates)
            estimateTotal=estimates*.amount.sum()
        def balanceToDate = estimateTotal - monthToDateCollectionAmount?:0.00
//        println "This Month Collection:" + thisMonthCollectionAmount
//        println "Month To Date Collection:" + monthToDateCollectionAmount

        // Operating Expenses
        def expenseCategories = ExpenseType.constraints.expenseCategory.inList
        def expenses = project.expenses
        def finalList = []

        expenseCategories.each {
            def expenseTypes = ExpenseType.findAllByExpenseCategory(it)
            def expenseTypeList = []
            expenseTypes.each {
                def expenseType = it
                def estimateItem = EstimateItem.findByProjectAndType(project, expenseType)
                def expensesToDate = expenses.findAll {
                    (it.expenseDate <= newLastDateOfMonth && it.type.id == expenseType.id)
                }
                def expensesThisMonth = expenses.findAll {
                    (it.expenseDate >= newFirstDateOfMonth && it.expenseDate <= newLastDateOfMonth && it.type.id == expenseType.id)
                }

                def expenseToDateAmount = expensesToDate*.getAmount().sum()
                def expenseThisMonthAmount = expensesThisMonth*.getAmount().sum()
                def estimateItemAmount = estimateItem?.amount ?: 0.00
                expenseToDateAmount = expenseToDateAmount ?: 0.00
                expenseThisMonthAmount = expenseThisMonthAmount ?: 0.00

//                println " "
//                println "Expense:"+it
//                println "To Date:"+ expenseToDateAmount
//                println "This Month:"+expenseThisMonthAmount
//                println "Balance to Date:"+(estimateItemAmount - expenseToDateAmount)
//                println "Estimate Amount:"+estimateItemAmount
                expenseTypeList << [it, expenseToDateAmount, expenseThisMonthAmount, (estimateItemAmount - expenseToDateAmount), estimateItemAmount]
            }
            finalList << [it, expenseTypeList]
        }

        render(view: 'costSummary', model: [monthToDateCollectionAmount: monthToDateCollectionAmount,
                thisMonthCollectionAmount: thisMonthCollectionAmount,
                estimateTotalAmount: estimateTotal,
                balanceToDate: balanceToDate,
                finalList: finalList,
                thisMonthCollectionFwtAmount:thisMonthCollectionFwtAmount,
                thisMonthCollectionWtAmount:thisMonthCollectionWtAmount,
                thisMonthCollectionRetentionAmount:thisMonthCollectionRetentionAmount,
                thisMonthCollectionRecoupmentAmount:thisMonthCollectionRecoupmentAmount,
                monthToDateCollectionFwtAmount:monthToDateCollectionFwtAmount,
                monthToDateCollectionWtAmount:monthToDateCollectionWtAmount,
                monthToDateCollectionRetentionAmount:monthToDateCollectionRetentionAmount,
                monthToDateCollectionRecoupmentAmount:monthToDateCollectionRecoupmentAmount,
                thisMonthCollectionNetAmount:thisMonthCollectionNetAmount,
                monthToDateCollectionNetAmount:monthToDateCollectionNetAmount,
                project:project,
                period:newLastDateOfMonth

        ])
    }
}
