package com.ripple.master



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.util.Formatter as FM
import com.ripple.ModalizerController
import org.codehaus.groovy.grails.commons.GrailsClassUtils

class RateController extends ModalizerController {

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'Rate'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [rateInstanceList: Rate.list(params), rateInstanceTotal: Rate.count()]
    }

    def create() {
        [rateInstance: new Rate(params)]
    }

    def save() {
        def rateInstance = new Rate(params)
        if (!rateInstance.save(flush: true)) {
            render(view: "create", model: [rateInstance: rateInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'rate.label', default: 'Rate'), rateInstance.id])
        redirect(action: "show", id: rateInstance.id)
    }


    def show() {
        def rateInstance = Rate.get(params.id)
        if (!rateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rate.label', default: 'Rate'), params.id])
            redirect(action: "list")
            return
        }

        [rateInstance: rateInstance]
    }

    def edit() {
        def rateInstance = Rate.get(params.id)
        if (!rateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rate.label', default: 'Rate'), params.id])
            redirect(action: "list")
            return
        }

        [rateInstance: rateInstance]
    }

    def update() {
        def rateInstance = Rate.get(params.id)
        if (!rateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rate.label', default: 'Rate'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (rateInstance.version > version) {
                rateInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'rate.label', default: 'Rate')] as Object[],
                        "Another user has updated this Rate while you were editing")
                render(view: "edit", model: [rateInstance: rateInstance])
                return
            }
        }

        rateInstance.properties = params

        if (!rateInstance.save(flush: true)) {
            render(view: "edit", model: [rateInstance: rateInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'rate.label', default: 'Rate'), rateInstance.id])
        redirect(action: "show", id: rateInstance.id)
    }

    def delete() {
        def rateInstance = Rate.get(params.id)
        if (!rateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rate.label', default: 'Rate'), params.id])
            redirect(action: "list")
            return
        }

        try {
            rateInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'rate.label', default: 'Rate'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'rate.label', default: 'Rate'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Rate.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    employee{
                        or{
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }

                    ilike('type', "%${sSearch}%")

                    sqlRestriction("date_format(effectivity_date,'%m/%d/%Y') like '%${sSearch}%'")

                    ilike('description', "%${sSearch}%")

                    sqlRestriction("convert(amount,char) like '%${sSearch}%'")

                    sqlRestriction("convert(daily_cola,char) like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = Rate.count();
        def totalDisplayRecords = Rate.createCriteria().count {
            if (sSearch) {
                or {

                    employee{
                        or{
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }

                    ilike('type', "%${sSearch}%")

                    sqlRestriction("date_format(effectivity_date,'%m/%d/%Y') like '%${sSearch}%'")

                    ilike('description', "%${sSearch}%")

                    sqlRestriction("convert(amount,char) like '%${sSearch}%'")

                    sqlRestriction("convert(daily_cola,char) like '%${sSearch}%'")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.employee?.toString(),

                    1: it.type,

                    2: FM.formatLocalDate(it.effectivityDate,"MM/dd/Y"),

                    3: it.description,

                    4: FM.formatCurrency(it.amount),

                    5: FM.formatCurrency(it.dailyCola),

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    @Override
    def getListing(){
        def className = params?.className;
        def parentProperty = params?.parentProperty
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz
        def parentHolder = GrailsClassUtils.getPropertyType(modelInstance, parentProperty)
        def parentId = params?.parentId

        def listing = modelInstance.createCriteria().list(){
            if(parentProperty && parentHolder && parentId){
                eq(parentProperty, parentHolder.get(params?.parentId))
            }
        }

        def newListing = [];
        def tempMap = [:];

        listing.each {
            tempMap.put('id',it.id);
            tempMap.put('name', "${it?.encodeAsHTML()} - ${it?.effectivityDate.toString('MMMMM dd, yyyy')} - ${it?.description}");
            newListing += tempMap;
            tempMap = [:];
        }

        if (newListing){
            response.status=response.SC_OK
        }else{
            response.status = response.SC_INTERNAL_SERVER_ERROR
        }

        render newListing as JSON
    }
}


