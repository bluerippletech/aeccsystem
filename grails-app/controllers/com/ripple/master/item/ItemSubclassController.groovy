package com.ripple.master.item



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class ItemSubclassController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [itemSubclassInstanceList: ItemSubclass.list(params), itemSubclassInstanceTotal: ItemSubclass.count()]
    }

    def create() {
        [itemSubclassInstance: new ItemSubclass(params)]
    }

    def save() {
        def itemSubclassInstance = new ItemSubclass(params)
        if (!itemSubclassInstance.save(flush: true)) {
            render(view: "create", model: [itemSubclassInstance: itemSubclassInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), itemSubclassInstance.id])
        redirect(action: "show", id: itemSubclassInstance.id)
    }

    def show() {
        def itemSubclassInstance = ItemSubclass.get(params.id)
        if (!itemSubclassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), params.id])
            redirect(action: "list")
            return
        }

        [itemSubclassInstance: itemSubclassInstance]
    }

    def edit() {
        def itemSubclassInstance = ItemSubclass.get(params.id)
        if (!itemSubclassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), params.id])
            redirect(action: "list")
            return
        }

        [itemSubclassInstance: itemSubclassInstance]
    }

    def update() {
        def itemSubclassInstance = ItemSubclass.get(params.id)
        if (!itemSubclassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (itemSubclassInstance.version > version) {
                itemSubclassInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'itemSubclass.label', default: 'ItemSubclass')] as Object[],
                        "Another user has updated this ItemSubclass while you were editing")
                render(view: "edit", model: [itemSubclassInstance: itemSubclassInstance])
                return
            }
        }

        itemSubclassInstance.properties = params

        if (!itemSubclassInstance.save(flush: true)) {
            render(view: "edit", model: [itemSubclassInstance: itemSubclassInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), itemSubclassInstance.id])
        redirect(action: "show", id: itemSubclassInstance.id)
    }

    def delete() {
        def itemSubclassInstance = ItemSubclass.get(params.id)
        if (!itemSubclassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), params.id])
            redirect(action: "list")
            return
        }

        try {
            itemSubclassInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'itemSubclass.label', default: 'ItemSubclass'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = ItemSubclass.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                }
            }
            if (params?.itemClass?.id) {
                itemClass{
                    eq('id', params.itemClass.id);
                }
            }
        }
        def totalRecords = ItemSubclass.count();
        def totalDisplayRecords = ItemSubclass.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                }
            }
            if (params?.itemClass?.id) {
                itemClass{
                    eq('id', params.itemClass.id);
                }
            }
        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.name,

                    1: it.description,

                    2: it.status,

                    3: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
