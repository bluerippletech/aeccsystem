package com.ripple.master.item



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class ItemController {

    def documentNumbersService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [itemInstanceList: Item.list(params), itemInstanceTotal: Item.count()]
    }

    def create() {
        [itemInstance: new Item(params)]
    }

    def save() {

        Boolean test = true;
        def itemInstance = new Item(params)

        if (params.baseLevel?.unitOfMeasure){
            def levelUnitInstance = LevelUnit.findOrSaveWhere(unitOfMeasure: params.baseLevel?.unitOfMeasure, value: (0 as BigDecimal))
            itemInstance.baseLevel = levelUnitInstance;
        }

        if (params.secondLevel?.unitOfMeasure){
            def levelUnitInstance = LevelUnit.findOrSaveWhere(unitOfMeasure: params.secondLevel?.unitOfMeasure, value: ((params.secondLevel?.value?:0) as BigDecimal))
            itemInstance.secondLevel = levelUnitInstance;
        }

        if (params.thirdLevel?.unitOfMeasure){
            def levelUnitInstance = LevelUnit.findOrSaveWhere(unitOfMeasure: params.thirdLevel?.unitOfMeasure, value: ((params.thirdLevel?.value?:0) as BigDecimal))
            itemInstance.thirdLevel = levelUnitInstance;
        }

        if (itemInstance.validate()){
            if (!itemInstance.save(flush: true) && !test) {
                render(view: "create", model: [itemInstance: itemInstance])
                return
            }
        }else{
            render(view: "create", model: [itemInstance: itemInstance])
            return
        }



        flash.message = message(code: 'default.created.message', args: [message(code: 'item.label', default: 'Item'), itemInstance.id])
        redirect(action: "show", id: itemInstance.id)
    }

    def show() {
        def itemInstance = Item.get(params.id)
        if (!itemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])
            redirect(action: "list")
            return
        }

        [itemInstance: itemInstance]
    }

    def edit() {
        def itemInstance = Item.get(params.id)
        if (!itemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])
            redirect(action: "list")
            return
        }

        [itemInstance: itemInstance]
    }

    def update() {
        def itemInstance = Item.get(params.id)
        if (!itemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (itemInstance.version > version) {
                itemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'item.label', default: 'Item')] as Object[],
                        "Another user has updated this Item while you were editing")
                render(view: "edit", model: [itemInstance: itemInstance])
                return
            }
        }


//        def newParams = params?.findAll { !(it?.key in ['controller', 'action', 'baseLevel', 'unitOfMeasure']) }.collect { it.key }

        println params;
        itemInstance.properties = params;

        if (params.baseLevel?.unitOfMeasure){
            def levelUnitInstance = LevelUnit.findOrSaveWhere(unitOfMeasure: params.baseLevel?.unitOfMeasure, value: (0 as BigDecimal))
            if(itemInstance.baseLevel != levelUnitInstance)
                itemInstance.baseLevel = levelUnitInstance;
        }else if (!params.baseLevel?.unitOfMeasure && itemInstance.baseLevel){
            itemInstance.baseLevel = null;
        }

        if (params.secondLevel?.unitOfMeasure){
            def levelUnitInstance = LevelUnit.findOrSaveWhere(unitOfMeasure: params.secondLevel?.unitOfMeasure, value: ((params.secondLevel?.value?:0) as BigDecimal))
            if(itemInstance.secondLevel != levelUnitInstance)
                itemInstance?.secondLevel = levelUnitInstance;
        }else if (!params.secondLevel?.unitOfMeasure && itemInstance.secondLevel){
            itemInstance?.secondLevel = null;
        }

        if (params.thirdLevel?.unitOfMeasure){
            def levelUnitInstance = LevelUnit.findOrSaveWhere(unitOfMeasure: params.thirdLevel?.unitOfMeasure, value: ((params.thirdLevel?.value?:0) as BigDecimal))
            if(itemInstance?.thirdLevel != levelUnitInstance)
                itemInstance?.thirdLevel = levelUnitInstance;
        }else if (!params.thirdLevel?.unitOfMeasure && itemInstance.thirdLevel){
            itemInstance?.thirdLevel = null;
        }

        if (!itemInstance.save(flush: true)) {
            render(view: "edit", model: [itemInstance: itemInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'item.label', default: 'Item'), itemInstance.id])
        redirect(action: "show", id: itemInstance.id)
    }

    def delete() {
        def itemInstance = Item.get(params.id)
        if (!itemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), params.id])
            redirect(action: "list")
            return
        }

        try {
            itemInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'item.label', default: 'Item'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'item.label', default: 'Item'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Item.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('itemCode', "%${sSearch}%")

                    ilike('itemName', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                    itemClass{

                        ilike('name', "%${sSearch}%")

                    }

                    itemSubclass{

                        ilike('name', "%${sSearch}%")

                    }

                }
            }
        }
        def totalRecords = Item.count();
        def totalDisplayRecords = Item.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('itemCode', "%${sSearch}%")

                    ilike('itemName', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                    itemClass{

                        ilike('name', "%${sSearch}%")

                    }

                    itemSubclass{

                        ilike('name', "%${sSearch}%")

                    }

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.itemCode,

                    1: it.itemName,

                    2: it.description,

                    3: it.status,

                    4: it.itemClass?.toString(),

                    5: it.itemSubclass?.toString(),

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def getSubCat(){
        def itemSubclassList = ItemSubclass.findAllByItemClass(ItemClass.get(params?.id))
        def data = []
        def tempMap = [:]
        itemSubclassList.each {
            tempMap.put('optionKey',it.id);
            tempMap.put('optionValue',it.unitOfMeasure);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }

    def listing(){
        def uoMList = Item.findAllByItemNameLike("%${params.q}%")
        def data = []
        def tempMap = [:]
        uoMList.each {
            tempMap.put('id',it.id);
            tempMap.put('name',it.itemName);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }

    def uoMlisting(){
        def uoMList = LevelUnit.createCriteria().list {
            ilike("unitOfMeasure", "%${params.q}%")
            projections{
                distinct("unitOfMeasure")
            }
        }
        def data = []
        def tempMap = [:]
        uoMList.each {
            tempMap.put('id',null);
            tempMap.put('name',it);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }
}
