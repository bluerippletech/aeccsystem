package com.ripple.master.item



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class ItemClassController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [itemClassInstanceList: ItemClass.list(params), itemClassInstanceTotal: ItemClass.count()]
    }

    def create() {
        [itemClassInstance: new ItemClass(params)]
    }

    def save() {
        def itemClassInstance = new ItemClass(params)
        if (!itemClassInstance.save(flush: true)) {
            render(view: "create", model: [itemClassInstance: itemClassInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), itemClassInstance.id])
        redirect(action: "show", id: itemClassInstance.id)
    }

    def show() {
        def itemClassInstance = ItemClass.get(params.id)
        if (!itemClassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), params.id])
            redirect(action: "list")
            return
        }

        [itemClassInstance: itemClassInstance]
    }

    def edit() {
        def itemClassInstance = ItemClass.get(params.id)
        if (!itemClassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), params.id])
            redirect(action: "list")
            return
        }

        [itemClassInstance: itemClassInstance]
    }

    def update() {
        def itemClassInstance = ItemClass.get(params.id)
        if (!itemClassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (itemClassInstance.version > version) {
                itemClassInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'itemClass.label', default: 'ItemClass')] as Object[],
                        "Another user has updated this ItemClass while you were editing")
                render(view: "edit", model: [itemClassInstance: itemClassInstance])
                return
            }
        }

        itemClassInstance.properties = params

        if (!itemClassInstance.save(flush: true)) {
            render(view: "edit", model: [itemClassInstance: itemClassInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), itemClassInstance.id])
        redirect(action: "show", id: itemClassInstance.id)
    }

    def delete() {
        def itemClassInstance = ItemClass.get(params.id)
        if (!itemClassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), params.id])
            redirect(action: "list")
            return
        }

        try {
            itemClassInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'itemClass.label', default: 'ItemClass'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = ItemClass.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                }
            }
        }
        def totalRecords = ItemClass.count();
        def totalDisplayRecords = ItemClass.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.name,

                    1: it.description,

                    2: it.status,

                    3: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
