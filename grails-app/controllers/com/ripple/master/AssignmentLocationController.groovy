package com.ripple.master



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.ModalizerController

class AssignmentLocationController extends ModalizerController {

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'AssignmentLocation'}


    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [assignmentLocationInstanceList: AssignmentLocation.list(params), assignmentLocationInstanceTotal: AssignmentLocation.count()]
    }

    def create() {
        [assignmentLocationInstance: new AssignmentLocation(params)]
    }

    def save() {
        def assignmentLocationInstance = new AssignmentLocation(params)
        if (!assignmentLocationInstance.save(flush: true)) {
            render(view: "create", model: [assignmentLocationInstance: assignmentLocationInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), assignmentLocationInstance.id])
        redirect(action: "show", id: assignmentLocationInstance.id)
    }

    def show() {
        def assignmentLocationInstance = AssignmentLocation.get(params.id)
        if (!assignmentLocationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), params.id])
            redirect(action: "list")
            return
        }

        [assignmentLocationInstance: assignmentLocationInstance]
    }

    def edit() {
        def assignmentLocationInstance = AssignmentLocation.get(params.id)
        if (!assignmentLocationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), params.id])
            redirect(action: "list")
            return
        }

        [assignmentLocationInstance: assignmentLocationInstance]
    }

    def update() {
        def assignmentLocationInstance = AssignmentLocation.get(params.id)
        if (!assignmentLocationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (assignmentLocationInstance.version > version) {
                assignmentLocationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'assignmentLocation.label', default: 'AssignmentLocation')] as Object[],
                        "Another user has updated this AssignmentLocation while you were editing")
                render(view: "edit", model: [assignmentLocationInstance: assignmentLocationInstance])
                return
            }
        }

        assignmentLocationInstance.properties = params

        if (!assignmentLocationInstance.save(flush: true)) {
            render(view: "edit", model: [assignmentLocationInstance: assignmentLocationInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), assignmentLocationInstance.id])
        redirect(action: "show", id: assignmentLocationInstance.id)
    }

    def delete() {
        def assignmentLocationInstance = AssignmentLocation.get(params.id)
        if (!assignmentLocationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), params.id])
            redirect(action: "list")
            return
        }

        try {
            assignmentLocationInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = AssignmentLocation.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('location', "%${sSearch}%")

                    ilike('assignment', "%${sSearch}%")

                }
            }
        }
        def totalRecords = AssignmentLocation.count();
        def totalDisplayRecords = AssignmentLocation.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('location', "%${sSearch}%")

                    ilike('assignment', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.location,

                    1: it.assignment.toString(),

                    2: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
