package com.ripple.master



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.util.Formatter as FM

class HolidayController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [holidayInstanceList: Holiday.list(params), holidayInstanceTotal: Holiday.count()]
    }

    def create() {
        [holidayInstance: new Holiday(params)]
    }

    def save() {
        println params;
        def holidayInstance = new Holiday(params)
        if (!holidayInstance.save(flush: true)) {
            render(view: "create", model: [holidayInstance: holidayInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'holiday.label', default: 'Holiday'), holidayInstance.id])
        redirect(action: "show", id: holidayInstance.id)
    }

    def show() {
        def holidayInstance = Holiday.get(params.id)
        if (!holidayInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'holiday.label', default: 'Holiday'), params.id])
            redirect(action: "list")
            return
        }

        [holidayInstance: holidayInstance]
    }

    def edit() {
        def holidayInstance = Holiday.get(params.id)
        if (!holidayInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'holiday.label', default: 'Holiday'), params.id])
            redirect(action: "list")
            return
        }

        [holidayInstance: holidayInstance]
    }

    def update() {
        def holidayInstance = Holiday.get(params.id)
        if (!holidayInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'holiday.label', default: 'Holiday'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (holidayInstance.version > version) {
                holidayInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'holiday.label', default: 'Holiday')] as Object[],
                        "Another user has updated this Holiday while you were editing")
                render(view: "edit", model: [holidayInstance: holidayInstance])
                return
            }
        }

        holidayInstance.properties = params

        if (!holidayInstance.save(flush: true)) {
            render(view: "edit", model: [holidayInstance: holidayInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'holiday.label', default: 'Holiday'), holidayInstance.id])
        redirect(action: "show", id: holidayInstance.id)
    }

    def delete() {
        def holidayInstance = Holiday.get(params.id)
        if (!holidayInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'holiday.label', default: 'Holiday'), params.id])
            redirect(action: "list")
            return
        }

        try {
            holidayInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'holiday.label', default: 'Holiday'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'holiday.label', default: 'Holiday'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Holiday.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    sqlRestriction("date_format(holiday_date,'%m/%d/%Y') like '%${sSearch}%'")

                    ilike('description', "%${sSearch}%")

                    switch (sSearch){
                        case 'true':
                            sqlRestriction("legal_holiday=1")
                            break
                        case 'false':
                            sqlRestriction("legal_holiday=0")
                            break
                    }

                }
            }
        }
        def totalRecords = Holiday.count();
        def totalDisplayRecords = Holiday.createCriteria().count {
            if (sSearch) {
                or {

                    sqlRestriction("date_format(holiday_date,'%m/%d/%Y') like '%${sSearch}%'")

                    ilike('description', "%${sSearch}%")

                    switch (sSearch){
                        case 'true':
                            sqlRestriction("legal_holiday=1")
                            break
                        case 'false':
                            sqlRestriction("legal_holiday=0")
                            break
                    }

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: FM.formatLocalDate(it.holidayDate,"MM/dd/Y"),

                    1: it.description,

                    2: it.legalHoliday,

                    3: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }


    def getHolidayCalendar(){
        println "called"
    }

    def getRegularHolidayCalendar(){
        def holidays=Holiday.findAllByLegalHoliday(false)
        def jsonList = [];
        jsonList = holidays.collect{
            ['title':it.description,
            'start':it.holidayDate,
             'id':it.id
            ]
        }

        render jsonList as JSON
    }

    def getSpecialHolidayCalendar(){
        def holidays=Holiday.findAllByLegalHoliday(true)
        def jsonList = [];
        jsonList = holidays.collect{
            [       'title':it.description,
                    'start':it.holidayDate,
                    'id':it.id

            ]
        }

        render jsonList as JSON

    }
}
