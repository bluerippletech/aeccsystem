package com.ripple.master

import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.loans.Loan
import com.ripple.master.item.ItemSubclass
import com.ripple.master.item.ItemClass
import org.hibernate.criterion.CriteriaSpecification

class EmployeeController {

    def employeeService, burningImageService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 20, 100)
        [employeeInstanceList: Employee.list(params), employeeInstanceTotal: Employee.count()]
    }

    def create() {
        def taxStatus = employeeService.retreiveTaxStatus()
        [employeeInstance: new Employee(params),taxStatus: taxStatus]
    }

    def save() {
        def employeeInstance = new Employee(params)
        def statuses = employeeService.retreiveTaxStatus()
        String stri = employeeInstance.employeeNumber
        def grpNumber
        def output
        if (stri) {
            grpNumber = stri.substring(stri.length() - 1, stri.length())
            employeeInstance.grpNumber = Integer.parseInt(grpNumber)
        }
        if (employeeInstance?.picture) {
            def file = request.getFile('picture')
            def fileName = file.fileItem.fileName
            try {
                burningImageService.doWith(file, "./").execute {
                    it.scaleApproximate(200, 200)
                }
                output = new File("./${fileName}")
                employeeInstance?.setPicture(output.getBytes())
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                output.delete()
            }
        }
        if (!employeeInstance.save(flush: true)) {
            render(view: "create", model: [employeeInstance: employeeInstance, statuses: statuses])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'employee.label', default: 'Employee'), employeeInstance.id])
        redirect(action: "show", id: employeeInstance.id)
    }

    def show() {
        def employeeInstance = Employee.get(params.id)
        def loans = Loan.findAllByEmployee(employeeInstance)
        if (!employeeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
            redirect(action: "list")
            return
        }
        [employeeInstance: employeeInstance, loans: loans]
    }

    def edit() {
        def employeeInstance = Employee.get(params.id)
        if (!employeeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
            redirect(action: "list")
            return
        }
        def rateInstance
        if (employeeInstance.rate) {
            rateInstance = Rate.get(employeeInstance.rate.id)
        } else {
            rateInstance = new Rate(employee: employeeInstance)
        }
        def taxStatus = employeeService.retreiveTaxStatus()
        [employeeInstance: employeeInstance, rateInstance: rateInstance, taxStatus:taxStatus]
    }

    def update() {
        def employeeInstance = Employee.get(params.id)
        if (!employeeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (employeeInstance.version > version) {
                employeeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'employee.label', default: 'Employee')] as Object[],
                        "Another user has updated this Employee while you were editing")
                render(view: "edit", model: [employeeInstance: employeeInstance])
                return
            }
        }
        def employeePicture = employeeInstance?.picture
        def output
        employeeInstance.properties = params

        if (employeeInstance?.picture) {
            def file = request.getFile('picture')
            def fileName = file.fileItem.fileName
            try {
                burningImageService.doWith(file, "./").execute {
                    it.scaleApproximate(200, 200)
                }
                output = new File("./${fileName}")
                employeeInstance?.setPicture(output.getBytes())
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                output.delete()
            }
        }
        else {
            employeeInstance?.picture = employeePicture
        }

        if (!employeeInstance.save(flush: true)) {
            render(view: "edit", model: [employeeInstance: employeeInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'employee.label', default: 'Employee'), employeeInstance.id])
        redirect(action: "show", id: employeeInstance.id)
    }

    def delete() {
        def employeeInstance = Employee.get(params.id)
        if (!employeeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
            redirect(action: "list")
            return
        }

        try {
            employeeInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        sortColumn = sortColumn?.equals('fullName') ? 'lastName' : sortColumn;
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Employee.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            createAlias("currentAssignment", "ca", CriteriaSpecification.LEFT_JOIN)
            createAlias("ca.project", "cap", CriteriaSpecification.LEFT_JOIN)
            createAlias("ca.workLocation", "caw", CriteriaSpecification.LEFT_JOIN)
            if (sortColumn != "project" && sortColumn != "location")
                order(sortColumn, sortOrder)
            if (sSearch) {
                or {
                    ilike('firstName', "${sSearch}%")
                    ilike('lastName', "${sSearch}%")
                    ilike('middleInitials', "${sSearch}%")
                    ilike('employeeNumber', "${sSearch}%")
                    ilike('cap.projectName', "${sSearch}%")
                    ilike('caw.location', "${sSearch}%")

                }
            }
        }

        if (sortColumn == "project" && sortOrder == "asc") {
            listing = listing.sort {it?.currentAssignment?.project?.projectName}
        } else if (sortColumn == "project" && sortOrder == "desc") {
            listing = listing.reverseEach {it?.currentAssignment?.project?.projectName}
        }

        if (sortColumn == "location" && sortOrder == "asc") {
            listing = listing.sort {it?.currentAssignment?.workLocation?.location}
        } else if (sortColumn == "location" && sortOrder == "desc") {
            listing = listing.reverseEach {it?.currentAssignment?.workLocation?.location}
        }

        def totalRecords = Employee.count();
        def totalDisplayRecords = Employee.createCriteria().count {
            createAlias("currentAssignment", "ca", CriteriaSpecification.LEFT_JOIN)
            createAlias("ca.project", "cap", CriteriaSpecification.LEFT_JOIN)
            createAlias("ca.workLocation", "caw", CriteriaSpecification.LEFT_JOIN)
            if (sSearch) {
                or {
                    ilike('firstName', "${sSearch}%")
                    ilike('lastName', "${sSearch}%")
                    ilike('middleInitials', "${sSearch}%")
                    ilike('employeeNumber', "${sSearch}%")
                    ilike('cap.projectName', "${sSearch}%")
                    ilike('caw.location', "${sSearch}%")
                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.toString(),

                    1: it.employeeNumber,

                    2: it.currentAssignment?.project?.projectName,

                    3: it.currentAssignment?.workLocation?.location,

                    4: it.picture?.length ? "Yes" : "No",

                    5: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def returnJSON = {
        def list = []
        def listData

        listData = Employee.list()
        listData.each {
            list << [
                    it.id,
                    it.toString(),
                    it.employeeNumber,
                    it.currentAssignment?.project.toString(),
                    it.currentAssignment?.workLocation.toString(),
                    it.picture?.length ? "Yes" : "No"
            ]
        }
        def data = ["aaData": list]
        render data as JSON
    }

    def getAnniversariesThisMonth = {
        def returnval = employeeService.getEmployeesCurrentAnniversary(false)
        response.status = response.SC_OK
        render returnval
    }
    def getAnniversariesNextMonth = {
        def returnval = employeeService.getEmployeesCurrentAnniversary(true)
        response.status = response.SC_OK
        render returnval
    }
    def getBirthdays = {
        def returnval = employeeService.getBirthdays()
        response.status = response.SC_OK
        render returnval
    }

    def getSubCat() {
        def itemSubclassList = Assignment.get(params?.id).locations;
        def data = []
        def tempMap = [:]
        itemSubclassList.each {
            tempMap.put('optionKey', it.id);
            tempMap.put('optionValue', it.toString());
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }

}
