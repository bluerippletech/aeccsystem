package com.ripple.master



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.ModalizerController

class AssignmentHistoryController extends ModalizerController {

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'AssignmentHistory'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [assignmentHistoryInstanceList: AssignmentHistory.list(params), assignmentHistoryInstanceTotal: AssignmentHistory.count()]
    }

    def create() {
        [assignmentHistoryInstance: new AssignmentHistory(params)]
    }

    def save() {
        def assignmentHistoryInstance = new AssignmentHistory(params)
        if (!assignmentHistoryInstance.save(flush: true)) {
            render(view: "create", model: [assignmentHistoryInstance: assignmentHistoryInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), assignmentHistoryInstance.id])
        redirect(action: "show", id: assignmentHistoryInstance.id)
    }

    def show() {
        def assignmentHistoryInstance = AssignmentHistory.get(params.id)
        if (!assignmentHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), params.id])
            redirect(action: "list")
            return
        }

        [assignmentHistoryInstance: assignmentHistoryInstance]
    }

    def edit() {
        def assignmentHistoryInstance = AssignmentHistory.get(params.id)
        if (!assignmentHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), params.id])
            redirect(action: "list")
            return
        }

        [assignmentHistoryInstance: assignmentHistoryInstance]
    }

    def update() {
        def assignmentHistoryInstance = AssignmentHistory.get(params.id)
        if (!assignmentHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (assignmentHistoryInstance.version > version) {
                assignmentHistoryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'assignmentHistory.label', default: 'AssignmentHistory')] as Object[],
                        "Another user has updated this AssignmentHistory while you were editing")
                render(view: "edit", model: [assignmentHistoryInstance: assignmentHistoryInstance])
                return
            }
        }

        assignmentHistoryInstance.properties = params

        if (!assignmentHistoryInstance.save(flush: true)) {
            render(view: "edit", model: [assignmentHistoryInstance: assignmentHistoryInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), assignmentHistoryInstance.id])
        redirect(action: "show", id: assignmentHistoryInstance.id)
    }

    def delete() {
        def assignmentHistoryInstance = AssignmentHistory.get(params.id)
        if (!assignmentHistoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), params.id])
            redirect(action: "list")
            return
        }

        try {
            assignmentHistoryInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'assignmentHistory.label', default: 'AssignmentHistory'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = AssignmentHistory.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('employee', "%${sSearch}%")

                    ilike('project', "%${sSearch}%")

                    ilike('startDate', "%${sSearch}%")

                    ilike('workLocation', "%${sSearch}%")

                }
            }
        }
        def totalRecords = AssignmentHistory.count();
        def totalDisplayRecords = AssignmentHistory.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('employee', "%${sSearch}%")

                    ilike('project', "%${sSearch}%")

                    ilike('startDate', "%${sSearch}%")

                    ilike('workLocation', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.employee.toString(),

                    1: it.project.toString(),

                    2: it.startDate,

                    3: it.workLocation.toString(),

                    4: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
