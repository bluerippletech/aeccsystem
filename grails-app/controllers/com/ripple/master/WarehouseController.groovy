package com.ripple.master



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.ModalizerController

class WarehouseController extends ModalizerController{

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'Warehouse'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [warehouseInstanceList: Warehouse.list(params), warehouseInstanceTotal: Warehouse.count()]
    }

    def create() {
        [warehouseInstance: new Warehouse(params)]
    }

    def save() {
        def warehouseInstance = new Warehouse(params)
        if (!warehouseInstance.save(flush: true)) {
            render(view: "create", model: [warehouseInstance: warehouseInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), warehouseInstance.id])
        redirect(action: "show", id: warehouseInstance.id)
    }

    def show() {
        def warehouseInstance = Warehouse.get(params.id)
        if (!warehouseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), params.id])
            redirect(action: "list")
            return
        }

        [warehouseInstance: warehouseInstance]
    }

    def edit() {
        def warehouseInstance = Warehouse.get(params.id)
        if (!warehouseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), params.id])
            redirect(action: "list")
            return
        }

        [warehouseInstance: warehouseInstance]
    }

    def update() {
        def warehouseInstance = Warehouse.get(params.id)
        if (!warehouseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (warehouseInstance.version > version) {
                warehouseInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'warehouse.label', default: 'Warehouse')] as Object[],
                        "Another user has updated this Warehouse while you were editing")
                render(view: "edit", model: [warehouseInstance: warehouseInstance])
                return
            }
        }

        warehouseInstance.properties = params

        if (!warehouseInstance.save(flush: true)) {
            render(view: "edit", model: [warehouseInstance: warehouseInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), warehouseInstance.id])
        redirect(action: "show", id: warehouseInstance.id)
    }

    def delete() {
        def warehouseInstance = Warehouse.get(params.id)
        if (!warehouseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), params.id])
            redirect(action: "list")
            return
        }

        try {
            warehouseInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'warehouse.label', default: 'Warehouse'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Warehouse.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('location', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                    assignment{
                        or{
                            ilike('projectName', "%${sSearch}%")
                        }
                    }

                }
            }
        }
        def totalRecords = Warehouse.count();
        def totalDisplayRecords = Warehouse.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('location', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                    assignment{
                        or{
                            ilike('projectName', "%${sSearch}%")
                        }
                    }

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.name,

                    1: it.location,

                    2: it.status,

                    3: it.assignment?.toString()?:"None",

                    4: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
