package com.ripple.master.supplier



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.geography.Address
import com.ripple.master.sociology.ContactPerson
import com.ripple.master.geography.City
import com.ripple.master.geography.Province

class SupplierController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [supplierInstanceList: Supplier.list(params), supplierInstanceTotal: Supplier.count()]
}

def create() {
    [supplierInstance: new Supplier(params), addressInstance:new Address(params), addressContactInstance:new ContactPerson(params)]
}

def save() {
    Boolean test = true;


    def supplierInstance = new Supplier(params.supplier)

    def addressInstance = new Address(params.address)

    if (params.address?.province?.name){
        def provinceInstance = Province.findOrSaveWhere(name: params.address?.province?.name)
        addressInstance.province = provinceInstance
    }

    if (params.address?.city?.name){
        def cityInstance = City.findOrSaveWhere(name: params.address?.city?.name)

        if (!cityInstance.id){
            cityInstance.province=addressInstance.province
            cityInstance.save(flush: true)
        }

        addressInstance.city = cityInstance
    }

    def contactInstance = new ContactPerson(params.contactPerson)

    if (supplierInstance.validate() && addressInstance.validate() && contactInstance.validate()){
        if (addressInstance.save(flush: true) && contactInstance.save(flush: true)){
            supplierInstance.primaryAddress = addressInstance
            supplierInstance.primaryAddressContact = contactInstance
        }else{
            test = false;
        }

        if (!supplierInstance.save(flush: true) && !test) {
            render(view: "create", model: [supplierInstance: supplierInstance, addressInstance:addressInstance, addressContactInstance:contactInstance])
            return
        }
    }else{
        render(view: "create", model: [supplierInstance: supplierInstance, addressInstance:addressInstance, addressContactInstance:contactInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'supplier.label', default: 'Supplier'), supplierInstance.id])
    redirect(action: "show", id: supplierInstance.id)
}

def show() {
    def supplierInstance = Supplier.get(params.id)
    if (!supplierInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'supplier.label', default: 'Supplier'), params.id])
        redirect(action: "list")
        return
    }

    [supplierInstance: supplierInstance,  addressInstance:supplierInstance.primaryAddress, addressContactInstance:supplierInstance.primaryAddressContact]
}

def edit() {
    def supplierInstance = Supplier.get(params.id)
    if (!supplierInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'supplier.label', default: 'Supplier'), params.id])
        redirect(action: "list")
        return
    }

    [supplierInstance: supplierInstance,  addressInstance:supplierInstance.primaryAddress, addressContactInstance:supplierInstance.primaryAddressContact]
}

def update() {
    def supplierInstance = Supplier.get(params.id)
    if (!supplierInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'supplier.label', default: 'Supplier'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (supplierInstance.version > version) {
                supplierInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'supplier.label', default: 'Supplier')] as Object[],
                        "Another user has updated this Supplier while you were editing")
            render(view: "edit", model: [supplierInstance: supplierInstance])
            return
        }
    }

    supplierInstance.properties = params.supplier
    supplierInstance.primaryAddress.properties = params.address

    if (params.address?.province?.name){
        def provinceInstance = Province.findOrSaveWhere(name: params.address?.province?.name)
        supplierInstance.primaryAddress.province = provinceInstance
    }

    if (params.address?.city?.name){
        def cityInstance = City.findOrSaveWhere(name: params.address?.city?.name)

        if (!cityInstance.id){
            cityInstance.province=supplierInstance.primaryAddress.province
            cityInstance.save(flush: true)
        }

        supplierInstance.primaryAddress.city = cityInstance
    }

    supplierInstance.primaryAddressContact.properties = params.contactPerson

    if (!supplierInstance.save(flush: true)) {
        render(view: "edit", model: [supplierInstance: supplierInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'supplier.label', default: 'Supplier'), supplierInstance.id])
    redirect(action: "show", id: supplierInstance.id)
}

def delete() {
    def supplierInstance = Supplier.get(params.id)
    if (!supplierInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'supplier.label', default: 'Supplier'), params.id])
        redirect(action: "list")
        return
    }

    try {
        supplierInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'supplier.label', default: 'Supplier'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'supplier.label', default: 'Supplier'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = Supplier.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{

                ilike('supplierCode',"%${sSearch}%")

                ilike('supplierName',"%${sSearch}%")

                ilike('taxIdentificationNo',"%${sSearch}%")

                addresses {
                    ilike('address1',"%${sSearch}%")
                }

                contactPersons {
                    ilike('contactName',"%${sSearch}%")
                }

                ilike('email',"%${sSearch}%")

            }
        }
    }
    def totalRecords = Supplier.count();
    def totalDisplayRecords = Supplier.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('supplierCode',"%${sSearch}%")
                        
                        ilike('supplierName',"%${sSearch}%")
                        
                        ilike('taxIdentificationNo',"%${sSearch}%")
                        
                        addresses {
                            ilike('address1',"%${sSearch}%")
                        }

                        contactPersons {
                            ilike('contactName',"%${sSearch}%")
                        }
                        
                        ilike('email',"%${sSearch}%")

            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.supplierCode,
                        
                        1:it.supplierName,
                        
                        2:it.taxIdentificationNo,
                        
                        3:it.primaryAddress.toString(),
                        
                        4:it.primaryAddressContact.toString(),
                        
                        5:it.email,
                        
                        6:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
}
