package com.ripple

class DashboardController {

    def employeeService

    def index() {
        def anniversariesNxtMonth = employeeService.getEmployeesCurrentAnniversary(true)
        def anniversaries = employeeService.getEmployeesCurrentAnniversary(false)
        def birthdays = employeeService.getBirthdays()

        [birthdays:birthdays, anniversaries:anniversaries, anniversariesNxtMonth:anniversariesNxtMonth]
    }
}
