package com.ripple.deductions



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class WithholdingTaxPeriodController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def deductionsService;

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [withholdingTaxPeriodInstanceList: WithholdingTaxPeriod.list(params), withholdingTaxPeriodInstanceTotal: WithholdingTaxPeriod.count()]
    }

    def create() {
        [withholdingTaxPeriodInstance: new WithholdingTaxPeriod(params)]
    }

    def save() {
        def withholdingTaxPeriodInstance = new WithholdingTaxPeriod(params)
        if (!withholdingTaxPeriodInstance.save(flush: true)) {
            render(view: "create", model: [withholdingTaxPeriodInstance: withholdingTaxPeriodInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), withholdingTaxPeriodInstance.id])
        redirect(action: "show", id: withholdingTaxPeriodInstance.id)
    }

    def show() {
        def withholdingTaxPeriodInstance = WithholdingTaxPeriod.get(params.id)
        if (!withholdingTaxPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxPeriodInstance: withholdingTaxPeriodInstance]
    }

    def edit() {
        def withholdingTaxPeriodInstance = WithholdingTaxPeriod.get(params.id)
        if (!withholdingTaxPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxPeriodInstance: withholdingTaxPeriodInstance]
    }

    def update() {
        def withholdingTaxPeriodInstance = WithholdingTaxPeriod.get(params.id)
        if (!withholdingTaxPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (withholdingTaxPeriodInstance.version > version) {
                withholdingTaxPeriodInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod')] as Object[],
                        "Another user has updated this WithholdingTaxPeriod while you were editing")
                render(view: "edit", model: [withholdingTaxPeriodInstance: withholdingTaxPeriodInstance])
                return
            }
        }

        withholdingTaxPeriodInstance.properties = params

        if (!withholdingTaxPeriodInstance.save(flush: true)) {
            render(view: "edit", model: [withholdingTaxPeriodInstance: withholdingTaxPeriodInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), withholdingTaxPeriodInstance.id])
        redirect(action: "show", id: withholdingTaxPeriodInstance.id)
    }

    def delete() {
        def withholdingTaxPeriodInstance = WithholdingTaxPeriod.get(params.id)
        if (!withholdingTaxPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), params.id])
            redirect(action: "list")
            return
        }

        try {
            withholdingTaxPeriodInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = WithholdingTaxPeriod.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                }
            }
        }
        def totalRecords = WithholdingTaxPeriod.count();
        def totalDisplayRecords = WithholdingTaxPeriod.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.name,

                    1: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def table = {
        [withholdingTaxPeriodInstance: WithholdingTaxPeriod.get(params?.id), deductionsService:deductionsService]
    }

}
