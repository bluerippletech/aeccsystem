package com.ripple.deductions



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class WithholdingTaxExemptionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [withholdingTaxExemptionInstanceList: WithholdingTaxExemption.list(params), withholdingTaxExemptionInstanceTotal: WithholdingTaxExemption.count()]
    }

    def create() {
        [withholdingTaxExemptionInstance: new WithholdingTaxExemption(params)]
    }

    def save() {
        def withholdingTaxExemptionInstance = new WithholdingTaxExemption(params)
        if (!withholdingTaxExemptionInstance.save(flush: true)) {
            render(view: "create", model: [withholdingTaxExemptionInstance: withholdingTaxExemptionInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), withholdingTaxExemptionInstance.id])
        redirect(controller: 'withholdingTaxPeriod', action: "table", id: withholdingTaxExemptionInstance.bracket.period.id)
    }

    def show() {
        def withholdingTaxExemptionInstance = WithholdingTaxExemption.get(params.id)
        if (!withholdingTaxExemptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxExemptionInstance: withholdingTaxExemptionInstance]
    }

    def edit() {
        def withholdingTaxExemptionInstance = WithholdingTaxExemption.get(params.id)
        if (!withholdingTaxExemptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxExemptionInstance: withholdingTaxExemptionInstance]
    }

    def update() {
        def withholdingTaxExemptionInstance = WithholdingTaxExemption.get(params.id)
        if (!withholdingTaxExemptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (withholdingTaxExemptionInstance.version > version) {
                withholdingTaxExemptionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption')] as Object[],
                        "Another user has updated this WithholdingTaxExemption while you were editing")
                render(view: "edit", model: [withholdingTaxExemptionInstance: withholdingTaxExemptionInstance])
                return
            }
        }

        withholdingTaxExemptionInstance.properties = params

        if (!withholdingTaxExemptionInstance.save(flush: true)) {
            render(view: "edit", model: [withholdingTaxExemptionInstance: withholdingTaxExemptionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), withholdingTaxExemptionInstance.id])
        redirect(controller: 'withholdingTaxPeriod', action: "table", id: withholdingTaxExemptionInstance.bracket.period.id)
    }

    def delete() {
        def withholdingTaxExemptionInstance = WithholdingTaxExemption.get(params.id)
        if (!withholdingTaxExemptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), params.id])
            redirect(action: "list")
            return
        }

        try {
            withholdingTaxExemptionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = WithholdingTaxExemption.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('status', "%${sSearch}%")

                    ilike('bracket', "%${sSearch}%")

                    ilike('value', "%${sSearch}%")

                }
            }
        }
        def totalRecords = WithholdingTaxExemption.count();
        def totalDisplayRecords = WithholdingTaxExemption.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('status', "%${sSearch}%")

                    ilike('bracket', "%${sSearch}%")

                    ilike('value', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.status,

                    1: it.bracket,

                    2: it.value,

                    3: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
