package com.ripple.deductions



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class PhilHealthContributionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [philHealthContributionInstanceList: PhilHealthContribution.list(params), philHealthContributionInstanceTotal: PhilHealthContribution.count()]
    }

    def create() {
        [philHealthContributionInstance: new PhilHealthContribution(params)]
    }

    def save() {
        def philHealthContributionInstance = new PhilHealthContribution(params)
        if (!philHealthContributionInstance.save(flush: true)) {
            render(view: "create", model: [philHealthContributionInstance: philHealthContributionInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), philHealthContributionInstance.id])
        redirect(action: "show", id: philHealthContributionInstance.id)
    }

    def show() {
        def philHealthContributionInstance = PhilHealthContribution.get(params.id)
        if (!philHealthContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), params.id])
            redirect(action: "list")
            return
        }

        [philHealthContributionInstance: philHealthContributionInstance]
    }

    def edit() {
        def philHealthContributionInstance = PhilHealthContribution.get(params.id)
        if (!philHealthContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), params.id])
            redirect(action: "list")
            return
        }

        [philHealthContributionInstance: philHealthContributionInstance]
    }

    def update() {
        def philHealthContributionInstance = PhilHealthContribution.get(params.id)
        if (!philHealthContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (philHealthContributionInstance.version > version) {
                philHealthContributionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution')] as Object[],
                        "Another user has updated this PhilHealthContribution while you were editing")
                render(view: "edit", model: [philHealthContributionInstance: philHealthContributionInstance])
                return
            }
        }

        philHealthContributionInstance.properties = params

        if (!philHealthContributionInstance.save(flush: true)) {
            render(view: "edit", model: [philHealthContributionInstance: philHealthContributionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), philHealthContributionInstance.id])
        redirect(action: "show", id: philHealthContributionInstance.id)
    }

    def delete() {
        def philHealthContributionInstance = PhilHealthContribution.get(params.id)
        if (!philHealthContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), params.id])
            redirect(action: "list")
            return
        }

        try {
            philHealthContributionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'philHealthContribution.label', default: 'PhilHealthContribution'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = PhilHealthContribution.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    sqlRestriction("convert(low_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(high_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(salary_base,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_share,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employer_share,char) like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = PhilHealthContribution.count();
        def totalDisplayRecords = PhilHealthContribution.createCriteria().count {
            if (sSearch) {
                or {

                    sqlRestriction("convert(low_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(high_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(salary_base,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_share,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employer_share,char) like '%${sSearch}%'")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: formatNumber(number:it.lowRange, format:"Php ###,##0.00")+ ' - ' +formatNumber(number:it.highRange, format:"Php ###,##0.00"),

                    1: formatNumber(number:it.salaryBase, format:"Php ###,##0.00"),

                    2: formatNumber(number:it.employerShare, format:"Php ###,##0.00"),

                    3: formatNumber(number:it.employeeShare, format:"Php ###,##0.00"),

                    4: formatNumber(number:it.totalPhilHealthContribution, format:"Php ###,##0.00"),

                    5: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
