package com.ripple.deductions



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class WithholdingTaxBracketController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [withholdingTaxBracketInstanceList: WithholdingTaxBracket.list(params), withholdingTaxBracketInstanceTotal: WithholdingTaxBracket.count()]
    }

    def create() {
        [withholdingTaxBracketInstance: new WithholdingTaxBracket(params)]
    }

    def save() {
        def withholdingTaxBracketInstance = new WithholdingTaxBracket(params)
        if (!withholdingTaxBracketInstance.save(flush: true)) {
            render(view: "create", model: [withholdingTaxBracketInstance: withholdingTaxBracketInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), withholdingTaxBracketInstance.id])
        redirect(controller: 'withholdingTaxPeriod', action: "table", id: withholdingTaxBracketInstance.period.id)
    }

    def show() {
        def withholdingTaxBracketInstance = WithholdingTaxBracket.get(params.id)
        if (!withholdingTaxBracketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxBracketInstance: withholdingTaxBracketInstance]
    }

    def edit() {
        def withholdingTaxBracketInstance = WithholdingTaxBracket.get(params.id)
        if (!withholdingTaxBracketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxBracketInstance: withholdingTaxBracketInstance]
    }

    def update() {
        def withholdingTaxBracketInstance = WithholdingTaxBracket.get(params.id)
        if (!withholdingTaxBracketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (withholdingTaxBracketInstance.version > version) {
                withholdingTaxBracketInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket')] as Object[],
                        "Another user has updated this WithholdingTaxBracket while you were editing")
                render(view: "edit", model: [withholdingTaxBracketInstance: withholdingTaxBracketInstance])
                return
            }
        }

        withholdingTaxBracketInstance.properties = params

        if (!withholdingTaxBracketInstance.save(flush: true)) {
            render(view: "edit", model: [withholdingTaxBracketInstance: withholdingTaxBracketInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), withholdingTaxBracketInstance.id])
        redirect(controller: 'withholdingTaxPeriod', action: "table", id: withholdingTaxBracketInstance.period.id)
    }

    def delete() {
        def withholdingTaxBracketInstance = WithholdingTaxBracket.get(params.id)
        if (!withholdingTaxBracketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), params.id])
            redirect(action: "list")
            return
        }

        try {
            withholdingTaxBracketInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = WithholdingTaxBracket.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('baseTax', "%${sSearch}%")

                    ilike('percentOver', "%${sSearch}%")

                    ilike('period', "%${sSearch}%")

                }
            }
        }
        def totalRecords = WithholdingTaxBracket.count();
        def totalDisplayRecords = WithholdingTaxBracket.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('baseTax', "%${sSearch}%")

                    ilike('percentOver', "%${sSearch}%")

                    ilike('period', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.name,

                    1: it.baseTax,

                    2: it.percentOver,

                    3: it.period,

                    4: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
