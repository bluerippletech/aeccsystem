package com.ripple.deductions



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class WithholdingTaxStatusController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [withholdingTaxStatusInstanceList: WithholdingTaxStatus.list(params), withholdingTaxStatusInstanceTotal: WithholdingTaxStatus.count()]
    }

    def create() {
        [withholdingTaxStatusInstance: new WithholdingTaxStatus(params)]
    }

    def save() {
        def withholdingTaxStatusInstance = new WithholdingTaxStatus(params)
        if (!withholdingTaxStatusInstance.save(flush: true)) {
            render(view: "create", model: [withholdingTaxStatusInstance: withholdingTaxStatusInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), withholdingTaxStatusInstance.id])
        redirect(controller: 'withholdingTaxPeriod', action: "table", id: withholdingTaxStatusInstance.period.id)
    }

    def show() {
        def withholdingTaxStatusInstance = WithholdingTaxStatus.get(params.id)
        if (!withholdingTaxStatusInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxStatusInstance: withholdingTaxStatusInstance]
    }

    def edit() {
        def withholdingTaxStatusInstance = WithholdingTaxStatus.get(params.id)
        if (!withholdingTaxStatusInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), params.id])
            redirect(action: "list")
            return
        }

        [withholdingTaxStatusInstance: withholdingTaxStatusInstance]
    }

    def update() {
        def withholdingTaxStatusInstance = WithholdingTaxStatus.get(params.id)
        if (!withholdingTaxStatusInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (withholdingTaxStatusInstance.version > version) {
                withholdingTaxStatusInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus')] as Object[],
                        "Another user has updated this WithholdingTaxStatus while you were editing")
                render(view: "edit", model: [withholdingTaxStatusInstance: withholdingTaxStatusInstance])
                return
            }
        }

        withholdingTaxStatusInstance.properties = params

        if (!withholdingTaxStatusInstance.save(flush: true)) {
            render(view: "edit", model: [withholdingTaxStatusInstance: withholdingTaxStatusInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), withholdingTaxStatusInstance.id])

        redirect(controller: 'withholdingTaxPeriod', action: "table", id: withholdingTaxStatusInstance.period.id)
    }

    def delete() {
        def withholdingTaxStatusInstance = WithholdingTaxStatus.get(params.id)
        if (!withholdingTaxStatusInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), params.id])
            redirect(action: "list")
            return
        }

        try {
            withholdingTaxStatusInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = WithholdingTaxStatus.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('dependents', "%${sSearch}%")

                    ilike('period', "%${sSearch}%")

                }
            }
        }
        def totalRecords = WithholdingTaxStatus.count();
        def totalDisplayRecords = WithholdingTaxStatus.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('name', "%${sSearch}%")

                    ilike('dependents', "%${sSearch}%")

                    ilike('period', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.name,

                    1: it.dependents,

                    2: it.period,

                    3: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
