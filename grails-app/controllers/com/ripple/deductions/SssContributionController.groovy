package com.ripple.deductions



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class SssContributionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [sssContributionInstanceList: SssContribution.list(params), sssContributionInstanceTotal: SssContribution.count()]
    }

    def create() {
        [sssContributionInstance: new SssContribution(params)]
    }

    def save() {
        def sssContributionInstance = new SssContribution(params)
        if (!sssContributionInstance.save(flush: true)) {
            render(view: "create", model: [sssContributionInstance: sssContributionInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), sssContributionInstance.id])
        redirect(action: "show", id: sssContributionInstance.id)
    }

    def show() {
        def sssContributionInstance = SssContribution.get(params.id)
        if (!sssContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), params.id])
            redirect(action: "list")
            return
        }

        [sssContributionInstance: sssContributionInstance]
    }

    def edit() {
        def sssContributionInstance = SssContribution.get(params.id)
        if (!sssContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), params.id])
            redirect(action: "list")
            return
        }

        [sssContributionInstance: sssContributionInstance]
    }

    def update() {
        def sssContributionInstance = SssContribution.get(params.id)
        if (!sssContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (sssContributionInstance.version > version) {
                sssContributionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'sssContribution.label', default: 'SssContribution')] as Object[],
                        "Another user has updated this SssContribution while you were editing")
                render(view: "edit", model: [sssContributionInstance: sssContributionInstance])
                return
            }
        }

        sssContributionInstance.properties = params

        if (!sssContributionInstance.save(flush: true)) {
            render(view: "edit", model: [sssContributionInstance: sssContributionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), sssContributionInstance.id])
        redirect(action: "show", id: sssContributionInstance.id)
    }

    def delete() {
        def sssContributionInstance = SssContribution.get(params.id)
        if (!sssContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), params.id])
            redirect(action: "list")
            return
        }

        try {
            sssContributionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'sssContribution.label', default: 'SssContribution'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = SssContribution.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    sqlRestriction("convert(low_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(high_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(monthly_salary_credit,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_compensation,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_contribution,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employer_contribution,char) like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = SssContribution.count();
        def totalDisplayRecords = SssContribution.createCriteria().count {
            if (sSearch) {
                or {

                    sqlRestriction("convert(low_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(high_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(monthly_salary_credit,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_compensation,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_contribution,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employer_contribution,char) like '%${sSearch}%'")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: formatNumber(number: it.lowRange, format: "Php ###,##0.00") + ' - ' + formatNumber(number: it.highRange, format: "Php ###,##0.00"),

                    1: formatNumber(number: it.monthlySalaryCredit, format: "Php ###,##0.00"),

                    2: formatNumber(number: it.employeeCompensation, format: "Php ###,##0.00"),

                    3: formatNumber(number: it.employeeContribution, format: "Php ###,##0.00"),

                    4: formatNumber(number: it.employerContribution, format: "Php ###,##0.00"),

                    5: formatNumber(number: it.totalSssContribution, format: "Php ###,##0.00"),

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
