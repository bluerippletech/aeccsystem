package com.ripple.deductions



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class PagIbigContributionController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [pagIbigContributionInstanceList: PagIbigContribution.list(params), pagIbigContributionInstanceTotal: PagIbigContribution.count()]
    }

    def create() {
        [pagIbigContributionInstance: new PagIbigContribution(params)]
    }

    def save() {
        def pagIbigContributionInstance = new PagIbigContribution(params)
        if (!pagIbigContributionInstance.save(flush: true)) {
            render(view: "create", model: [pagIbigContributionInstance: pagIbigContributionInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), pagIbigContributionInstance.id])
        redirect(action: "show", id: pagIbigContributionInstance.id)
    }

    def show() {
        def pagIbigContributionInstance = PagIbigContribution.get(params.id)
        if (!pagIbigContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), params.id])
            redirect(action: "list")
            return
        }

        [pagIbigContributionInstance: pagIbigContributionInstance]
    }

    def edit() {
        def pagIbigContributionInstance = PagIbigContribution.get(params.id)
        if (!pagIbigContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), params.id])
            redirect(action: "list")
            return
        }

        [pagIbigContributionInstance: pagIbigContributionInstance]
    }

    def update() {
        def pagIbigContributionInstance = PagIbigContribution.get(params.id)
        if (!pagIbigContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (pagIbigContributionInstance.version > version) {
                pagIbigContributionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution')] as Object[],
                        "Another user has updated this PagIbigContribution while you were editing")
                render(view: "edit", model: [pagIbigContributionInstance: pagIbigContributionInstance])
                return
            }
        }

        pagIbigContributionInstance.properties = params

        if (!pagIbigContributionInstance.save(flush: true)) {
            render(view: "edit", model: [pagIbigContributionInstance: pagIbigContributionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), pagIbigContributionInstance.id])
        redirect(action: "show", id: pagIbigContributionInstance.id)
    }

    def delete() {
        def pagIbigContributionInstance = PagIbigContribution.get(params.id)
        if (!pagIbigContributionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), params.id])
            redirect(action: "list")
            return
        }

        try {
            pagIbigContributionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'pagIbigContribution.label', default: 'PagIbigContribution'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = PagIbigContribution.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    sqlRestriction("convert(low_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(high_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_multiplier,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employer_multiplier,char) like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = PagIbigContribution.count();
        def totalDisplayRecords = PagIbigContribution.createCriteria().count {
            if (sSearch) {
                or {

                    sqlRestriction("convert(low_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(high_range,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employee_multiplier,char) like '%${sSearch}%'")

                    sqlRestriction("convert(employer_multiplier,char) like '%${sSearch}%'")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.lowRange,

                    1: it.highRange,

                    2: it.employeeMultiplier,

                    3: it.employerMultiplier,

                    4: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
