package com.ripple.stock



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class CycleCountItemsController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [cycleCountItemsInstanceList: CycleCountItems.list(params), cycleCountItemsInstanceTotal: CycleCountItems.count()]
    }

    def create() {
        [cycleCountItemsInstance: new CycleCountItems(params)]
    }

    def save() {
        def cycleCountItemsInstance = new CycleCountItems(params)
        if (!cycleCountItemsInstance.save(flush: true)) {
            render(view: "create", model: [cycleCountItemsInstance: cycleCountItemsInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), cycleCountItemsInstance.id])
        redirect(action: "show", id: cycleCountItemsInstance.id)
    }

    def show() {
        def cycleCountItemsInstance = CycleCountItems.get(params.id)
        if (!cycleCountItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), params.id])
            redirect(action: "list")
            return
        }

        [cycleCountItemsInstance: cycleCountItemsInstance]
    }

    def edit() {
        def cycleCountItemsInstance = CycleCountItems.get(params.id)
        if (!cycleCountItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), params.id])
            redirect(action: "list")
            return
        }

        [cycleCountItemsInstance: cycleCountItemsInstance]
    }

    def update() {
        def cycleCountItemsInstance = CycleCountItems.get(params.id)
        if (!cycleCountItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (cycleCountItemsInstance.version > version) {
                cycleCountItemsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'cycleCountItems.label', default: 'CycleCountItems')] as Object[],
                        "Another user has updated this CycleCountItems while you were editing")
                render(view: "edit", model: [cycleCountItemsInstance: cycleCountItemsInstance])
                return
            }
        }

        cycleCountItemsInstance.properties = params

        if (!cycleCountItemsInstance.save(flush: true)) {
            render(view: "edit", model: [cycleCountItemsInstance: cycleCountItemsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), cycleCountItemsInstance.id])
        redirect(action: "show", id: cycleCountItemsInstance.id)
    }

    def delete() {
        def cycleCountItemsInstance = CycleCountItems.get(params.id)
        if (!cycleCountItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), params.id])
            redirect(action: "list")
            return
        }

        try {
            cycleCountItemsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = CycleCountItems.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('item', "%${sSearch}%")

                }
            }
        }
        def totalRecords = CycleCountItems.count();
        def totalDisplayRecords = CycleCountItems.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('item', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.isDeleted,

                    1: it.createdDate,

                    2: it.editedDate,

                    3: it.editedBy,

                    4: it.createdBy,

                    5: it.item,

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
