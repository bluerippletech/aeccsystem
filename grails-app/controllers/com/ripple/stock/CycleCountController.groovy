package com.ripple.stock



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.item.ItemSubclass
import com.ripple.master.item.ItemClass

class CycleCountController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [cycleCountInstanceList: CycleCount.list(params), cycleCountInstanceTotal: CycleCount.count()]
    }

    def create() {
        [cycleCountInstance: new CycleCount(params)]
    }

    def save() {
        def cycleCountInstance = new CycleCount(params)

        def stockCardItems = StockCard.findByWarehouse(cycleCountInstance.warehouse)

        if (!stockCardItems){
            render(view: "create", model: [cycleCountInstance: cycleCountInstance])
            return
        }

        if (!cycleCountInstance.save(flush: true)) {
            render(view: "create", model: [cycleCountInstance: cycleCountInstance])
            return
        }

        stockCardItems.each {
            if(it?.remainingBalance>0){
                def cycleCountItems = new CycleCountItems(
                        stockReference: it, item: it?.item,
                        onHandBaseLevel: it?.remainingBalance
                )
                cycleCountInstance.addToItems(cycleCountItems)
            }
        }


        flash.message = message(code: 'default.created.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), cycleCountInstance.id])
        redirect(action: "show", id: cycleCountInstance.id)
    }

    def show() {
        def cycleCountInstance = CycleCount.get(params.id)
        if (!cycleCountInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), params.id])
            redirect(action: "list")
            return
        }

        [cycleCountInstance: cycleCountInstance]
    }

    def edit() {
        def cycleCountInstance = CycleCount.get(params.id)
        if (!cycleCountInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), params.id])
            redirect(action: "list")
            return
        }

        [cycleCountInstance: cycleCountInstance]
    }

    def update() {
        def cycleCountInstance = CycleCount.get(params.id)
        if (!cycleCountInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (cycleCountInstance.version > version) {
                cycleCountInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'cycleCount.label', default: 'CycleCount')] as Object[],
                        "Another user has updated this CycleCount while you were editing")
                render(view: "edit", model: [cycleCountInstance: cycleCountInstance])
                return
            }
        }

        cycleCountInstance.properties = params

        if (!cycleCountInstance.save(flush: true)) {
            render(view: "edit", model: [cycleCountInstance: cycleCountInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), cycleCountInstance.id])
        redirect(action: "show", id: cycleCountInstance.id)
    }

    def delete() {
        def cycleCountInstance = CycleCount.get(params.id)
        if (!cycleCountInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), params.id])
            redirect(action: "list")
            return
        }

        try {
            cycleCountInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'cycleCount.label', default: 'CycleCount'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = CycleCount.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('cycleCountNumber', "%${sSearch}%")

                }
            }
        }
        def totalRecords = CycleCount.count();
        def totalDisplayRecords = CycleCount.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('cycleCountNumber', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.cycleCountNumber,

                    1: it.warehouse?.toString(),

                    2: it.createdDate,

                    3: it.status,

                    4: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def getSubCat(){
        def itemSubclassList = ItemSubclass.findAllByItemClass(ItemClass.get(params?.id))
        def data = []
        def tempMap = [:]
        itemSubclassList.each {
            tempMap.put('optionKey',it.id);
            tempMap.put('optionValue',it.name);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }
}
