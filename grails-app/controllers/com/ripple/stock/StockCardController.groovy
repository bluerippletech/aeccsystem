package com.ripple.stock



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class StockCardController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [stockCardInstanceList: StockCard.list(params), stockCardInstanceTotal: StockCard.count()]
}

def create() {
    [stockCardInstance: new StockCard(params)]
}

def save() {
    def stockCardInstance = new StockCard(params)
    if (!stockCardInstance.save(flush: true)) {
        render(view: "create", model: [stockCardInstance: stockCardInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'stockCard.label', default: 'StockCard'), stockCardInstance.id])
    redirect(action: "show", id: stockCardInstance.id)
}

def show() {
    def stockCardInstance = StockCard.get(params.id)
    if (!stockCardInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'stockCard.label', default: 'StockCard'), params.id])
        redirect(action: "list")
        return
    }

    [stockCardInstance: stockCardInstance]
}

def edit() {
    def stockCardInstance = StockCard.get(params.id)
    if (!stockCardInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'stockCard.label', default: 'StockCard'), params.id])
        redirect(action: "list")
        return
    }

    [stockCardInstance: stockCardInstance]
}

def update() {
    def stockCardInstance = StockCard.get(params.id)
    if (!stockCardInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'stockCard.label', default: 'StockCard'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (stockCardInstance.version > version) {
                stockCardInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'stockCard.label', default: 'StockCard')] as Object[],
                        "Another user has updated this StockCard while you were editing")
            render(view: "edit", model: [stockCardInstance: stockCardInstance])
            return
        }
    }

    stockCardInstance.properties = params

    if (!stockCardInstance.save(flush: true)) {
        render(view: "edit", model: [stockCardInstance: stockCardInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'stockCard.label', default: 'StockCard'), stockCardInstance.id])
    redirect(action: "show", id: stockCardInstance.id)
}

def delete() {
    def stockCardInstance = StockCard.get(params.id)
    if (!stockCardInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'stockCard.label', default: 'StockCard'), params.id])
        redirect(action: "list")
        return
    }

    try {
        stockCardInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'stockCard.label', default: 'StockCard'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'stockCard.label', default: 'StockCard'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = StockCard.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('transactionCode',"%${sSearch}%")
                        
                        ilike('transactionReferenceId',"%${sSearch}%")
                        
                        ilike('rrReference',"%${sSearch}%")
                        
                        ilike('referenceStock',"%${sSearch}%")
                        
                        ilike('movement',"%${sSearch}%")
                        
                        ilike('quantity',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = StockCard.count();
    def totalDisplayRecords = StockCard.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('transactionCode',"%${sSearch}%")
                        
                        ilike('transactionReferenceId',"%${sSearch}%")
                        
                        ilike('rrReference',"%${sSearch}%")
                        
                        ilike('referenceStock',"%${sSearch}%")
                        
                        ilike('movement',"%${sSearch}%")
                        
                        ilike('quantity',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.transactionCode,
                        
                        1:it.transactionReferenceId,
                        
                        2:it.rrReference,
                        
                        3:it.referenceStock,
                        
                        4:it.movement,
                        
                        5:it.quantity,
                        
                        6:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
}
