package com.ripple.security



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class RequestmapController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [requestmapInstanceList: Requestmap.list(params), requestmapInstanceTotal: Requestmap.count()]
    }

    def create() {
        [requestmapInstance: new Requestmap(params)]
    }

    def save() {
        def requestmapInstance = new Requestmap(params)
        if (!requestmapInstance.save(flush: true)) {
            render(view: "create", model: [requestmapInstance: requestmapInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), requestmapInstance.id])
        redirect(action: "show", id: requestmapInstance.id)
    }

    def show() {
        def requestmapInstance = Requestmap.get(params.id)
        if (!requestmapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])
            redirect(action: "list")
            return
        }

        [requestmapInstance: requestmapInstance]
    }

    def edit() {
        def requestmapInstance = Requestmap.get(params.id)
        if (!requestmapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])
            redirect(action: "list")
            return
        }

        [requestmapInstance: requestmapInstance]
    }

    def update() {
        def requestmapInstance = Requestmap.get(params.id)
        if (!requestmapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (requestmapInstance.version > version) {
                requestmapInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'requestmap.label', default: 'Requestmap')] as Object[],
                        "Another user has updated this Requestmap while you were editing")
                render(view: "edit", model: [requestmapInstance: requestmapInstance])
                return
            }
        }

        requestmapInstance.properties = params

        if (!requestmapInstance.save(flush: true)) {
            render(view: "edit", model: [requestmapInstance: requestmapInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), requestmapInstance.id])
        redirect(action: "show", id: requestmapInstance.id)
    }

    def delete() {
        def requestmapInstance = Requestmap.get(params.id)
        if (!requestmapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])
            redirect(action: "list")
            return
        }

        try {
            requestmapInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'requestmap.label', default: 'Requestmap'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Requestmap.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('url', "%${sSearch}%")

                    ilike('configAttribute', "%${sSearch}%")

                }
            }
        }
        def totalRecords = Requestmap.count();
        def totalDisplayRecords = Requestmap.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('url', "%${sSearch}%")

                    ilike('configAttribute', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.url,

                    1: it.configAttribute,

                    2: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
