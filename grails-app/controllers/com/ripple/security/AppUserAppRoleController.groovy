package com.ripple.security

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class AppUserAppRoleController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [appUserAppRoleInstanceList: AppUserAppRole.list(params), appUserAppRoleInstanceTotal: AppUserAppRole.count()]
}

def create() {
    [appUserAppRoleInstance: new AppUserAppRole(params)]
}

def save() {
    def appUserAppRoleInstance = new AppUserAppRole(params)
    if (!appUserAppRoleInstance.save(flush: true)) {
        render(view: "create", model: [appUserAppRoleInstance: appUserAppRoleInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), appUserAppRoleInstance.id])
    redirect(action: "show", id: appUserAppRoleInstance.id)
}

def show() {
    def appUserAppRoleInstance = AppUserAppRole.get(params.id)
    if (!appUserAppRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), params.id])
        redirect(action: "list")
        return
    }

    [appUserAppRoleInstance: appUserAppRoleInstance]
}

def edit() {
    def appUserAppRoleInstance = AppUserAppRole.get(params.id)
    if (!appUserAppRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), params.id])
        redirect(action: "list")
        return
    }

    [appUserAppRoleInstance: appUserAppRoleInstance]
}

def update() {
    def appUserAppRoleInstance = AppUserAppRole.get(params.id)
    if (!appUserAppRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (appUserAppRoleInstance.version > version) {
                appUserAppRoleInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'appUserAppRole.label', default: 'AppUserAppRole')] as Object[],
                        "Another user has updated this AppUserAppRole while you were editing")
            render(view: "edit", model: [appUserAppRoleInstance: appUserAppRoleInstance])
            return
        }
    }

    appUserAppRoleInstance.properties = params

    if (!appUserAppRoleInstance.save(flush: true)) {
        render(view: "edit", model: [appUserAppRoleInstance: appUserAppRoleInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), appUserAppRoleInstance.id])
    redirect(action: "show", id: appUserAppRoleInstance.id)
}

def delete() {
    def appUserAppRoleInstance = AppUserAppRole.get(params.id)
    if (!appUserAppRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), params.id])
        redirect(action: "list")
        return
    }

    try {
        appUserAppRoleInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'appUserAppRole.label', default: 'AppUserAppRole'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = AppUserAppRole.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('appRole',"%${sSearch}%")
                        
                        ilike('appUser',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = AppUserAppRole.count();
    def totalDisplayRecords = AppUserAppRole.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('appRole',"%${sSearch}%")
                        
                        ilike('appUser',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.appRole,
                        
                        1:it.appUser,
                        
                        2:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
    def assignRoles(){

    }


    def assignRoleJSON(){
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho)+1

        def listing = AppUser.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int, order: sortOrder, sort:'username')

        def totalRecords = AppUser.count();
        def totalDisplayRecords = AppUser.count();

        def jsonList = [];
        def newMap = [:];

        listing.each {
            def user = it
            newMap.put(0, user.username);
            int i = 0
            AppRole.list().each {
                newMap.put((i+1), g.render(template: 'roleCheckbox', model: [userInstance: user, roleInstance: it]));
                i++
            }
            newMap.put("DT_RowId", user.id)
            jsonList += newMap;
            newMap = [:];
        }

        def data = [
                sEcho:sEcho,
                iTotalRecords:totalRecords,
                iTotalDisplayRecords:totalDisplayRecords,
                aaData:jsonList];
        render data as JSON;

    }

    def updateRoles(){
        def usersInstance = getUsers(params);
        usersInstance.each {
            def user= it
            try{
                AppUserAppRole.removeAll(user);
            }catch (Exception e){
                println(e);
            }
            if (params[user.username] instanceof String){
                try{
                    AppUserAppRole.create(user, AppRole.findByAuthority(params[user.username]), true);
                }catch (Exception e){
                    println(e);
                }
            }else{
                params[user.username].each{
                    try{
                        AppUserAppRole.create(user, AppRole.findByAuthority(it), true);
                    }catch (Exception e){
                        println(e);
                    }
                }
            }
        }
        flash.message=message(code: 'new.update.message', default: 'Record has been successfully updated.');
        redirect(action: 'assignRoles')
    }

    private Set<AppUser> getUsers(fparams){
        def users = []
        fparams?.each{
            if(!(it?.key in ['controller', 'datatable_length', 'action'])){
                users += AppUser.findByUsername(it?.key);
            }
        }
        return users as Set;
    }
}
