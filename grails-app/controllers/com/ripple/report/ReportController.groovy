package com.ripple.report

import org.codehaus.groovy.grails.plugins.jasper.JasperController
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class ReportController{

    def reportService

    def index = {
        def testModel = this.getProperties().containsKey('chainModel') ? chainModel : null
        JasperReportDef report = reportService.buildReportDefinition(params, request.getLocale(), testModel)

        generateResponse(report)
    }

    /**
     * Generate a html response.
     */
    def generateResponse = {reportDef ->
        if (!reportDef.fileFormat.inline && !reportDef.parameters._inline) {
            response.setHeader("Content-disposition", "attachment; filename="+(reportDef.parameters._name ?: reportDef.name) + "." + reportDef.fileFormat.extension);
            response.contentType = reportDef.fileFormat.mimeTyp
            response.characterEncoding = "UTF-8"
            response.outputStream << reportDef.contentStream.toByteArray()
        } else {
            render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8');
        }
    }

    def view() {
        render(view: 'report')
    }
}
