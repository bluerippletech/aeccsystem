package com.ripple.materialIssuance



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.ModalizerController
import com.ripple.master.item.Item
import com.ripple.procurement.Procurement
import com.ripple.procurement.ProcurementLineItem

class MaterialIssuanceLineItemController extends ModalizerController{

    //setter for Domain className for inherited class
    String getDomainClazz() {return 'MaterialIssuanceLineItem'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def unitOfMesurementService;

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [materialIssuanceLineItemInstanceList: MaterialIssuanceLineItem.list(params), materialIssuanceLineItemInstanceTotal: MaterialIssuanceLineItem.count()]
    }

    def create() {
        [materialIssuanceLineItemInstance: new MaterialIssuanceLineItem(params)]
    }

    def save() {
        def materialIssuanceLineItemInstance = new MaterialIssuanceLineItem(params)
        if (!materialIssuanceLineItemInstance.save(flush: true)) {
            render(view: "create", model: [materialIssuanceLineItemInstance: materialIssuanceLineItemInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), materialIssuanceLineItemInstance.id])
        redirect(action: "show", id: materialIssuanceLineItemInstance.id)
    }

    def show() {
        def materialIssuanceLineItemInstance = MaterialIssuanceLineItem.get(params.id)
        if (!materialIssuanceLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [materialIssuanceLineItemInstance: materialIssuanceLineItemInstance]
    }

    def edit() {
        def materialIssuanceLineItemInstance = MaterialIssuanceLineItem.get(params.id)
        if (!materialIssuanceLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [materialIssuanceLineItemInstance: materialIssuanceLineItemInstance]
    }

    def update() {
        def materialIssuanceLineItemInstance = MaterialIssuanceLineItem.get(params.id)
        if (!materialIssuanceLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (materialIssuanceLineItemInstance.version > version) {
                materialIssuanceLineItemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem')] as Object[],
                        "Another user has updated this MaterialIssuanceLineItem while you were editing")
                render(view: "edit", model: [materialIssuanceLineItemInstance: materialIssuanceLineItemInstance])
                return
            }
        }

        materialIssuanceLineItemInstance.properties = params

        if (!materialIssuanceLineItemInstance.save(flush: true)) {
            render(view: "edit", model: [materialIssuanceLineItemInstance: materialIssuanceLineItemInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), materialIssuanceLineItemInstance.id])
        redirect(action: "show", id: materialIssuanceLineItemInstance.id)
    }

    def delete() {
        def materialIssuanceLineItemInstance = MaterialIssuanceLineItem.get(params.id)
        if (!materialIssuanceLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), params.id])
            redirect(action: "list")
            return
        }

        try {
            materialIssuanceLineItemInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = MaterialIssuanceLineItem.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
//            order(sortColumn, sortOrder)
            if (params?.id){
                issuance{
                    eq('id', params?.id);
                }
            }
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('item', "%${sSearch}%")

                }
            }
        }
        def totalRecords = MaterialIssuanceLineItem.count();
        def totalDisplayRecords = MaterialIssuanceLineItem.createCriteria().count {
            if (params?.id){
                issuance{
                    eq('id', params?.id);
                }
            }
            if (sSearch) {
                or {

                    ilike('isDeleted', "%${sSearch}%")

                    ilike('createdDate', "%${sSearch}%")

                    ilike('editedDate', "%${sSearch}%")

                    ilike('editedBy', "%${sSearch}%")

                    ilike('createdBy', "%${sSearch}%")

                    ilike('item', "%${sSearch}%")

                }
            }
        }

        def jsonList = [];
        def lineItem = 0;
        jsonList = listing.collect {
            def itemInstance = it.item
            def baseItem = it?.issuance?.procurementSlip?.lineItems.find {it.item==itemInstance}
            lineItem++
            [

                    0: lineItem,

                    1: it?.item?.itemCode,

                    2: it?.item?.itemName,

                    3: unitOfMesurementService.convertToBase(baseItem?.quantity,  baseItem?.unitOfMeasure, it?.item?.id),

                    4: it?.item?.baseLevel?.unitOfMeasure?.toString(),

                    5: unitOfMesurementService.convertToBase((baseItem?.quantity-baseItem?.servedQuantity), baseItem?.unitOfMeasure, it?.item?.id),

                    6: it?.item?.baseLevel?.unitOfMeasure?.toString(),

                    7: unitOfMesurementService.convertToBase(baseItem?.servedQuantity, baseItem?.unitOfMeasure, it?.item?.id),

                    8: it?.item?.baseLevel?.unitOfMeasure?.toString(),

                    9: unitOfMesurementService.convertToLevel(baseItem?.servedQuantity, it.unitOfMeasure, baseItem?.unitOfMeasure, it?.item?.id),

                    10: it.unitOfMeasure.toString(),

                    11: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def itemListing(){
        def procInstance = Procurement.get(MaterialIssuance.get(params?.parentId).procurementSlipId)
        def itemList = procInstance.lineItems*.item?.findAll {it.itemName.toLowerCase() =~ (params?.q?.toLowerCase())}
        def data = []
        def tempMap = [:]
        itemList.each {
            tempMap.put('id',it.id);
            tempMap.put('name',it.itemName);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }

    def itemListJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch
        def listing = Procurement.get(MaterialIssuance.get(params?.id).procurementSlipId).lineItems

        if (sSearch)
            listing = listing*.item.findAll {it.itemName.toLowerCase() =~ (sSearch?.toLowerCase())}
        else
            listing = listing*.item;

        def totalRecords = Procurement.get(MaterialIssuance.get(params?.id).procurementSlipId).lineItems.size();

        def totalDisplayRecords = listing.size();
//        def listing = Item.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
//            order(sortColumn, sortOrder)
//            if (sSearch) {
//                or {
//
//                    ilike('itemCode', "%${sSearch}%")
//
//                    ilike('itemName', "%${sSearch}%")
//
//                    ilike('description', "%${sSearch}%")
//
//                    ilike('status', "%${sSearch}%")
//
//                    itemClass{
//
//                        ilike('name', "%${sSearch}%")
//
//                    }
//
//                    itemSubclass{
//
//                        ilike('name', "%${sSearch}%")
//
//                    }
//
//                }
//            }
//        }
//        def totalRecords = Item.count();
//        def totalDisplayRecords = Item.createCriteria().count {
//            if (sSearch) {
//                or {
//
//                    ilike('itemCode', "%${sSearch}%")
//
//                    ilike('itemName', "%${sSearch}%")
//
//                    ilike('description', "%${sSearch}%")
//
//                    ilike('status', "%${sSearch}%")
//
//                    itemClass{
//
//                        ilike('name', "%${sSearch}%")
//
//                    }
//
//                    itemSubclass{
//
//                        ilike('name', "%${sSearch}%")
//
//                    }
//
//                }
//            }
//
//        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.itemCode,

                    1: it.itemName,

                    2: it.description,

                    3: it.status,

                    4: it.itemClass?.toString(),

                    5: it.itemSubclass?.toString(),

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def getUnitMeasure(){
        def data = MaterialIssuanceLineItem.get(params?.id)?.unitOfMeasure?.id;
        render data
    }

    @Override
    def getInstance(){
        def className = getDomainClazz()
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)

        def propertyMap = [:]

        propertyMap.put('id', modelInstance?.id);
        modelInstance?.properties?.each {
            def perValue = it;
            if(perValue?.value?.hasProperty("id")){
                def instanceVal = perValue.value;
                def tempMap = [:];
                tempMap.put('id', instanceVal.id);
                instanceVal?.properties?.each {
                    if(!(it.key in ['beforeInsert', 'beforeDelete', 'documentNumbersService', 'afterInsert', 'afterDelete']))
                        tempMap.put(it.key, it.value)
                }
                propertyMap.put(perValue.key, tempMap);
                tempMap = [:];
            }else
                propertyMap.put(perValue.key, perValue.value)
        }
        render propertyMap as JSON;
    }

}
