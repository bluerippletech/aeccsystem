package com.ripple.materialIssuance



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.procurement.Procurement
import com.ripple.procurement.ProcurementLineItem
import org.joda.time.LocalDate
import javassist.NotFoundException
import com.ripple.master.item.LevelUnit

class MaterialIssuanceController {

    def springSecurityService, stockService

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [materialIssuanceInstanceList: MaterialIssuance.list(params), materialIssuanceInstanceTotal: MaterialIssuance.count()]
}

def create() {
    [materialIssuanceInstance: new MaterialIssuance(params)]
}

def save() {
    def materialIssuanceInstance = new MaterialIssuance(params)
    if (!materialIssuanceInstance.save(flush: true)) {
        render(view: "create", model: [materialIssuanceInstance: materialIssuanceInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), materialIssuanceInstance.id])
    redirect(action: "show", id: materialIssuanceInstance.id)
}

def show() {
    def materialIssuanceInstance = MaterialIssuance.get(params.id)
    if (!materialIssuanceInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), params.id])
        redirect(action: "list")
        return
    }

    [materialIssuanceInstance: materialIssuanceInstance, procurementInstance: materialIssuanceInstance.procurementSlip]
}

def edit() {
    def materialIssuanceInstance = MaterialIssuance.get(params.id)
    if (!materialIssuanceInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), params.id])
        redirect(action: "list")
        return
    }

    [materialIssuanceInstance: materialIssuanceInstance]
}

def update() {
    def materialIssuanceInstance = MaterialIssuance.get(params.id)
    if (!materialIssuanceInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (materialIssuanceInstance.version > version) {
                materialIssuanceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'materialIssuance.label', default: 'MaterialIssuance')] as Object[],
                        "Another user has updated this MaterialIssuance while you were editing")
            render(view: "edit", model: [materialIssuanceInstance: materialIssuanceInstance])
            return
        }
    }

    materialIssuanceInstance.properties = params

    if (!materialIssuanceInstance.save(flush: true)) {
        render(view: "edit", model: [materialIssuanceInstance: materialIssuanceInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), materialIssuanceInstance.id])
    redirect(action: "show", id: materialIssuanceInstance.id)
}

def delete() {
    def materialIssuanceInstance = MaterialIssuance.get(params.id)
    if (!materialIssuanceInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), params.id])
        redirect(action: "list")
        return
    }

    try {
        materialIssuanceInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = MaterialIssuance.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('misNumber',"%${sSearch}%")

                        procurementSlip{
                            project{
                                ilike('projectName',"%${sSearch}%")
                            }
                        }

//                        ilike('dateStarted',"%${sSearch}%")

                        ilike('status',"%${sSearch}%")

//                        ilike('dueDate',"%${sSearch}%")

                        procurementSlip{
                            requestor{
                                ilike('firstName',"%${sSearch}%")
                            }
                        }
                        procurementSlip{
                            requestor{
                                ilike('middleInitials',"%${sSearch}%")
                            }
                        }
                        procurementSlip{
                            requestor{
                                ilike('lastName',"%${sSearch}%")
                            }
                        }
                        
            }
        }
    }
    def totalRecords = MaterialIssuance.count();
    def totalDisplayRecords = MaterialIssuance.createCriteria().count{
        if (sSearch){
            or{
                        ilike('misNumber',"%${sSearch}%")

                        procurementSlip{
                            project{
                                ilike('projectName',"%${sSearch}%")
                            }
                        }

//                        ilike('dateStarted',"%${sSearch}%")

                        ilike('status',"%${sSearch}%")

//                        ilike('dueDate',"%${sSearch}%")

                        procurementSlip{
                            requestor{
                                ilike('firstName',"%${sSearch}%")
                            }
                        }
                        procurementSlip{
                            requestor{
                                ilike('middleInitials',"%${sSearch}%")
                            }
                        }
                        procurementSlip{
                            requestor{
                                ilike('lastName',"%${sSearch}%")
                            }
                        }
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.misNumber,
                        
                        1:it.procurementSlip.project.toString(),
                        
                        2:it.dateStarted,
                        
                        3:it.status,
                        
                        4:it.dueDate,
                        
                        5:it.procurementSlip.requestor.toString(),
                        
                        6:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
    def newFromProcurement(){
        def newParams = params;
        def procurementInstance = Procurement.get(params['procurement.id'])
        def materialIssuanceInstance = new MaterialIssuance(procurementSlip:procurementInstance, createdBy:springSecurityService.getCurrentUser().username, dateStarted: new LocalDate(), status: 'Draft')
        def selectedItems = ProcurementLineItem.getAll(params.list("selectedItemLine"))

        if (!materialIssuanceInstance.save(flush: true)) {
            render(view: "create", model: [materialIssuanceInstance: materialIssuanceInstance])
            return
        }

        selectedItems.each {
//            it.servedQuantity = it.servedQuantity+(params[it.id] as Long)
            if(it.item){
                def materialLineItemInstance = new MaterialIssuanceLineItem(item: it.item, unitOfMeasure: LevelUnit.get(newParams.unitOfMeasure?.get(it?.id)), quantity: params[it.id])
                materialIssuanceInstance.addToLineItems(materialLineItemInstance);
            }
//            if (!it.save(flush: true)){
//                throw new Exception('Failed to save servedQuantity');
//            }
        }

        if (!materialIssuanceInstance.save(flush: true)) {
            render(view: "create", model: [materialIssuanceInstance: materialIssuanceInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), materialIssuanceInstance.id])
        redirect(action: "show", id: materialIssuanceInstance.id)
    }

    def formStatus(){
        def materialIssuanceInstance = MaterialIssuance.get(params.id)

        if (!materialIssuanceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (materialIssuanceInstance.version > version) {
                materialIssuanceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'materialIssuance.label', default: 'MaterialIssuance')] as Object[],
                        "Another user has updated this MaterialIssuance while you were editing")
                render(view: "edit", model: [materialIssuanceInstance: materialIssuanceInstance])
                return
            }
        }

        def newStatus = params._action_formStatus

        if (newStatus){
            switch (newStatus){
                case 'For Approval':
                    newStatus = materialIssuanceInstance.ForApproval
                    break

                case 'Cancel':
                    newStatus = materialIssuanceInstance.Cancelled
                    break

                case 'Approve':
                    newStatus = materialIssuanceInstance.Approved
                    break

                case 'Post':
                    newStatus = materialIssuanceInstance.Posted
                    break

                default:
                    newStatus = 'Draft'
            }
        }

        materialIssuanceInstance.status = newStatus

        if (!materialIssuanceInstance.save(flush: true)) {
            render(view: "edit", model: [materialIssuanceInstance: materialIssuanceInstance])
            return
        }

        materialIssuanceInstance.lineItems.each {
            def lineItem = it;
            try{
                if(materialIssuanceInstance.status == "For Approval")
                    stockService.materialIssuancePerItem(lineItem, "OUT", "MIS")
                else if (materialIssuanceInstance.status == "Posted")
                    stockService.materialIssuancePerItem(lineItem, "OUT", "MIS")
            }catch (Exception e){
                println e.getLocalizedMessage();
            }
        }


        flash.message = message(code: 'default.updated.message', args: [message(code: 'materialIssuance.label', default: 'MaterialIssuance'), materialIssuanceInstance.id])
        redirect(action: "show", id: materialIssuanceInstance.id)
    }
}
