package com.ripple.project



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class ExpenseTypeController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [expenseTypeInstanceList: ExpenseType.list(params), expenseTypeInstanceTotal: ExpenseType.count()]
}

def create() {
    [expenseTypeInstance: new ExpenseType(params)]
}

def save() {
    def expenseTypeInstance = new ExpenseType(params)
    if (!expenseTypeInstance.save(flush: true)) {
        render(view: "create", model: [expenseTypeInstance: expenseTypeInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), expenseTypeInstance.id])
    redirect(action: "show", id: expenseTypeInstance.id)
}

def show() {
    def expenseTypeInstance = ExpenseType.get(params.id)
    if (!expenseTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), params.id])
        redirect(action: "list")
        return
    }

    [expenseTypeInstance: expenseTypeInstance]
}

def edit() {
    def expenseTypeInstance = ExpenseType.get(params.id)
    if (!expenseTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), params.id])
        redirect(action: "list")
        return
    }

    [expenseTypeInstance: expenseTypeInstance]
}

def update() {
    def expenseTypeInstance = ExpenseType.get(params.id)
    if (!expenseTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (expenseTypeInstance.version > version) {
                expenseTypeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'expenseType.label', default: 'ExpenseType')] as Object[],
                        "Another user has updated this ExpenseType while you were editing")
            render(view: "edit", model: [expenseTypeInstance: expenseTypeInstance])
            return
        }
    }

    expenseTypeInstance.properties = params

    if (!expenseTypeInstance.save(flush: true)) {
        render(view: "edit", model: [expenseTypeInstance: expenseTypeInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), expenseTypeInstance.id])
    redirect(action: "show", id: expenseTypeInstance.id)
}

def delete() {
    def expenseTypeInstance = ExpenseType.get(params.id)
    if (!expenseTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), params.id])
        redirect(action: "list")
        return
    }

    try {
        expenseTypeInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'expenseType.label', default: 'ExpenseType'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = ExpenseType.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('code',"%${sSearch}%")
                        
                        ilike('expenseCategory',"%${sSearch}%")
                        
                        ilike('expenseType',"%${sSearch}%")
                        
                        ilike('units',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = ExpenseType.count();
    def totalDisplayRecords = ExpenseType.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('code',"%${sSearch}%")
                        
                        ilike('expenseCategory',"%${sSearch}%")
                        
                        ilike('expenseType',"%${sSearch}%")
                        
                        ilike('units',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.code,
                        
                        1:it.expenseCategory,
                        
                        2:it.expenseType,
                        
                        3:it.units,
                        
                        4:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
    def categorized(){
        def expenseType = ExpenseType.findAllByExpenseCategory(params?.category);
        def data = []
        def tempMap = [:]
        expenseType.each {
            tempMap.put('optionKey',it.id);
            tempMap.put('optionValue',it.expenseType);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }
}
