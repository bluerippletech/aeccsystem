package com.ripple.project



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.Assignment
import com.ripple.ModalizerController
import com.ripple.util.Formatter as FM

class CollectionController extends ModalizerController {

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'Collection'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [collectionInstanceList: Collection.list(params), collectionInstanceTotal: Collection.count()]
    }

    def create() {
        [collectionInstance: new Collection(params)]
    }

    def save() {
        def collectionInstance = new Collection(params)
        if (!collectionInstance.save(flush: true)) {
            render(view: "create", model: [collectionInstance: collectionInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'collection.label', default: 'Collection'), collectionInstance.id])
        redirect(action: "show", id: collectionInstance.id)
    }

    def show() {
        def collectionInstance = Collection.get(params.id)
        if (!collectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'collection.label', default: 'Collection'), params.id])
            redirect(action: "list")
            return
        }

        [collectionInstance: collectionInstance]
    }

    def edit() {
        def collectionInstance = Collection.get(params.id)
        if (!collectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'collection.label', default: 'Collection'), params.id])
            redirect(action: "list")
            return
        }

        [collectionInstance: collectionInstance]
    }

    def update() {
        def collectionInstance = Collection.get(params.id)
        if (!collectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'collection.label', default: 'Collection'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (collectionInstance.version > version) {
                collectionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'collection.label', default: 'Collection')] as Object[],
                        "Another user has updated this Collection while you were editing")
                render(view: "edit", model: [collectionInstance: collectionInstance])
                return
            }
        }

        collectionInstance.properties = params

        if (!collectionInstance.save(flush: true)) {
            render(view: "edit", model: [collectionInstance: collectionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'collection.label', default: 'Collection'), collectionInstance.id])
        redirect(action: "show", id: collectionInstance.id)
    }

    def delete() {
        def collectionInstance = Collection.get(params.id)
        if (!collectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'collection.label', default: 'Collection'), params.id])
            redirect(action: "list")
            return
        }

        try {
            collectionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'collection.label', default: 'Collection'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'collection.label', default: 'Collection'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns?.tokenize(",");
        def sortColumn = columns?.get(Integer.parseInt(params.iSortCol_0?:0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Collection.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if(params?.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
            if (sSearch) {
                or {

                    sqlRestriction("date_format(collection_date,'%m/%d/%Y') like '%${sSearch}%'")

                    sqlRestriction("convert(amount,char) like '%${sSearch}%'")

                    ilike('remarks', "%${sSearch}%")

                }
            }
        }
        def totalRecords = Collection.createCriteria().count {
            if(params?.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
        };
        def totalDisplayRecords = Collection.createCriteria().count {
            if(params?.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
            if (sSearch) {
                or {

                    sqlRestriction("date_format(collection_date,'%m/%d/%Y') like '%${sSearch}%'")

                    sqlRestriction("convert(amount,char) like '%${sSearch}%'")

                    ilike('remarks', "%${sSearch}%")

                }
            }
        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.collectionDate,

                    1: FM.formatCurrency(it.amount),

                    2: it.remarks,

                    3: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
