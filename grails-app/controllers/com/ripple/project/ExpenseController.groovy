package com.ripple.project



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.Assignment
import com.ripple.ModalizerController
import com.ripple.util.Formatter as FM

class ExpenseController extends ModalizerController{

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'Expense'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [expenseInstanceList: Expense.list(params), expenseInstanceTotal: Expense.count()]
    }

    def create() {
        [expenseInstance: new Expense(params)]
    }

    def save() {
        def expenseInstance = new Expense(params)
        if (!expenseInstance.save(flush: true)) {
            render(view: "create", model: [expenseInstance: expenseInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'expense.label', default: 'Expense'), expenseInstance.id])
        redirect(action: "show", id: expenseInstance.id)
    }

    def show() {
        def expenseInstance = Expense.get(params.id)
        if (!expenseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'expense.label', default: 'Expense'), params.id])
            redirect(action: "list")
            return
        }

        [expenseInstance: expenseInstance]
    }

    def edit() {
        def expenseInstance = Expense.get(params.id)
        if (!expenseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'expense.label', default: 'Expense'), params.id])
            redirect(action: "list")
            return
        }

        [expenseInstance: expenseInstance]
    }

    def update() {
        def expenseInstance = Expense.get(params.id)
        if (!expenseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'expense.label', default: 'Expense'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (expenseInstance.version > version) {
                    expenseInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'expense.label', default: 'Expense')] as Object[],
                            "Another user has updated this Expense while you were editing")
                render(view: "edit", model: [expenseInstance: expenseInstance])
                return
            }
        }

        expenseInstance.properties = params

        if (!expenseInstance.save(flush: true)) {
            render(view: "edit", model: [expenseInstance: expenseInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'expense.label', default: 'Expense'), expenseInstance.id])
        redirect(action: "show", id: expenseInstance.id)
    }

    def delete() {
        def expenseInstance = Expense.get(params.id)
        if (!expenseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'expense.label', default: 'Expense'), params.id])
            redirect(action: "list")
            return
        }

        try {
            expenseInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'expense.label', default: 'Expense'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'expense.label', default: 'Expense'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON(){
        println params;
        def columns = params.sColumns?.tokenize(",");
        def sortColumn = columns?.get(Integer.parseInt(params.iSortCol_0?:0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho)+1
        def sSearch = params.sSearch

        def listing = Expense.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
            order(sortColumn,sortOrder)
            if(params?.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
            if (sSearch){
                or{

                    type{
                        or{
                            ilike('expenseType',"%${sSearch}%")

                            sqlRestriction("date_format(expense_date,'%m/%d/%Y') like '%${sSearch}%'")

                        }
                    }

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(unit_price,char) like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = Expense.createCriteria().count {
            if(params?.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
        };
        def totalDisplayRecords = Expense.createCriteria().count{
            if(params?.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
            if (sSearch){
                or{

                    type{
                        or{
                            ilike('expenseType',"%${sSearch}%")

                            sqlRestriction("date_format(expense_date,'%m/%d/%Y') like '%${sSearch}%'")

                        }
                    }

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(unit_price,char) like '%${sSearch}%'")

                }
            }
        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                            0:it.type.expenseType,

                            1:it.expenseDate,

                            2:FM.formatNumber("#,##0.00", it.quantity),

                            3:FM.formatCurrency(it.unitPrice),

                            4:FM.formatCurrency(it.getAmount()),

                            5:'',
                            "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho:sEcho,
                iTotalRecords:totalRecords,
                iTotalDisplayRecords:totalDisplayRecords,
                aaData:jsonList];
        render data as JSON;
    }
}
