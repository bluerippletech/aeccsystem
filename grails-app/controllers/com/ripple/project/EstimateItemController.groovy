package com.ripple.project



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.Assignment
import com.ripple.ModalizerController
import com.ripple.util.Formatter as FM

class EstimateItemController extends ModalizerController{

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'EstimateItem'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [estimateItemInstanceList: EstimateItem.list(params), estimateItemInstanceTotal: EstimateItem.count()]
    }

    def create() {
        [estimateItemInstance: new EstimateItem(params)]
    }

    def save() {
        def estimateItemInstance = new EstimateItem(params)
        if (!estimateItemInstance.save(flush: true)) {
            render(view: "create", model: [estimateItemInstance: estimateItemInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), estimateItemInstance.id])
        redirect(action: "show", id: estimateItemInstance.id)
    }

    def show() {
        def estimateItemInstance = EstimateItem.get(params.id)
        if (!estimateItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), params.id])
            redirect(action: "list")
            return
        }

        [estimateItemInstance: estimateItemInstance]
    }

    def edit() {
        def estimateItemInstance = EstimateItem.get(params.id)
        if (!estimateItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), params.id])
            redirect(action: "list")
            return
        }

        [estimateItemInstance: estimateItemInstance]
    }

    def update() {
        def estimateItemInstance = EstimateItem.get(params.id)
        if (!estimateItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (estimateItemInstance.version > version) {
                    estimateItemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'estimateItem.label', default: 'EstimateItem')] as Object[],
                            "Another user has updated this EstimateItem while you were editing")
                render(view: "edit", model: [estimateItemInstance: estimateItemInstance])
                return
            }
        }

        estimateItemInstance.properties = params

        if (!estimateItemInstance.save(flush: true)) {
            render(view: "edit", model: [estimateItemInstance: estimateItemInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), estimateItemInstance.id])
        redirect(action: "show", id: estimateItemInstance.id)
    }

    def delete() {
        def estimateItemInstance = EstimateItem.get(params.id)
        if (!estimateItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), params.id])
            redirect(action: "list")
            return
        }

        try {
            estimateItemInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'estimateItem.label', default: 'EstimateItem'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON(){
        def columns = params.sColumns?.tokenize(",");
        def sortColumn = columns?.get(Integer.parseInt(params?.iSortCol_0?:0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho)+1
        def sSearch = params.sSearch

        def listing = EstimateItem.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
            order(sortColumn,sortOrder)
            if(params.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
            if (sSearch){
                or{
                    type{
                        or{
                            ilike('expenseCategory',"%${sSearch}%")

                            ilike('expenseType',"%${sSearch}%")
                        }
                    }

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(unit_price,char) like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = EstimateItem.createCriteria().count {
            if(params?.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
        };
        def totalDisplayRecords = EstimateItem.createCriteria().count{
            if(params.id){
                project{
                    eq('id', params?.id as Long)
                }
            }
            if (sSearch){
                or{
                    type{
                        or{
                            ilike('expenseCategory',"%${sSearch}%")

                            ilike('expenseType',"%${sSearch}%")
                        }
                    }

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(unit_price,char) like '%${sSearch}%'")

                }

            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                            0:it?.type?.expenseCategory,

                            1:it?.type?.expenseType,

                            2:FM?.formatNumber("#,##0.00", it?.quantity),

                            3:FM?.formatCurrency(it?.unitPrice),

                            4:FM?.formatCurrency(it?.getAmount()),

                            5:'',
                            "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho:sEcho,
                iTotalRecords:totalRecords,
                iTotalDisplayRecords:totalDisplayRecords,
                aaData:jsonList];
        render data as JSON;
    }
}
