package com.ripple.entry



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.Assignment
import com.ripple.master.AssignmentLocation
import com.ripple.util.Formatter as FM

class PayrollEntryController {

    def payrollEntryService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {

        def project = Assignment.get(params?.assignment?.id)
        def location = AssignmentLocation.get(params?.assignmentLocation?.id)
        if (params?.payPeriod?.id){
            params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
            def pp = PayPeriod.get(params?.payPeriod?.id)
            def c = PayrollEntry.createCriteria()
            def aList = c {
                employee {
                    currentAssignment{
                        if (params?.assignment?.id){
                            and {
                                eq('project.id', Long.parseLong(params?.assignment?.id))
                                if (params?.assignmentLocation?.id){
                                    eq('workLocation.id', Long.parseLong(params?.assignmentLocation?.id))
                                }

                            }
                        }
                    }
                    order('lastName')
                }
                eq('payPeriod.id',pp.id)
            }
            [payrollEntryInstanceList: aList, payrollEntryInstanceTotal: aList.size(), payPeriod:pp, project:project, location:location ]
        }else{
            render(view:'filter')
        }

//        params.max = Math.min(params.max ? params.int('max') : 10, 100)
//        [payrollEntryInstanceList: PayrollEntry.list(params), payrollEntryInstanceTotal: PayrollEntry.count()]
    }

    def create() {
        [payrollEntryInstance: new PayrollEntry(params)]
    }

    def save() {
        def payrollEntryInstance = new PayrollEntry(params)
        if (!payrollEntryInstance.save(flush: true)) {
            render(view: "create", model: [payrollEntryInstance: payrollEntryInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), payrollEntryInstance.id])
        redirect(action: "show", id: payrollEntryInstance.id)
    }

    def show() {
        def payrollEntryInstance = PayrollEntry.get(params.id)
        if (!payrollEntryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), params.id])
            redirect(action: "list")
            return
        }

        [payrollEntryInstance: payrollEntryInstance]
    }

    def edit() {
        def payrollEntryInstance = PayrollEntry.get(params.id)
        if (!payrollEntryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), params.id])
            redirect(action: "list")
            return
        }

        [payrollEntryInstance: payrollEntryInstance]
    }

    def update() {
        def payrollEntryInstance = PayrollEntry.get(params.id)
        if (!payrollEntryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (payrollEntryInstance.version > version) {
                payrollEntryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'payrollEntry.label', default: 'PayrollEntry')] as Object[],
                        "Another user has updated this PayrollEntry while you were editing")
                render(view: "edit", model: [payrollEntryInstance: payrollEntryInstance])
                return
            }
        }

        payrollEntryInstance.properties = params
        payrollEntryService.recompute(payrollEntryInstance)
        if (!payrollEntryInstance.save(flush: true)) {
            render(view: "edit", model: [payrollEntryInstance: payrollEntryInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), payrollEntryInstance.id])
        redirect(action: "show", id: payrollEntryInstance.id)
    }

    def delete() {
        def payrollEntryInstance = PayrollEntry.get(params.id)
        if (!payrollEntryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), params.id])
            redirect(action: "list")
            return
        }

        try {
            payrollEntryInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'payrollEntry.label', default: 'PayrollEntry'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {

        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = PayrollEntry.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (params?.payPeriod?.id){
                if(params?.assignment?.id){
                    employee {
                        currentAssignment{
                            if (params?.assignment?.id){
                                and {
                                    eq('project.id', Long.parseLong(params?.assignment?.id))
                                    if (params?.assignmentLocation?.id){
                                        eq('workLocation.id', Long.parseLong(params?.assignmentLocation?.id))
                                    }

                                }
                            }
                        }
                        order('lastName')
                    }
                }
                eq('payPeriod.id', params?.payPeriod?.id as Long)
            }
            if (sSearch) {
                or {
                    employee{
                        or{
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }

                    sqlRestriction("convert(gross_pay,char) like '%${sSearch}%'")

//                    sqlRestriction("convert(sss_contribution,char) like '%${sSearch}%'")

//                    sqlRestriction("convert(phil_health_contribution,char) like '%${sSearch}%'")

//                    sqlRestriction("convert(pag_ibig_contribution,char) like '%${sSearch}%'")

                    sqlRestriction("convert(cola,char) like '%${sSearch}%'")

                    sqlRestriction("convert(withholding_tax,char) like '%${sSearch}%'")

                    sqlRestriction("convert(net_pay,char) like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = PayrollEntry.count();
        def totalDisplayRecords = PayrollEntry.createCriteria().count {
            if (params?.payPeriod?.id){
                if(params?.assignment?.id){
                    employee {
                        currentAssignment{
                            if (params?.assignment?.id){
                                and {
                                    eq('project.id', Long.parseLong(params?.assignment?.id))
                                    if (params?.assignmentLocation?.id){
                                        eq('workLocation.id', Long.parseLong(params?.assignmentLocation?.id))
                                    }

                                }
                            }
                        }
                        order('lastName')
                    }
                }
                eq('payPeriod.id', params?.payPeriod?.id as Long)
            }
            if (sSearch) {
                or {

                    employee{
                        or{
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }

                    sqlRestriction("convert(gross_pay,char) like '%${sSearch}%'")

//                    sqlRestriction("convert(sss_contribution,char) like '%${sSearch}%'")
//
//                    sqlRestriction("convert(phil_health_contribution,char) like '%${sSearch}%'")
//
//                    sqlRestriction("convert(pag_ibig_contribution,char) like '%${sSearch}%'")

                    sqlRestriction("convert(cola,char) like '%${sSearch}%'")

                    sqlRestriction("convert(withholding_tax,char) like '%${sSearch}%'")

                    sqlRestriction("convert(net_pay,char) like '%${sSearch}%'")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.employee.toString(),

                    1: FM.formatCurrency(it.grossPay),

                    2: FM.formatCurrency(it.sssContribution),

                    3: FM.formatCurrency(it.philHealthContribution),

                    4: FM.formatCurrency(it.pagIbigContribution),

                    5: FM.formatCurrency(it.cola),

                    6: FM.formatCurrency(it.withholdingTax),

                    7: FM.formatCurrency(it.netPay),

                    8: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def filter(){

    }

    def getAssignmentLocation() {
        def assignment = Assignment.get(params?.assignment?.id)
        List projectLocations
        if (assignment){
            projectLocations = AssignmentLocation.findAllByAssignment(assignment)
        }
        render(template:"projectLocations",model:['projectLocations':projectLocations])
    }
}
