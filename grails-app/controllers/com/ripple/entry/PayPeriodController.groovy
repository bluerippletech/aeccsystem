package com.ripple.entry



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.util.Formatter as FM

class PayPeriodController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def payPeriodService

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [payPeriodInstanceList: PayPeriod.list(params), payPeriodInstanceTotal: PayPeriod.count()]
    }

    def create() {
        [payPeriodInstance: new PayPeriod(params)]
    }

    def save() {
        def payPeriodInstance = new PayPeriod(params)
        if (!payPeriodInstance.save(flush: true)) {
            render(view: "create", model: [payPeriodInstance: payPeriodInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), payPeriodInstance.id])
        redirect(action: "show", id: payPeriodInstance.id)
    }

    def show() {
        def payPeriodInstance = PayPeriod.get(params.id)
        if (!payPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), params.id])
            redirect(action: "list")
            return
        }

        [payPeriodInstance: payPeriodInstance, payPeriodService:payPeriodService ]
    }

    def edit() {
        def payPeriodInstance = PayPeriod.get(params.id)
        if (!payPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), params.id])
            redirect(action: "list")
            return
        }

        [payPeriodInstance: payPeriodInstance]
    }

    def update() {
        def payPeriodInstance = PayPeriod.get(params.id)
        if (!payPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (payPeriodInstance.version > version) {
                payPeriodInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'payPeriod.label', default: 'PayPeriod')] as Object[],
                        "Another user has updated this PayPeriod while you were editing")
                render(view: "edit", model: [payPeriodInstance: payPeriodInstance])
                return
            }
        }

        payPeriodInstance.properties = params

        if (!payPeriodInstance.save(flush: true)) {
            render(view: "edit", model: [payPeriodInstance: payPeriodInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), payPeriodInstance.id])
        redirect(action: "show", id: payPeriodInstance.id)
    }

    def delete() {
        def payPeriodInstance = PayPeriod.get(params.id)
        if (!payPeriodInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), params.id])
            redirect(action: "list")
            return
        }

        try {
            payPeriodInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'payPeriod.label', default: 'PayPeriod'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = PayPeriod.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    switch (sSearch){
                        case 'true':
                            sqlRestriction("first_pay_period=1")
                            break
                        case 'false':
                            sqlRestriction("first_pay_period=0")
                            break
                    }

                    sqlRestriction("date_format(month_year,'%m/%d/%Y') like '%${sSearch}%'")

                }
            }
        }
        def totalRecords = PayPeriod.count();
        def totalDisplayRecords = PayPeriod.createCriteria().count {
            if (sSearch) {
                or {

                    switch (sSearch){
                        case 'true':
                            sqlRestriction("first_pay_period=1")
                            break
                        case 'false':
                            sqlRestriction("first_pay_period=0")
                            break
                    }

                    sqlRestriction("date_format(month_year,'%m/%d/%Y') like '%${sSearch}%'")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.firstPayPeriod,

                    1: FM?.formatLocalDate(it?.monthYear,"MM/dd/Y"),

                    2: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
