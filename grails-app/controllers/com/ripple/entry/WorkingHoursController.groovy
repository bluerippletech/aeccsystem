package com.ripple.entry



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.Rate
import com.ripple.master.Employee
import com.ripple.master.Assignment
import com.ripple.master.AssignmentLocation

class WorkingHoursController {

def employeeService, workingHoursService, holidayService, payrollEntryService;
static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [workingHoursInstanceList: WorkingHours.list(params), workingHoursInstanceTotal: WorkingHours.count()]
}

def create() {
    [workingHoursInstance: new WorkingHours(params)]
}

def save() {
    if (params?.id)
        return update(params)


    def employee = Employee.get(params?.employee?.id)
    def payPeriod = PayPeriod.get(params?.payPeriod?.id)
    def employeeList=employeeService.retreiveEmployeeList(params?.project?.id,params?.projectLocation?.id)

    def workingHoursInstance = new WorkingHours(params)
    if (!workingHoursInstance.save(flush: true)) {
//        render(view: "create", model: [workingHoursInstance: workingHoursInstance])
//        return
        response.status = response.SC_INTERNAL_SERVER_ERROR
        render workingHoursInstance.errors as JSON
    }

//    flash.message = message(code: 'default.created.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), workingHoursInstance.id])
//    redirect(action: "show", id: workingHoursInstance.id)
    flash.message = "WorkingHours ${workingHoursInstance.id} created"
    workingHoursInstance = WorkingHours.get(workingHoursInstance.id)
    payrollEntryService.calculatePay(workingHoursInstance)

    response.status=response.SC_OK
    render workingHoursInstance as JSON
}

def show() {
    def workingHoursInstance = WorkingHours.get(params.id)
    if (!workingHoursInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), params.id])
        redirect(action: "list")
        return
    }

    render(view: 'create',
            model: ['workingHoursInstance':workingHoursInstance,
                    'employee':workingHoursInstance.employee,
                    'payPeriod':workingHoursInstance.payPeriod]
    )
}

def edit() {
    def workingHoursInstance = WorkingHours.get(params.id)
    if (!workingHoursInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), params.id])
        redirect(action: "list")
        return
    }

    render(view: 'create',
    model: ['workingHoursInstance':workingHoursInstance,
            'employee':workingHoursInstance.employee,
            'payPeriod':workingHoursInstance.payPeriod]
    )
}

def update= {
    def workingHoursInstance = WorkingHours.get(params.id)

    def employee = Employee.get(params?.employee?.id)
    def payPeriod = PayPeriod.get(params?.payPeriod?.id)
    def employeeList=employeeService.retreiveEmployeeList(params?.project?.id,params?.projectLocation?.id)

    if (!workingHoursInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (workingHoursInstance.version > version) {
                workingHoursInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'workingHours.label', default: 'WorkingHours')] as Object[],
                        "Another user has updated this WorkingHours while you were editing")
            render(view: "edit", model: [workingHoursInstance: workingHoursInstance])
            return
        }
    }

    workingHoursInstance.properties = params

    if (!workingHoursInstance.save(flush: true)) {
        render(view: "edit", model: [workingHoursInstance: workingHoursInstance])
        return
    }

    flash.message = "WorkingHours ${workingHoursInstance.id} created"
    workingHoursInstance = WorkingHours.get(workingHoursInstance.id)
    payrollEntryService.calculatePay(workingHoursInstance)

    flash.message = message(code: 'default.updated.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), workingHoursInstance.id])
    redirect(action: "show", id: workingHoursInstance.id)
}

def delete() {
    def workingHoursInstance = WorkingHours.get(params.id)
    if (!workingHoursInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), params.id])
        redirect(action: "list")
        return
    }

    try {
        workingHoursInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'workingHours.label', default: 'WorkingHours'), params.id])
        redirect(action: "show", id: params.id)
    }
}

    def getAssignmentLocation() {
        def assignment = Assignment.get(params?.assignment?.id)
        List projectLocations
        if (assignment){
            projectLocations = AssignmentLocation.findAllByAssignment(assignment)
        }
        render(template:"projectLocations",model:['projectLocations':projectLocations])
    }

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = WorkingHours.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{

                employee{
                    or{
                        ilike('firstName', "%${sSearch}%")
                        ilike('lastName', "%${sSearch}%")
                        ilike('middleInitials', "%${sSearch}%")
                    }
                }

            }
        }
    }
    def totalRecords = WorkingHours.count();
    def totalDisplayRecords = WorkingHours.createCriteria().count{
        if (sSearch){
            or{

                employee{
                    or{
                        ilike('firstName', "%${sSearch}%")
                        ilike('lastName', "%${sSearch}%")
                        ilike('middleInitials', "%${sSearch}%")
                    }
                }

            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.employee.toString(),
                        
                        1:it.payPeriod.toString(),

                        
                        2:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}

    def renderForm(){
        def whi = new WorkingHours()
        whi.properties = params

        def employee
        if (params?.employee?.id)
            employee=Employee.get(params?.employee?.id)

        def payPeriod
        if (params?.payPeriod?.id){
            payPeriod = PayPeriod.get(params?.payPeriod?.id)
        }else
            throw new Exception("A Pay Period must be specified.")


        def employeeList=employeeService.retreiveEmployeeList(params?.assignment?.id,params?.assignmentLocation?.id)
//        employeeList = employeeList.sort {it.wholeName};
        def project = Assignment.get(params?.assignment?.id)
        def location = AssignmentLocation.get(params?.assignmentLocation?.id)

        def emp
        if (employee==null)
            emp = employeeList.getAt(0)
        else{
            if(params?.nav == 'next'){
                def tempEmp=employeeList.findIndexOf{it.id==employee.id};
                if ((tempEmp+1)>=employeeList.size())
                    emp = employeeList.getAt(0)
                else
                    emp = employeeList.getAt(tempEmp+1);
            }else if (params?.nav == 'prev'){
                def tempEmp=employeeList.findIndexOf{it.id==employee.id};
                if ((tempEmp-1)<0)
                    emp = employeeList.getAt(employeeList.size()-1)
                else
                    emp = employeeList.getAt(tempEmp-1);
            }else{
                emp=employeeList.find{it.id==employee.id}
            }
        }

        whi=WorkingHours.findByEmployeeAndPayPeriod(Employee.get(emp?.id),payPeriod)

        if (whi==null){
            whi = new WorkingHours()
            workingHoursService.setWorkingHoursDefaultValues(whi,payPeriod)
        }

        def payrollEntry = PayrollEntry.findByPayPeriodAndEmployee(payPeriod, Employee.get(emp?.id))

        response.status=response.SC_OK
        if (params?.nav){
            def renderMap = [:]
            def tempMap = [:]
            renderMap.put('workingHoursInstance', whi)
            tempMap.put('employee', emp)
            renderMap.put('employeeInstance', tempMap)
            tempMap = [:]
            tempMap.put('payPeriod', payPeriod)
            renderMap.put('payPeriodInstance', tempMap)
            tempMap = [:]
            if(payrollEntry){
                tempMap.put('payrollEntry', payrollEntry)
                renderMap.put('payrollEntryInstance', tempMap)
                tempMap = [:]
            }
            render renderMap as JSON;
        }else{
            g:render(template: 'fields',
            model: ['workingHoursInstance':whi,'employeeList':employeeList,
                    'payPeriod':payPeriod,'employee':emp,'payrollEntry':payrollEntry,
                    'project':project,'projectLocation':location,
                    'holidayService':holidayService]
            )
        }
    }
}
