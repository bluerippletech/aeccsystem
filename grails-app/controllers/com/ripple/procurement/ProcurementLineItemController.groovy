package com.ripple.procurement



import org.springframework.dao.DataIntegrityViolationException
import com.ripple.ModalizerController
import com.ripple.master.item.Item
import grails.converters.JSON
import org.codehaus.groovy.grails.commons.GrailsDomainClass

class ProcurementLineItemController extends ModalizerController {

    //setter for Domain className for inherited class
    String getDomainClazz() {return 'ProcurementLineItem'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def stockService, unitOfMesurementService

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [procurementLineItemInstanceList: ProcurementLineItem.list(params), procurementLineItemInstanceTotal: ProcurementLineItem.count()]
    }

    def create() {
        [procurementLineItemInstance: new ProcurementLineItem(params)]
    }

    def save() {
        def procurementLineItemInstance = new ProcurementLineItem(params)
        if (!procurementLineItemInstance.save(flush: true)) {
            render(view: "create", model: [procurementLineItemInstance: procurementLineItemInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), procurementLineItemInstance.id])
        redirect(action: "show", id: procurementLineItemInstance.id)
    }

    def show() {
        def procurementLineItemInstance = ProcurementLineItem.get(params.id)
        if (!procurementLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [procurementLineItemInstance: procurementLineItemInstance]
    }

    def edit() {
        def procurementLineItemInstance = ProcurementLineItem.get(params.id)
        if (!procurementLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [procurementLineItemInstance: procurementLineItemInstance]
    }

    def update() {
        def procurementLineItemInstance = ProcurementLineItem.get(params.id)
        if (!procurementLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (procurementLineItemInstance.version > version) {
                procurementLineItemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem')] as Object[],
                        "Another user has updated this ProcurementLineItem while you were editing")
                render(view: "edit", model: [procurementLineItemInstance: procurementLineItemInstance])
                return
            }
        }

        procurementLineItemInstance.properties = params

        if (!procurementLineItemInstance.save(flush: true)) {
            render(view: "edit", model: [procurementLineItemInstance: procurementLineItemInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), procurementLineItemInstance.id])
        redirect(action: "show", id: procurementLineItemInstance.id)
    }

    def delete() {
        def procurementLineItemInstance = ProcurementLineItem.get(params.id)
        if (!procurementLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), params.id])
            redirect(action: "list")
            return
        }

        try {
            procurementLineItemInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = ProcurementLineItem.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    item{
                        or{

                            ilike('itemCode', "%${sSearch}%")

                            ilike('itemName', "%${sSearch}%")
                        }
                    }

                    purpose{
                            ilike('name', "%${sSearch}%")
                    }

                }
            }
            and{eq('procurement', Procurement.get(params?.id))}
        }
        def totalRecords = ProcurementLineItem.count();
        def totalDisplayRecords = ProcurementLineItem.createCriteria().count {
            if (sSearch) {
                or {

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    item{
                         or{
                            ilike('itemCode', "%${sSearch}%")

                            ilike('itemName', "%${sSearch}%")
                         }
                    }

                    purpose{
                        ilike('name', "%${sSearch}%")
                    }

                }
            }
            and{eq('procurement', Procurement.get(params?.id))}
        }

        def jsonList = [];
        jsonList = listing.collect {
            [
                    0: '',

                    1: it.item?.itemCode,

                    2: it.item?.itemName,

                    3: it.quantity,

                    4: unitOfMesurementService.convertToLevel(stockService.stockCount(it?.item, it?.procurement?.warehouse), it?.unitOfMeasure, it?.item?.baseLevel, it?.item?.id),

                    5: it.unitOfMeasure?.unitOfMeasure,

                    6: it.purpose?.name,

                    7: '',

                    8: '',

                    9: "${select(from:[it.item.baseLevel?:null, it.item.secondLevel?:null, it.item.thirdLevel?:null], optionKey:'id', optionValue:'unitOfMeasure', name:'unitOfMeasure.'+it.id, style:'width: 90px', value:it.item?.baseLevel?.id)}",

                    "DT_RowId": it.id
            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def categorized(){
        def itemInstance = Item.get(params.category)
        def levelUnitList = [];
            if (itemInstance?.baseLevel)
                levelUnitList.add(itemInstance?.baseLevel)
            if (itemInstance?.secondLevel)
                levelUnitList.add(itemInstance?.secondLevel)
            if (itemInstance?.thirdLevel)
                levelUnitList.add(itemInstance?.thirdLevel)
        def data = []
        def tempMap = [:]
        levelUnitList.each {
            tempMap.put('optionKey',it?.id);
            tempMap.put('optionValue',it?.unitOfMeasure);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }

    def getUnitMeasure(){
        def data = ProcurementLineItem.get(params?.id)?.unitOfMeasure?.id;
        render data
    }

    @Override
    def getInstance(){
        def className = getDomainClazz()
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)

        def propertyMap = [:]

        propertyMap.put('id', modelInstance?.id);
        modelInstance?.properties?.each {
            def perValue = it;
            if(perValue?.value?.hasProperty("id")){
                def instanceVal = perValue.value;
                def tempMap = [:];
                tempMap.put('id', instanceVal.id);
                    instanceVal?.properties?.each {
                        if(!(it.key in ['beforeInsert', 'beforeDelete', 'documentNumbersService', 'afterInsert', 'afterDelete']))
                            tempMap.put(it.key, "${it.value}")
                    }
                propertyMap.put(perValue.key, tempMap);
                tempMap = [:];
            }else
                propertyMap.put(perValue.key, "${perValue.value}")
        }
        render propertyMap as JSON;
    }
}
