package com.ripple.procurement



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class MaterialIssuanceClassController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [materialIssuanceClassInstanceList: MaterialIssuanceClass.list(params), materialIssuanceClassInstanceTotal: MaterialIssuanceClass.count()]
}

def create() {
    [materialIssuanceClassInstance: new MaterialIssuanceClass(params)]
}

def save() {
    def materialIssuanceClassInstance = new MaterialIssuanceClass(params)
    if (!materialIssuanceClassInstance.save(flush: true)) {
        render(view: "create", model: [materialIssuanceClassInstance: materialIssuanceClassInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), materialIssuanceClassInstance.id])
    redirect(action: "show", id: materialIssuanceClassInstance.id)
}

def show() {
    def materialIssuanceClassInstance = MaterialIssuanceClass.get(params.id)
    if (!materialIssuanceClassInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), params.id])
        redirect(action: "list")
        return
    }

    [materialIssuanceClassInstance: materialIssuanceClassInstance]
}

def edit() {
    def materialIssuanceClassInstance = MaterialIssuanceClass.get(params.id)
    if (!materialIssuanceClassInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), params.id])
        redirect(action: "list")
        return
    }

    [materialIssuanceClassInstance: materialIssuanceClassInstance]
}

def update() {
    def materialIssuanceClassInstance = MaterialIssuanceClass.get(params.id)
    if (!materialIssuanceClassInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (materialIssuanceClassInstance.version > version) {
                materialIssuanceClassInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass')] as Object[],
                        "Another user has updated this MaterialIssuanceClass while you were editing")
            render(view: "edit", model: [materialIssuanceClassInstance: materialIssuanceClassInstance])
            return
        }
    }

    materialIssuanceClassInstance.properties = params

    if (!materialIssuanceClassInstance.save(flush: true)) {
        render(view: "edit", model: [materialIssuanceClassInstance: materialIssuanceClassInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), materialIssuanceClassInstance.id])
    redirect(action: "show", id: materialIssuanceClassInstance.id)
}

def delete() {
    def materialIssuanceClassInstance = MaterialIssuanceClass.get(params.id)
    if (!materialIssuanceClassInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), params.id])
        redirect(action: "list")
        return
    }

    try {
        materialIssuanceClassInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'materialIssuanceClass.label', default: 'MaterialIssuanceClass'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = MaterialIssuanceClass.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('name',"%${sSearch}%")
                        
                        ilike('description',"%${sSearch}%")
                        
                        ilike('status',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = MaterialIssuanceClass.count();
    def totalDisplayRecords = MaterialIssuanceClass.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('name',"%${sSearch}%")
                        
                        ilike('description',"%${sSearch}%")
                        
                        ilike('status',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.name,
                        
                        1:it.description,
                        
                        2:it.status,
                        
                        3:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
}
