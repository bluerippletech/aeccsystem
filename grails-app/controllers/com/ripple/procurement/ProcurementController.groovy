package com.ripple.procurement



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.master.item.ItemSubclass
import com.ripple.master.item.ItemClass
import com.ripple.master.Warehouse
import com.ripple.master.Assignment
import com.ripple.security.AppUser

class ProcurementController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [procurementInstanceList: Procurement.list(params), procurementInstanceTotal: Procurement.count()]
    }

    def create() {
        [procurementInstance: new Procurement(params)]
    }

    def save() {
        def procurementInstance = new Procurement(params)
        if (!procurementInstance.save(flush: true)) {
            render(view: "create", model: [procurementInstance: procurementInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'procurement.label', default: 'Procurement'), procurementInstance.id])
        redirect(action: "show", id: procurementInstance.id)
    }

    def show() {
        def procurementInstance = Procurement.get(params.id)
        if (!procurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "list")
            return
        }

        [procurementInstance: procurementInstance]
    }

    def edit() {
        def procurementInstance = Procurement.get(params.id)
        if (!procurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "list")
            return
        }

        sec.ifNotGranted(roles: 'ROLE_ADMIN') {
            if (procurementInstance?.status == 'Cancelled' || procurementInstance?.status == 'For Approval') {
                procurementInstance?.setStatus('Draft')
            }
        }

        [procurementInstance: procurementInstance]
    }

    def update() {
        def procurementInstance = Procurement.get(params.id)
        if (!procurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (procurementInstance.version > version) {
                procurementInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'procurement.label', default: 'Procurement')] as Object[],
                        "Another user has updated this Procurement while you were editing")
                render(view: "edit", model: [procurementInstance: procurementInstance])
                return
            }
        }

        if (params.status == 'Cancelled') {
            flash.message = message(code: 'form.cancelled.message', default: 'Form is cancelled')
            render(view: "show", model: [procurementInstance: procurementInstance])
            return
        }

        procurementInstance.properties = params

        if (!procurementInstance.save(flush: true)) {
            render(view: "edit", model: [procurementInstance: procurementInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'procurement.label', default: 'Procurement'), procurementInstance.id])
        redirect(action: "show", id: procurementInstance.id)
    }

    def delete() {
        def procurementInstance = Procurement.get(params.id)
        if (!procurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "list")
            return
        }

        try {
            procurementInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Procurement.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('procurementNumber', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                    sqlRestriction("date_format(date_created,'%m/%d/%Y') like '%${sSearch}%'")

                    sqlRestriction("date_format(date_closed,'%m/%d/%Y') like '%${sSearch}%'")

                    requestor {
                        or {
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }

                    project {
                        ilike('projectName', "%${sSearch}%")
                    }

                }
            }
        }
        def totalRecords = Procurement.count();
        def totalDisplayRecords = Procurement.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('procurementNumber', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                    sqlRestriction("date_format(date_created,'%m/%d/%Y') like '%${sSearch}%'")

                    sqlRestriction("date_format(date_closed,'%m/%d/%Y') like '%${sSearch}%'")

                    requestor {
                        or {
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }

                    project {
                        ilike('projectName', "%${sSearch}%")
                    }

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.procurementNumber,

                    1: it.project.toString(),

                    2: it.dateStarted,

                    3: it.status,

                    4: it.dateClosed,

                    5: it.requestor.toString(),

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def newFromExisting() {
        def procurement = Procurement.get(params?.id)
        if (!procurement) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "list")
            return
        }

        def procurementInstance = new Procurement(creator: procurement?.creator,
                requestor: procurement?.requestor,
                project: procurement?.project,
                warehouse: procurement?.warehouse,
                dateStarted: procurement?.dateStarted,
                dateClosed: procurement?.dateClosed)

        def lineItems = procurement.getLineItems()
        lineItems.each {
            def newLineItems = new ProcurementLineItem(
                    item: it?.item, purpose: it?.purpose, quantity: it?.quantity)
            procurementInstance.addToLineItems(newLineItems)
        }

        if (!procurementInstance.save(flush: true)) {
            render(view: "create", model: [procurementInstance: procurementInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'procurement.label', default: 'Procurement'), procurementInstance.id])
        redirect(action: "show", id: procurementInstance.id)
    }

    def getSubCat() {
        def projectWarehouseList = Warehouse.findAllByAssignment(Assignment.get(params?.id))
        def data = []
        def tempMap = [:]
        projectWarehouseList.each {
            tempMap.put('optionKey', it.id);
            tempMap.put('optionValue', it.name);
            data += tempMap;
            tempMap = [:];
        }
        render data as JSON
    }

    def formStatus() {
        def procurementInstance = Procurement.get(params.id)

        if (!procurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'procurement.label', default: 'Procurement'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (procurementInstance.version > version) {
                procurementInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'procurement.label', default: 'Procurement')] as Object[],
                        "Another user has updated this Procurement while you were editing")
                render(view: "edit", model: [procurementInstance: procurementInstance])
                return
            }
        }

        def newStatus = params._action_formStatus
        if (newStatus){
            switch (newStatus){
                case 'For Approval':
                    newStatus = 'For Approval'
                    break

                case 'Cancel':
                    newStatus = 'Cancelled'
                    break

                case 'Approve':
                    newStatus = 'Approved'
                    break

                case 'Disapprove':
                    newStatus = 'Disapproved'
                    break

                default:
                    newStatus = 'Draft'
            }
        }

        procurementInstance.status = newStatus

        if (!procurementInstance.save(flush: true)) {
            render(view: "edit", model: [procurementInstance: procurementInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'procurement.label', default: 'Procurement'), procurementInstance.id])
        redirect(action: "show", id: procurementInstance.id)

    }

    def newFromProcurementToPurchaseOrder(){
        def keys = params?.findAll { !(it?.key in ['controller', '_action_newFromProcurementToPurchaseOrder', 'datatable-procurement-item_length', 'action', 'unitOfMeasure']) }.collect { it.key }
        def newParams = params.subMap(keys)
        redirect(controller: 'purchaseOrder', action: 'newFromProcurement', params: newParams)
    }

    def newFromProcurementToMIS(){
        def keys = params?.findAll { !(it?.key in ['controller', '_action_newFromProcurementToMIS', 'datatable-procurement-item_length', 'action', 'unitOfMeasure']) }.collect { it.key }
        def newParams = params.subMap(keys)
        redirect(controller: 'materialIssuance', action: 'newFromProcurement', params: newParams)
    }
}
