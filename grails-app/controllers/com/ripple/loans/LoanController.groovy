package com.ripple.loans

import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.util.Formatter as FM

class LoanController {

    def loanService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [loanInstanceList: Loan.list(params), loanInstanceTotal: Loan.count()]
    }

    def create() {
        [loanInstance: new Loan(params)]
    }

    def save() {
        def loanInstance = new Loan(params)
        if (!loanInstance.save(flush: true)) {
            render(view: "create", model: [loanInstance: loanInstance])
            return
        }
        loanService.computeRemainingBalance(loanInstance)
        flash.message = message(code: 'default.created.message', args: [message(code: 'loan.label', default: 'Loan'), loanInstance.id])
        redirect(action: "show", id: loanInstance.id)
    }

    def show() {
        def loanInstance = Loan.get(params.id)
        if (!loanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loan.label', default: 'Loan'), params.id])
            redirect(action: "list")
            return
        }
        [loanInstance: loanInstance]
    }

    def edit() {
        def loanInstance = Loan.get(params.id)
        if (!loanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loan.label', default: 'Loan'), params.id])
            redirect(action: "list")
            return
        }

        [loanInstance: loanInstance]
    }

    def update() {
        def loanInstance = Loan.get(params.id)
        if (!loanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loan.label', default: 'Loan'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (loanInstance.version > version) {
                loanInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'loan.label', default: 'Loan')] as Object[],
                        "Another user has updated this Loan while you were editing")
                render(view: "edit", model: [loanInstance: loanInstance])
                return
            }
        }

        loanInstance.properties = params

        if (!loanInstance.save(flush: true)) {
            render(view: "edit", model: [loanInstance: loanInstance])
            return
        }
        loanService.computeRemainingBalance(loanInstance)
        flash.message = message(code: 'default.updated.message', args: [message(code: 'loan.label', default: 'Loan'), loanInstance.id])
        redirect(action: "show", id: loanInstance.id)
    }

    def delete() {
        def loanInstance = Loan.get(params.id)
        if (!loanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loan.label', default: 'Loan'), params.id])
            redirect(action: "list")
            return
        }

        try {
            loanInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'loan.label', default: 'Loan'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'loan.label', default: 'Loan'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Loan.createCriteria().listDistinct {
            order(sortColumn, sortOrder)
            firstResult(params.iDisplayStart as int)
            maxResults(params.iDisplayLength as int)
            if (sSearch) {
                or {
                    employee{
                        or{
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }


                    loanType{
                        ilike('name', "%${sSearch}%")
                    }

                    sqlRestriction("date_format(loan_date,'%m/%d/%Y') like '%${sSearch}%'")

                    sqlRestriction("convert(total_amount,char) like '%${sSearch}%'")

                    sqlRestriction("convert(monthly_amortization,char) like '%${sSearch}%'")

                    sqlRestriction("convert(remaining_balance,char) like '%${sSearch}%'")

                }
            }

        }
        println(listing)
        def totalRecords = Loan.count();
        def totalDisplayRecords = Loan.createCriteria().count {
            if (sSearch) {
                or {

                    employee{
                        or{
                            ilike('firstName', "%${sSearch}%")
                            ilike('lastName', "%${sSearch}%")
                            ilike('middleInitials', "%${sSearch}%")
                        }
                    }

                    loanType{
                        ilike('name', "%${sSearch}%")
                    }

                    sqlRestriction("date_format(loan_date,'%m/%d/%Y') like '%${sSearch}%'")

                    sqlRestriction("convert(total_amount,char) like '%${sSearch}%'")

                    sqlRestriction("convert(monthly_amortization,char) like '%${sSearch}%'")

                    sqlRestriction("convert(remaining_balance,char) like '%${sSearch}%'")


                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it?.employee?.lastName + ', ' + it?.employee?.firstName + ' ' + it?.employee?.middleInitials,

                    1: it?.loanType?.getName(),

                    2: FM?.formatLocalDate(it?.loanDate,"MM/dd/yyyy"),

                    3: FM?.formatCurrency(it?.totalAmount),

                    4: FM?.formatCurrency(it?.monthlyAmortization),

                    5: FM?.formatCurrency(it?.remainingBalance),

                    6: it?.payments?.size(),

                    7: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }


}
