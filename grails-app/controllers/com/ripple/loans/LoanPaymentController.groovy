package com.ripple.loans



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.entry.PayrollEntry

import com.ripple.util.Formatter
import com.ripple.ModalizerController

class LoanPaymentController extends ModalizerController{

    def loanService, payrollEntryService

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'LoanPayment'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [loanPaymentInstanceList: LoanPayment.list(params), loanPaymentInstanceTotal: LoanPayment.count()]
    }

    def create() {
        [loanPaymentInstance: new LoanPayment(params)]
    }

    def save() {
        def loan = Loan.get(params?.loan?.id)
        def employee = loan?.employee
        def loanList = Loan.findAllByEmployee(employee)
        def payrollEntries = PayrollEntry.findAllByEmployee(employee, [sort: 'payPeriod'])
        def loanPaymentInstance = new LoanPayment(params)
        if (!loanPaymentInstance.save(flush: true)) {
            render(view: "create", model: [loanPaymentInstance: loanPaymentInstance])
            return
        }

        loan.addToPayments(loanPaymentInstance)
        loanService.computeRemainingBalance(loan)
        if (loanPaymentInstance?.payrollEntry != null){
            payrollEntryService.recompute(loanPaymentInstance?.payrollEntry)
        }
        flash.message = message(code: 'default.created.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), loanPaymentInstance.id])
//        redirect(action: "show", id: loanPaymentInstance.id)
        redirect(controller: 'loan', action: "show", id: loan?.id)
    }

    def show() {
        def loanPaymentInstance = LoanPayment.get(params.id)
        if (!loanPaymentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), params.id])
            redirect(action: "list")
            return
        }

        [loanPaymentInstance: loanPaymentInstance]
    }

    def edit() {
        def loanPaymentInstance = LoanPayment.get(params.id)
        if (!loanPaymentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), params.id])
            redirect(action: "list")
            return
        }

        [loanPaymentInstance: loanPaymentInstance]
    }

    def editJSON(){
        def loanPaymentInstance = LoanPayment.get(params.id)
        def version = loanPaymentInstance.version
        def payrollId = loanPaymentInstance.payrollEntryId
        def loan = loanPaymentInstance?.loan
        def employee = loan?.employee
        def loanList = Loan.findAllByEmployee(employee)
        def payrollEntries = PayrollEntry.findAllByEmployeeAndId(employee, payrollId, [sort: 'payPeriod'])
        if (!loanPaymentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), params.id])
            redirect(action: "list")
            return
        }
        def payroll = payrollEntries.toString()
        def paymentDate = Formatter?.formatDate(loanPaymentInstance?.paymentDate,"MM/dd/yyyy")
        def loanPayment = [loanPayment: loanPaymentInstance, version: version, loanList: loanList, payroll: payroll, paymentDate: paymentDate]
        render loanPayment as JSON
    }

    def update() {
        def loanPaymentInstance = LoanPayment.get(params.id)
        def loan = Loan.get(params?.loan?.id)
        def employee = loan?.employee
        def loanList = Loan.findAllByEmployee(employee)
        def payrollEntries = PayrollEntry.findAllByEmployee(employee, [sort: 'payPeriod'])
        if (!loanPaymentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (loanPaymentInstance.version > version) {
                loanPaymentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'loanPayment.label', default: 'LoanPayment')] as Object[],
                        "Another user has updated this LoanPayment while you were editing")
                render(view: "edit", model: [loanPaymentInstance: loanPaymentInstance])
                return
            }
        }

        loanPaymentInstance.properties = params

        if (!loanPaymentInstance.save(flush: true)) {
            render(view: "edit", model: [loanPaymentInstance: loanPaymentInstance])
            return
        }
        loanService.computeRemainingBalance(loanPaymentInstance.getLoan())
        payrollEntryService.recompute(loanPaymentInstance.getPayrollEntry())
        flash.message = message(code: 'default.updated.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), loanPaymentInstance.id])
//        redirect(action: "show", id: loanPaymentInstance.id)
        redirect(controller: 'loan', action: "show", id: params.loanId)
    }

    def delete() {

        def loanPaymentInstance = LoanPayment.get(params.id)
        if (!loanPaymentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), params.id])
//            redirect(action: "list")
            redirect(controller: 'loan', action: "show", id: params.id)
            return
        }
        def loanInstance = loanPaymentInstance?.loan
        def payrollEntryInstance = loanPaymentInstance?.payrollEntry

        try {
            loanInstance?.removeFromPayments(loanPaymentInstance)
            loanPaymentInstance.delete(flush: true)
            loanService.computeRemainingBalance(loanInstance)
            if (payrollEntryInstance != null){

                payrollEntryService.recompute(payrollEntryInstance)
            }
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), params.id])
//            redirect(action: "list")
            redirect(controller: 'loan', action: "show", id: loanInstance.id)
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'loanPayment.label', default: 'LoanPayment'), params.id])
//            redirect(action: "show", id: params.id)
            redirect(controller: 'loan', action: "show", id: loanInstance.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = LoanPayment.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('remarks', "%${sSearch}%")

                    ilike('payrollEntry', "%${sSearch}%")

                    ilike('amount', "%${sSearch}%")

                    ilike('loan', "%${sSearch}%")

                    ilike('paymentDate', "%${sSearch}%")

                }
            }
        }
        def totalRecords = LoanPayment.count();
        def totalDisplayRecords = LoanPayment.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('remarks', "%${sSearch}%")

                    ilike('payrollEntry', "%${sSearch}%")

                    ilike('amount', "%${sSearch}%")

                    ilike('loan', "%${sSearch}%")

                    ilike('paymentDate', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.remarks,

                    1: it.payrollEntry,

                    2: it.amount,

                    3: it.loan,

                    4: it.paymentDate,

                    5: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    @Override
    def saveAjax(){
        def loan = Loan.get(params?.loan?.id)
        def className = getDomainClazz();
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)

        if(modelInstance){
            modelInstance.properties = params;
            if (!modelInstance.save(flush: true)) {
                //render(view: "create", model: [modelInstance: modelInstance])
                modelInstance.errors.each {println(it)};
                response.status = response.SC_INTERNAL_SERVER_ERROR
                render modelInstance.errors as JSON
            }else{
                loan.addToPayments(modelInstance)
                loanService.computeRemainingBalance(loan)
                response.status=response.SC_OK
                render modelInstance as JSON
            }
        }else{

            modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.newInstance()
            modelInstance.properties = params;

            if (!modelInstance.save(flush: true)) {
                //render(view: "create", model: [modelInstance: modelInstance])
                modelInstance.errors.each {println(it)};
                response.status = response.SC_INTERNAL_SERVER_ERROR
                render modelInstance.errors as JSON
            }else{
                loan.addToPayments(modelInstance)
                loanService.computeRemainingBalance(loan)
                response.status=response.SC_OK
                render modelInstance as JSON
            }
        }
    }

    @Override
    def deleteAjax(){
        def className = getDomainClazz();
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)
        def newMapResult = [:]

        def payrollEntryInstance = modelInstance?.payrollEntry
        if(modelInstance){
            try{
                response.status=response.SC_OK
                modelInstance.delete(flush: true);
                if (payrollEntryInstance != null){
                    payrollEntryService.recompute(payrollEntryInstance)
                }
                newMapResult.put('result', "Success");
                flash.message = message(code: 'item.delete.success', default:'Item has been successfully deleted.')
                render newMapResult as JSON
            }catch (Exception e){
                e.printStackTrace()
                response.status = response.SC_INTERNAL_SERVER_ERROR
                flash.message = message(code: 'item.delete.failed', default:'Item has not been deleted.')
                render e as JSON
            }
        }else{
            response.status = response.SC_INTERNAL_SERVER_ERROR
            newMapResult.put('result', "Failed");
            flash.message = message(code: 'item.delete.failed', default:'Item has not been deleted.')
            render newMapResult as JSON
        }
    }
}
