package com.ripple.equipment

class ImageController {

    def defaultAction = 'show'

    def show(){
        def obj = Class.forName("${params.classname}",true,Thread.currentThread().contextClassLoader).newInstance();
        def object = obj.get( params.id )
        response.setContentType(params.mime)
        byte[] image = object."${params.fieldName}"
        response.outputStream << image
    }

    def index() { }

}
