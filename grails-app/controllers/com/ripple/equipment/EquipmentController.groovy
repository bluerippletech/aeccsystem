package com.ripple.equipment



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.util.ImageUtils

class EquipmentController {

    def burningImageService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
//        File test = new File("./")

//        public String[] list(File("./"))
        [equipmentInstanceList: Equipment.list(params), equipmentInstanceTotal: Equipment.count()]
    }

    def create() {
        [equipmentInstance: new Equipment(params)]
    }

    def save() {
        def equipmentInstance = new Equipment(params)
        def output
        if(equipmentInstance?.image){
            def file = request.getFile('image')
            def fileName = file.fileItem.fileName
            try{
                burningImageService.doWith(file,"./").execute {
                    it.scaleApproximate(200, 200)
                }
                output = new File("./${fileName}")
                equipmentInstance?.setImage(output.getBytes())
            }catch (Exception e){
                e.printStackTrace();
            }
            finally{
                output.delete()
            }
        }

        if (!equipmentInstance.save(flush: true)) {
            render(view: "create", model: [equipmentInstance: equipmentInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'equipment.label', default: 'Equipment'), equipmentInstance.id])
        redirect(action: "show", id: equipmentInstance.id)
    }

    def show() {
        def equipmentInstance = Equipment.get(params.id)
        if (!equipmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipment.label', default: 'Equipment'), params.id])
            redirect(action: "list")
            return
        }

        def equipmentAssignmentInstance = new EquipmentAssignment()

        [equipmentInstance: equipmentInstance, equipmentAssignmentInstance: equipmentAssignmentInstance]
    }

    def edit() {
        def equipmentInstance = Equipment.get(params.id)
        if (!equipmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipment.label', default: 'Equipment'), params.id])
            redirect(action: "list")
            return
        }

        [equipmentInstance: equipmentInstance]
    }

    def update() {
        def equipmentInstance = Equipment.get(params.id)
        if (!equipmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipment.label', default: 'Equipment'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (equipmentInstance.version > version) {
                equipmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'equipment.label', default: 'Equipment')] as Object[],
                        "Another user has updated this Equipment while you were editing")
                render(view: "edit", model: [equipmentInstance: equipmentInstance])
                return
            }
        }
        def equipmentImage = equipmentInstance?.image
        def output

        equipmentInstance.properties = params
        if (equipmentInstance?.image){
            def file = request.getFile('image')
            def fileName = file.fileItem.fileName
            try{
                burningImageService.doWith(file,"./").execute {
                    it.scaleApproximate(200, 200)
                }
                output = new File("./${fileName}")
                equipmentInstance?.setImage(output.getBytes())
            }catch (Exception e){
                e.printStackTrace();
            }
            finally{
                output.delete()
            }
        }
        else{
            equipmentInstance?.image = equipmentImage
        }

        if (!equipmentInstance.save(flush: true)) {
            render(view: "edit", model: [equipmentInstance: equipmentInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'equipment.label', default: 'Equipment'), equipmentInstance.id])
        redirect(action: "show", id: equipmentInstance.id)
    }

    def delete() {
        def equipmentInstance = Equipment.get(params.id)
        if (!equipmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipment.label', default: 'Equipment'), params.id])
            redirect(action: "list")
            return
        }

        try {
            equipmentInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'equipment.label', default: 'Equipment'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'equipment.label', default: 'Equipment'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Equipment.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('algonNumber', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('plateNumber', "%${sSearch}%")

                    ilike('model', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                }
            }
        }
        def totalRecords = Equipment.count();
        def totalDisplayRecords = Equipment.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('algonNumber', "%${sSearch}%")

                    ilike('description', "%${sSearch}%")

                    ilike('plateNumber', "%${sSearch}%")

                    ilike('model', "%${sSearch}%")

                    ilike('status', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.algonNumber,

                    1: it.description,

                    2: it.plateNumber,

                    3: it.model,

                    4: it.status,

                    5: '<img src="'+createLink(controller: 'image', action: 'show', id: it.id, params: [classname: 'com.ripple.equipment.Equipment', fieldName: 'image'])+'" alt="No Image Uploaded." style="max-width:80px;"/>',

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
