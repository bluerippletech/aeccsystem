package com.ripple.equipment



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class EquipmentTypeController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [equipmentTypeInstanceList: EquipmentType.list(params), equipmentTypeInstanceTotal: EquipmentType.count()]
}

def create() {
    [equipmentTypeInstance: new EquipmentType(params)]
}

def save() {
    def equipmentTypeInstance = new EquipmentType(params)
    if (!equipmentTypeInstance.save(flush: true)) {
        render(view: "create", model: [equipmentTypeInstance: equipmentTypeInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), equipmentTypeInstance.id])
    redirect(action: "show", id: equipmentTypeInstance.id)
}

def show() {
    def equipmentTypeInstance = EquipmentType.get(params.id)
    if (!equipmentTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), params.id])
        redirect(action: "list")
        return
    }

    [equipmentTypeInstance: equipmentTypeInstance]
}

def edit() {
    def equipmentTypeInstance = EquipmentType.get(params.id)
    if (!equipmentTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), params.id])
        redirect(action: "list")
        return
    }

    [equipmentTypeInstance: equipmentTypeInstance]
}

def update() {
    def equipmentTypeInstance = EquipmentType.get(params.id)
    if (!equipmentTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (equipmentTypeInstance.version > version) {
                equipmentTypeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'equipmentType.label', default: 'EquipmentType')] as Object[],
                        "Another user has updated this EquipmentType while you were editing")
            render(view: "edit", model: [equipmentTypeInstance: equipmentTypeInstance])
            return
        }
    }

    equipmentTypeInstance.properties = params

    if (!equipmentTypeInstance.save(flush: true)) {
        render(view: "edit", model: [equipmentTypeInstance: equipmentTypeInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), equipmentTypeInstance.id])
    redirect(action: "show", id: equipmentTypeInstance.id)
}

def delete() {
    def equipmentTypeInstance = EquipmentType.get(params.id)
    if (!equipmentTypeInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), params.id])
        redirect(action: "list")
        return
    }

    try {
        equipmentTypeInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'equipmentType.label', default: 'EquipmentType'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = EquipmentType.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('type',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = EquipmentType.count();
    def totalDisplayRecords = EquipmentType.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('type',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.type,
                        
                        1:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
}
