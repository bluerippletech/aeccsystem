package com.ripple.equipment



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.ModalizerController

class EquipmentAssignmentController extends ModalizerController{

    //setter for Domain className for inherited class
    String getDomainClazz(){return 'EquipmentAssignment'}

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [equipmentAssignmentInstanceList: EquipmentAssignment.list(params), equipmentAssignmentInstanceTotal: EquipmentAssignment.count()]
    }

    def create() {
        [equipmentAssignmentInstance: new EquipmentAssignment(params)]
    }

    def save() {
        def equipmentAssignmentInstance = new EquipmentAssignment(params)
        if (!equipmentAssignmentInstance.save(flush: true)) {
            render(view: "create", model: [equipmentAssignmentInstance: equipmentAssignmentInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), equipmentAssignmentInstance.id])
        redirect(action: "show", id: equipmentAssignmentInstance.id)
    }

    def show() {
        def equipmentAssignmentInstance = EquipmentAssignment.get(params.id)
        if (!equipmentAssignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), params.id])
            redirect(action: "list")
            return
        }

        [equipmentAssignmentInstance: equipmentAssignmentInstance]
    }

    def edit() {
        def equipmentAssignmentInstance = EquipmentAssignment.get(params.id)
        if (!equipmentAssignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), params.id])
            redirect(action: "list")
            return
        }

        [equipmentAssignmentInstance: equipmentAssignmentInstance]
    }

    def update() {
        def equipmentAssignmentInstance = EquipmentAssignment.get(params.id)
        if (!equipmentAssignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (equipmentAssignmentInstance.version > version) {
                equipmentAssignmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment')] as Object[],
                        "Another user has updated this EquipmentAssignment while you were editing")
                render(view: "edit", model: [equipmentAssignmentInstance: equipmentAssignmentInstance])
                return
            }
        }

        equipmentAssignmentInstance.properties = params

        if (!equipmentAssignmentInstance.save(flush: true)) {
            render(view: "edit", model: [equipmentAssignmentInstance: equipmentAssignmentInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), equipmentAssignmentInstance.id])
        redirect(action: "show", id: equipmentAssignmentInstance.id)
    }

    def delete() {
        def equipmentAssignmentInstance = EquipmentAssignment.get(params.id)
        if (!equipmentAssignmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), params.id])
            redirect(action: "list")
            return
        }

        try {
            equipmentAssignmentInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = EquipmentAssignment.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('fromDate', "%${sSearch}%")

                    ilike('toDate', "%${sSearch}%")

                    ilike('project', "%${sSearch}%")

                    ilike('projectLocation', "%${sSearch}%")

                    ilike('operator', "%${sSearch}%")

                    ilike('comments', "%${sSearch}%")

                }

            }
            and{eq('equipment', Equipment.get(params?.id))}
        }
        def totalRecords = EquipmentAssignment.count();
        def totalDisplayRecords = EquipmentAssignment.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('fromDate', "%${sSearch}%")

                    ilike('toDate', "%${sSearch}%")

                    ilike('project', "%${sSearch}%")

                    ilike('projectLocation', "%${sSearch}%")

                    ilike('operator', "%${sSearch}%")

                    ilike('comments', "%${sSearch}%")

                }
            }
            and{eq('equipment', Equipment.get(params?.id))}

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.fromDate,

                    1: it.toDate,

                    2: it.project,

                    3: it.projectLocation,

                    4: it.operator.fullName,

                    5: it.comments,

                    6: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }
}
