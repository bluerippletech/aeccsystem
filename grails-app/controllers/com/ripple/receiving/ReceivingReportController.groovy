package com.ripple.receiving



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON
import com.ripple.purchasing.PurchaseOrder
import com.ripple.purchasing.PurchaseOrderLineItem
import org.joda.time.LocalDate

class ReceivingReportController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def stockService, springSecurityService

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [receivingReportInstanceList: ReceivingReport.list(params), receivingReportInstanceTotal: ReceivingReport.count()]
    }

    def create() {
        [receivingReportInstance: new ReceivingReport(params)]
    }

    def save() {
        def receivingReportInstance = new ReceivingReport(params)
        if (receivingReportInstance?.purchaseOrder) {
            def purchaseOrderLineItems = receivingReportInstance?.purchaseOrder?.lineItems

            purchaseOrderLineItems.each {
                if (it.quantityRemaining > 0) {
                    def receivingReportLineItem = new ReceivingReportLineItem(item: it?.item,
                            quantityRemaining: it?.quantityRemaining,
                            unitOfMeasure: it?.unitOfMeasure, unitPrice: it?.unitPrice
                    )
                    receivingReportInstance.addToLineItems(receivingReportLineItem)
                }
            }
        }

        receivingReportInstance.createdBy=springSecurityService.currentUser

        if (!receivingReportInstance.save(flush: true)) {
            render(view: "create", model: [receivingReportInstance: receivingReportInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), receivingReportInstance.id])
        redirect(action: "show", id: receivingReportInstance.id)
    }

    def show() {
        def receivingReportInstance = ReceivingReport.get(params.id)
        if (!receivingReportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), params.id])
            redirect(action: "list")
            return
        }

        [receivingReportInstance: receivingReportInstance]
    }

    def edit() {
        def receivingReportInstance = ReceivingReport.get(params.id)
        if (!receivingReportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), params.id])
            redirect(action: "list")
            return
        }

        sec.ifNotGranted(roles: 'ROLE_ADMIN') {
            if (receivingReportInstance?.status == 'Cancelled' || receivingReportInstance?.status == 'For Approval') {
                receivingReportInstance?.setStatus('Draft')
            }
        }

        [receivingReportInstance: receivingReportInstance]
    }

    def update() {
        def receivingReportInstance = ReceivingReport.get(params.id)
        if (!receivingReportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (receivingReportInstance.version > version) {
                receivingReportInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'receivingReport.label', default: 'ReceivingReport')] as Object[],
                        "Another user has updated this ReceivingReport while you were editing")
                render(view: "edit", model: [receivingReportInstance: receivingReportInstance])
                return
            }
        }

        if (params.checkedSupplierInvoice || params.checkedPurchaseOrder){
            if (params.checkedPurchaseOrder){
                receivingReportInstance.dateCheckedPurchaseOrder = LocalDate.now()
                receivingReportInstance.purchaseOrderCheckedBy = springSecurityService.currentUser
            }
            if (params.checkedSupplierInvoice){
                receivingReportInstance.dateCheckedSupplierInvoice = LocalDate.now()
                receivingReportInstance.supplierInvoiceCheckedBy = springSecurityService.currentUser
            }
        }

        receivingReportInstance.properties = params

        if (!receivingReportInstance.save(flush: true)) {
            render(view: "edit", model: [receivingReportInstance: receivingReportInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), receivingReportInstance.id])
        redirect(action: "show", id: receivingReportInstance.id)
    }

    def delete() {
        def receivingReportInstance = ReceivingReport.get(params.id)
        if (!receivingReportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), params.id])
            redirect(action: "list")
            return
        }

        try {
            receivingReportInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'receivingReport.label', default: 'ReceivingReport'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = ReceivingReport.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    ilike('receivingReportNumber', "%${sSearch}%")

                    purchaseOrder {
                        or {
                            ilike('purchaseOrderNumber', "%${sSearch}%")
                            supplier {
                                ilike('supplierName', "%${sSearch}%")
                            }
                        }
                    }

                    ilike('status', "%${sSearch}%")

                }
            }
        }
        def totalRecords = ReceivingReport.count();
        def totalDisplayRecords = ReceivingReport.createCriteria().count {
            if (sSearch) {
                or {

                    ilike('receivingReportNumber', "%${sSearch}%")

                    purchaseOrder {
                        or {
                            ilike('purchaseOrderNumber', "%${sSearch}%")
                            supplier {
                                ilike('supplierName', "%${sSearch}%")
                            }
                        }
                    }

                    ilike('status', "%${sSearch}%")

                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.receivingReportNumber,

                    1: it.purchaseOrder?.toString(),

                    2: it.purchaseOrder?.supplier?.toString(),

                    3: it.dateStarted,

                    4: it.status,

                    5: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def formStatus() {
        def receivingReportInstance = ReceivingReport.get(params.id)

        if (!receivingReportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReport.label', default: 'receivingReport'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (receivingReportInstance.version > version) {
                receivingReportInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'receivingReport.label', default: 'receivingReport')] as Object[],
                        "Another user has updated this receivingReport while you were editing")
                render(view: "edit", model: [receivingReportInstance: receivingReportInstance])
                return
            }
        }

        def newStatus = params._action_formStatus

        if (newStatus) {
            switch (newStatus) {
                case 'For Approval':
                    newStatus = receivingReportInstance?.ForApproval
                    break

                case 'Cancel':
                    newStatus = receivingReportInstance?.Cancelled
                    break

                case 'Approve':
                    newStatus = receivingReportInstance?.Approved
                    break

                case 'Post':
                    newStatus = receivingReportInstance?.Posted
                    break

                default:
                    newStatus = receivingReportInstance?.Draft
            }
        }

        if (newStatus == receivingReportInstance?.Approved) {
            def receivingReportLineItem = ReceivingReportLineItem.findAllByReceivingReport(receivingReportInstance)
            def countBalance = 0
            receivingReportLineItem.each {
                def rrItem = it.item
                def purchaseOrderLineItemInstance = receivingReportInstance?.purchaseOrder?.lineItems?.find {it.item == rrItem}
                def totalQuantity = purchaseOrderLineItemInstance?.quantityDelivered + it.quantity
                if (purchaseOrderLineItemInstance) {
                    if (totalQuantity <= purchaseOrderLineItemInstance?.quantity) {
                        purchaseOrderLineItemInstance?.quantityDelivered = purchaseOrderLineItemInstance?.quantityDelivered + it?.quantity
                        purchaseOrderLineItemInstance?.quantityRemaining = purchaseOrderLineItemInstance?.quantityRemaining - it?.quantity
                        if (!purchaseOrderLineItemInstance.save(flush: true)) {
                            throw new Exception("Unable to save purchaseOrder count.");
                        }
                        countBalance += purchaseOrderLineItemInstance?.quantityRemaining
                    }
                }
            }
            def purchaseOrderInstance = receivingReportInstance?.purchaseOrder
            if (countBalance > 0) {
                if (purchaseOrderInstance.status != 'Partially Served') {
                    purchaseOrderInstance?.status = "Partially Served"
                }
            }
            else {
                purchaseOrderInstance?.status = "Closed"
            }

        }

        receivingReportInstance.status = newStatus

        if (!receivingReportInstance.save(flush: true)) {
            render(view: "edit", model: [receivingReportInstance: receivingReportInstance])
            return
        }

        if (receivingReportInstance.status == receivingReportInstance.Posted)
            stockService.stockActionSelector(receivingReportInstance, "IN", "RR");

        flash.message = message(code: 'default.updated.message', args: [message(code: 'receivingReport.label', default: 'receivingReport'), receivingReportInstance.id])
        redirect(action: "show", id: receivingReportInstance.id)

    }
}
