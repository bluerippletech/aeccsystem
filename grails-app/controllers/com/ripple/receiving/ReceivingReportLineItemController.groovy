package com.ripple.receiving



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class ReceivingReportLineItemController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [receivingReportLineItemInstanceList: ReceivingReportLineItem.list(params), receivingReportLineItemInstanceTotal: ReceivingReportLineItem.count()]
    }

    def create() {
        [receivingReportLineItemInstance: new ReceivingReportLineItem(params)]
    }

    def save() {
        def receivingReportLineItemInstance = new ReceivingReportLineItem(params)
        if (!receivingReportLineItemInstance.save(flush: true)) {
            render(view: "create", model: [receivingReportLineItemInstance: receivingReportLineItemInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), receivingReportLineItemInstance.id])
        redirect(action: "show", id: receivingReportLineItemInstance.id)
    }

    def show() {
        def receivingReportLineItemInstance = ReceivingReportLineItem.get(params.id)
        if (!receivingReportLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [receivingReportLineItemInstance: receivingReportLineItemInstance]
    }

    def edit() {
        def receivingReportLineItemInstance = ReceivingReportLineItem.get(params.id)
        if (!receivingReportLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), params.id])
            redirect(action: "list")
            return
        }

        [receivingReportLineItemInstance: receivingReportLineItemInstance]
    }

    def update() {
        def receivingReportLineItemInstance = ReceivingReportLineItem.get(params.id)
        if (!receivingReportLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (receivingReportLineItemInstance.version > version) {
                receivingReportLineItemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem')] as Object[],
                        "Another user has updated this ReceivingReportLineItem while you were editing")
                render(view: "edit", model: [receivingReportLineItemInstance: receivingReportLineItemInstance])
                return
            }
        }

        receivingReportLineItemInstance.properties = params

        if (!receivingReportLineItemInstance.save(flush: true)) {
            render(view: "edit", model: [receivingReportLineItemInstance: receivingReportLineItemInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), receivingReportLineItemInstance.id])
        redirect(action: "show", id: receivingReportLineItemInstance.id)
    }

    def delete() {
        def receivingReportLineItemInstance = ReceivingReportLineItem.get(params.id)
        if (!receivingReportLineItemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), params.id])
            redirect(action: "list")
            return
        }

        try {
            receivingReportLineItemInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = ReceivingReportLineItem.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {

                    item{
                        or{

                            ilike('itemCode', "%${sSearch}%")

                            ilike('itemName', "%${sSearch}%")

                        }
                    }

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(quantity_remaining,char) like '%${sSearch}%'")

                    unitOfMeasure{
                        unitOfMeasure{
                            ilike('name', "%${sSearch}%")
                        }
                    }

                }
            }
            and{eq('receivingReport', ReceivingReport.get(params?.id))}
        }
        def totalRecords = ReceivingReportLineItem.count();
        def totalDisplayRecords = ReceivingReportLineItem.createCriteria().count {
            if (sSearch) {
                or {

                    item{
                        or{

                            ilike('itemCode', "%${sSearch}%")

                            ilike('itemName', "%${sSearch}%")

                        }
                    }

                    sqlRestriction("convert(quantity,char) like '%${sSearch}%'")

                    sqlRestriction("convert(quantity_remaining,char) like '%${sSearch}%'")

                    unitOfMeasure{
                        unitOfMeasure{
                            ilike('name', "%${sSearch}%")
                        }
                    }

                }
            }
            and{eq('receivingReport', ReceivingReport.get(params?.id))}
        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.item?.itemCode,

                    1: it.item?.itemName,

                    2: it.remainingQuantity,

                    3: it.unitOfMeasure.toString(),

                    4: '',

                    5: com.ripple.util.Formatter?.formatCurrency(it.totalItemAmount),
                    "DT_RowId": it.id,
                    "quantity": it.quantity

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def updateLineItems() {
        def newParams = [:]
        params?.each{
            if(!(it?.key in ['controller', '_action_updateLineItems', 'datatable_length', 'action']))
                newParams.put(it.key, it.value);
        }
        newParams.each{
            def receivingReportLineItemInstance = ReceivingReportLineItem.get(it.key)
            if (it?.value.toLong() <= receivingReportLineItemInstance?.quantityRemaining){
                receivingReportLineItemInstance.setQuantity(it?.value.toLong())
                if (!receivingReportLineItemInstance.save(flush: true)) {
                    response.status = response.SC_INTERNAL_SERVER_ERROR
                    render "error"
                }
            }
        }
        response.status=response.SC_OK
        render "okay"
    }

}
