package com.ripple

import grails.converters.JSON
import org.codehaus.groovy.grails.commons.GrailsClassUtils

class ModalizerController {

    String getDomainClazz(){return null}

    def getInstance(){
        def className = getDomainClazz()
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)
        JSON jsonObject
        if (!modelInstance) {
            response.status=response.SC_OK
            modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.newInstance()
            modelInstance.properties = params;
            jsonObject=modelInstance as JSON
        }else{
            response.status=response.SC_OK
            jsonObject=modelInstance as JSON
        }
        render jsonObject
        return
    }

    def saveAjax(){
        def className = getDomainClazz();
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)

        if(modelInstance){
            modelInstance.properties = params;
            if (!modelInstance.save(flush: true)) {
                //render(view: "create", model: [modelInstance: modelInstance])
                modelInstance.errors.each {println(it)};
                response.status = response.SC_INTERNAL_SERVER_ERROR
                render modelInstance.errors as JSON
                return
            }else{
                response.status=response.SC_OK
                render modelInstance as JSON
                return
            }
        }else{

            modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.newInstance()
            modelInstance.properties = params;

            if (!modelInstance.save(flush: true)) {
                //render(view: "create", model: [modelInstance: modelInstance])
                modelInstance.errors.each {println(it)};
                response.status = response.SC_INTERNAL_SERVER_ERROR
                render modelInstance.errors as JSON
                return
            }else{
                response.status=response.SC_OK
                render modelInstance as JSON
                return
            }
        }
    }

    def deleteAjax(){
        def className = getDomainClazz();
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz.get(params?.id)
        def newMapResult = [:]

        if(modelInstance){
            try{
                response.status=response.SC_OK
                modelInstance.delete(flush: true);
                newMapResult.put('result', "Success");
                flash.message = message(code: 'item.delete.success', default:'Item has been successfully deleted.')
                render newMapResult as JSON
                return
            }catch (Exception e){
                e.printStackTrace()
                response.status = response.SC_INTERNAL_SERVER_ERROR
                flash.message = message(code: 'item.delete.failed', default:'Item has not been deleted.')
                render e as JSON
                return
            }
        }else{
            response.status = response.SC_INTERNAL_SERVER_ERROR
            newMapResult.put('result', "Failed");
            flash.message = message(code: 'item.delete.failed', default:'Item has not been deleted.')
            render newMapResult as JSON
            return
        }
    }

    def getListing(){
        def className = params?.className;
        def parentProperty = params?.parentProperty
        def modelInstance = grailsApplication.domainClasses.find { it.clazz.simpleName == className }.clazz
        def parentHolder = GrailsClassUtils.getPropertyType(modelInstance, parentProperty)
        def parentId = params?.parentId

        def listing = modelInstance.createCriteria().list(){
            if(parentProperty && parentHolder && parentId){
                eq(parentProperty, parentHolder.get(params?.parentId))
            }
        }

        def newListing = [];
        def tempMap = [:];

        listing.each {
            tempMap.put('id',it.id);
            tempMap.put('name', it?.encodeAsHTML());
            newListing += tempMap;
            tempMap = [:];
        }

        if (newListing){
            response.status=response.SC_OK
        }else{
            response.status = response.SC_INTERNAL_SERVER_ERROR
        }

        render newListing?.sort {it.name}.reverse() as JSON
        return
    }
}
