package com.ripple.procurement

import com.ripple.master.Base
import org.joda.time.LocalDate
import com.ripple.master.Assignment
import com.ripple.master.Employee
import com.ripple.master.Warehouse
import com.ripple.security.AppUser
import com.ripple.master.DocumentNumbers

class Procurement extends Base {

    def documentNumbersService

    String procurementNumber = ("0").padLeft(7,'0')
    LocalDate dateStarted = new LocalDate()
    LocalDate dateClosed = new LocalDate()
    Assignment project
    Employee requestor
    AppUser creator
    Warehouse warehouse
    String status = "Draft"

    static auditable = true

    static hasMany = [lineItems: ProcurementLineItem]

    static constraints = {
        procurementNumber(nullable: false, blank: false)
        dateStarted(nullable: false, blank:false)
        dateClosed(nullable: false, blank: false)
        project(nullable: false, blank: false)
        requestor(nullable: false, blank: false)
        creator(nullable: false)
        warehouse(nullable: false, blank: false)
        status(inList: ['Draft','For Approval','Cancelled','Approved','Rejected','Closed'],blank: false)
    }

    def beforeInsert = {
        if (("0").padLeft(7,'0').equals(procurementNumber)) {
            DocumentNumbers.withNewSession {
                procurementNumber = documentNumbersService.generateDocumentNumber("Procurement")
            }
        }
    }

    String toString(){
        procurementNumber
    }
}
