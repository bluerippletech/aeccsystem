package com.ripple.procurement

import com.ripple.master.Base
import com.ripple.master.item.Item
import com.ripple.master.item.LevelUnit

class ProcurementLineItem extends Base{

    Item item
    Long quantity = 0;
    Long baseQuantity = 0;
    MaterialIssuanceClass purpose
    Long servedQuantity = 0;

    LevelUnit unitOfMeasure

    static belongsTo = [procurement:Procurement]

    static constraints = {
        item(blank: false, validator:{val, obj ->
            if(ProcurementLineItem.findAllByItemAndProcurementAndIdNotEqual(val, obj?.procurement, obj?.id))
                return 'materialIssuance.duplicate.item';
        })
        quantity(blank: false, nullable: false)
        purpose(blank: false, nullable: false)
        unitOfMeasure(blank: true, nullable: true)
    }

    def unitOfMesurementService
    def beforeInsert(){
        baseQuantity = unitOfMesurementService.convertToBase(quantity, unitOfMeasure, item.id);
    }

}
