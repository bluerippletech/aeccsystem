package com.ripple.procurement

import com.ripple.master.Base

class MaterialIssuanceClass extends Base{

    String name
    String description
    String status

    static constraints = {
        name(blank: false, maxSize: 30, unique: true)
        description(blank: true)
        status(inList: ['Active','Inactive'], blank: false)
    }

    String toString(){
        "${name}"
    }
}
