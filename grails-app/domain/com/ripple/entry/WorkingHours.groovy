package com.ripple.entry;


import com.ripple.master.Employee

class WorkingHours {


//    Employee               employee
//    PayPeriod              payPeriod
    BigDecimal             day01 = 0.00
    BigDecimal             day02 = 0.00
    BigDecimal             day03 = 0.00
    BigDecimal             day04 = 0.00
    BigDecimal             day05 = 0.00
    BigDecimal             day06 = 0.00
    BigDecimal             day07 = 0.00
    BigDecimal             day08 = 0.00
    BigDecimal             day09 = 0.00
    BigDecimal             day10 = 0.00
    BigDecimal             day11 = 0.00
    BigDecimal             day12 = 0.00
    BigDecimal             day13 = 0.00
    BigDecimal             day14 = 0.00
    BigDecimal             day15 = 0.00
    BigDecimal             day16 = 0.00
    BigDecimal             day17 = 0.00
    BigDecimal             day18 = 0.00
    BigDecimal             day19 = 0.00
    BigDecimal             day20 = 0.00
    BigDecimal             day21 = 0.00
    BigDecimal             day22 = 0.00
    BigDecimal             day23 = 0.00
    BigDecimal             day24 = 0.00
    BigDecimal             day25 = 0.00
    BigDecimal             day26 = 0.00
    BigDecimal             day27 = 0.00
    BigDecimal             day28 = 0.00
    BigDecimal             day29 = 0.00
    BigDecimal             day30 = 0.00
    BigDecimal             day31 = 0.00


    static belongsTo = [employee:Employee,payPeriod:PayPeriod]
    
    static transients = ['dayValue']


    static constraints = {

        employee(nullable:false)
        payPeriod(nullable:false)
        day01(scale:2)
        day02(scale:2)
        day03(scale:2)
        day04(scale:2)
        day05(scale:2)
        day06(scale:2)
        day07(scale:2)
        day08(scale:2)
        day09(scale:2)
        day10(scale:2)
        day11(scale:2)
        day12(scale:2)
        day13(scale:2)
        day14(scale:2)
        day15(scale:2)
        day16(scale:2)
        day17(scale:2)
        day18(scale:2)
        day19(scale:2)
        day20(scale:2)
        day21(scale:2)
        day22(scale:2)
        day23(scale:2)
        day24(scale:2)
        day25(scale:2)
        day26(scale:2)
        day27(scale:2)
        day28(scale:2)
        day29(scale:2)
        day30(scale:2)
        day31(scale:2)

        
    }

    BigDecimal getDayValue(i){
        if (i<10)
        this."day0${i}"
        else
        this."day${i}"
    }

    void setDayValue(i,val){
        if (i<10)
        this."day0${i}"=val
        else
        this."day${i}" =val
    }

}
