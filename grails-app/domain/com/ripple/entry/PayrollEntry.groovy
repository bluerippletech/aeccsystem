package com.ripple.entry

import com.ripple.loans.LoanPayment
import com.ripple.master.Employee

class PayrollEntry {

    static belongsTo = [payPeriod:PayPeriod, employee:Employee]

    //Time entries, records.
    BigDecimal regularHoursWorked = 0.00
    BigDecimal lateHours = 0.00
    BigDecimal totalRegularHours = 0.00
    BigDecimal regularOvertimeHours = 0.00
    BigDecimal sundayHoursWorked = 0.00
    BigDecimal legalHolidayHours = 0.00
    BigDecimal totalDaysWorked = 0.00
    /*value reflected already represents actual credit. e.g. if on regular
     *holiday, work was rendered, number of hours reflected is multipled by 2
     *already.
     */
    BigDecimal specialHolidayHours = 0.00

    // Actual values.
    BigDecimal regularHoursPay = 0.00
    BigDecimal lateValue = 0.00
    BigDecimal regularOvertimePay = 0.00
    BigDecimal sundayPay = 0.00
    BigDecimal legalHolidayPay = 0.00
    BigDecimal specialHolidayPay = 0.00

    BigDecimal basicPay = 0.00
    BigDecimal grossPay  = 0.00// without deductions
    BigDecimal netPay = 0.00
    BigDecimal cola = 0.00
    //Deductions
    BigDecimal sssContribution = 0.00
    BigDecimal philHealthContribution = 0.00
    BigDecimal pagIbigContribution = 0.00
    BigDecimal withholdingTax = 0.00

    BigDecimal sssContributionEmployer = 0.00
    BigDecimal sssEmployeeCompensation = 0.00
    BigDecimal philHealthContributionEmployer = 0.00
    BigDecimal pagIbigContributionEmployer = 0.00

    BigDecimal annualLeaveCredits = 0.00
    BigDecimal bonuses = 0.00

    //loan Payments placeholders
    BigDecimal sssLoan = 0.00
    BigDecimal pagIbigLoan = 0.00
    BigDecimal hdmfLoan = 0.00
    BigDecimal cashAdvance = 0.00

    //cash advances/vale
    BigDecimal advances = 0.00


    String payPeriodLabel
   
    static hasMany = [payments:LoanPayment]

    static constraints = {
        employee(unique:['payPeriod'])
        regularHoursWorked(nullable:true,scale:2)
        lateHours(nullable:true,scale:2)
        totalRegularHours(nullable:true,scale:2)
        regularOvertimeHours(nullable:true,scale:2)
        sundayHoursWorked(nullable:true,scale:2)
        legalHolidayHours(nullable:true,scale:2)
        totalDaysWorked(nullable:true,scale:2)
        specialHolidayHours(nullable:true)
        regularHoursPay(nullable:true,scale:2)
        lateValue(nullable:true,scale:2)
        regularOvertimePay(nullable:true,scale:2)
        sundayPay(nullable:true,scale:2)
        legalHolidayPay(nullable:true,scale:2)
        specialHolidayPay(nullable:true,scale:2)
        basicPay(nullable:true,scale:2)
        annualLeaveCredits(nullable:true,scale:2)
        bonuses(nullable:true,scale:2)
        grossPay (nullable:true,scale:2)
        netPay(nullable:true,scale:2)
        cola(nullable:true,scale:2)
        sssContribution(nullable:true,scale:2)
        philHealthContribution(nullable:true,scale:2)
        pagIbigContribution(nullable:true,scale:2)
        withholdingTax(nullable:true,scale:2)
        payPeriodLabel(nullable:true)
    }

    String toString(){
        "${payPeriod} - ${employee}"
    }
}
