package com.ripple.entry;


import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class PayPeriod implements Comparable {
    Boolean     firstPayPeriod
    LocalDate   monthYear = new LocalDate()

    static constraints = {
        monthYear(blank: false, unique: true)
    }
     static mapping = {
        sort:'monthYear'
    }

    static transients = ['periodStartDate','periodEndDate', 'dateRange']

    String toString(){
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMMM d, yyyy");
        String str = fmt.print(toLocalDate(periodStartDay())) +" to " +fmt.print(toLocalDate(periodEndDay()));
    }

    String toProperLabel(aDay){
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMM dd,yyyy - EEEEE");
        return fmt.print(aDay);
    }

    LocalDate toLocalDate(){
        return toLocalDate(1)
    }

    LocalDate toLocalDate(aDay){
        def localDate = monthYear?.withDayOfMonth(aDay)
        if (aDay>25)
            localDate = localDate?.minusMonths(1)
        return localDate
    }

    Integer toDayOfWeek(aDay){
        def localDate = monthYear?.withDayOfMonth(aDay)
        if (aDay>25)
            localDate = localDate?.minusMonths(1)
        return localDate?.getDayOfWeek()
    }

    LocalDate getPeriodStartDate(){
        this.toLocalDate(periodStartDay())
    }

    LocalDate getPeriodEndDate(){
        this.toLocalDate(periodEndDay())
    }

    Integer periodStartDay(){
        if (firstPayPeriod)
            return 26
        else
            return 11
    }

    Integer periodEndDay(){
        if (firstPayPeriod)
            return 10
        else
            return 25
    }

    int compareTo(o) {
        this.getPeriodStartDate().compareTo(o?.getPeriodStartDate())
    }

    String getDateRange(){
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMMM d, yyyy");
        String str = fmt.print(toLocalDate(periodStartDay())) +" to " +fmt.print(toLocalDate(periodEndDay()));
        return str;
    }
}
