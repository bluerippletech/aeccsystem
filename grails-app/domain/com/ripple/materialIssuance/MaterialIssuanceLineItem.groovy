package com.ripple.materialIssuance

import com.ripple.master.Employee
import com.ripple.procurement.Procurement
import org.joda.time.LocalDate
import com.ripple.master.Base
import com.ripple.master.DocumentNumbers
import com.ripple.master.item.Item
import com.ripple.master.item.LevelUnit
import com.ripple.procurement.ProcurementLineItem

class MaterialIssuanceLineItem extends Base {

    Item item
    LevelUnit unitOfMeasure
    Long quantity = 0
//    BigDecimal unitPrice = 0.00

    static belongsTo = [issuance: MaterialIssuance]

    static constraints = {
        unitOfMeasure(blank: false, nullable: false)
        quantity(nullable:false, validator:{
            if(it.toLong() < 1){
                return ['min.error', 1]
            }
        })
        item(blank: false, validator:{val, obj ->
            if(MaterialIssuanceLineItem.findAllByItemAndIssuanceAndIdNotEqual(val, obj?.issuance, obj?.id))
                return 'materialIssuance.duplicate.item';
        })
    }

    // Auto-generates control number
//    def unitOfMesurementService

//    def beforeInsert = {
//        if (("0").padLeft(7,'0').equals(canvassCode)) {
//            DocumentNumbers.withNewSession {
//                mISNumber = documentNumbersService.generateDocumentNumber("MIS")
//            }
//        }
//
//        def procurementLineItemInstance = issuance.procurementSlip.lineItems.find {it.item == item}
//        if(procurementLineItemInstance){
//            if(procurementLineItemInstance.servedQuantity >= 0){
//                def converted = unitOfMesurementService.convertToLevel(quantity, procurementLineItemInstance.unitOfMeasure, unitOfMeasure, item?.id)
//                procurementLineItemInstance?.servedQuantity = procurementLineItemInstance?.servedQuantity+converted;
//                if(!procurementLineItemInstance.save()){
//                    throw new Exception("Unable to save procurement count.");
//                }
//            }
//        }
//
//    }
//
//    def beforeUpdate = {
//        def procurementLineItemInstance = issuance.procurementSlip.lineItems.find {it.item == item}
//        if(procurementLineItemInstance){
//            if(procurementLineItemInstance.servedQuantity >= 0){
//                def converted = unitOfMesurementService.convertToLevel(quantity, procurementLineItemInstance.unitOfMeasure, unitOfMeasure, item?.id)
//                procurementLineItemInstance?.servedQuantity = procurementLineItemInstance?.servedQuantity+converted;
//                if(!procurementLineItemInstance.save()){
//                    throw new Exception("Unable to save procurement count.");
//                }
//            }
//        }
//    }
//
//    def beforeDelete = {
//        def procurementLineItemInstance = issuance.procurementSlip.lineItems.find {it.item == item}
//        if(procurementLineItemInstance){
//            def converted = unitOfMesurementService.convertToLevel(quantity, procurementLineItemInstance.unitOfMeasure, unitOfMeasure, item?.id)
//            if(procurementLineItemInstance.servedQuantity >= converted){
//                procurementLineItemInstance?.servedQuantity = procurementLineItemInstance?.servedQuantity-converted;
//                if(!procurementLineItemInstance.save()){
//                    throw new Exception("Unable to save procurement count.");
//                }
//            }
//        }
//    }
}
