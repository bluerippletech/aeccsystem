package com.ripple.materialIssuance

import com.ripple.procurement.Procurement
import com.ripple.master.Employee
import org.joda.time.LocalDate
import com.ripple.master.DocumentNumbers
import com.ripple.master.Base

class MaterialIssuance extends Base {

    public static final String Draft = "Draft"
    public static final String ForApproval = "For Approval"
    public static final String Cancelled = "Cancelled"
    public static final String Approved = "Approved"
    public static final String Posted = "Posted"

    String misNumber = ("0").padLeft(7,'0');
    Procurement procurementSlip;
//    Employee createdBy;
    LocalDate dateStarted = new LocalDate()
    LocalDate dueDate = new LocalDate()
    String remarks = ""
    String status = Draft

    static hasMany = [lineItems: MaterialIssuanceLineItem];

    static constraints = {
        misNumber(blank: false)
        procurementSlip(blank: false);
        createdBy(blank: false);
        dateStarted(blank: false);
        remarks(blank: true, nullable: true)
        status(inList: [Draft, ForApproval, Cancelled, Approved, Posted],blank: false)
    }

    // Auto-generates control number
    def documentNumbersService
    def beforeInsert = {
        if (("0").padLeft(7,'0').equals(misNumber)) {
            DocumentNumbers.withNewSession {
                misNumber = documentNumbersService.generateDocumentNumber("MIS")
            }
        }
    }

    def unitOfMesurementService

    def afterUpdate = {
        if(status == Posted)
            lineItems.each{
                def lineItem = it
                def procurementLineItemInstance = lineItem.issuance.procurementSlip.lineItems.find {it.item == lineItem.item}
                if(procurementLineItemInstance){
                    if(procurementLineItemInstance.servedQuantity >= 0){
                        def converted = unitOfMesurementService.convertToLevel(lineItem?.quantity, procurementLineItemInstance.unitOfMeasure, lineItem?.unitOfMeasure, lineItem?.item?.id)
                        procurementLineItemInstance?.servedQuantity = procurementLineItemInstance?.servedQuantity+converted;
                        if(!procurementLineItemInstance.save()){
                            throw new Exception("Unable to save procurement count.");
                        }
                    }
                }
            }
    }
}
