package com.ripple.loans

import com.ripple.entry.PayrollEntry

import java.text.DecimalFormat
import org.joda.time.LocalDate
import org.jadira.usertype.dateandtime.joda.PersistentLocalDate

class LoanPayment {
    LocalDate       paymentDate = new LocalDate()
    BigDecimal      amount = 0.00
    String          remarks

    static belongsTo = [loan:Loan,payrollEntry:PayrollEntry]
 
    static mapping = {
    	paymentDate type: PersistentLocalDate
    }

    static constraints = {
        remarks(blank:true)
        payrollEntry(nullable:true,blank:true)
    }

    String toString(){
        DecimalFormat myFormatter = new DecimalFormat("P #,##0.00");
        "${paymentDate} - ${myFormatter.format(amount)}"
    }
}
