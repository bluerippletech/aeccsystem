package com.ripple.loans

import com.ripple.util.Formatter
import com.ripple.master.Employee
import org.joda.time.LocalDate

class Loan {

    Employee employee
    LoanType loanType
    LocalDate loanDate = new LocalDate()
    BigDecimal totalAmount = 0.00
    BigDecimal monthlyAmortization = 0.00
    BigDecimal remainingBalance = 0.00
    String remarks

    static hasMany = [payments: LoanPayment]

    static belongsTo = Employee

    static mapping = {
        payments lazy: false
        payments sort: 'paymentDate', order: 'desc'

    }

    static fetchMode = [payments: 'eager']

    static constraints = {
        loanDate(blank: false)
        totalAmount(blank: false, min: 1.00)
        monthlyAmortization(blank: false, min: 1.00)
        remainingBalance(blank: false)
        remarks(blank: true, maxSize: 2000)
        loanType(blank: false, validator: {val, obj ->
            def c = Loan.createCriteria()
            def results = c {
                eq("employee", obj.employee)
                gt("remainingBalance", 0.00)
                eq("loanType", val)
            }
            def count = 0
            results.each {
                def payments = LoanPayment.findAllByLoanAndAmountGreaterThan(it, 0.00)
                if (!(it.loanType.code == "HDMF_LOAN" && payments?.size() >= 6)) {
                    count++
                }
            }
            if (count > 0 && obj.id == null) {
                return ['loan.loantype.existingloan', val.name]
            }
        }
        )
    }

    String toString() {
        "${loanType.name} / ${loanDate} / ${Formatter.formatCurrency(totalAmount)}"
    }
}