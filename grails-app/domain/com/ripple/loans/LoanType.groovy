package com.ripple.loans

class LoanType {
    public static final String CASH_ADVANCE = "CASH_ADVANCE"
    public static final String SSS_LOAN = "SSS_LOAN"
    public static final String HOUSING_LOAN = "HOUSING_LOAN"
    public static final String HDMF_LOAN = "HDMF_LOAN"


    String name
    String code
    static constraints = {
        code(inList:[CASH_ADVANCE,SSS_LOAN,HOUSING_LOAN,HDMF_LOAN],unique:true,blank: false)
        name(blank:false, maxSize: 20)
    }


    String toString(){
        "${name}"
    }
}
