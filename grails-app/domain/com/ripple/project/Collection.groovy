package com.ripple.project

import com.ripple.master.Assignment
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class Collection {
	
	LocalDate collectionDate=new LocalDate()
	BigDecimal amount=0.00
    BigDecimal retention=0.00
    BigDecimal wt=0.00
    BigDecimal fwt=0.00
    BigDecimal recoupment=0.00

	String remarks

    static belongsTo = [project:Assignment]

    static transients = ['netAmount']

    static constraints = {
    }

    BigDecimal getNetAmount(){
        return (amount-retention-wt-fwt-recoupment)
    }
}
