package com.ripple.project

class ExpenseType {


    String code
    String expenseCategory
    String expenseType
    String units

    static constraints = {
        code(blank: false, nullable: false)
        expenseCategory(inList: ['Construction Materials', 'Engineer\'s Facilities', 'Equipment', 'Labor', 'Overhead'])
        expenseType(blank: false, nullable: false)
        units(blank: true, nullable: true)
    }

    static mapping = {
        sort "code"
    }

    String toString() {
        expenseType
    }

}
