package com.ripple.project

import com.ripple.master.Assignment
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class EstimateItem {

    ExpenseType type
    BigDecimal  quantity=0.00
    BigDecimal  unitPrice=0.00

    static belongsTo = [project:Assignment]

    static constraints = {
    }

    static transients = ['amount']

    BigDecimal getAmount(){
        quantity * unitPrice
    }
}
