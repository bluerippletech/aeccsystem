package com.ripple.project

import com.ripple.master.Assignment
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class Expense {
    Assignment project
    ExpenseType type
    LocalDate expenseDate
    BigDecimal quantity = 0.00
    BigDecimal unitPrice = 0.00
    String remarks = ""
    String voucherNumber=""

    static constraints = {
        type(nullable: false)
        expenseDate(nullable: false)
        remarks(blank: true)
    }

    static transients = ['amount']

    BigDecimal getAmount(){
        quantity * unitPrice
    }
}
