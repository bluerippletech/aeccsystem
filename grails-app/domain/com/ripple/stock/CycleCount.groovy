package com.ripple.stock

import com.ripple.master.Warehouse
import com.ripple.master.Base
import com.ripple.master.DocumentNumbers
import com.ripple.master.item.ItemClass
import com.ripple.master.item.ItemSubclass
import com.ripple.master.item.Item
import org.joda.time.LocalDate

class CycleCount extends Base {

    def documentNumbersService,springSecurityService
    
    String cycleCountNumber = ("0").padLeft(7,'0')
    Warehouse warehouse
    Item item
    ItemClass itemClass
    ItemSubclass itemSubclass
    String includeInactiveItems = 'No'
    String status = 'Draft'

    static hasMany = [items:CycleCountItems]

    static constraints = {
        cycleCountNumber(nullable: false, blank: false)
        warehouse(nullable: false)
        status(nullable: false, blank: false, inList: ['Draft','For Approval','Saved','Cancelled','Approved','Rejected'])
        item(nullable: true)
        itemClass(nullable: true)
        itemSubclass(nullable: true)
        includeInactiveItems(nullable: false, blank: false, inList: ['Yes','No'])
    }

    def beforeInsert = {
        if (("0").padLeft(7,'0').equals(cycleCountNumber)) {
            DocumentNumbers.withNewSession {
                cycleCountNumber = documentNumbersService.generateDocumentNumber("CycleCount")
            }
        }
        createdDate = new Date()
        createdBy = springSecurityService.getCurrentUser().id
    }
}
