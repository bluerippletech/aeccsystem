package com.ripple.stock

import com.ripple.materialIssuance.MaterialIssuanceLineItem
import com.ripple.master.Base

class LineItemReference extends Base{

    StockCard stockCard;
    Long allocated;

    static belongsTo = ['lineItem':MaterialIssuanceLineItem];

    static constraints = {
        allocated(blank: true, nullable: true);
    }
}
