package com.ripple.stock

import com.ripple.master.Base
import com.ripple.master.item.Item

class CycleCountItems extends Base{

    StockCard stockReference
    Item item
    Long onHandBaseLevel
    Long onHandSecondLevel
    Long onHandThirdLevel
    Long actualCountBaseLevel
    Long actualCountSecondLevel
    Long actualCountThirdLevel

    static belongsTo = [cycleCount: CycleCount]

    static constraints = {
        stockReference(nullable: false)
        item(nullable: false)
        onHandBaseLevel(nullable: true)
        onHandSecondLevel(nullable: true)
        onHandThirdLevel(nullable: true)
        actualCountBaseLevel(nullable: true)
        actualCountSecondLevel(nullable: true)
        actualCountThirdLevel(nullable: true)
    }
}
