package com.ripple.stock

import com.ripple.master.item.Item
import com.ripple.master.Warehouse
import com.ripple.master.Base
import com.ripple.receiving.ReceivingReport
import com.ripple.master.item.LevelUnit

class StockCard extends Base{

    public static final String RR = "RR"
    public static final String MIS = "MIS"
    public static final String SAO = "SAO"
    public static final String SAI = "SAI"

    String transactionCode;
    String transactionReferenceId;
    ReceivingReport rrReference;
    StockCard referenceStock;
    String movement;
    Long quantity = 0;
    Long baseQuantity = 0;
    Item item;
    Long remainingBalance;
    Long allocated;
    Long served;
    BigDecimal unitPrice;
    Warehouse warehouse;
    LevelUnit unitOfMeasure;


    static constraints = {
        transactionCode(blank: false, inList: [RR, MIS, SAO, SAI]);
        transactionReferenceId(blank: false);
        rrReference(blank: true, nullable: true);
        referenceStock(nullable: true, blank: true);
        movement(blank: false, inList: ['IN', 'OUT']);
        quantity(nullable: true, blank: true);
        baseQuantity(blank:true, nullable: true);
        item(blank: false);
        remainingBalance(nullable: true, blank: true);
        allocated(nullable: true, blank: true);
        served(nullable: true, blank: true);
        unitPrice(nullable: true, blank: true);
        warehouse(blank: false)
        unitOfMeasure(blank: true, nullable: true);
    }

    def springSecurityService,unitOfMesurementService
    //for Logging
    def beforeInsert(){
        createdDate = new Date();
        createdBy = springSecurityService.getCurrentUser().id;
        baseQuantity = unitOfMesurementService.convertToBase(quantity, unitOfMeasure, item.id);
    }
    //for Logging
    def beforeUpdate(){
        editedDate = new Date();
        editedBy = springSecurityService.getCurrentUser().id
    }
}
