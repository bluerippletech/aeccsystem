package com.ripple.purchasing

import com.ripple.master.item.Item
import com.ripple.master.Base
import com.ripple.master.item.LevelUnit

class PurchaseOrderLineItem extends Base{

    Item item
    LevelUnit unitOfMeasure
    Long quantity = 0
    Long baseQuantity = 0
    Long quantityRemaining = 0
    Long quantityDelivered = 0
    BigDecimal unitPrice = 0.00

    static belongsTo = [purchaseOrder: PurchaseOrder]

    def unitOfMesurementService
    def beforeInsert = {
        quantityRemaining = quantity
        baseQuantity = unitOfMesurementService.convertToBase(quantity, unitOfMeasure, item.id);
    }

    def beforeUpdate = {
        quantityRemaining = quantity
        baseQuantity = unitOfMesurementService.convertToBase(quantity, unitOfMeasure, item.id)
    }

    static constraints = {
        unitPrice(nullable: false, scale: 2)
        unitOfMeasure(blank: true, nullable: true)
        quantity(nullable:false, validator:{
            if(it.toLong() < 1){
                return ['min.error', 1]
            }
        })
        quantityRemaining(nullable:false, validator:{
            if(it.toLong() < 0){
                return ['min.error', 0]
            }
        })
        quantityDelivered(nullable:false, validator:{
            if(it.toLong() < 0){
                return ['min.error', 0]
            }
        })
        item(blank:false, validator:{val, obj ->
            if(PurchaseOrderLineItem.findAllByItemAndPurchaseOrderAndIdNotEqual(val, obj?.purchaseOrder, obj?.id))
                return 'materialIssuance.duplicate.item';
        })
    }

    BigDecimal getTotalItemAmount() {
        def cost = unitPrice ?: 0
        def qty = quantity ?: 0
        cost * qty
    }

    String toString() {
        item?.itemName
    }
}
