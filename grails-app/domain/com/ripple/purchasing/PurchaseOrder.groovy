package com.ripple.purchasing

import com.ripple.master.Base
import org.joda.time.LocalDate
import com.ripple.master.supplier.Supplier
import com.ripple.master.Warehouse
import com.ripple.master.DocumentNumbers
import com.ripple.procurement.Procurement

class PurchaseOrder extends Base{

    def documentNumbersService

    String purchaseOrderNumber = ("0").padLeft(7,'0')
    Procurement procurement
    Supplier supplier
    Warehouse warehouse
    LocalDate dateStarted = new LocalDate()
    LocalDate dateClosed = new LocalDate()
    String status = 'Draft'
    BigDecimal totalAmount = 0.00

    static hasMany = [lineItems: PurchaseOrderLineItem]

    static constraints = {
        purchaseOrderNumber(nullable: false)
        procurement(nullable: true, blank: true)
        supplier(nullable: true, blank: true, validator: { val, obj ->
            if(obj.status=='For Approval'){
                if(!val) return ['purchaseOrder.supplier.blank.error']
            }
        })
        warehouse(nullable: true, blank: true, validator: { val, obj ->
            if(obj.status=='For Approval'){
                if(!val) return ['purchaseOrder.warehouse.blank.error']
            }
        })
        dateStarted(nullable: false, blank: false)
        dateClosed(nullable: false, blank: false)
        totalAmount(nullable: true)
        status(inList: ['Draft','For Approval','Cancelled','Approved','Rejected','Partially Served','Closed'],blank: false, nullable: false)
    }

    def beforeInsert = {
        if (("0").padLeft(7,'0').equals(purchaseOrderNumber)) {
            DocumentNumbers.withNewSession {
                purchaseOrderNumber = documentNumbersService.generateDocumentNumber("PurchaseOrder")
            }
        }
    }

    def getTotalAmount(){
        def purchaseOrderItems = lineItems?.findAll()
        if(purchaseOrderItems?.size() > 0){
            totalAmount = lineItems?.totalItemAmount?.sum()
        }
        totalAmount
    }

    String toString(){
        purchaseOrderNumber
    }
}
