package com.ripple.equipment

class EquipmentType {
    String type

    static constraints = {
        type(blank: false)
    }

    static mapping = {
        sort type:'asc'
    }

    String toString() {
        type
    }
}

