package com.ripple.equipment

import org.joda.time.LocalDate
import org.jadira.usertype.dateandtime.joda.PersistentLocalDate
import com.ripple.master.Employee

class Equipment {
    EquipmentType equipmentType
    String purchasedFrom
    LocalDate datePurchased
    String algonNumber
    String invoiceNumber
    BigDecimal amount       =0.00
    BigDecimal freightCharges    =0.00
    LocalDate arrivalDate
    Employee  assignedTo
    Employee  assignedDriver

    String makeAndType
    String engineNumber
    String chassisNumber
    String crNumber
    String plateNumber
    String color
    String tireSizeFront
    String tireSizeBack
    String model
    String yearModel
    String grossWeight
    String netCapacity
    String numberOfCylinders
    String description
    String comments

    String status

    SortedSet assignments


    byte[] image

    static hasMany = [assignments:EquipmentAssignment]

    static constraints = {
        equipmentType(nullable: true)
        image(size: 0..5000000, nullable: true)
        algonNumber(nullable:false,blank:false)
        model(nullable:true,blank:true)
        makeAndType(nullable:true,blank:true)
        purchasedFrom(nullable:true,blank:true)
        invoiceNumber(nullable:true,blank:true)
        amount(nullable:false,blank:false)
        freightCharges(nullable:true,blank:true)
        status(blank:true,inList:['Sold','Under Repair','Operational','Idle','Junked'])
        description(nullable:true,blank:true)
        comments(nullable:true,blank:true)
        crNumber(nullable:true,blank:true)
        plateNumber(nullable:true,blank:true)
        netCapacity(nullable:true,blank:true)
        numberOfCylinders(nullable:true,blank:true)
        engineNumber(nullable:true,blank:true)
        chassisNumber(nullable:true,blank:true)
        grossWeight(nullable:true,blank:true)
        color(nullable:true,blank:true)
        tireSizeFront(nullable:true,blank:true)
        tireSizeBack(nullable:true,blank:true)
        assignedTo(nullable:true)
        assignedDriver(nullable:true)

    }

    static mapping = {
        cache true
        datePurchased type: PersistentLocalDate
        arrivalDate type: PersistentLocalDate
        assignments sort:'fromDate'
    }
}
