package com.ripple.equipment

import org.joda.time.LocalDate
import com.ripple.master.Assignment
import com.ripple.master.AssignmentLocation
import com.ripple.master.Employee
import org.jadira.usertype.dateandtime.joda.PersistentLocalDate

class EquipmentAssignment implements Comparable {

    LocalDate fromDate
    LocalDate toDate
    Assignment project
    AssignmentLocation projectLocation
    Employee operator
    String comments

    static belongsTo = [equipment:Equipment]

    static constraints = {
        operator(nullable: true)
        toDate(blank: true, nullable: true)
        project(blank: true, nullable: true)
        projectLocation(blank: true, nullable: true)
        comments(blank: true)
    }

    static mapping = {
        fromDate type: PersistentLocalDate
        toDate type: PersistentLocalDate
        cache true
    }

    String toString() {
        "${project}"

    }


    int compareTo(Object t) {
        this.fromDate?.compareTo(t.fromDate)
    }
}
