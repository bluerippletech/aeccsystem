package com.ripple.master.supplier

import com.ripple.master.geography.Address
import com.ripple.master.sociology.ContactPerson
import com.ripple.master.Base

class Supplier extends Base{

    String supplierCode = ("0").padLeft(7,'0')
    String supplierName = ""
//    SupplierType supplierType = SupplierType.LOCAL
    String taxIdentificationNo = ""
    String website = ""
    String email = ""
    String infoStatus = "Active"

    // Supplier Address
    Address	primaryAddress
//
    // Supplier Contact
    ContactPerson primaryAddressContact

    static hasMany = [
            addresses: Address,
            contactPersons: ContactPerson,
            supplierCredits: SupplierCredit
    ]

    static transients = [
            "addressList",
            "contactPersonList",
            "supplierCreditList"
    ]

    // Auto-generates control number
//    def documentNumbersService
//    def beforeInsert = {
//        if (("0").padLeft(7,'0').equals(supplierCode)) {
//            DocumentNumbers.withNewSession {
//                supplierCode = documentNumbersService.generateDocumentNumber("Supplier")
//            }
//        }
//    }

    static constraints = {
        supplierCode(nullable:true)
        supplierName(nullable:false)
        taxIdentificationNo(nullable:true)
        website(nullable:true)
        email(nullable:true)
        infoStatus(inList:["New", "Active", "Inactive"], blank:false)
        primaryAddress(nullable:true)
        primaryAddressContact(nullable:true)
    }

    String toString() {
        supplierName
    }

    def getAddressList() {
        def listAddress = []
        if(addresses.size() > 0) {
            listAddress = addresses.findAll{it?.isDeleted=="No"}
        }
        listAddress
    }

    def getContactPersonList() {
        def listContactPerson = []
        if(contactPersons.size() > 0) {
            listContactPerson = contactPersons.findAll{it?.isDeleted=="No"}
        }
        listContactPerson
    }

    def getSupplierCreditList() {
        def listSupplierCredit = []
        if(supplierCredits.size() > 0) {
            listSupplierCredit = supplierCredits.findAll{it?.isDeleted=="No"}
        }
        listSupplierCredit
    }
}
