package com.ripple.master.supplier

import com.ripple.master.Base

class SupplierCredit extends Base {
	BigDecimal creditLimit = 0.00
	Long terms = 0
	String termsDescription = ""
	String infoStatus = "Active"
	
	static belongsTo = [supplier: Supplier]
	
    static constraints = {
		creditLimit(nullable:false)
		terms(nullable:false)
		termsDescription(nullable:false)
    	infoStatus(inList:["Active", "Inactive"])
	}
	
	String toString() {
		termsDescription + ", " + creditLimit
	}
}