package com.ripple.master

import org.joda.time.LocalDate

class Rate implements Comparable {

    public static final String DAILY = "Daily"
    public static final String MONTHLY = "Monthly"

    Employee                employee
    String                  type
    LocalDate               effectivityDate
    String                  description
    BigDecimal              amount
    BigDecimal              monthlyCola = 393.00
    BigDecimal              dailyCola = 15.00
    Boolean                 exemptFromDeductions = false

    static belongsTo = Employee

    static constraints = {
        employee(false:true)
        type(inList:[DAILY, MONTHLY])
        effectivityDate(unique:['employee'])
        description(nullable:true)
        amount(scale:2)
    }

    String toString(){
        "Php ${amount}"
    }

    int compareTo(o) {
        this.getEffectivityDate().compareTo(o?.getEffectivityDate())
//        o?.getEffectivityDate().compareTo(this.getEffectivityDate());
    }
}
