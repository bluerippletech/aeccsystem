package com.ripple.master.item

import com.ripple.master.Base

class ItemSubclass extends Base {

    String name
    String description
    String status

    static belongsTo = [itemClass: ItemClass]

    static constraints = {
        name(nullable: false, blank: false, unique: true)
        description(blank: true)
        status(inList: ['Active','Inactive'], blank: false)
    }

    String toString() {
        name
    }
}
