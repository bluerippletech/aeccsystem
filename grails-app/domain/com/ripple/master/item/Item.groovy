package com.ripple.master.item

import com.ripple.master.Base
import com.ripple.master.DocumentNumbers

class Item extends Base {

    public static final String FIFO = "FIFO"
    public static final String LIFO = "LIFO"
    public static final String MANUAL = "MANUAL"

    String itemNumber = ("0").padLeft(7,'0')
    String itemCode
    String itemName
    ItemClass itemClass
    ItemSubclass itemSubclass
    String description
    String status
    String stockMovement
    String algonPartNumber
    String supplierPartNumber
    String oemPartNumber

    LevelUnit baseLevel
    LevelUnit secondLevel
    LevelUnit thirdLevel

    def documentNumbersService
    def beforeInsert = {
        DocumentNumbers.withSession {
            itemNumber = documentNumbersService.generateDocumentNumber("Item")
        }
    }

    static constraints = {
        itemCode(nullable: false, blank: false, unique: true)
        itemName(nullable: false, blank: false)
        itemClass(nullable: false,blank:false)
        itemSubclass(blank:true,nullable: true)
        description(nullable: false, blank: true)
        status(inList: ['Active','Inactive','Temporary'], blank: false)
        stockMovement(inList: [FIFO,LIFO,MANUAL], blank: false)
        baseLevel(blank: false, nullable: false)
        secondLevel(blank: true, nullable: true)
        thirdLevel(blank: true, nullable: true,validator:{val,obj->
         if (val&&!obj.secondLevel){
                return ['item.invalid.thirdLevel']
         }
        })
    }
}
