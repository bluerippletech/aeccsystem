package com.ripple.master.item

import com.ripple.master.Base

class ItemClass extends Base {

    String name
    String description
    String status

    static hasMany = [itemSubclasses:ItemSubclass]

    static constraints = {
        name(nullable: false, blank: false, unique: true)
        description(blank: true)
        status(inList: ['Active','Inactive'], blank: false)
    }

    String toString() {
        name
    }
}
