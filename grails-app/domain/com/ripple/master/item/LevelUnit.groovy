package com.ripple.master.item

import com.ripple.master.Base

class LevelUnit extends Base{

    String unitOfMeasure;
    BigDecimal value;

    static constraints = {
        unitOfMeasure(blank:false)
        value(blank: true)
    }

    String toString(){
        return unitOfMeasure
    }
}
