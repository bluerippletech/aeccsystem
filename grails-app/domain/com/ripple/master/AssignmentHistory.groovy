package com.ripple.master

import org.joda.time.LocalDate

class AssignmentHistory {

    Assignment project
    AssignmentLocation workLocation
    LocalDate startDate


    static belongsTo = [employee:Employee]

    static mapping = {
        sort startDate:'desc'
    }

    static constraints = {
    }

    String toString(){
        "${project} - ${workLocation} : ${startDate?.toString("MMMMM dd, yyyy")}"
    }
}
