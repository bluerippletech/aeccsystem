package com.ripple.master

class Warehouse extends Base{

    String name
    String location
    String status
    Assignment assignment

    static hasMany = [lineItems: WarehouseLineItem]

    static constraints = {
        name(blank: false)
        location(blank: true)
        status(inList: ['Active','Inactive'], blank: false)
        assignment(blank: true, nullable: true)

    }

    String toString() {
        name
    }
}
