package com.ripple.master

import org.joda.time.LocalDate

class Employee {

    String employeeNumber
    String firstName
    String lastName
    String middleInitials
    String sex
    String currentPosition

    String sssIdNumber
    String hdmfIdNumber
    String philHealthIDNumber
    String taxIdentificationNumber

    AssignmentHistory currentAssignment

    LocalDate birthDate
    LocalDate employmentDate
    Boolean isSeparated
    Boolean isActive
    Boolean retired
    Boolean emergencyLaborer
    Integer grpNumber

    String address
    String telephoneNumber
    String mobileNumber
    String provincialAddress
    //house type

    String emergencyContactPerson
    String emergencyContactAddress
    String emergencyContactNumber

    String religion

    String fathersName
    String mothersName
    String spouseName
    String height
    String weight
    String citizenship
    String resCertNo
    LocalDate resCertIssueDate
    String resCertIssuePlace
    String remarks

    //dependents has many here.


    String taxStatus
    SortedSet rates

    //Voluntary Contributions Data
    BigDecimal sssContribution
    BigDecimal philHealthContribution

    //Bank Details
    String atmAccountNumber
    String bank

    //ID Picture
    byte[] picture

    static hasMany = [rates: Rate, assignmentHistories: AssignmentHistory]

    static transients = ['rate', 'fullName']

    static constraints = {
        picture(size:0..3000000,nullable:true)
        employeeNumber(blank: false)
        firstName(blank: false)
        lastName(blank: false)
        middleInitials(blank: false)
        sex(blank: false)
        currentPosition(blank: false)
        sssIdNumber(blank: false)
        hdmfIdNumber(blank: false)
        philHealthIDNumber(blank: false)
        taxIdentificationNumber(blank: false)
        birthDate(nullable: true)
        employmentDate(nullable: true)
        currentAssignment(nullable: true)
        resCertIssueDate(nullable: true)
        grpNumber(blank: false)
        taxStatus(nullable: true)

        address(nullable: true)
        telephoneNumber(nullable: true)
        mobileNumber(nullable: true)
        provincialAddress(nullable: true)
        //house type

        emergencyContactPerson(nullable: true)
        emergencyContactAddress(nullable: true)
        emergencyContactNumber(nullable: true)

        religion(nullable: true)

        fathersName(nullable: true)
        mothersName(nullable: true)
        spouseName(nullable: true)
        height(nullable: true)
        weight(nullable: true)
        citizenship(nullable: true)
        resCertNo(nullable: true)
        resCertIssueDate(nullable: true)
        resCertIssuePlace(nullable: true)
        sssContribution(nullable: true)
        philHealthContribution(nullable: true)
        remarks(maxSize: 4000, nullable: true, blank: true)

        bank(nullable: true, blank: true)
        atmAccountNumber(blank: true, nullable: true)



    }

    static mapping = {
        sort lastName: 'asc'
        assignmentHistories sort: 'startDate', order:  'desc'
        rates sort: 'effectivityDate', order: 'desc'
        cache true
    }


    String getFullName() {
        firstName + middleInitials + lastName
    }

    Rate getRate() {
        rates ? rates.last() : null
    }

    String toString() {
        "${lastName}, ${firstName} ${middleInitials}"
    }
}
