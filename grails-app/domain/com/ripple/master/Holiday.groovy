package com.ripple.master

import org.joda.time.LocalDate

class Holiday {
    LocalDate holidayDate
    String description

    Boolean legalHoliday //true: legal holiday. false: special holiday;
    
    static hasMany=[applicableSites:AssignmentLocation]

    static mapping = {
        cache true
        applicableSites lazy: false
    }


    static constraints = {
        holidayDate(nullable:false);
        description(blank:false);
        legalHoliday(nullable:false);
    }

}
