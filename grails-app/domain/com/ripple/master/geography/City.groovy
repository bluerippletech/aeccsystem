package com.ripple.master.geography

import com.ripple.master.Base

class City extends Base {

    String name
	static belongsTo = [province:Province]
	
	static constraints = {
		name(nullable:false, blank:false)
	}
	
	static mapping = {
		id generator:'uuid'
		cache true
		locations sort:'name', order:'asc'
	}
	
    String toString(){
        name
    }
}
