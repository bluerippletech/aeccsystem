package com.ripple.master.geography

import com.ripple.master.Base

class Province extends Base {
    
    String name
    static hasMany = [cities:City]

	static constraints = {
		name(nullable:false, blank:false)
	}
	
	static mapping = {
		id generator:'uuid'
		cache true
		cities sort:'name', order:'asc'
	}
	
    String toString(){
        name
    }
}
