package com.ripple.master.geography

import com.ripple.master.Base

class Country extends Base {
    
    String name
	
	static constraints = {
		name(nullable:false, blank:false)
	}
	
	static mapping = {
		id generator:'uuid'
		cache true
		regions sort:'name', order:'asc'
	}
	
    String toString(){
        name
    }
}
