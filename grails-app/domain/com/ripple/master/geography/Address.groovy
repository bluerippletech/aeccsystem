package com.ripple.master.geography

import com.ripple.master.Base

//@gorm.AuditStamp
class Address extends Base {

    String		address1
    String		address2
    City		city
    Province	province
//    Region		region
//    Country		country
    String		zipCode
	String		telephoneNo
	String		faxNo
//	String		ermAddress

    static constraints = {
        address1(nullable:false, blank:false)
        address2(nullable:true)
		city(nullable:true)
		province(nullable:true)
//		region(nullable:true)
//		country(nullable:true)
        zipCode(nullable:true)
		telephoneNo(nullable:true)
		faxNo(nullable:true)
//		ermAddress(nullable:true)
    }

    String toString(){
		def output = ''
		if (address1)	{ output += address1 + ", " }
		if (address2)	{ output += address2 + ", " }
		if (city)		{ output += city.toString() + ", " }
        if (province)	{ output += province.toString() + ", " }
//		if (country)	{ output += country.toString() + " " }
		if (zipCode)	{ output += zipCode }
		if (output) {
			output
		} else {
			''
		}
    }
}
