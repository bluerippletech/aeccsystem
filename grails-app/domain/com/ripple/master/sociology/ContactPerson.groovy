package com.ripple.master.sociology

import com.ripple.master.Base

//@gorm.AuditStamp
class ContactPerson extends Base {

    String	contactName
    String	position
    String	department
    String	directLine
	String	otherPhone
	String	faxNo
    String	mobileNo
    String	emailAddress
//    String	signatory="No"
//	String	officer="No"

    static constraints = {
        contactName(nullable:false)
        position(nullable:true)
        department(nullable:true)
        directLine(nullable:false)
		otherPhone(nullable:true)
        mobileNo(nullable:true)
        emailAddress(nullable:true, email:true)
//        signatory(inList:["Yes","No"])
//		officer(inList:["Yes","No"])
    }

	String toString() {
		def output = contactName
        if (position || department || emailAddress) {
            output += " - "
            if (position) {
                output += position
                if (department) {
                    output += ", " + department
                }
                output += " "
            }
            if (department) {
                output += department
                if (emailAddress) {
                    output += " "
                }
            }
            if (emailAddress) {
                output += "(" + emailAddress + ")"
            }
        }
//		if ("Yes".equals(signatory)){
//			output += "[Signatory] "
//		}
//		if ("Yes".equals(officer)){
//			output += "[Officer] "
//		}
		output
	}
	
	String toStringContactName(){
		def output = contactName
		output	
    }
}
