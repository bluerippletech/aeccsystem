package com.ripple.master

import com.ripple.project.Expense
import com.ripple.project.EstimateItem

class Assignment {
    String projectName
    String projectAddress=""
    String description
    Boolean active=true

    static hasMany = [locations:AssignmentLocation,
                      collections:com.ripple.project.Collection,
                      expenses:Expense,
                      estimateItems:EstimateItem,
                      warehouses: Warehouse]

    static constraints = {
        projectName(blank:false,unique:true)
        description(blank:false)
    }

    String toString(){
        projectName
    }

    static mapping = {
        locations sort:"location"
        cache true
    }
}
