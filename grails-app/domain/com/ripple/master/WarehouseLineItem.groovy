package com.ripple.master

import com.ripple.master.item.Item
import com.ripple.master.item.LevelUnit

class WarehouseLineItem extends Base{

    Item item
    LevelUnit unitOfMeasure
    Long quantity = 0
//    BigDecimal unitPrice = 0.00

    static belongsTo = [warehouse: Warehouse]

    static constraints = {
        item(blank: false);
        unitOfMeasure(blank: false, nullable: false)
        quantity(nullable:false, validator:{
            if(it.toLong() < 1){
                return ['min.error', 1]
            }
        })
    }
}
