package com.ripple.master

class AssignmentLocation {


    String location


    static belongsTo = [assignment: Assignment]

    static constraints = {
        location(blank: false)
    }

    static transients = ['projectAndLocation']

    String getProjectAndLocation() {
        assignment?.toString() + " - " + location
    }

    static mapping = {
        cache true
    }

    String toString() {
        location
    }
}
