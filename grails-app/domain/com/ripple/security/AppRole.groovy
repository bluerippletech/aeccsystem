package com.ripple.security

class AppRole {

	String authority
    String roleName;

	static mapping = {
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
        roleName(blank: false)
	}

    public String toString(){
        return roleName;
    }
}
