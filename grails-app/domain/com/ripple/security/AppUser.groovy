package com.ripple.security

class AppUser {

	transient springSecurityService

	String username
	String password
    String email
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static constraints = {
		username blank: false, unique: true
		password blank: false
        email(blank: false, unique: true, email: true)
	}

	static mapping = {
		password column: '`password`'
	}

	Set<AppRole> getAuthorities() {
		AppUserAppRole.findAllByAppUser(this).collect { it.appRole } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

    String toString(){
        username
    }
}
