package com.ripple.receiving

import com.ripple.master.Base
import com.ripple.purchasing.PurchaseOrder
import org.joda.time.LocalDate
import com.ripple.security.AppUser
import com.ripple.master.DocumentNumbers


class ReceivingReport extends Base{

    def documentNumbersService

    public static final String Draft = "Draft"
    public static final String ForApproval = "For Approval"
    public static final String Cancelled = "Cancelled"
    public static final String Approved = "Approved"
    public static final String Posted = "Posted"

    String receivingReportNumber = ("0").padLeft(7,'0')
    PurchaseOrder purchaseOrder
    String invoiceNumber
    LocalDate dateStarted = new LocalDate()
    LocalDate dateClosed = new LocalDate()
    LocalDate dateCheckedPurchaseOrder
    LocalDate dateCheckedSupplierInvoice
    AppUser purchaseOrderCheckedBy
    AppUser supplierInvoiceCheckedBy
    String status = 'Draft'
    BigDecimal totalAmount = 0.00

    static hasMany = [lineItems: ReceivingReportLineItem]

    static constraints = {
        receivingReportNumber(nullable: false, blank: false)
        purchaseOrder(nullable: false, blank: false)
        invoiceNumber(nullable: true, blank: true, validator: { val, obj ->
            if(obj.status==ForApproval){
                if(!val) return ['receivingReport.invoiceNumber.blank.error']
            }
        })
        dateStarted(nullable: false, blank: false)
        dateClosed(nullable: false, blank: false)
        status(nullable: false, maxSize: 255, inList: [Draft, ForApproval, Cancelled, Approved, Posted])
        totalAmount(nullable: false)
        dateCheckedPurchaseOrder(nullable: true)
        dateCheckedSupplierInvoice(nullable: true)
        purchaseOrderCheckedBy(nullable: true, validator: { val, obj ->
            if(obj.status==ForApproval){
                if(!val) return ['receivingReport.checkedPurchaseOrder.blank.error']
            }
        })
        supplierInvoiceCheckedBy(nullable: true, validator: { val, obj ->
            if(obj.status==ForApproval){
                if(!val) return ['receivingReport.checkedSupplierInvoice.blank.error']
            }
        })
    }

    def beforeInsert = {
        if (("0").padLeft(7,'0').equals(receivingReportNumber)) {
            DocumentNumbers.withNewSession {
                receivingReportNumber = documentNumbersService.generateDocumentNumber("ReceivingReport")
            }
        }
    }

    def computeTotals = {
        def receivingReportItems = lineItems.findAll()
        if(receivingReportItems.size() > 0){
            totalAmount = receivingReportItems?.totalItemAmount?.sum()
        }
    }

    String toString() {
        receivingReportNumber
    }
}
