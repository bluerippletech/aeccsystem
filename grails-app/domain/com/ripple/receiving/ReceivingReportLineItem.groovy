package com.ripple.receiving

import com.ripple.master.Base
import com.ripple.master.item.Item
import com.ripple.master.item.LevelUnit

class ReceivingReportLineItem extends Base{

    Item item
    LevelUnit unitOfMeasure
    Long quantity = 0
    Long baseQuantity = 0
    Long quantityRemaining
    BigDecimal unitPrice

    static belongsTo = [receivingReport: ReceivingReport]

    static constraints = {
        unitOfMeasure(nullable: false)
        quantity(nullable: false)
        quantityRemaining(nullable: false)
        unitPrice(nullable: false)
        item(blank: false)
    }

    def unitOfMesurementService
    def beforeUpdate = {
        baseQuantity = unitOfMesurementService.convertToBase(quantity, unitOfMeasure, item.id);
    }

    Long getRemainingQuantity() {
        def remainingQuantity = quantityRemaining ?: 0
        def quantity = quantity ?: 0
        remainingQuantity - quantity
    }

    BigDecimal getTotalItemAmount() {
        def cost = unitPrice ?: 0
        def qty = quantity ?: 0
        cost * qty
    }

}