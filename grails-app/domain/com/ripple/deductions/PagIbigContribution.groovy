package com.ripple.deductions

class PagIbigContribution {

    BigDecimal lowRange
    BigDecimal highRange
    BigDecimal employeeMultiplier
    BigDecimal employerMultiplier

    static constraints = {
        lowRange(scale:2, overlap:false)
        highRange(scale:2, overlap:false, highRange:true)
        employeeMultiplier(scale:2)
        employeeMultiplier(scale:2)
    }

    static mapping = {
        cache true
    }

}
