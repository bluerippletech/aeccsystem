package com.ripple.deductions

class WithholdingTaxPeriod implements Comparable {

    public static final String DAILY = "Daily"
    public static final String WEEKLY = "Weekly"
    public static final String SEMIMONTHLY = "Semi-Monthly"
    public static final String MONTHLY = "Monthly"
    
    String name
    SortedSet brackets, statuses

    static hasMany = [brackets:WithholdingTaxBracket, statuses:WithholdingTaxStatus]

    static mapping = {
        cache true
    }


    static constraints = {
        name(unique:true, inList:[DAILY, WEEKLY, SEMIMONTHLY, MONTHLY])
    }

     String toString(){
        name
    }

    int compareTo(o) {
        this.toString().compareTo(o?.toString())
    }
}
