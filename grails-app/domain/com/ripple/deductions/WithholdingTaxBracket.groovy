package com.ripple.deductions

class WithholdingTaxBracket implements Comparable {

    String name
    BigDecimal baseTax
    BigInteger percentOver
    SortedSet exemptions

    static belongsTo = [period:WithholdingTaxPeriod]
    static hasMany = [exemptions:WithholdingTaxExemption]

    static constraints = {
        name(blank:false, unique:['period'])
        baseTax(scale:2)
        percentOver(max:new BigInteger(32))
    }

    static mapping = {
        cache true
    }

    static transients = ['percentOverMultiplier']

    BigDecimal getPercentOverMultiplier() {
        new BigDecimal(percentOver, 2)
    }

    List statuses() {
		return exemptions.collect{it.status}
	}

	List addToStatuses(WithholdingTaxStatus status) {
		WithholdingTaxExemption.link(status, this)
		return statuses()
	}

	List removeFromStatuses(WithholdingTaxBracket bracket) {
		WithholdingTaxExemption.unlink(status, this)
		return statuses()
	}

    String toString(){
        "Bracket ${name}"
    }

    int compareTo(o) {
        this.toString().compareTo(o?.toString())
    }
}
