package com.ripple.deductions

class PhilHealthContribution {

    BigDecimal lowRange
    BigDecimal highRange
    BigDecimal salaryBase
    BigDecimal employeeShare  //EE
    BigDecimal employerShare  //ER

    // Transients
    String getMonthlySalaryRange() {
        "Php ${lowRange} - Php ${highRange}"
    }
    BigDecimal getTotalPhilHealthContribution() {
        employerShare + employeeShare  //EE + ER
    }
    BigDecimal getTotalContribution() {
        employerShare + employeeShare  //EE + ER
    }

    static transients = ['monthlySalaryRange', 'totalPhilHealthContribution',
        'totalContribution']

    static mapping = {
        cache true
    }

    static constraints = {
        lowRange(scale:2, overlap:false)
        highRange(scale:2, overlap:false, highRange:true)
        salaryBase(scale:2,
            validator: { value, object ->
                value >= object.getLowRange() && value <= object.getHighRange() ?:
                ['not.within.range.error', object.getLowRange(), object.getHighRange()]
            })
        employeeShare(scale:2)
        employerShare(scale:2)
    }

    String toString(){
        "${monthlySalaryRange}"
    }

}
