package com.ripple.deductions

class WithholdingTaxStatus implements Comparable {
    
    String name
    BigInteger dependents
    SortedSet exemptions

    static belongsTo = [period:WithholdingTaxPeriod]
    static hasMany = [brackets:WithholdingTaxBracket, exemptions:WithholdingTaxExemption]

    static constraints = {
        name(unique:['period'])
        dependents(range:0..4)
    }

    static mapping = {
        cache true
    }
    
    List brackets() {
		return exemptions.collect{it.bracket}
	}

	List addToBrackets(WithholdingTaxBracket bracket) {
		WithholdingTaxExemption.link(this, bracket)
		return brackets()
	}

	List removeFromBrackets(WithholdingTaxBracket bracket) {
		WithholdingTaxExemption.unlink(this, bracket)
		return brackets()
	}

    String toString(){
        name
    }

    int compareTo(o) {
        this.toString().compareTo(o?.toString())
    }
}
