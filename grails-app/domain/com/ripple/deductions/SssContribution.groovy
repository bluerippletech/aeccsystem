package com.ripple.deductions

class SssContribution {

    BigDecimal lowRange
    BigDecimal highRange
    BigDecimal monthlySalaryCredit
    BigDecimal employeeCompensation  //EC (Employer)
    BigDecimal employeeContribution  //EE (Employee)
    BigDecimal employerContribution  //ER (Employer)
    
    // Transients
    String getRangeOfCompensation() {
        "Php ${lowRange}  - Php ${highRange}"
    }
    BigDecimal getTotalEmployerContribution() {
        employerContribution + employeeCompensation  //ER + EC
    }
    BigDecimal getTotalEmployeeContribution() {
        employeeContribution  //EE
    }
    BigDecimal getTotalSssContribution() {
        employerContribution + employeeContribution  //ER + EE
    }
    BigDecimal getTotalContribution() {
        employerContribution + employeeContribution + employeeCompensation  // ER + EE + EC
    }
    BigDecimal getVoluntaryMemberContribution() {
        employerContribution + employeeContribution  //ER + EE
    }

    static mapping = {
        cache true
        sort lowRange:'asc'
    }

    static transients = ['rangeOfCompensation', 'totalEmployerContribution',
        'totalEmployeeContribution', 'totalSssContribution','totalContribution',
        'voluntaryMemberContribution']

    static constraints = {
        lowRange(scale:2, overlap:false)
        highRange(scale:2, overlap:false, highRange:true)
        monthlySalaryCredit(scale:2,
            validator: { value, object ->
                value >= object.getLowRange() && value <= object.getHighRange() ?:
                ['not.within.range.error', object.getLowRange(), object.getHighRange()]
            })
        employeeCompensation(scale:2)
        employeeContribution(scale:2)
        employerContribution(scale:2)
    }

    String toString(){
        "${rangeOfCompensation}"
    }
    
}
