package com.ripple.deductions

class WithholdingTaxExemption implements Comparable {
    
    BigDecimal value
    WithholdingTaxStatus status
    WithholdingTaxBracket bracket

    static belongsTo = [status:WithholdingTaxStatus, bracket:WithholdingTaxBracket]

    static mapping = {
        cache true
    }

    static constraints = {
        status(nullable:false)
        bracket(nullable:false)
        value(scale:2, unique:['status', 'bracket'])
    }

    static WithholdingTaxExemption link(status, bracket) {
		def e = WithholdingTaxExemption.findByStatusAndBracket(status, bracket)
		if (!e)
		{
			e = new WithholdingTaxExemption()
			status?.addToExemptions(e)
            bracket?.addToExemptions(e)
			e.save()
		}
		return e
	}

	static void unlink(bracket, status) {
		def e = WithholdingTaxExemption.findByStatusAndBracket(status, bracket)
		if (e)
		{
			status?.removeFromExemptions(e)
            bracket?.removeFromExemptions(e)
			e.delete()
		}
	}

    String toString(){
        "P${value}"
    }

    int compareTo(o) {
        if (!this.status.equals(o?.status))
            return this.status.compareTo(o?.status)
        if (!this.bracket.equals(o?.bracket))
            return this.bracket.compareTo(o?.bracket)
        if (this.value > o?.value)
            return 1
        else if (this.value < o?.value)
            return -1
        return 0
    }
}
