<%@ page import="com.ripple.master.AssignmentLocation" %>



<div class="fieldcontain ${hasErrors(bean: assignmentLocationInstance, field: 'location', 'error')} required">
	<label for="location">
		<g:message code="assignmentLocation.location.label" default="Location" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="location" required="" value="${assignmentLocationInstance?.location}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: assignmentLocationInstance, field: 'assignment', 'error')} required">
	<label for="assignment">
		<g:message code="assignmentLocation.assignment.label" default="Assignment" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="assignment" name="assignment.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" required="" value="${assignmentLocationInstance?.assignment?.id}" class="many-to-one"/>
</div>

