<%@ page import="com.ripple.procurement.MaterialIssuanceClass" %>



<div class="fieldcontain ${hasErrors(bean: materialIssuanceClassInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="materialIssuanceClass.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" maxlength="30" required="" value="${materialIssuanceClassInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceClassInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="materialIssuanceClass.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${materialIssuanceClassInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceClassInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="materialIssuanceClass.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="status" from="${materialIssuanceClassInstance.constraints.status.inList}" required="" value="${materialIssuanceClassInstance?.status}" valueMessagePrefix="materialIssuanceClass.status"/>
</div>

