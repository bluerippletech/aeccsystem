<%@ page import="com.ripple.materialIssuance.MaterialIssuanceLineItem" %>
<!doctype html>
<html>
	<head>
        <meta charset="utf-8" name="layout" content="main"/>
		<g:set var="entityName" value="${message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <r:require modules="all"/>
	</head>
	<body>
    <div class="row-fluid">
        <div class="span1 action-btn round-all">
            <a href="${createLink(uri: '/')}">
                <div><i class="icon-home"></i></div>
                <div><strong><g:message code="default.home.label"/></strong></div>
            </a>
        </div>
        <div class="span1 action-btn round-all">
            <a href="${createLink(action: 'list')}">
                <div><i class="icon-align-justify"></i></div>
                <div><strong>List</strong></div>
            </a>
        </div>
    </div>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            ${flash.message}
        </div>
    </g:if>
    <g:hasErrors bean="${materialIssuanceLineItemInstance}">
        <div class="alert alert-error">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            Validation has failed. Please correct your inputs.
            <ul>
                <g:eachError bean="${materialIssuanceLineItemInstance}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </div>
    </g:hasErrors>
    <div class="row-fluid">
        <div class="span12 column" id="create-materialIssuanceLineItem" role="main">
            <!-- Portlet: Form Control States -->
            <div class="box" id="box-0">
                <h4 class="box-header round-top"><g:message code="default.create.label" args="[entityName]" />
                    <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                    <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                    <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
                </h4>
                <div class="box-container-toggle">
                    <div class="box-content">
                        <g:form class="form-horizontal" action="save" >
                            <fieldset>
                                <legend><g:message code="default.create.label" args="[entityName]" /></legend>
                                <f:all bean="materialIssuanceLineItemInstance"/>
                                <div class="form-actions">
                                    <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                                    <g:submitButton name="cancel" class="btn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />
                                </div>
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div><!--/span-->

        </div>
    </div>
	</body>
</html>
