<%@ page import="com.ripple.materialIssuance.MaterialIssuanceLineItem" %>



<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="materialIssuanceLineItem.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${materialIssuanceLineItemInstance.constraints.isDeleted.inList}" value="${materialIssuanceLineItemInstance?.isDeleted}" valueMessagePrefix="materialIssuanceLineItem.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="materialIssuanceLineItem.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${materialIssuanceLineItemInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="materialIssuanceLineItem.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${materialIssuanceLineItemInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="materialIssuanceLineItem.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${materialIssuanceLineItemInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="materialIssuanceLineItem.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${materialIssuanceLineItemInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'item', 'error')} required">
	<label for="item">
		<g:message code="materialIssuanceLineItem.item.label" default="Item" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="item" name="item.id" from="${com.ripple.master.item.Item.list()}" optionKey="id" required="" value="${materialIssuanceLineItemInstance?.item?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'unitOfMeasure', 'error')} required">
	<label for="unitOfMeasure">
		<g:message code="materialIssuanceLineItem.unitOfMeasure.label" default="Unit Of Measure" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="unitOfMeasure" name="unitOfMeasure.id" from="${com.ripple.master.item.LevelUnit.list()}" optionKey="id" required="" value="${materialIssuanceLineItemInstance?.unitOfMeasure?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'quantity', 'error')} required">
	<label for="quantity">
		<g:message code="materialIssuanceLineItem.quantity.label" default="Quantity" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="quantity" required="" value="${fieldValue(bean: materialIssuanceLineItemInstance, field: 'quantity')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceLineItemInstance, field: 'issuance', 'error')} required">
	<label for="issuance">
		<g:message code="materialIssuanceLineItem.issuance.label" default="Issuance" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="issuance" name="issuance.id" from="${com.ripple.materialIssuance.MaterialIssuance.list()}" optionKey="id" required="" value="${materialIssuanceLineItemInstance?.issuance?.id}" class="many-to-one"/>
</div>

