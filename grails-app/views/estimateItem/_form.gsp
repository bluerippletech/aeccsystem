<%@ page import="com.ripple.project.EstimateItem" %>



<div class="fieldcontain ${hasErrors(bean: estimateItemInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="estimateItem.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project" name="project.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" required="" value="${estimateItemInstance?.project?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: estimateItemInstance, field: 'quantity', 'error')} required">
	<label for="quantity">
		<g:message code="estimateItem.quantity.label" default="Quantity" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="quantity" required="" value="${fieldValue(bean: estimateItemInstance, field: 'quantity')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: estimateItemInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="estimateItem.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="type" name="type.id" from="${com.ripple.project.ExpenseType.list()}" optionKey="id" required="" value="${estimateItemInstance?.type?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: estimateItemInstance, field: 'unitPrice', 'error')} required">
	<label for="unitPrice">
		<g:message code="estimateItem.unitPrice.label" default="Unit Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="unitPrice" required="" value="${fieldValue(bean: estimateItemInstance, field: 'unitPrice')}"/>
</div>

