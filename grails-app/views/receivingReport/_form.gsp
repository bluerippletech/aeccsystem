<%@ page import="com.ripple.receiving.ReceivingReport" %>



<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="receivingReport.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${receivingReportInstance.constraints.isDeleted.inList}" value="${receivingReportInstance?.isDeleted}" valueMessagePrefix="receivingReport.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="receivingReport.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${receivingReportInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="receivingReport.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${receivingReportInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="receivingReport.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${receivingReportInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="receivingReport.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${receivingReportInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'receivingReportNumber', 'error')} required">
	<label for="receivingReportNumber">
		<g:message code="receivingReport.receivingReportNumber.label" default="Receiving Report Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="receivingReportNumber" required="" value="${receivingReportInstance?.receivingReportNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'purchaseOrder', 'error')} required">
	<label for="purchaseOrder">
		<g:message code="receivingReport.purchaseOrder.label" default="Purchase Order" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="purchaseOrder" name="purchaseOrder.id" from="${com.ripple.purchasing.PurchaseOrder.list()}" optionKey="id" required="" value="${receivingReportInstance?.purchaseOrder?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'invoiceNumber', 'error')} required">
	<label for="invoiceNumber">
		<g:message code="receivingReport.invoiceNumber.label" default="Invoice Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="invoiceNumber" required="" value="${receivingReportInstance?.invoiceNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'dateStarted', 'error')} required">
	<label for="dateStarted">
		<g:message code="receivingReport.dateStarted.label" default="Date Started" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="dateStarted" value="${receivingReportInstance?.dateStarted}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'dateClosed', 'error')} required">
	<label for="dateClosed">
		<g:message code="receivingReport.dateClosed.label" default="Date Closed" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="dateClosed" value="${receivingReportInstance?.dateClosed}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="receivingReport.status.label" default="Status" />
		
	</label>
	<g:select name="status" from="${receivingReportInstance.constraints.status.inList}" value="${receivingReportInstance?.status}" valueMessagePrefix="receivingReport.status" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'totalAmount', 'error')} required">
	<label for="totalAmount">
		<g:message code="receivingReport.totalAmount.label" default="Total Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="totalAmount" required="" value="${fieldValue(bean: receivingReportInstance, field: 'totalAmount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportInstance, field: 'lineItems', 'error')} ">
	<label for="lineItems">
		<g:message code="receivingReport.lineItems.label" default="Line Items" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${receivingReportInstance?.lineItems?}" var="l">
    <li><g:link controller="receivingReportLineItem" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="receivingReportLineItem" action="create" params="['receivingReport.id': receivingReportInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'receivingReportLineItem.label', default: 'ReceivingReportLineItem')])}</g:link>
</li>
</ul>

</div>

