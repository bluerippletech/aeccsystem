<%@ page import="com.ripple.receiving.ReceivingReport" %>
<!doctype html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'receivingReport.label', default: 'ReceivingReport')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <r:script>
        var dataTable;
        $('.rrLineItem').submit( function(e){
            e.preventDefault();
            $.ajax({
                url: "${createLink(controller: 'receivingReportLineItem', action: 'updateLineItems')}",
                type: "POST",
                data: $('.rrLineItem').serialize(),
                success:function(response){
                    dataTable.fnDraw();
                }
            })
        })
    </r:script>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="show-purchaseOrder" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="['Purchase Order']"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="purchaseOrderNumber-label" class="control-label">
                                            <g:message code="purchaseOrder.purchaseOrderNumber.label"
                                                       default="Purchase Order Number"/>
                                        </h5>

                                        <div class="controls" style="margin-top:4px">
                                            <a href="${createLink(controller: 'purchaseOrder', action: 'show', id: "${receivingReportInstance?.purchaseOrderId}")}">
                                                <span class="property-value"
                                                      aria-labelledby="purchaseOrderNumber-label">
                                                    <g:fieldValue bean="${receivingReportInstance}"
                                                                  field="purchaseOrder"/>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="supplier-label" class="control-label">
                                            <g:message code="purchaseOrder.supplier.label" default="Supplier"/>
                                        </h5>

                                        <div class="controls" style="margin-top:4px">
                                            <a href="${createLink(controller: 'supplier', action: 'show', id: "${receivingReportInstance?.purchaseOrder?.supplierId}")}">
                                                <span class="property-value" aria-labelledby="supplier-label">
                                                    ${receivingReportInstance?.purchaseOrder?.supplier?.toString()}
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="warehouse-label" class="control-label">
                                            <g:message code="purchaseOrder.warehouse.label" default="Warehouse"/>
                                        </h5>

                                        <div class="controls" style="margin-top:4px">
                                            <a href="${createLink(controller: 'warehouse', action: 'show', id: "${receivingReportInstance?.purchaseOrder?.warehouseId}")}">
                                                <span class="property-value"
                                                      aria-labelledby="receivingReportNumber-label">
                                                    ${receivingReportInstance?.purchaseOrder?.warehouse?.toString()}
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="span6 column">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="purchaseOrderCreated-label" class="control-label">
                                            <g:message code="purchaseOrder.dateStarted.label" default="Date Created"/>
                                        </h5>

                                        <div class="controls" style="margin-top:4px">
                                            <span class="property-value" aria-labelledby="purchaseOrderCreated-label">
                                                ${com.ripple.util.Formatter?.formatLocalDate(receivingReportInstance?.purchaseOrder?.dateStarted, 'MM/dd/yyyy')}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="purchaseOrderClosed-label" class="control-label">
                                            <g:message code="purchaseOrder.dateClosed.label" default="Date Closed"/>
                                        </h5>

                                        <div class="controls" style="margin-top:4px">
                                            <span class="property-value" aria-labelledby="purchaseOrderClosed-label">
                                                ${com.ripple.util.Formatter?.formatLocalDate(receivingReportInstance?.purchaseOrder?.dateClosed, 'MM/dd/yyyy')}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12" id="edit-receivingReport" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="receivingReportNumber-label" class="control-label"><g:message
                                                code="receivingReport.receivingReportNumber.label"
                                                default="Receiving Report Number"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="receivingReportNumber-label"><g:fieldValue
                                                    bean="${receivingReportInstance}"
                                                    field="receivingReportNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="invoiceNumber-label" class="control-label"><g:message
                                                code="receivingReport.invoiceNumber.label"
                                                default="Invoice Number"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="invoiceNumber-label"><g:fieldValue
                                                    bean="${receivingReportInstance}" field="invoiceNumber"/></span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="status-label" class="control-label"><g:message
                                                code="receivingReport.status.label"
                                                default="Status"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                    bean="${receivingReportInstance}" field="status"/></span>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="dateStarted-label" class="control-label"><g:message
                                                code="receivingReport.dateStarted.label"
                                                default="Date Started"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="dateStarted-label"><g:fieldValue
                                                    bean="${receivingReportInstance}" field="dateStarted"/></span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="dateClosed-label" class="control-label"><g:message
                                                code="receivingReport.dateClosed.label"
                                                default="Date Closed"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="dateClosed-label"><g:fieldValue
                                                    bean="${receivingReportInstance}" field="dateClosed"/></span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="createdBy-label" class="control-label"><g:message
                                                code="receivingReport.createdBy.label"
                                                default="Created By"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="createdBy-label"><g:fieldValue
                                                    bean="${receivingReportInstance}" field="createdBy"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="checked-label" class="control-label"><g:message
                                                code="receivingReport.checked.label"
                                                default="Created By"/></h5>

                                        <div class="controls" style="margin-top:4px">
                                            <g:if test="${receivingReportInstance?.purchaseOrderCheckedBy}">
                                                <h5>Purchase Order</h5>
                                                By: ${receivingReportInstance?.purchaseOrderCheckedBy}<br/>
                                                Date: ${com.ripple.util.Formatter?.formatLocalDate(receivingReportInstance?.dateCheckedPurchaseOrder, 'MM/dd/Y')}
                                            </g:if>
                                            <g:if test="${receivingReportInstance?.supplierInvoiceCheckedBy}">
                                                <h5>Supplier Invoice</h5>
                                                By: ${receivingReportInstance?.supplierInvoiceCheckedBy}<br/>
                                                Date: ${com.ripple.util.Formatter?.formatLocalDate(receivingReportInstance?.dateCheckedSupplierInvoice, 'MM/dd/Y')}
                                            </g:if>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>

                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${receivingReportInstance?.id}"/>

                        %{--<g:if test="${receivingReportInstance?.status!='Approved'}">--}%
                            <g:actionSubmit class="btn btn-primary" action="edit"
                                            value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_USER">
                                <input type="hidden" name="version" value="${receivingReportInstance?.version}"/>
                                <g:actionSubmit class="btn btn-danger" action="formStatus"
                                                value="${message(code: 'receivingReport.cancel.label', default: 'Cancel')}"/>
                                <g:if test="${receivingReportInstance?.status == receivingReportInstance.Draft}">
                                    <g:actionSubmit class="btn btn-success" action="formStatus"
                                                    value="${message(code: 'receivingReport.forApproval.label', default: 'For Approval')}"/>
                                </g:if>
                            </sec:ifAnyGranted>

                            <sec:ifAnyGranted roles="ROLE_ADMIN">
                                <g:if test="${receivingReportInstance?.status == receivingReportInstance.ForApproval}">
                                    <g:actionSubmit class="btn btn-primary" action="formStatus"
                                                    value="${message(code: 'receivingReport.disapproved.label', default: 'Send back to draft')}"/>
                                    <g:actionSubmit class="btn btn-success" action="formStatus"
                                                    value="${message(code: 'receivingReport.approved.label', default: 'Approve')}"/>
                                </g:if>
                                <g:if test="${receivingReportInstance?.status == receivingReportInstance.Approved}">
                                    <g:actionSubmit class="btn btn-success" action="formStatus"
                                                    value="${message(code: 'receivingReport.approved.label', default: 'Post')}"/>
                                </g:if>
                            </sec:ifAnyGranted>
                        %{--</g:if>--}%
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12" id="list-receivingReportLineItem" role="main">
        <div class="box" id="box-2">
            <h4 class="box-header round-top"><g:message code="default.list.label"
                                                        args="['Receiving Report Line Item']"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <g:form controller="receivingReportLineItem" class="rrLineItem">
                        <table cellpadding="0" cellspacing="0" border="0"
                               class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                            <thead>
                            <tr>

                                <th><g:message code="receivingReportLineItem.itemCode.label" default="Item Code"/></th>

                                <th><g:message code="receivingReportLineItem.itemName.label" default="Item Name"/></th>

                                <th><g:message code="receivingReportLineItem.balanceQuantity.label"
                                               default="Balance Quantity"/></th>

                                <th><g:message code="receivingReportLineItem.unitOfMeasure.label"
                                               default="Unit Of Measure"/></th>

                                <th><g:message code="receivingReportLineItem.receiptQuantity.label"
                                               default="Receipt Quantity"/></th>

                                <th><g:message code="receivingReportLineItem.receiptAmount.label"
                                               default="Receipt Amount"/></th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <div class="row-fluid">
                            <div class="span12" style="text-align: right;margin-top:10px">
                                <g:if test="${receivingReportInstance.status == receivingReportInstance.Draft || sec.ifAnyGranted(roles: 'ROLE_ADMIN')}">
                                    <g:actionSubmit class="btn btn-primary" action="updateLineItems"
                                                    value="${message(code: 'receivingReportLineItem.button.update.label', default: 'Update Line Item')}"/>
                                </g:if>
                                <a class="jasperButton btn btn-info" title="PDF"
                                   href="${createLink(controller: 'report', params: [_format: 'PDF', _name: 'Receiving Report', RECEIVING_REPORT_ID: "${receivingReportInstance?.id}", _file: 'receivingreport'])}">
                                    Print Receiving Report
                                </a>
                            </div>
                        </div>
                    </g:form>
                    <g:if test="${!receivingReportInstance?.dateCheckedPurchaseOrder || !receivingReportInstance?.dateCheckedSupplierInvoice}">
                        <div class="row-fluid">
                            <div class="span9">
                                &nbsp;
                            </div>

                            <div class="span3" style="border:1px solid #DDD;padding:10px;margin-top:10px;"
                                 id="checking">
                                <g:form class="form-horizontal" id="${receivingReportInstance?.id}">
                                    <g:if test="${!receivingReportInstance?.dateCheckedPurchaseOrder}">
                                        <label class="checkbox">
                                            <g:checkBox name="checkedPurchaseOrder"/> Checked w/ Purchase Order
                                        </label>
                                    </g:if>
                                    <g:if test="${!receivingReportInstance?.dateCheckedSupplierInvoice}">
                                        <label class="checkbox">
                                            <g:checkBox name="checkedSupplierInvoice"/> Checked w/ Supplier Invoice
                                        </label>
                                    </g:if>
                                    <g:actionSubmit value="Save" action="update" class="btn btn-primary"/>
                                </g:form>
                            </div>
                        </div>
                    </g:if>
                    <script type="text/javascript">
                        $(function () {
                            dataTable = $('#datatable').dataTable({
                                bProcessing:true,
                                bServerSide:true,
                                iDisplayLength:10,
                                sAjaxSource:'${createLink(controller:'receivingReportLineItem', action:'listJSON', id: receivingReportInstance?.id)}',
                                "sDom":"<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType":"bootstrap",
                                "oLanguage":{
                                    "sLengthMenu":"_MENU_ records per page"
                                },
                                "aoColumnDefs":[

                                    { "sName":"item", "aTargets":[ 0 ] },

                                    { "sName":"item", "aTargets":[ 1 ] },

                                    { "sName":"quantityRemaining", "aTargets":[ 2 ] },

                                    { "sName":"unitOfMeasure", "aTargets":[ 3 ] },

                                    { "sName":"quantity", "aTargets":[ 4 ] },

                                    { "bSortable":false, "aTargets":[ 5 ] }


                                ],
                                "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                                    <g:if test="${receivingReportInstance.status=='Draft' || sec.ifAnyGranted(roles: 'ROLE_ADMIN')}">
                                    $('td:eq(4)', nRow).append('<input type="number" class="input-mini" id="' + nRow.id + '" name="' + nRow.id + '" value="' + aData.quantity + '" step="0.01"/>');
                                    </g:if>
                                    <g:else>
                                    $('td:eq(4)', nRow).append('' + aData.quantity + '');
                                    </g:else>
                                }
                            });

                        })
                        function submitForm(elem) {
                            if (confirm("Are you sure?"))
                                $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
