<%@ page import="com.ripple.deductions.WithholdingTaxBracket" %>



<div class="fieldcontain ${hasErrors(bean: withholdingTaxBracketInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="withholdingTaxBracket.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${withholdingTaxBracketInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxBracketInstance, field: 'baseTax', 'error')} required">
	<label for="baseTax">
		<g:message code="withholdingTaxBracket.baseTax.label" default="Base Tax" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="baseTax" required="" value="${fieldValue(bean: withholdingTaxBracketInstance, field: 'baseTax')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxBracketInstance, field: 'percentOver', 'error')} required">
	<label for="percentOver">
		<g:message code="withholdingTaxBracket.percentOver.label" default="Percent Over" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="percentOver" max="32" required="" value="${fieldValue(bean: withholdingTaxBracketInstance, field: 'percentOver')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxBracketInstance, field: 'exemptions', 'error')} ">
	<label for="exemptions">
		<g:message code="withholdingTaxBracket.exemptions.label" default="Exemptions" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${withholdingTaxBracketInstance?.exemptions?}" var="e">
    <li><g:link controller="withholdingTaxExemption" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="withholdingTaxExemption" action="create" params="['withholdingTaxBracket.id': withholdingTaxBracketInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxBracketInstance, field: 'period', 'error')} required">
	<label for="period">
		<g:message code="withholdingTaxBracket.period.label" default="Period" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="period" name="period.id" from="${com.ripple.deductions.WithholdingTaxPeriod.list()}" optionKey="id" required="" value="${withholdingTaxBracketInstance?.period?.id}" class="many-to-one"/>
</div>

