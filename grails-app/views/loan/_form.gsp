<%@ page import="com.ripple.loans.Loan" %>



<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'loanDate', 'error')} required">
	<label for="loanDate">
		<g:message code="loan.loanDate.label" default="Loan Date" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="loanDate" value="${loanInstance?.loanDate}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'totalAmount', 'error')} required">
	<label for="totalAmount">
		<g:message code="loan.totalAmount.label" default="Total Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="totalAmount" min="1.00" required="" value="${fieldValue(bean: loanInstance, field: 'totalAmount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'monthlyAmortization', 'error')} required">
	<label for="monthlyAmortization">
		<g:message code="loan.monthlyAmortization.label" default="Monthly Amortization" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="monthlyAmortization" min="1.00" required="" value="${fieldValue(bean: loanInstance, field: 'monthlyAmortization')}"/>
</div>

%{--<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'remainingBalance', 'error')} required">--}%
	%{--<label for="remainingBalance">--}%
		%{--<g:message code="loan.remainingBalance.label" default="Remaining Balance" />--}%
		%{--<span class="required-indicator">*</span>--}%
	%{--</label>--}%
	%{--<g:field type="number" name="remainingBalance" required="" value="${fieldValue(bean: loanInstance, field: 'remainingBalance')}"/>--}%
%{--</div>--}%

<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'remarks', 'error')} ">
	<label for="remarks">
		<g:message code="loan.remarks.label" default="Remarks" />
		
	</label>
	<g:textArea name="remarks" cols="40" rows="5" maxlength="2000" value="${loanInstance?.remarks}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'loanType', 'error')} required">
	<label for="loanType">
		<g:message code="loan.loanType.label" default="Loan Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="loanType" name="loanType.id" from="${com.ripple.loans.LoanType.list()}" optionKey="id" required="" value="${loanInstance?.loanType?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="loan.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${com.ripple.master.Employee.list()}" optionKey="id" required="" value="${loanInstance?.employee?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanInstance, field: 'payments', 'error')} ">
	<label for="payments">
		<g:message code="loan.payments.label" default="Payments" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${loanInstance?.payments?}" var="p">
    <li><g:link controller="loanPayment" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="loanPayment" action="create" params="['loan.id': loanInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'loanPayment.label', default: 'LoanPayment')])}</g:link>
</li>
</ul>

</div>

