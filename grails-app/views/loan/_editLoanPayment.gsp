<div class="modal hide" id="editLoanPayment">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Add Loan Payment</h3>
    </div>
<div class="modal-body">
    <g:form class="form-horizontal" controller="loanPayment" action="update" method="post">
        <g:hiddenField id="editPaymentId" name="id" value="${loanPaymentInstance?.id}"/>
        <g:hiddenField id="editPaymentVersion" name="version" value="${loanPaymentInstance?.version}"/>
        <g:hiddenField name="loanId" value="${loanInstance.id}"/>
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="loan-label" class="control-label"><g:message
                        code="loanPayment.loan.label"
                        default="Loan"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="loan-label">
                        <input type="hidden" name="loan.id" value="${loanInstance?.id}"/>${loanInstance?.toString()}
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="payrollEntry-label" class="control-label"><g:message
                        code="loanPayment.payrollEntry.label"
                        default="Payroll Entry"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span id="editPaymentPayroll" class="property-value" aria-labelledby="payrollEntry-label">
                        %{--<g:select id="editPaymentPayroll" optionKey="id" from="${payrollEntries}" name="payrollEntry.id" value="${loanPaymentInstance?.payrollEntry?.id}" noSelection="['':'-Please select Payroll Entry-']"></g:select>--}%
                        ${loanPaymentInstance?.payrollEntry?.id?:'No Payroll Entry'}
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="paymentAmount-label" class="control-label"><g:message
                        code="loanPayment.amount.label"
                        default="Amount"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="paymentAmount-label">
                        <g:textField name="amount" id="editPaymentAmount" value="${fieldValue(bean: loanPaymentInstance, field: 'amount')}" />
                    </span>

                </div>
            </div>


            <div class="control-group">
                <h5 id="paymentDate-label" class="control-label"><g:message
                        code="loanPayment.paymentDate.label"
                        default="Payment Date"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="paymentDate-label">
                        <input name="paymentDate" class="input-xlarge datepicker" id="editPaymentDate" value="${fieldValue(bean: loanPaymentInstance, field: 'paymentDate')}" style="width:210px"/>

                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="paymentRemarks-label" class="control-label"><g:message
                        code="loanPayment.remarks.label"
                        default="Remarks"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="paymentRemarks-label">
                        <g:textArea id="editPaymentRemarks" name="remarks" rows="3" cols="1" value="${fieldValue(bean: loanPaymentInstance, field: 'remarks')}"/>
                    </span>

                </div>
            </div>

        </div>
</div>
        <div class="modal-footer">
            <g:actionSubmit class="btn btn-primary" action="update"
                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            <g:actionSubmit class="btn btn-danger" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            formnovalidate=""
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>

        </div>
    </g:form>
</div>

<r:script>
    function showEditPayment($id){
    $.get('${createLink(controller: 'loanPayment', action: 'editJSON')}',
    {id: $id},
    function(response){
        var payrollEntry = response.payroll;
        payrollEntry = payrollEntry.substring(1, payrollEntry.length-1);
        $('#editPaymentId').val(response.loanPayment.id)
        $('#editPaymentVersion').val(response.version)
        if(payrollEntry){
        $('#editPaymentPayroll').text(payrollEntry)}
        else{
        $('#editPaymentPayroll').text('No Payroll Entry')
        }
        $('#editPaymentAmount').val(response.loanPayment.amount)
        $('#editPaymentDate').val(response.paymentDate)
        $('#editPaymentRemarks').val(response.loanPayment.remarks)
        $('#editLoanPayment').modal('show');
    }, "json");

    }

</r:script>