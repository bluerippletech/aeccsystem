<div class="modal hide" id="addLoanPayment">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Add Loan Payment</h3>
    </div>
<div class="modal-body">
    <g:form class="form-horizontal" controller="loanPayment" action="save">
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="loan-label" class="control-label"><g:message
                        code="loanPayment.loan.label"
                        default="Loan"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="loan-label">
                        <input type="hidden" name="loan.id" value="${loanInstance?.id}"/>${loanInstance?.toString()}
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="payrollEntry-label" class="control-label"><g:message
                        code="loanPayment.payrollEntry.label"
                        default="Payroll Entry"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="payrollEntry-label">
                        <g:select optionKey="id" from="${payrollEntries}" name="payrollEntry.id" value="${loanPaymentInstance?.payrollEntry?.id}" noSelection="['':'-Please select Payroll Entry-']"></g:select>
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="paymentAmount-label" class="control-label"><g:message
                        code="loanPayment.amount.label"
                        default="Amount"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="paymentAmount-label">
                        <g:textField name="amount" id="amount" value="${fieldValue(bean: loanPaymentInstance, field: 'amount')}" />
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="paymentDate-label" class="control-label"><g:message
                        code="loanPayment.paymentDate.label"
                        default="Payment Date"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="paymentDate-label">
                        <g:textField name="paymentDate" class="input-xlarge datepicker" id="paymentDate" value="${fieldValue(bean: loanPaymentInstance, field: 'paymentDate')}" style="width:210px"/>
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="paymentRemarks-label" class="control-label"><g:message
                        code="loanPayment.remarks.label"
                        default="Remarks"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="paymentRemarks-label">
                        <g:textArea name="remarks" rows="3" cols="1" value="${fieldValue(bean: loanPaymentInstance, field: 'remarks')}"/>
                    </span>

                </div>
            </div>

        </div>
        </div>
        <div class="modal-footer">
            <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
            <g:submitButton name="cancel" class="btn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />
        </div>
    </g:form>
</div>