<%@ page import="com.ripple.loans.Loan" %>
<%@ page import="com.ripple.util.Formatter" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'loan.label', default: 'Loan')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-loan" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="employee-label" class="control-label"><g:message
                                                    code="loan.employee.label"
                                                    default="Employee Name"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="employee-label">
                                                    <g:link controller="employee" action="show"
                                                            id="${loanInstance?.employee?.id}">${loanInstance?.employee?.encodeAsHTML()}</g:link></span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="loanDate-label" class="control-label"><g:message
                                                    code="loan.loanDate.label"
                                                    default="Loan Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="loanDate-label"><g:fieldValue
                                                        bean="${loanInstance}" field="loanDate"/></span>

                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="monthlyAmortization-label" class="control-label"><g:message
                                                    code="loan.monthlyAmortization.label"
                                                    default="Monthly Amortization"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="monthlyAmortization-label">
                                                    ${Formatter.formatCurrency(loanInstance.monthlyAmortization)}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="remarks-label" class="control-label"><g:message
                                                    code="loan.remarks.label"
                                                    default="Remarks"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                <span class="property-value" aria-labelledby="remarks-label"><g:fieldValue
                                                        bean="${loanInstance}" field="remarks"/></span>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="loanType-label" class="control-label"><g:message
                                                    code="loan.loanType.label"
                                                    default="Loan Type"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="loanType-label"><g:fieldValue
                                                        bean="${loanInstance}" field="loanType"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="totalAmount-label" class="control-label"><g:message
                                                    code="loan.totalAmount.label"
                                                    default="Total Amount"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="totalAmount-label">
                                                    ${Formatter?.formatCurrency(loanInstance?.totalAmount)}
                                                </span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="remainingBalance-label" class="control-label"><g:message
                                                    code="loan.remainingBalance.label"
                                                    default="Remaining Balance"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="remainingBalance-label">
                                                    ${Formatter?.formatCurrency(loanInstance?.remainingBalance)}
                                                </span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="payments-label" class="control-label"><g:message
                                                    code="loan.payments.label"
                                                    default="Payments"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <ul id="loanPaymentListing" class="listing">
                                                    <g:each var="p" in="${loanInstance.payments}">
                                                        <li><a href="javascript:" onclick="renderModal('${p?.id}', 'loanPayment')">${p?.encodeAsHTML()}</a></li>
                                                    </g:each>
                                                </ul>

                                                <a id="showModal" class="btn" href="javascript:" onclick="renderModal('', 'loanPayment')">Add Loan Payment</a>

                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${loanInstance?.id}"/>
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="modalContainer">
    <ripple:modal className="LoanPayment" entity="Loan Payment" elementId="loanPayment" parentId="${loanInstance.id}" parentClass="Loan" parentProperty="loan"/>
    <ripple:modalScript parentProperty="loan"  parentId="${loanInstance.id}"/>
</div>
</body>
</html>
