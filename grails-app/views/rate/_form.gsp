<%@ page import="com.ripple.master.Rate" %>



<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'employee', 'error')} ">
	<label for="employee">
		<g:message code="rate.employee.label" default="Employee" />
		
	</label>
	<g:select id="employee" name="employee.id" from="${com.ripple.master.Employee.list()}" optionKey="id" value="${rateInstance?.employee?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'type', 'error')} ">
	<label for="type">
		<g:message code="rate.type.label" default="Type" />
		
	</label>
	<g:select name="type" from="${rateInstance.constraints.type.inList}" value="${rateInstance?.type}" valueMessagePrefix="rate.type" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'effectivityDate', 'error')} required">
	<label for="effectivityDate">
		<g:message code="rate.effectivityDate.label" default="Effectivity Date" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="effectivityDate" value="${rateInstance?.effectivityDate}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="rate.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${rateInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'amount', 'error')} required">
	<label for="amount">
		<g:message code="rate.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="amount" required="" value="${fieldValue(bean: rateInstance, field: 'amount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'dailyCola', 'error')} required">
	<label for="dailyCola">
		<g:message code="rate.dailyCola.label" default="Daily Cola" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="dailyCola" required="" value="${fieldValue(bean: rateInstance, field: 'dailyCola')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'exemptFromDeductions', 'error')} ">
	<label for="exemptFromDeductions">
		<g:message code="rate.exemptFromDeductions.label" default="Exempt From Deductions" />
		
	</label>
	<g:checkBox name="exemptFromDeductions" value="${rateInstance?.exemptFromDeductions}" />
</div>

<div class="fieldcontain ${hasErrors(bean: rateInstance, field: 'monthlyCola', 'error')} required">
	<label for="monthlyCola">
		<g:message code="rate.monthlyCola.label" default="Monthly Cola" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="monthlyCola" required="" value="${fieldValue(bean: rateInstance, field: 'monthlyCola')}"/>
</div>

