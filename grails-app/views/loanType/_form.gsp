<%@ page import="com.ripple.loans.LoanType" %>



<div class="fieldcontain ${hasErrors(bean: loanTypeInstance, field: 'code', 'error')} required">
	<label for="code">
		<g:message code="loanType.code.label" default="Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="code" from="${com.ripple.loans.LoanType$LoanTypeCodes?.values()}" keys="${com.ripple.loans.LoanType$LoanTypeCodes.values()*.name()}" required="" value="${loanTypeInstance?.code?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanTypeInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="loanType.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${loanTypeInstance?.name}"/>
</div>

