<%@ page import="com.ripple.materialIssuance.MaterialIssuance" %>



<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="materialIssuance.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${materialIssuanceInstance.constraints.isDeleted.inList}" value="${materialIssuanceInstance?.isDeleted}" valueMessagePrefix="materialIssuance.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="materialIssuance.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${materialIssuanceInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="materialIssuance.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${materialIssuanceInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="materialIssuance.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${materialIssuanceInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="materialIssuance.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${materialIssuanceInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'misNumber', 'error')} required">
	<label for="misNumber">
		<g:message code="materialIssuance.misNumber.label" default="Mis Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="misNumber" required="" value="${materialIssuanceInstance?.misNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'procurementSlip', 'error')} required">
	<label for="procurementSlip">
		<g:message code="materialIssuance.procurementSlip.label" default="Procurement Slip" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="procurementSlip" name="procurementSlip.id" from="${com.ripple.procurement.Procurement.list()}" optionKey="id" required="" value="${materialIssuanceInstance?.procurementSlip?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'dateStarted', 'error')} required">
	<label for="dateStarted">
		<g:message code="materialIssuance.dateStarted.label" default="Date Started" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="dateStarted" value="${materialIssuanceInstance?.dateStarted}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'remarks', 'error')} ">
	<label for="remarks">
		<g:message code="materialIssuance.remarks.label" default="Remarks" />
		
	</label>
	<g:textField name="remarks" value="${materialIssuanceInstance?.remarks}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: materialIssuanceInstance, field: 'lineItems', 'error')} ">
	<label for="lineItems">
		<g:message code="materialIssuance.lineItems.label" default="Line Items" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${materialIssuanceInstance?.lineItems?}" var="l">
    <li><g:link controller="materialIssuanceLineItem" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="materialIssuanceLineItem" action="create" params="['materialIssuance.id': materialIssuanceInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'materialIssuanceLineItem.label', default: 'MaterialIssuanceLineItem')])}</g:link>
</li>
</ul>

</div>

