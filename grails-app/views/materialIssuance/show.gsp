
<%@ page import="com.ripple.materialIssuance.MaterialIssuance" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'materialIssuance.label', default: 'MaterialIssuance')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <r:script>
        var dataTableArray = {};
        $(function(){$('#'+"materialIssuanceLineItemModal").on('show', function () {
            changeCategory($('#item\\.id'))
            $.post('${createLink(controller: 'materialIssuanceLineItem', action: 'getUnitMeasure')}', { 'id': $('#materialIssuanceLineItemModal').find('#id').val() }, function(data) {
                $("#materialIssuanceLineItemModal").find("#unitOfMeasure\\.id").ajaxComplete(function(event,request, settings){
                    $(this).val(data)
                });
            }); // end of post
        })});
    </r:script>
</head>

<body>
<g:set var="isEditable" value="${materialIssuanceInstance?.status!=materialIssuanceInstance?.Approved && materialIssuanceInstance?.status!=materialIssuanceInstance?.ForApproval && materialIssuanceInstance?.status!=materialIssuanceInstance?.Posted}"/>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-procurement" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="procurement.label"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="procurementNumber-label" class="control-label"><g:message
                                                code="procurement.procurementNumber.label"
                                                default="Procurement Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="procurementNumber-label"><g:fieldValue
                                                    bean="${procurementInstance}" field="procurementNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="project-label" class="control-label"><g:message
                                                code="procurement.project.label"
                                                default="Project"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="project-label"><g:link
                                                    controller="assignment" action="show"
                                                    id="${procurementInstance?.project?.id}">${procurementInstance?.project?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="requestor-label" class="control-label"><g:message
                                                code="procurement.requestor.label"
                                                default="Requestor"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="requestor-label"><g:link
                                                    controller="employee" action="show"
                                                    id="${procurementInstance?.requestor?.id}">${procurementInstance?.requestor?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="creator-label" class="control-label"><g:message
                                                code="procurement.creator.label"
                                                default="Creator"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="creator-label"><g:link
                                                    controller="employee" action="show"
                                                    id="${procurementInstance?.creator?.id}">${procurementInstance?.creator?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="warehouse-label" class="control-label"><g:message
                                                code="procurement.warehouse.label"
                                                default="Warehouse"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="warehouse-label"><g:link
                                                    controller="warehouse" action="show"
                                                    id="${procurementInstance?.warehouse?.id}">${procurementInstance?.warehouse?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="dateStarted-label" class="control-label"><g:message
                                                code="procurement.dateStarted.label"
                                                default="Date Created"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="dateStarted-label"><g:fieldValue
                                                    bean="${procurementInstance}" field="dateStarted"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="dateClosed-label" class="control-label"><g:message
                                                code="procurement.dateClosed.label"
                                                default="Date Closed"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="dateClosed-label"><g:fieldValue
                                                    bean="${procurementInstance}" field="dateClosed"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="status-label" class="control-label"><g:message
                                                code="procurement.status.label"
                                                default="Status"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                    bean="${procurementInstance}" field="status"/></span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12 column" id="edit-materialIssuance" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="materialIssuance.show.label" args="['Material Issuance Slip']" default="Material Issuance Slip"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        %{--<legend><g:message code="default.show.label" args="[entityName]"/></legend>--}%
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    %{--<div class="form-horizontal">--}%
                                        %{--<div class="control-group">--}%
                                            %{--<h5 id="createdDate-label" class="control-label"><g:message--}%
                                                    %{--code="materialIssuance.createdDate.label"--}%
                                                    %{--default="Created Date"/></h5>--}%
                                            %{--<div class="controls" style="margin-top:4px">--}%
                                                %{----}%
                                                %{--<span class="property-value" aria-labelledby="createdDate-label"><g:formatDate--}%
                                                        %{--date="${materialIssuanceInstance?.createdDate}" format="MMMMM dd, yyyy"/></span>--}%
                                                %{----}%
                                            %{--</div>--}%
                                        %{--</div>--}%
                                    %{--</div>--}%
                                %{----}%
                                    %{--<div class="form-horizontal">--}%
                                        %{--<div class="control-group">--}%
                                            %{--<h5 id="editedBy-label" class="control-label"><g:message--}%
                                                    %{--code="materialIssuance.editedBy.label"--}%
                                                    %{--default="Edited By"/></h5>--}%
                                            %{--<div class="controls" style="margin-top:4px">--}%
                                                %{----}%
                                                %{--<span class="property-value" aria-labelledby="editedBy-label"><g:fieldValue--}%
                                                        %{--bean="${materialIssuanceInstance}" field="editedBy"/></span>--}%
                                                %{----}%
                                            %{--</div>--}%
                                        %{--</div>--}%
                                    %{--</div>--}%
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="misNumber-label" class="control-label"><g:message
                                                    code="materialIssuance.misNumber.label"
                                                    default="Mis Number"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="misNumber-label"><g:fieldValue
                                                        bean="${materialIssuanceInstance}" field="misNumber"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="dateStarted-label" class="control-label"><g:message
                                                    code="materialIssuance.dateStarted.label"
                                                    default="Date Started"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="dateStarted-label"><g:fieldValue
                                                        bean="${materialIssuanceInstance}" field="dateStarted"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="lineItems-label" class="control-label"><g:message
                                                    code="materialIssuance.lineItems.label"
                                                    default="Line Items"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${materialIssuanceInstance.lineItems}" var="l">
                                                    <span class="property-value" aria-labelledby="lineItems-label"><g:link
                                                            controller="materialIssuanceLineItem" action="show"
                                                            id="${l.id}">${l?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    %{--<div class="form-horizontal">--}%
                                        %{--<div class="control-group">--}%
                                            %{--<h5 id="isDeleted-label" class="control-label"><g:message--}%
                                                    %{--code="materialIssuance.isDeleted.label"--}%
                                                    %{--default="Is Deleted"/></h5>--}%
                                            %{--<div class="controls" style="margin-top:4px">--}%
                                                %{----}%
                                                %{--<span class="property-value" aria-labelledby="isDeleted-label"><g:fieldValue--}%
                                                        %{--bean="${materialIssuanceInstance}" field="isDeleted"/></span>--}%
                                                %{----}%
                                            %{--</div>--}%
                                        %{--</div>--}%
                                    %{--</div>--}%
                                %{----}%
                                    %{--<div class="form-horizontal">--}%
                                        %{--<div class="control-group">--}%
                                            %{--<h5 id="editedDate-label" class="control-label"><g:message--}%
                                                    %{--code="materialIssuance.editedDate.label"--}%
                                                    %{--default="Edited Date"/></h5>--}%
                                            %{--<div class="controls" style="margin-top:4px">--}%
                                                %{----}%
                                                %{--<span class="property-value" aria-labelledby="editedDate-label"><g:formatDate--}%
                                                        %{--date="${materialIssuanceInstance?.editedDate}" format="MMMMM dd, yyyy"/></span>--}%
                                                %{----}%
                                            %{--</div>--}%
                                        %{--</div>--}%
                                    %{--</div>--}%
                                
                                    %{--<div class="form-horizontal">--}%
                                        %{--<div class="control-group">--}%
                                            %{--<h5 id="createdBy-label" class="control-label"><g:message--}%
                                                    %{--code="materialIssuance.createdBy.label"--}%
                                                    %{--default="Created By"/></h5>--}%
                                            %{--<div class="controls" style="margin-top:4px">--}%
                                                %{----}%
                                                %{--<span class="property-value" aria-labelledby="createdBy-label"><g:fieldValue--}%
                                                        %{--bean="${materialIssuanceInstance}" field="createdBy"/></span>--}%
                                                %{----}%
                                            %{--</div>--}%
                                        %{--</div>--}%
                                    %{--</div>--}%
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="procurementSlip-label" class="control-label"><g:message
                                                    code="materialIssuance.procurementSlip.label"
                                                    default="Procurement Slip"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="procurementSlip-label"><g:link
                                                        controller="procurement" action="show"
                                                        id="${materialIssuanceInstance?.procurementSlip?.id}">${materialIssuanceInstance?.procurementSlip?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="remarks-label" class="control-label"><g:message
                                                    code="materialIssuance.remarks.label"
                                                    default="Remarks"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="remarks-label"><g:fieldValue
                                                        bean="${materialIssuanceInstance}" field="remarks"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${materialIssuanceInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${materialIssuanceInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:if test="${isEditable}">
                                    <g:actionSubmit class="btn btn-primary" action="edit"
                                                    value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>
                                </g:if>

                                %{--<g:actionSubmit class="delete btn-danger" action="delete"--}%
                                                %{--value="${message(code: 'default.button.delete.label', default: 'Delete')}"--}%
                                                %{--onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>--}%
                                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_USER">
                                    <input type="hidden" name="version" value="${materialIssuanceInstance?.version}"/>
                                    <g:if test="${materialIssuanceInstance?.status==materialIssuanceInstance?.Draft}">
                                        <g:actionSubmit class="btn btn-success" action="formStatus"
                                                        value="${message(code: 'materialIssuance.forApproval.label', default: 'For Approval')}"/>
                                    </g:if>
                                    <g:actionSubmit class="btn btn-danger" action="formStatus"
                                                    value="${message(code: 'materialIssuance.cancel.label', default: 'Cancel')}"/>
                                    <g:if test="${materialIssuanceInstance?.status==materialIssuanceInstance?.Approved}">
                                        <g:actionSubmit class="btn btn-success" action="formStatus"
                                                        value="${message(code: 'materialIssuance.approved.label', default: 'Post')}"/>
                                    </g:if>
                                </sec:ifAnyGranted>

                                <sec:ifAnyGranted roles="ROLE_ADMIN">
                                    <g:if test="${materialIssuanceInstance?.status!=materialIssuanceInstance?.Draft && materialIssuanceInstance?.status!=materialIssuanceInstance?.Posted}">
                                        <g:actionSubmit class="btn btn-primary" action="formStatus"
                                                    value="${message(code: 'materialIssuance.disapproved.label', default: 'Send back to draft')}"/>
                                    </g:if>
                                    <g:if test="${materialIssuanceInstance?.status==materialIssuanceInstance?.ForApproval}">
                                        <g:actionSubmit class="btn btn-success" action="formStatus"
                                                        value="${message(code: 'materialIssuance.approved.label', default: 'Approve')}"/>
                                    </g:if>
                                </sec:ifAnyGranted>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
<div class="row-fluid">
    <div class="span12 column" id="list-materialIssuanceLineItem" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-2">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="['Material Issuance Line Item']"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <g:if test="${isEditable}">
                        <a id="showModal" href="javascript:" onclick="renderModal('', 'materialIssuanceLineItem')" class="btn btn-primary" style="margin-bottom:10px;">Add New Line Item</a>
                    </g:if>
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>
                            <th rowspan="2" ><g:message code="materialIssuanceLineItem.lineItem.label" default="Line Item" /></th>

                            <th rowspan="2"><g:message code="materialIssuanceLineItem.itemCode.label" default="Item Code" /></th>

                            <th rowspan="2"><g:message code="materialIssuanceLineItem.itemName.label" default="Item Name" /></th>

                            <th rowspan="2"><g:message code="materialIssuanceLineItem.baseQty.label" default="Base Qty" /></th>

                            <th rowspan="2"><g:message code="materialIssuanceLineItem.baseUom.label" default="Base UoM" /></th>

                            <th colspan="2"><g:message code="materialIssuanceLineItem.balanceToServe.label" default="Balance to Serve" /></th>

                            <th colspan="2"><g:message code="materialIssuanceLineItem.baseRelease.label" default="Base Release" /></th>

                            <th colspan="2"><g:message code="materialIssuanceLineItem.actualRelease.label" default="Actual Release" /></th>

                            <th rowspan="2"><g:message code="materialIssuanceLineItem.actions.label" default="Actions" /></th>
                        </tr>
                        <tr>

                            %{--<th><g:message code="materialIssuanceLineItem.lineItem.label" default="Is Deleted" /></th>--}%

                            <th><g:message code="materialIssuanceLineItem.balanceBaseQty.label" default="Base Qty" /></th>

                            <th><g:message code="materialIssuanceLineItem.balanceBase.label" default="Base UoM" /></th>

                            <th><g:message code="materialIssuanceLineItem.releaseBaseQty.label" default="Base Qty" /></th>

                            <th><g:message code="materialIssuanceLineItem.releaseBase.label" default="Base UoM" /></th>

                            <th><g:message code="materialIssuanceLineItem.actualRelease.label" default="Qty UoM" /></th>

                            <th><g:message code="materialIssuanceLineItem.actualQty.label" default="Qty" /></th>

                            %{--<th><g:message code="materialIssuanceLineItem.actions.label" default="Actions" /></th>--}%

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            dataTableArray['materialIssuanceLineItem'] = $('#datatable').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'materialIssuanceLineItem', action:'listJSON', id:materialIssuanceInstance.id)}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[

                                    { "sName": "lineItem", "aTargets": [ 0 ] },

                                    { "sName": "itemCode", "aTargets": [ 1 ] },

                                    { "sName": "itemName", "aTargets": [ 2 ] },

                                    { "sName": "baseQty", "aTargets": [ 3 ] },

                                    { "sName": "baseUom", "aTargets": [ 4 ] },

                                    { "sName": "balanceToServeQty", "aTargets": [ 5 ] },

                                    { "sName": "balanceToServeBase", "aTargets": [ 6 ] },

                                    { "sName": "baseReleaseQty", "aTargets": [ 7 ] },

                                    { "sName": "baseReleaseBase", "aTargets": [ 8 ] },

                                    { "sName": "actualReleaseQtyUom", "aTargets": [ 9 ] },

                                    { "sName": "actualReleaseQty", "aTargets": [ 10 ] }

                                ],
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    <g:if test="${isEditable}">
                                        $('td:eq(11)', nRow).append('<a class="btn btn-info mini" href="javascript:" onclick="renderModal(\''+nRow.id+'\', \'materialIssuanceLineItem\')"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                    </g:if>
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div><!--/span-->

        </div>
    </div>
</div>
<div id="modalContainer">
    <g:if test="${isEditable}">
        <ripple:modal className="MaterialIssuanceLineItem" entity="Material Issuance Line Item" elementId="materialIssuanceLineItem" parentId="${materialIssuanceInstance.id}" parentClass="MaterialIssuance" parentProperty="issuance"/>
        <ripple:modalScript parentProperty="issuance"  parentId="${materialIssuanceInstance.id}"/>
    </g:if>
</div>
</body>
</html>
