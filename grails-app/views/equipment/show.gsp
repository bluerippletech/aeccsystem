
<%@ page import="com.ripple.equipment.Equipment" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'equipment.label', default: 'Equipment')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <r:script>
        var dataTableArray = {};
    </r:script>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-equipment" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="equipment.details.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="algonNumber-label" class="control-label"><g:message
                                                code="equipment.algonNumber.label"
                                                default="Algon Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="algonNumber-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="algonNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="model-label" class="control-label"><g:message
                                                code="equipment.model.label"
                                                default="Model"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="model-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="model"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="yearModel-label" class="control-label"><g:message
                                                code="equipment.yearModel.label"
                                                default="Year Model"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="yearModel-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="yearModel"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="invoiceNumber-label" class="control-label"><g:message
                                                code="equipment.invoiceNumber.label"
                                                default="Invoice Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="invoiceNumber-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="invoiceNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="freightCharges-label" class="control-label"><g:message
                                                code="equipment.freightCharges.label"
                                                default="Freight Charges"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="freightCharges-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="freightCharges"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="arrivalDate-label" class="control-label"><g:message
                                                code="equipment.arrivalDate.label"
                                                default="Arrival Date"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="arrivalDate-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="arrivalDate"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="equipmentType-label" class="control-label"><g:message
                                                code="equipment.equipmentType.label"
                                                default="Equipment Type"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="equipmentType-label"><g:link
                                                    controller="equipmentType" action="show"
                                                    id="${equipmentInstance?.equipmentType?.id}">${equipmentInstance?.equipmentType?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="image-label" class="control-label"><g:message
                                                code="equipment.image.label"
                                                default="Image"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="plateNumber-label" class="control-label"><g:message
                                                code="equipment.plateNumber.label"
                                                default="Plate Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="plateNumber-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="plateNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="numberOfCylinders-label" class="control-label"><g:message
                                                code="equipment.numberOfCylinders.label"
                                                default="Number Of Cylinders"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="numberOfCylinders-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="numberOfCylinders"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="engineNumber-label" class="control-label"><g:message
                                                code="equipment.engineNumber.label"
                                                default="Engine Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="engineNumber-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="engineNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="color-label" class="control-label"><g:message
                                                code="equipment.color.label"
                                                default="Color"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="color-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="color"/></span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="tireSizeBack-label" class="control-label"><g:message
                                                code="equipment.tireSizeBack.label"
                                                default="Tire Size Back"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="tireSizeBack-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="tireSizeBack"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="assignedTo-label" class="control-label"><g:message
                                                code="equipment.assignedTo.label"
                                                default="Assigned To"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="assignedTo-label"><g:link
                                                    controller="employee" action="show"
                                                    id="${equipmentInstance?.assignedTo?.id}">${equipmentInstance?.assignedTo?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="description-label" class="control-label"><g:message
                                                code="equipment.description.label"
                                                default="Description"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="description"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="makeAndType-label" class="control-label"><g:message
                                                code="equipment.makeAndType.label"
                                                default="Make And Type"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="makeAndType-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="makeAndType"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="purchasedFrom-label" class="control-label"><g:message
                                                code="equipment.purchasedFrom.label"
                                                default="Purchased From"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="purchasedFrom-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="purchasedFrom"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="amount-label" class="control-label"><g:message
                                                code="equipment.amount.label"
                                                default="Amount"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="amount-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="amount"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="datePurchased-label" class="control-label"><g:message
                                                code="equipment.datePurchased.label"
                                                default="Date Purchased"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="datePurchased-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="datePurchased"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="comments-label" class="control-label"><g:message
                                                code="equipment.comments.label"
                                                default="Comments"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="comments-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="comments"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="crNumber-label" class="control-label"><g:message
                                                code="equipment.crNumber.label"
                                                default="Cr Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="crNumber-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="crNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="netCapacity-label" class="control-label"><g:message
                                                code="equipment.netCapacity.label"
                                                default="Net Capacity"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="netCapacity-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="netCapacity"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="grossWeight-label" class="control-label"><g:message
                                                code="equipment.grossWeight.label"
                                                default="Gross Weight"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="grossWeight-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="grossWeight"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="chassisNumber-label" class="control-label"><g:message
                                                code="equipment.chassisNumber.label"
                                                default="Chassis Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="chassisNumber-label"><g:fieldValue
                                                    bean="${equipmentInstance}" field="chassisNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                            <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="tireSizeFront-label" class="control-label"><g:message
                                            code="equipment.tireSizeFront.label"
                                            default="Tire Size Front"/></h5>
                                    <div class="controls" style="margin-top:4px">

                                        <span class="property-value" aria-labelledby="tireSizeFront-label"><g:fieldValue
                                                bean="${equipmentInstance}" field="tireSizeFront"/></span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="status-label" class="control-label"><g:message
                                            code="equipment.status.label"
                                            default="Status"/></h5>
                                    <div class="controls" style="margin-top:4px">

                                        <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                bean="${equipmentInstance}" field="status"/></span>

                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="assignedDriver-label" class="control-label"><g:message
                                            code="equipment.assignedDriver.label"
                                            default="Assigned Driver"/></h5>
                                    <div class="controls" style="margin-top:4px">

                                        <span class="property-value" aria-labelledby="assignedDriver-label"><g:link
                                                controller="employee" action="show"
                                                id="${equipmentInstance?.assignedDriver?.id}">${equipmentInstance?.assignedDriver?.encodeAsHTML()}</g:link></span>

                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="form-actions form-horizontal">
                    <g:form>
                        <g:hiddenField name="id" value="${equipmentInstance?.id}"/>
                        <g:actionSubmit class="btn btn-primary" action="edit"
                                        value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                        <g:actionSubmit class="delete btn-danger" action="delete"
                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                    </g:form>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="edit-equipment" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="equipmentAssignment.show.label" default="Equipment Assignment History"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>

                            <th><g:message code="equipmentAssignment.fromDate.label" default="From Date" /></th>

                            <th><g:message code="equipmentAssignment.toDate.label" default="To Date" /></th>

                            <th><g:message code="equipmentAssignment.project.label" default="Project" /></th>

                            <th><g:message code="equipmentAssignment.projectLocation.label" default="Project Location" /></th>

                            <th><g:message code="equipmentAssignment.operator.label" default="Operator" /></th>

                            <th><g:message code="equipmentAssignment.comments.label" default="Comments" /></th>


                            <th><g:message code="equipmentAssignment.actions.label" default="Actions" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            dataTableArray['equipmentAssignment'] = $('#datatable').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'equipmentAssignment', action:'listJSON', id: "${params.id}")}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[

                                    { "sName": "fromDate", "aTargets": [ 0 ] },

                                    { "sName": "toDate", "aTargets": [ 1 ] },

                                    { "sName": "project", "aTargets": [ 2 ] },

                                    { "sName": "projectLocation", "aTargets": [ 3 ] },

                                    { "sName": "operator", "aTargets": [ 4 ] },

                                    { "sName": "comments", "aTargets": [ 5 ] }


                                ],
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    $('td:eq(6)', nRow).append('<a class="btn btn-info mini" href="#" onclick="renderModal(\''+nRow.id+'\', \'equipmentAssignment\')"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                $(elem).closest("form").submit();
                        }
                    </script>
                    <a id="showModal" href="javascript:" onclick="renderModal('', 'equipmentAssignment')" class="btn btn-primary" >Add New Assignment</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="edit-equipment" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="equipment.image.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <div style="text-align:center;">
                        <img src="${createLink(controller: 'image', action: 'show', id: equipmentInstance.id, params: [classname: 'com.ripple.equipment.Equipment', fieldName: 'image'])}" alt="No Image Uploaded."/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modalContainer">
    <ripple:modal className="EquipmentAssignment" entity="Equipment Assignment" elementId="equipmentAssignment" parentId="${equipmentInstance.id}" parentClass="Equipment" parentProperty="equipment"/>
    <ripple:modalScript parentProperty="equipment"  parentId="${equipmentInstance.id}"/>
</div>
</body>
</html>
