<div class="modal hide" id="addEquipmentAssignment">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Add Equipment Assignment</h3>
    </div>
<div class="modal-body">
    <g:form class="form-horizontal" controller="equipmentAssignment" action="save">
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="fromDate-label" class="control-label"><g:message
                        code="equipmentAssignment.fromDate.label"
                        default="From Date"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="fromDate-label">
                    <g:textField name="fromDate" class="input-xlarge datepicker" id="fromDate" value="${fieldValue(bean: equipmentAssignmentInstance, field: 'fromDate')}" style="width:210px"/>
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="toDate-label" class="control-label"><g:message
                        code="equipmentAssignment.toDate.label"
                        default="To Date"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="toDate-label">
                        <g:textField name="toDate" class="input-xlarge datepicker" id="toDate" value="${fieldValue(bean: equipmentAssignmentInstance, field: 'toDate')}" style="width:210px"/>
                        %{--<g:select optionKey="id" from="${payrollEntries}" name="payrollEntry.id" value="${equipmentAssignmentInstance?.payrollEntry?.id}" noSelection="['':'-Please select Payroll Entry-']"></g:select>--}%
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="operator-label" class="control-label"><g:message
                        code="equipmentAssignment.operator.label"
                        default="Operator"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="operator-label">
                        <g:select optionKey="id" from="${com.ripple.master.Employee.list()}" name="operator.id" value="${equipmentAssignmentInstance?.operator?.id}" noSelection="['null':'']"></g:select>
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="project-label" class="control-label"><g:message
                        code="equipmentAssignment.project.label"
                        default="Project"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="project-label">
                        <g:set var="remoteParams" value="'project.id=' + this.value"/>
                        <g:select optionKey="id" from="${com.ripple.master.Assignment.list()}" name="project.id" value="${equipmentAssignmentInstance?.project?.id}"
                                  noSelection="['':'-Please select a Project-']"
                                  onchange="${remoteFunction(action:'getProjectLocations',controller:'workingHours',
                                          update:[success:'projectLocations'],
                                          params:remoteParams)
                                  }"></g:select>
                    </span>

                </div>
            </div>

            <div class="control-group">
                <h5 id="comments-label" class="control-label"><g:message
                        code="equipmentAssignment.comments.label"
                        default="Comments"/></h5>
                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="paymentDate-label">
                        <g:textArea name="comments" rows="3" cols="1" id="comments" value="${fieldValue(bean: equipmentAssignmentInstance, field: 'comments')}" style="width:210px"/>
                    </span>

                </div>
            </div>

        </div>
        </div>
        <div class="modal-footer">
            <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
            <g:submitButton name="cancel" class="btn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />
        </div>
    </g:form>
</div>