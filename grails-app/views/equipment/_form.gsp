<%@ page import="com.ripple.equipment.Equipment" %>



<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'equipmentType', 'error')} ">
	<label for="equipmentType">
		<g:message code="equipment.equipmentType.label" default="Equipment Type" />
		
	</label>
	<g:select id="equipmentType" name="equipmentType.id" from="${com.ripple.equipment.EquipmentType.list()}" optionKey="id" value="${equipmentInstance?.equipmentType?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="equipment.image.label" default="Image" />
		
	</label>
	<input type="file" id="image" name="image" />
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'algonNumber', 'error')} required">
	<label for="algonNumber">
		<g:message code="equipment.algonNumber.label" default="Algon Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="algonNumber" required="" value="${equipmentInstance?.algonNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'model', 'error')} ">
	<label for="model">
		<g:message code="equipment.model.label" default="Model" />
		
	</label>
	<g:textField name="model" value="${equipmentInstance?.model}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'makeAndType', 'error')} ">
	<label for="makeAndType">
		<g:message code="equipment.makeAndType.label" default="Make And Type" />
		
	</label>
	<g:textField name="makeAndType" value="${equipmentInstance?.makeAndType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'purchasedFrom', 'error')} ">
	<label for="purchasedFrom">
		<g:message code="equipment.purchasedFrom.label" default="Purchased From" />
		
	</label>
	<g:textField name="purchasedFrom" value="${equipmentInstance?.purchasedFrom}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'invoiceNumber', 'error')} ">
	<label for="invoiceNumber">
		<g:message code="equipment.invoiceNumber.label" default="Invoice Number" />
		
	</label>
	<g:textField name="invoiceNumber" value="${equipmentInstance?.invoiceNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'amount', 'error')} required">
	<label for="amount">
		<g:message code="equipment.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="amount" required="" value="${fieldValue(bean: equipmentInstance, field: 'amount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'freightCharges', 'error')} ">
	<label for="freightCharges">
		<g:message code="equipment.freightCharges.label" default="Freight Charges" />
		
	</label>
	<g:field type="number" name="freightCharges" value="${fieldValue(bean: equipmentInstance, field: 'freightCharges')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="equipment.status.label" default="Status" />
		
	</label>
	<g:select name="status" from="${equipmentInstance.constraints.status.inList}" value="${equipmentInstance?.status}" valueMessagePrefix="equipment.status" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="equipment.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${equipmentInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'comments', 'error')} ">
	<label for="comments">
		<g:message code="equipment.comments.label" default="Comments" />
		
	</label>
	<g:textField name="comments" value="${equipmentInstance?.comments}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'crNumber', 'error')} ">
	<label for="crNumber">
		<g:message code="equipment.crNumber.label" default="Cr Number" />
		
	</label>
	<g:textField name="crNumber" value="${equipmentInstance?.crNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'plateNumber', 'error')} ">
	<label for="plateNumber">
		<g:message code="equipment.plateNumber.label" default="Plate Number" />
		
	</label>
	<g:textField name="plateNumber" value="${equipmentInstance?.plateNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'netCapacity', 'error')} ">
	<label for="netCapacity">
		<g:message code="equipment.netCapacity.label" default="Net Capacity" />
		
	</label>
	<g:textField name="netCapacity" value="${equipmentInstance?.netCapacity}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'numberOfCylinders', 'error')} ">
	<label for="numberOfCylinders">
		<g:message code="equipment.numberOfCylinders.label" default="Number Of Cylinders" />
		
	</label>
	<g:textField name="numberOfCylinders" value="${equipmentInstance?.numberOfCylinders}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'engineNumber', 'error')} ">
	<label for="engineNumber">
		<g:message code="equipment.engineNumber.label" default="Engine Number" />
		
	</label>
	<g:textField name="engineNumber" value="${equipmentInstance?.engineNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'chassisNumber', 'error')} ">
	<label for="chassisNumber">
		<g:message code="equipment.chassisNumber.label" default="Chassis Number" />
		
	</label>
	<g:textField name="chassisNumber" value="${equipmentInstance?.chassisNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'grossWeight', 'error')} ">
	<label for="grossWeight">
		<g:message code="equipment.grossWeight.label" default="Gross Weight" />
		
	</label>
	<g:textField name="grossWeight" value="${equipmentInstance?.grossWeight}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'color', 'error')} ">
	<label for="color">
		<g:message code="equipment.color.label" default="Color" />
		
	</label>
	<g:textField name="color" value="${equipmentInstance?.color}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'tireSizeFront', 'error')} ">
	<label for="tireSizeFront">
		<g:message code="equipment.tireSizeFront.label" default="Tire Size Front" />
		
	</label>
	<g:textField name="tireSizeFront" value="${equipmentInstance?.tireSizeFront}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'tireSizeBack', 'error')} ">
	<label for="tireSizeBack">
		<g:message code="equipment.tireSizeBack.label" default="Tire Size Back" />
		
	</label>
	<g:textField name="tireSizeBack" value="${equipmentInstance?.tireSizeBack}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'assignedTo', 'error')} ">
	<label for="assignedTo">
		<g:message code="equipment.assignedTo.label" default="Assigned To" />
		
	</label>
	<g:select id="assignedTo" name="assignedTo.id" from="${com.ripple.master.Employee.list()}" optionKey="id" value="${equipmentInstance?.assignedTo?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'assignedDriver', 'error')} ">
	<label for="assignedDriver">
		<g:message code="equipment.assignedDriver.label" default="Assigned Driver" />
		
	</label>
	<g:select id="assignedDriver" name="assignedDriver.id" from="${com.ripple.master.Employee.list()}" optionKey="id" value="${equipmentInstance?.assignedDriver?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'arrivalDate', 'error')} required">
	<label for="arrivalDate">
		<g:message code="equipment.arrivalDate.label" default="Arrival Date" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="arrivalDate" value="${equipmentInstance?.arrivalDate}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'assignments', 'error')} ">
	<label for="assignments">
		<g:message code="equipment.assignments.label" default="Assignments" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${equipmentInstance?.assignments?}" var="a">
    <li><g:link controller="equipmentAssignment" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="equipmentAssignment" action="create" params="['equipment.id': equipmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'equipmentAssignment.label', default: 'EquipmentAssignment')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'datePurchased', 'error')} required">
	<label for="datePurchased">
		<g:message code="equipment.datePurchased.label" default="Date Purchased" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="datePurchased" value="${equipmentInstance?.datePurchased}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentInstance, field: 'yearModel', 'error')} ">
	<label for="yearModel">
		<g:message code="equipment.yearModel.label" default="Year Model" />
		
	</label>
	<g:textField name="yearModel" value="${equipmentInstance?.yearModel}"/>
</div>

