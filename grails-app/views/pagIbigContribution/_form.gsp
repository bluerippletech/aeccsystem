<%@ page import="com.ripple.deductions.PagIbigContribution" %>



<div class="fieldcontain ${hasErrors(bean: pagIbigContributionInstance, field: 'lowRange', 'error')} required">
	<label for="lowRange">
		<g:message code="pagIbigContribution.lowRange.label" default="Low Range" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="lowRange" required="" value="${fieldValue(bean: pagIbigContributionInstance, field: 'lowRange')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: pagIbigContributionInstance, field: 'highRange', 'error')} required">
	<label for="highRange">
		<g:message code="pagIbigContribution.highRange.label" default="High Range" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="highRange" required="" value="${fieldValue(bean: pagIbigContributionInstance, field: 'highRange')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: pagIbigContributionInstance, field: 'employeeMultiplier', 'error')} required">
	<label for="employeeMultiplier">
		<g:message code="pagIbigContribution.employeeMultiplier.label" default="Employee Multiplier" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="employeeMultiplier" required="" value="${fieldValue(bean: pagIbigContributionInstance, field: 'employeeMultiplier')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: pagIbigContributionInstance, field: 'employerMultiplier', 'error')} required">
	<label for="employerMultiplier">
		<g:message code="pagIbigContribution.employerMultiplier.label" default="Employer Multiplier" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="employerMultiplier" required="" value="${fieldValue(bean: pagIbigContributionInstance, field: 'employerMultiplier')}"/>
</div>

