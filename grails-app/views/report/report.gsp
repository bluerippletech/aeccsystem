<%@ page import="com.ripple.master.Assignment; com.ripple.entry.PayPeriod" %>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'report.label', default: 'Report Generation')}"/>
    <title><g:message code="default.show.label" args="[entityName]" default="Report Generation"/></title>
    <r:require modules="all"/>
    <style type="text/css">
    .mini{
        padding: 3px !important;
    }
    </style>
</head>
<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span4" id="report-payrollJournal" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="report.payrollJournal.label" default="Payroll Journal Report"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <script type="text/javascript">
                        function submit_payrolljournal(link) {
                            link.parentNode.parentNode._format.value = link.title;
                            link.parentNode.parentNode.submit();
                            return false;
                        }
                    </script>

                    <form class="jasperReport" name="payrolljournal" action="${createLink(controller: 'report')}"><input name="_format" type="hidden">
                        <input name="_name" value="Payroll Journal Report" type="hidden">
                        <input name="_file" value="payrolljournal" type="hidden">
                        <div class="dialog">
                            <div class="prop">
                                <label for="payPeriod">Pay Period:</label>
                                <g:select optionKey="id" from="${PayPeriod.list(sort:'monthYear',order:'desc',max:'10')}" name="PERIOD" value=""></g:select>
                            </div>
                            <div class="prop">
                                <label for="PROJECT">Project:</label>
                                <g:select optionKey="id"
                                          from="${Assignment.findAllByActive(true)}"
                                          noSelection="['':'-Please select a Project-']"
                                          name="PROJECT" value=""></g:select>
                            </div>
                        </div>
                        <div>
                            XLS File
                            <a href="#" class="jasperButton" title="XLS" onclick="return submit_payrolljournal(this)">
                                <img alt="XLS" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'XLS.gif')}" border="0"></a>
                            |
                            CSV File
                            <a href="#" class="jasperButton" title="CSV" onclick="return submit_payrolljournal(this)">
                                <img alt="CSV" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'CSV.gif')}" border="0"></a>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
    <div class="span4" id="report-loanBalances" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="report.loanBalances.label" default="Load Balances Report"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content" style="text-align:center">
                    XLS File
                    <a class="jasperButton" title="XLS" href="${createLink(controller: 'report', params: [_format: 'XLS', _name: 'Loan Balances Report', _file: 'loanbalances'])}">
                        <img alt="XLS" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'XLS.gif')}" border="0">
                    </a>
                    |
                    CSV File
                    <a class="jasperButton" title="CSV" href="${createLink(controller: 'report', params: [_format: 'CSV', _name: 'Loan Balances Report', _file: 'loanbalances'])}">
                        <img alt="CSV" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'CSV.gif')}" border="0">
                    </a>

                </div>
            </div>

        </div>
    </div>
    <div class="span4" id="report-monthlySalary" role="main">
        <div class="box" id="box-2">
            <h4 class="box-header round-top"><g:message code="report.monthlySalary.label" default="Salaries and Wages Report"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">

                    <script type="text/javascript">
                        function submit_salariesandwages(link) {
                            link.parentNode.parentNode._format.value = link.title;
                            link.parentNode.parentNode.submit();
                            return false;
                        }
                    </script>
                    <form class="jasperReport" name="salariesandwages" action="${createLink(controller: 'report')}"><input name="_format" type="hidden">
                        <input name="_name" value="Salaries and Wages Report" type="hidden">
                        <input name="_file" value="salariesandwages" type="hidden">
                        <div class="dialog">
                            <div class="prop">
                                <label>Period Low:</label>
                                <input id="PERIOD_LOW_3" class="input-xlarge datepicker" value="" name="PERIOD_LOW_3" type="text">
                            </div>
                            <div class="prop">
                                <label>Period High:</label>
                                <input id="PERIOD_HIGH_3" class="input-xlarge datepicker" value="" name="PERIOD_HIGH_3" type="text">
                            </div>
                        </div>
                        <div>
                            XLS File
                            <a href="#" class="jasperButton" title="XLS" onclick="return submit_salariesandwages(this)">
                                <img alt="XLS" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'XLS.gif')}" border="0"></a>
                            |
                            CSV File
                            <a href="#" class="jasperButton" title="CSV" onclick="return submit_salariesandwages(this)">
                                <img alt="CSV" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'CSV.gif')}" border="0"></a>
                            </a>

                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>

</div>
<div class="row-fluid">
    <div class="span4" id="report-employeeContribution" role="main">
        <div class="box" id="box-3">
            <h4 class="box-header round-top"><g:message code="report.employeeContribution.label" default="Employee Contribution Report"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">


                    <script type="text/javascript">
                        function submit_employeecontributions(link) {
                            link.parentNode.parentNode._format.value = link.title;
                            link.parentNode.parentNode.submit();
                            return false;
                        }
                    </script>
                    <form class="jasperReport" name="employeecontributions" action="${createLink(controller: 'report')}"><input name="_format" type="hidden">
                        <input name="_name" value="Employee Contributions Report" type="hidden">
                        <input name="_file" value="employeecontributions" type="hidden">
                        <div class="dialog">
                            <div class="prop">
                                <label>Period Low:</label>

                                <input id="PERIOD_LOW_1" class="input-xlarge datepicker" value="" name="PERIOD_LOW_1" type="text">

                            </div>
                            <div class="prop">
                                <label>Period High:</label>

                                <input id="PERIOD_HIGH_1" class="input-xlarge datepicker" value="" name="PERIOD_HIGH_1" type="text">

                            </div>
                        </div>
                        <div>
                            XLS File
                            <a href="#" class="jasperButton" title="XLS" onclick="return submit_employeecontributions(this)">
                                <img alt="XLS" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'XLS.gif')}" border="0"></a>
                            |
                            CSV File
                            <a href="#" class="jasperButton" title="CSV" onclick="return submit_employeecontributions(this)">
                                <img alt="CSV" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'CSV.gif')}" border="0"></a>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <div class="span4" id="report-employerContribution" role="main">
        <div class="box" id="box-4">
            <h4 class="box-header round-top"><g:message code="report.employerContribution.label" default="Employer Contribution Report"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">


                    <script type="text/javascript">
                        function submit_employercontributions(link) {
                            link.parentNode.parentNode._format.value = link.title;
                            link.parentNode.parentNode.submit();
                            return false;
                        }
                    </script>
                    <form class="jasperReport" name="employercontributions" action="${createLink(controller: 'report')}"><input name="_format" type="hidden">
                        <input name="_name" value="Employer Contributions Report" type="hidden">
                        <input name="_file" value="employercontributions" type="hidden">
                        <div class="dialog">
                            <div class="prop">
                                <label>Period Low:</label>
                                <input id="PERIOD_LOW_2" class="input-xlarge datepicker" value="" name="PERIOD_LOW_2" type="text">
                            </div>
                            <div class="prop">
                                <label>Period High:</label>
                                <input id="PERIOD_HIGH_2" class="input-xlarge datepicker" value="" name="PERIOD_HIGH_2" type="text">

                            </div>
                        </div>
                        <div>
                            XLS File
                            <a href="#" class="jasperButton" title="XLS" onclick="return submit_employercontributions(this)">
                                <img alt="XLS" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'XLS.gif')}" border="0"></a>
                            |
                            CSV File
                            <a href="#" class="jasperButton" title="CSV" onclick="return submit_employercontributions(this)">
                                <img alt="CSV" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'CSV.gif')}" border="0"></a>

                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
    <div class="span4" id="report-accumulatedPay" role="main">
        <div class="box" id="box-5">
            <h4 class="box-header round-top"><g:message code="report.accumulatedPay.label" default="Accumulated Pay Information Report"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <script type="text/javascript">
                        function submit_accumulatedpayinformation(link) {
                            link.parentNode.parentNode._format.value = link.title;
                            link.parentNode.parentNode.submit();
                            return false;
                        }
                    </script>
                    <form class="jasperReport" name="accumulatedpayinformation" action="${createLink(controller: 'report')}">
                        <input name="_format" type="hidden">
                        <input name="_name" value="Accumulated Pay Information" type="hidden">
                        <input name="_file" value="accumulatedpayinformation" type="hidden">
                        <div class="dialog">
                            <div class="prop">
                                <label>Period Low:</label>
                                <input id="PERIOD_LOW_4" class="input-xlarge datepicker" value="" name="PERIOD_LOW_4" type="text">

                            </div>
                            <div class="prop">
                                <label>Period High:</label>
                                <input id="PERIOD_HIGH_4" class="input-xlarge datepicker" value="" name="PERIOD_HIGH_4" type="text">

                            </div>
                        </div>
                        <div>
                            XLS File
                            <a href="#" class="jasperButton" title="XLS" onclick="return submit_accumulatedpayinformation(this)">
                                <img alt="XLS" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'XLS.gif')}" border="0"></a>
                            |
                            CSV File
                            <a href="#" class="jasperButton" title="CSV" onclick="return submit_accumulatedpayinformation(this)">
                                <img alt="CSV" src="${resource(plugin: 'jasper', dir: 'images/icons', file: 'CSV.gif')}" border="0"></a>

                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>