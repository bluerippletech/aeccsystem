<%@ page import="com.ripple.entry.PayPeriod" %>



<div class="fieldcontain ${hasErrors(bean: payPeriodInstance, field: 'firstPayPeriod', 'error')} ">
	<label for="firstPayPeriod">
		<g:message code="payPeriod.firstPayPeriod.label" default="First Pay Period" />
		
	</label>
	<g:checkBox name="firstPayPeriod" value="${payPeriodInstance?.firstPayPeriod}" />
</div>

<div class="fieldcontain ${hasErrors(bean: payPeriodInstance, field: 'monthYear', 'error')} required">
	<label for="monthYear">
		<g:message code="payPeriod.monthYear.label" default="Month Year" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="monthYear" value="${payPeriodInstance?.monthYear}" ></joda:datePicker>
</div>

