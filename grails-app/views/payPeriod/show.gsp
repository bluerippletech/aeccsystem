
<%@ page import="com.ripple.entry.PayPeriod" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'payPeriod.label', default: 'PayPeriod')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-payPeriod" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="monthYear-label" class="control-label"><g:message
                                                    code="payPeriod.monthYear.label"
                                                    default="Month Year"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="monthYear-label"><g:fieldValue
                                                        bean="${payPeriodInstance}" field="monthYear"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="firstPayPeriod-label" class="control-label"><g:message
                                                    code="payPeriod.firstPayPeriod.label"
                                                    default="First Pay Period"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="firstPayPeriod-label"><g:formatBoolean
                                                        boolean="${payPeriodInstance?.firstPayPeriod}"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="regularWorkingDays-label" class="control-label"><g:message
                                                    code="payPeriod.regularWorkingDays.label"
                                                    default="Number Of Regular Working Days"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                <span class="property-value" aria-labelledby="firstPayPeriod-label">${payPeriodService.getNumberOfRegularWorkingDays(payPeriodInstance)}</span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="legalHolidays-label" class="control-label"><g:message
                                                    code="payPeriod.legalHolidays.label"
                                                    default="Number Of Legal Holidays"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                <span class="property-value" aria-labelledby="firstPayPeriod-label">${payPeriodService.getNumberOfLegalHolidays(payPeriodInstance)}</span>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="nonWorkingHolidays-label" class="control-label"><g:message
                                                code="payPeriod.nonWorkingHolidays.label"
                                                default="Number Of Special Non-working Holidays"/></h5>
                                        <div class="controls" style="margin-top:4px">
                                            <span class="property-value" aria-labelledby="firstPayPeriod-label">${payPeriodService.getNumberOfSpecialHolidays(payPeriodInstance)}</span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="periodStartDate-label" class="control-label"><g:message
                                                code="payPeriod.nonWorkingHolidays.label"
                                                default="Period Start Date"/></h5>
                                        <div class="controls" style="margin-top:4px">
                                            <span class="property-value" aria-labelledby="firstPayPeriod-label">${payPeriodInstance?.periodStartDate?.toString("MMMMM dd, yyyy")}</span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="periodEndDate-label" class="control-label"><g:message
                                                code="payPeriod.periodEndDate.label"
                                                default="Period End Date"/></h5>
                                        <div class="controls" style="margin-top:4px">
                                            <span class="property-value" aria-labelledby="firstPayPeriod-label">${payPeriodInstance?.periodEndDate?.toString("MMMMM dd, yyyy")}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${payPeriodInstance?.id}"/>
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
