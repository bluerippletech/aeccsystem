<%@ page import="com.ripple.master.item.ItemClass" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'itemClass.label', default: 'ItemClass')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <r:require modules="bootstrapjs,simplenso"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>

<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<g:hasErrors bean="${itemClassInstance}">
    <div class="alert alert-error">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        Validation has failed. Please correct your inputs.
        <ul>
            <g:eachError bean="${itemClassInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </div>
</g:hasErrors>
<g:form class="form-horizontal"
        method="post" >
<g:hiddenField name="id" value="${itemClassInstance?.id}"/>
<g:hiddenField name="version" value="${itemClassInstance?.version}"/>
<div class="row-fluid">
    <div class="span12" id="edit-itemClass" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.edit.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">

                    <fieldset>
                        <div class="row-fluid">
                            <f:all bean="itemClassInstance"/>
                        </div>
                    </fieldset>
                    <div class="form-actions">
                        <g:actionSubmit class="btn btn-primary" action="update"
                                        value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                        <g:actionSubmit class="btn" action="delete"
                                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                        formnovalidate=""
                                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</g:form>
</body>
</html>
