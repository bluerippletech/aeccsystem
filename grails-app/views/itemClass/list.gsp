<%@ page import="com.ripple.master.item.ItemClass" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'itemClass.label', default: 'ItemClass')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="bootstrapjs,simplenso,datatables"/>
    <style type="text/css">
    .mini{
        padding: 3px !important;
    }
    </style>
    <r:script>
        var subClass
    </r:script>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="list-itemClass" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>

                            <th><g:message code="itemClass.name.label" default="Name" /></th>

                            <th><g:message code="itemClass.description.label" default="Description" /></th>

                            <th><g:message code="itemClass.status.label" default="Status" /></th>

                            <th><g:message code="itemClass.actions.label" default="Actions" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            $('#datatable').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'itemClass', action:'listJSON')}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[
                                    
                                    { "sName": "name", "aTargets": [ 0 ] },
                                    
                                    { "sName": "description", "aTargets": [ 1 ] },
                                    
                                    { "sName": "status", "aTargets": [ 2 ],
                                        "fnRender": function (o,val){
                                            if(val=='Active'){
                                                return '<span class="label label-success">Active</span>';
                                            }
                                            else{
                                                return '<span class="label label-important">Inactive</span>';
                                            }
                                        }
                                    },

                                    { "bSortable": false, "aTargets": [ 3 ] }


                                ],
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    $('td:eq(3)', nRow).html( '<a class="btn btn-success mini" href="${createLink(action:'show')}/'+nRow.id+'"><i class="icon-zoom-in icon-white"></i></a>&nbsp;' );
                                $('td:eq(3)', nRow).append('<a class="btn btn-info mini" href="${createLink(action:'edit')}/'+nRow.id+'"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                $('td:eq(3)', nRow).append('<form method="POST" action="delete" name="delete" style="display:inline"><input type="hidden" name="id" value="'+nRow.id+'"><a class="btn btn-danger mini" href="javascript:" onclick="submitForm(this)"><i class="icon-trash icon-white"></i></a></form>');
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                    $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="list-itemSubclass" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="itemSubclass.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <div class="row-fluid" style="margin-bottom: 10px;">
                    <div class="span6">
                        <g:select name="itemSubclass" from="${itemClassInstanceList}" onchange="subClass.fnDraw();" optionKey="id" optionValue="name" noSelection="${['':'Select a Inventory Class']}"></g:select>
                    </div>
                    <div class="span6" style="text-align: right">
                        <a href="${createLink(controller: 'itemSubclass', action: 'create')}" class="btn btn-primary">Create a new Subclass</a>
                    </div>
                    </div>
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable2">
                        <thead>
                        <tr>

                            <th><g:message code="itemSubclass.name.label" default="Name" /></th>

                            <th><g:message code="itemSubclass.description.label" default="Description" /></th>

                            <th><g:message code="itemSubclass.status.label" default="Status" /></th>

                            <th><g:message code="itemSubclass.actions.label" default="Actions" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            subClass = $('#datatable2').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'itemSubclass', action:'listJSON')}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[

                                    { "sName": "name", "aTargets": [ 0 ] },

                                    { "sName": "description", "aTargets": [ 1 ] },

                                    { "sName": "status", "aTargets": [ 2 ],
                                        "fnRender": function (o,val){
                                            if(val=='Active'){
                                                return '<span class="label label-success">Active</span>';
                                            }
                                            else{
                                                return '<span class="label label-important">Inactive</span>';
                                            }
                                        }
                                    },

                                    { "bSortable": false, "aTargets": [ 3 ] }


                                ],
                                "fnServerData": function ( sSource, aoData, fnCallback ) {

                                    aoData.push( { "name": "itemClass.id", "value": $('#itemSubclass').val() } );


                                    $.getJSON( sSource, aoData, function (json) {
                                        fnCallback(json);
                                    } );
                                },
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    $('td:eq(3)', nRow).html( '<a class="btn btn-success mini" href="${createLink(controller: 'itemSubclass', action:'show')}/'+nRow.id+'"><i class="icon-zoom-in icon-white"></i></a>&nbsp;' );
                                    $('td:eq(3)', nRow).append('<a class="btn btn-info mini" href="${createLink(controller: 'itemSubclass',action:'edit')}/'+nRow.id+'"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                    $('td:eq(3)', nRow).append('<form method="POST" action="${createLink(controller: 'itemSubclass', action: 'delete')}" name="delete" style="display:inline"><input type="hidden" name="id" value="'+nRow.id+'"><a class="btn btn-danger mini" href="javascript:" onclick="submitForm(this)"><i class="icon-trash icon-white"></i></a></form>');
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
