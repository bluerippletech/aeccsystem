<%@ page import="com.ripple.master.item.ItemClass" %>



<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="itemClass.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${itemClassInstance.constraints.isDeleted.inList}" value="${itemClassInstance?.isDeleted}" valueMessagePrefix="itemClass.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="itemClass.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${itemClassInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="itemClass.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${itemClassInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="itemClass.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${itemClassInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="itemClass.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${itemClassInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="itemClass.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${itemClassInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="itemClass.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${itemClassInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemClassInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="itemClass.status.label" default="Status" />
		
	</label>
	<g:select name="status" from="${itemClassInstance.constraints.status.inList}" value="${itemClassInstance?.status}" valueMessagePrefix="itemClass.status" noSelection="['': '']"/>
</div>

