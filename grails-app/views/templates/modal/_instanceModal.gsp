<div class="modal hide" id="${idHolder}Modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3><span id="modalAction">New</span> ${entity}</h3>
    </div>
    <g:form action="create">
        <div class="modal-body">
            <input type="hidden" name="id" id="id" value="">
            <f:all bean="modelInstance"/>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Close</a>
            <a href="javascript:" id="deleteButton" style="display: none;" onclick="deleteAjax('${idHolder}', '${className}', '${parentProperty}', '${parentId}')" class="btn btn-danger mini"><i class="icon-trash icon-white"></i> Delete</a>
            <a href="javascript:" onclick="createAjax(this, '${idHolder}', '${className}', '${parentProperty}', '${parentId}')" class="btn btn-primary">Save changes</a>
        </div>
    </g:form>
</div>