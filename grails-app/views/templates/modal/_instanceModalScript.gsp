<script type="text/javascript">
    function createAjax(obj, idHolder, className, parentProperty, parentId){
        $.ajax({
            url:"${createLink(uri: '/')}"+idHolder+"/saveAjax",
            type:"POST",
            data:$(obj).closest('form').serialize(),
            //dataType:"json",
            beforeSend:function () {
            },
            success:function (results) {
                $('#'+idHolder+'Modal').modal('toggle')
                try{
                    if(dataTableArray[idHolder])
                        dataTableArray[idHolder].fnDraw();
                    else
                        getListAjax(idHolder, className, parentProperty, parentId);
                }catch (e){
                    getListAjax(idHolder, className, parentProperty, parentId);
                    console.log(e);
                }
                $.jGrowl("Record with ID "+ results.id + " has been successfully saved/updated.");
            },
            error:function(error) {
                displayErrors(error.responseText,$(obj).closest('form'));
            },
            complete: function(data){

            }
        })
    }
    function deleteAjax(idHolder, className, parentProperty, parentId){
        $.ajax({
            url:"${createLink(uri: '/')}"+idHolder+"/deleteAjax",
            type:"POST",
            data:{'id':$('#'+idHolder+'Modal').find('form').find("[name='id']").val()},
            //dataType:"json",
            beforeSend:function () {
            },
            success:function (results) {
                $('#'+idHolder+'Modal').modal('toggle')
                try{
                    if(dataTableArray[idHolder])
                        dataTableArray[idHolder].fnDraw();
                    else
                        getListAjax(idHolder, className, parentProperty, parentId);
                }catch (e){
                    getListAjax(idHolder, className, parentProperty, parentId);
                    console.log(e);
                }
                $.jGrowl("Record has been successfully deleted.");
//                location.reload();
            },
            error:function(error) {
                $('#'+idHolder+'Modal').modal('toggle')
                location.reload();
            },
            complete: function(data){

            }
        })
    }
    function displayErrors(errorsJSON,form){
        var errors = $.parseJSON(errorsJSON);
        $.each( errors.errors, function(anObject){
            if($(form).find("[name='"+this.field+".id']")){
                if(!$(form).find("[name='"+this.field+".id']").parent().parent().hasClass('error')){
                    $(form).find("[name='"+this.field+".id']").parent().parent().addClass('error')
                    $(form).find("[name='"+this.field+".id']").parent().append("<span class='help-inline'>"+this.message+"</span>");
                }
            }else{
                if(!$(form).find("[name='"+this.field+"']").parent().parent().hasClass('error')){
                    $(form).find("[name='"+this.field+"']").parent().parent().addClass('error')
                    $(form).find("[name='"+this.field+"']").parent().append("<span class='help-inline'>"+this.message+"</span>");
                }
            }
        });
    }

    function renderModal(id, idHolder){
        if(id){
            $.ajax({
                url:"${createLink(uri: '/')}"+idHolder+"/getInstance",
                type:"POST",
                data:{'id':id},
                beforeSend:function () {
                },
                success:function (results) {
                    fillFields(results,$('#'+idHolder+'Modal'));
                    $('#'+idHolder+'Modal').find("#deleteButton").show();
                    $('#'+idHolder+'Modal').find("#modalAction").html("Edit");
                    $('#'+idHolder+'Modal').modal('show');
                },
                error:function(error) {

                },
                complete: function(data){

                }
            })
        }else{
            $.ajax({
                url:"${createLink(uri: '/')}"+idHolder+"/getInstance",
                type:"POST",
                <g:if test="${attrs.parentProperty instanceof java.lang.String}">
                    data:{"${attrs.parentProperty}.id":'${attrs.parentId}'},
                </g:if>
                <g:elseif test="${attrs.parentProperty instanceof java.util.List}">
                    data:{
                        <g:each in="${attrs.parentProperty}" var="parent" status="i">
                            "${parent}.id":${attrs.parentId}${i<(attrs.parentProperty.size()-1)?',':''}
                        </g:each>
                    },
                </g:elseif>
                <g:else>
                    data:{},
                </g:else>
                beforeSend:function () {
                },
                success:function (results) {
                    fillFields(results,$('#'+idHolder+'Modal'));
                    $('#'+idHolder+'Modal').find("form").get(0).reset()
                    $('#'+idHolder+'Modal').find('form').find("[name='id']").val('')
                    $('#'+idHolder+'Modal').find("#deleteButton").hide();
                    $('#'+idHolder+'Modal').find("#modalAction").html("New");
                    $('#'+idHolder+'Modal').modal('show');
                },
                error:function(error) {

                },
                complete: function(data){

                }
            })
        }
    }

//    function fillFields(instanceJSON, modal){
//        _.each(instanceJSON, function(value, key){
//            if(_.isObject(value)){
//                _.each(value, function(objValue, objKey){
//                    modal.find('form').find("[name='"+key+"."+objKey+"']").val(objValue)
//                });
//            }
//            else
//                modal.find('form').find("[name='"+key+"']").val(value)
//        });
//        modal.find('.error').find('.help-inline').remove()
//        modal.find('.error').removeClass('error')
//    }

    function fillFields(instanceJSON, container){
        _.each(instanceJSON, function(value, key){
            if(_.isObject(value)){
                _.each(value, function(objValue, objKey){
                    container.find('form').find("[name='"+key+"."+objKey+"']").val(objValue)
                    if(!objValue){
                        container.find('form').find("[name='"+key+"."+objKey+"']").val('')
                    }
                });
            }else if(value === true || value === false){
                container.find('form').find("[name='"+key+"']").val('true')
                if(value == true)
                    container.find('form').find("[name='"+key+"']").attr('checked','checked')
                else
                    container.find('form').find("[name='"+key+"']").removeAttr('checked')
            }else if(!value){
                if(value == 0)
                    container.find('form').find("[name='"+key+"']").val(value)
                else
                    container.find('form').find("[name='"+key+"']").val('')
            }
            else
                container.find('form').find("[name='"+key+"']").val(value)
        });
        container.find('.error').find('.help-inline').remove()
        container.find('.error').removeClass('error')
    }


    function getListAjax(idHolder, className, parentProperty, parentId){
        var stringList = ""
        $.ajax({
            url:"${createLink(uri: '/')}"+idHolder+"/getListing",
            type:"POST",
            data:{className:className, parentProperty:parentProperty, parentId:parentId},
            beforeSend:function () {
            },
            success:function (results) {
                _.each(results, function(value){
                    stringList += "<li>"
                    stringList += "<a href=\"javascript:\" onclick=\"renderModal('"+value['id']+"', '"+idHolder+"')\">"+value['name']+"</a>"
                    stringList += "</li>"
                });
            },
            error:function(error) {

            },
            complete: function(data){
                $("#"+idHolder+"Listing").html(stringList);
            }
        })
    }
</script>