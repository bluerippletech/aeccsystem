<r:script>
    $(function(){
        $("select#${attrs.itemId}").change(function(){changeCategory(this)});
    })

    function changeCategory(selectObj){
        $.getJSON("${createLink(action: 'getSubCat')}",{'id': $(selectObj).val(), ajax: 'true'}, function(j){
            var options = '';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].optionKey + '">' + j[i].optionValue + '</option>';
            }
            $("select#${attrs.subItemId}").html(options);
        })
    };
</r:script>