<a class="btn btn-info pull-left" onclick="table.fnDraw();$('#itemPicker').fadeIn()"><i class="icon-zoom-in icon-white"></i></a>
<div id="itemPicker" class="modal hide">
    <div class="modal-header">
        <button type="button" class="close" onclick="$('#itemPicker').fadeOut()">×</button>
        <h3>${attrs?.pickerName}</h3>
    </div>
    <div class="modal-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="itemPickerTable">
            <thead>
            <tr>
                <g:each in="${attrs.fields.split(',')}">
                    <th><g:message code="${attrs?.controller}.${it}.label" default="${it}" /></th>
                </g:each>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    var table;
    $(function(){
        table = $('#itemPickerTable').dataTable( {
            bProcessing: true,
            bServerSide: true,
            iDisplayLength: 10,
            sAjaxSource: '${createLink(controller:"${attrs?.controller}", action:"${attrs?.action}", id: "${params.id}")}',
            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            },
            "aoColumnDefs":[

                <g:each in="${attrs.fields.split(',')}" var="fields" status="i">
                    { "sName": "${it}", "aTargets": [ ${i} ] },
                </g:each>
            ]
            } );

        });

        $(document).ready( function(){
         $('#itemPicker').hide();
         $('#modalContainer').prepend($('#itemPicker'))

        });

        $('#'+"${attrs.classId}"+'Modal').on('hidden', function () {
            $('#itemPicker').hide();
        });

        $('#itemPicker tbody tr').live('dblclick', function () {
            var rowName=$(this).children(':first').next().text();
            if(confirm('Are you sure you want to select '+rowName+'')){
                $('#${attrs?.property}\\.id').val(this.id)
                $('#${attrs?.searchProperty}').val(rowName);
                $('#itemPicker').fadeOut();
                ${attrs.onComplete}($('#${attrs?.property}\\.id'));
            }
        })
</script>