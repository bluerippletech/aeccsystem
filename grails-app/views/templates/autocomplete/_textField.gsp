<input type="hidden" name="${attrs.property}.id" id="${attrs.id?:attrs.property}.id" value="${attrs.valueId}"/>
<input type="text" class="${attrs.class}" autocomplete="off" data-provide="typeahead" name="${attrs.name}" id="${attrs.id?:attrs.name}" value="${attrs.value}" />
<script type="text/javascript">
    $(function(){
        var ${attrs.id}Data
        $('#${attrs.id}').typeahead({
            source: function(query, process) {
                $.post('${createLink(controller: attrs.controller, action: attrs.action)}', { q: query, limit: 4, parentId: "${params.id}" }, function(data) {
                    var suggest = []
                    ${attrs.id}Data = data;
                    _.each(data, function(value){
                        suggest.push(value.name);
                    });
                    process(suggest);
                }); // end of post
            }
        });
        $('#${attrs.id}').blur(function(){
            if(_.isObject(${attrs.id}Data)){
                _.each(${attrs.id}Data, function(value){
                    if(value.name == $('#${attrs.id}').val())
                        $('#${attrs.id}\\.id').val(value.id);
                });
            }
            ${attrs.onComplete}($('#${attrs.id}\\.id'));
        })
    })
</script>