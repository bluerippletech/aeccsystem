<%@ page import="com.ripple.equipment.EquipmentType" %>



<div class="fieldcontain ${hasErrors(bean: equipmentTypeInstance, field: 'type', 'error')} ">
	<label for="type">
		<g:message code="equipmentType.type.label" default="Type" />
		
	</label>
	<g:textField name="type" value="${equipmentTypeInstance?.type}"/>
</div>

