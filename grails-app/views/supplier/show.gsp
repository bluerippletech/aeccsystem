
<%@ page import="com.ripple.master.supplier.Supplier" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'supplier.label', default: 'Supplier')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-supplier" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="createdDate-label" class="control-label"><g:message
                                                    code="supplier.createdDate.label"
                                                    default="Created Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="createdDate-label"><g:formatDate
                                                        date="${supplierInstance?.createdDate}" format="MMMMM dd, yyyy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="editedBy-label" class="control-label"><g:message
                                                    code="supplier.editedBy.label"
                                                    default="Edited By"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="editedBy-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="editedBy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="supplierCode-label" class="control-label"><g:message
                                                    code="supplier.supplierCode.label"
                                                    default="Supplier Code"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="supplierCode-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="supplierCode"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="taxIdentificationNo-label" class="control-label"><g:message
                                                    code="supplier.taxIdentificationNo.label"
                                                    default="Tax Identification No"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="taxIdentificationNo-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="taxIdentificationNo"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="email-label" class="control-label"><g:message
                                                    code="supplier.email.label"
                                                    default="Email"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="email-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="email"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="primaryAddress-label" class="control-label"><g:message
                                                    code="supplier.primaryAddress.label"
                                                    default="Primary Address"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="primaryAddress-label"><g:link
                                                        controller="address" action="show"
                                                        id="${supplierInstance?.primaryAddress?.id}">${supplierInstance?.primaryAddress?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="addresses-label" class="control-label"><g:message
                                                    code="supplier.addresses.label"
                                                    default="Addresses"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${supplierInstance.addresses}" var="a">
                                                    <span class="property-value" aria-labelledby="addresses-label"><g:link
                                                            controller="address" action="show"
                                                            id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="supplierCredits-label" class="control-label"><g:message
                                                    code="supplier.supplierCredits.label"
                                                    default="Supplier Credits"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${supplierInstance.supplierCredits}" var="s">
                                                    <span class="property-value" aria-labelledby="supplierCredits-label"><g:link
                                                            controller="supplierCredit" action="show"
                                                            id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="isDeleted-label" class="control-label"><g:message
                                                    code="supplier.isDeleted.label"
                                                    default="Is Deleted"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="isDeleted-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="isDeleted"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="editedDate-label" class="control-label"><g:message
                                                    code="supplier.editedDate.label"
                                                    default="Edited Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="editedDate-label"><g:formatDate
                                                        date="${supplierInstance?.editedDate}" format="MMMMM dd, yyyy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="createdBy-label" class="control-label"><g:message
                                                    code="supplier.createdBy.label"
                                                    default="Created By"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="createdBy-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="createdBy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="supplierName-label" class="control-label"><g:message
                                                    code="supplier.supplierName.label"
                                                    default="Supplier Name"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="supplierName-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="supplierName"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="website-label" class="control-label"><g:message
                                                    code="supplier.website.label"
                                                    default="Website"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="website-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="website"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="infoStatus-label" class="control-label"><g:message
                                                    code="supplier.infoStatus.label"
                                                    default="Info Status"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="infoStatus-label"><g:fieldValue
                                                        bean="${supplierInstance}" field="infoStatus"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="primaryAddressContact-label" class="control-label"><g:message
                                                    code="supplier.primaryAddressContact.label"
                                                    default="Primary Address Contact"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="primaryAddressContact-label"><g:link
                                                        controller="contactPerson" action="show"
                                                        id="${supplierInstance?.primaryAddressContact?.id}">${supplierInstance?.primaryAddressContact?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="contactPersons-label" class="control-label"><g:message
                                                    code="supplier.contactPersons.label"
                                                    default="Contact Persons"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${supplierInstance.contactPersons}" var="c">
                                                    <span class="property-value" aria-labelledby="contactPersons-label"><g:link
                                                            controller="contactPerson" action="show"
                                                            id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${supplierInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${supplierInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
