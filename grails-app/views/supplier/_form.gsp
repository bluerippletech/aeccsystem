<%@ page import="com.ripple.master.supplier.Supplier" %>



<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="supplier.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${supplierInstance.constraints.isDeleted.inList}" value="${supplierInstance?.isDeleted}" valueMessagePrefix="supplier.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="supplier.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${supplierInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="supplier.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${supplierInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="supplier.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${supplierInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="supplier.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${supplierInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'supplierCode', 'error')} ">
	<label for="supplierCode">
		<g:message code="supplier.supplierCode.label" default="Supplier Code" />
		
	</label>
	<g:textField name="supplierCode" value="${supplierInstance?.supplierCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'supplierName', 'error')} ">
	<label for="supplierName">
		<g:message code="supplier.supplierName.label" default="Supplier Name" />
		
	</label>
	<g:textField name="supplierName" value="${supplierInstance?.supplierName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'taxIdentificationNo', 'error')} ">
	<label for="taxIdentificationNo">
		<g:message code="supplier.taxIdentificationNo.label" default="Tax Identification No" />
		
	</label>
	<g:textField name="taxIdentificationNo" value="${supplierInstance?.taxIdentificationNo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'website', 'error')} ">
	<label for="website">
		<g:message code="supplier.website.label" default="Website" />
		
	</label>
	<g:textField name="website" value="${supplierInstance?.website}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="supplier.email.label" default="Email" />
		
	</label>
	<g:textField name="email" value="${supplierInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'infoStatus', 'error')} required">
	<label for="infoStatus">
		<g:message code="supplier.infoStatus.label" default="Info Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="infoStatus" from="${supplierInstance.constraints.infoStatus.inList}" required="" value="${supplierInstance?.infoStatus}" valueMessagePrefix="supplier.infoStatus"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'primaryAddress', 'error')} ">
	<label for="primaryAddress">
		<g:message code="supplier.primaryAddress.label" default="Primary Address" />
		
	</label>
	<g:select id="primaryAddress" name="primaryAddress.id" from="${com.ripple.master.geography.Address.list()}" optionKey="id" value="${supplierInstance?.primaryAddress?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'primaryAddressContact', 'error')} ">
	<label for="primaryAddressContact">
		<g:message code="supplier.primaryAddressContact.label" default="Primary Address Contact" />
		
	</label>
	<g:select id="primaryAddressContact" name="primaryAddressContact.id" from="${com.ripple.master.sociology.ContactPerson.list()}" optionKey="id" value="${supplierInstance?.primaryAddressContact?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'addresses', 'error')} ">
	<label for="addresses">
		<g:message code="supplier.addresses.label" default="Addresses" />
		
	</label>
	<g:select name="addresses" from="${com.ripple.master.geography.Address.list()}" multiple="multiple" optionKey="id" size="5" value="${supplierInstance?.addresses*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'contactPersons', 'error')} ">
	<label for="contactPersons">
		<g:message code="supplier.contactPersons.label" default="Contact Persons" />
		
	</label>
	<g:select name="contactPersons" from="${com.ripple.master.sociology.ContactPerson.list()}" multiple="multiple" optionKey="id" size="5" value="${supplierInstance?.contactPersons*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: supplierInstance, field: 'supplierCredits', 'error')} ">
	<label for="supplierCredits">
		<g:message code="supplier.supplierCredits.label" default="Supplier Credits" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${supplierInstance?.supplierCredits?}" var="s">
    <li><g:link controller="supplierCredit" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="supplierCredit" action="create" params="['supplier.id': supplierInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'supplierCredit.label', default: 'SupplierCredit')])}</g:link>
</li>
</ul>

</div>

