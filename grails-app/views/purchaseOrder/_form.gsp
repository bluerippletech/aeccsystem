<%@ page import="com.ripple.purchasing.PurchaseOrder" %>



<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="purchaseOrder.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${purchaseOrderInstance.constraints.isDeleted.inList}" value="${purchaseOrderInstance?.isDeleted}" valueMessagePrefix="purchaseOrder.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="purchaseOrder.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${purchaseOrderInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="purchaseOrder.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${purchaseOrderInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="purchaseOrder.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${purchaseOrderInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="purchaseOrder.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${purchaseOrderInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'purchaseOrderNumber', 'error')} ">
	<label for="purchaseOrderNumber">
		<g:message code="purchaseOrder.purchaseOrderNumber.label" default="Purchase Order Number" />
		
	</label>
	<g:textField name="purchaseOrderNumber" value="${purchaseOrderInstance?.purchaseOrderNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'supplier', 'error')} required">
	<label for="supplier">
		<g:message code="purchaseOrder.supplier.label" default="Supplier" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="supplier" name="supplier.id" from="${com.ripple.master.supplier.Supplier.list()}" optionKey="id" required="" value="${purchaseOrderInstance?.supplier?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="purchaseOrder.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="status" from="${purchaseOrderInstance.constraints.status.inList}" required="" value="${purchaseOrderInstance?.status}" valueMessagePrefix="purchaseOrder.status"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'dateClosed', 'error')} required">
	<label for="dateClosed">
		<g:message code="purchaseOrder.dateClosed.label" default="Date Closed" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="dateClosed" value="${purchaseOrderInstance?.dateClosed}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'dateStarted', 'error')} required">
	<label for="dateStarted">
		<g:message code="purchaseOrder.dateStarted.label" default="Date Started" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="dateStarted" value="${purchaseOrderInstance?.dateStarted}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'purchaseOrderDetails', 'error')} ">
	<label for="purchaseOrderDetails">
		<g:message code="purchaseOrder.purchaseOrderDetails.label" default="Purchase Order Details" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${purchaseOrderInstance?.purchaseOrderDetails?}" var="p">
    <li><g:link controller="purchaseOrderDetail" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="purchaseOrderDetail" action="create" params="['purchaseOrder.id': purchaseOrderInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'purchaseOrderDetail.label', default: 'PurchaseOrderLineItem')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderInstance, field: 'warehouse', 'error')} required">
	<label for="warehouse">
		<g:message code="purchaseOrder.warehouse.label" default="Warehouse" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="warehouse" name="warehouse.id" from="${com.ripple.master.Warehouse.list()}" optionKey="id" required="" value="${purchaseOrderInstance?.warehouse?.id}" class="many-to-one"/>
</div>

