<%@ page import="com.ripple.purchasing.PurchaseOrder" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'purchaseOrder.label', default: 'PurchaseOrder')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <r:script>
        var dataTableArray = {};
        $(function(){$('#'+"purchaseOrderLineItemModal").on('show', function () {
            changeCategory($('#item\\.id'))
            $.post('${createLink(controller: 'purchaseOrderLineItem', action: 'getUnitMeasure')}', { 'id': $('#purchaseOrderLineItemModal').find('#id').val() }, function(data) {
                $("#purchaseOrderLineItemModal").find("#unitOfMeasure\\.id").ajaxComplete(function(event,request, settings){
                    $(this).val(data)
                });
            }); // end of post
        })});
    </r:script>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-purchaseOrder" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="purchaseOrderNumber-label" class="control-label"><g:message
                                                code="purchaseOrder.purchaseOrderNumber.label"
                                                default="Purchase Order Number"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="purchaseOrderNumber-label"><g:fieldValue
                                                    bean="${purchaseOrderInstance}" field="purchaseOrderNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="procurementNumber-label" class="control-label"><g:message
                                                code="purchaseOrder.procurement.label"
                                                default="Procurement Number"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="procurementNumber-label">
                                                <g:link controller="procurement" action="show"
                                                        id="${purchaseOrderInstance?.procurement?.id}">${purchaseOrderInstance?.procurement?.encodeAsHTML()}</g:link>
                                            </span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="supplier-label" class="control-label"><g:message
                                                code="purchaseOrder.supplier.label"
                                                default="Supplier"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="supplier-label"><g:link
                                                    controller="supplier" action="show"
                                                    id="${purchaseOrderInstance?.supplier?.id}">${purchaseOrderInstance?.supplier?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="warehouse-label" class="control-label"><g:message
                                                code="purchaseOrder.warehouse.label"
                                                default="Warehouse"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="warehouse-label"><g:link
                                                    controller="warehouse" action="show"
                                                    id="${purchaseOrderInstance?.warehouse?.id}">${purchaseOrderInstance?.warehouse?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="status-label" class="control-label"><g:message
                                                code="purchaseOrder.status.label"
                                                default="Status"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                    bean="${purchaseOrderInstance}" field="status"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="dateStarted-label" class="control-label"><g:message
                                                code="purchaseOrder.dateStarted.label"
                                                default="Date Started"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="dateStarted-label"><g:fieldValue
                                                    bean="${purchaseOrderInstance}" field="dateStarted"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="dateClosed-label" class="control-label"><g:message
                                                code="purchaseOrder.dateClosed.label"
                                                default="Date Closed"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="dateClosed-label"><g:fieldValue
                                                    bean="${purchaseOrderInstance}" field="dateClosed"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="totalAmount-label" class="control-label"><g:message
                                                code="purchaseOrder.totalAmount.label"
                                                default="Total Amount"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="totalAmount-label">
                                                ${com.ripple.util.Formatter?.formatCurrency(purchaseOrderInstance?.getTotalAmount())}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>

                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${purchaseOrderInstance?.id}"/>

                            <g:if test="${purchaseOrderInstance?.status=='Approved'}">
                                <sec:ifAnyGranted roles="ROLE_ADMIN">
                                    <g:actionSubmit class="btn btn-primary" action="edit"
                                                    value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>
                                </sec:ifAnyGranted>
                            </g:if>
                            <g:else>
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>
                            </g:else>

                            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_USER">
                                <input type="hidden" name="version" value="${purchaseOrderInstance?.version}"/>
                                <g:if test="${purchaseOrderInstance?.status=='Draft'}">
                                    <g:actionSubmit class="btn btn-success" action="formStatus"
                                                    value="${message(code: 'purchaseOrder.forApproval.label', default: 'For Approval')}"/>
                                </g:if>
                                <g:actionSubmit class="btn btn-danger" action="formStatus"
                                                value="${message(code: 'purchaseOrder.cancel.label', default: 'Cancel')}"/>
                            </sec:ifAnyGranted>

                            <sec:ifAnyGranted roles="ROLE_ADMIN">
                                <g:actionSubmit class="btn btn-primary" action="formStatus"
                                                value="${message(code: 'purchaseOrder.disapproved.label', default: 'Send back to draft')}"/>
                                <g:if test="${purchaseOrderInstance?.status=='For Approval'}">
                                    <g:actionSubmit class="btn btn-success" action="formStatus"
                                                    value="${message(code: 'purchaseOrder.approved.label', default: 'Approve')}"/>
                                    <g:actionSubmit class="btn btn-danger" action="formStatus"
                                                    value="${message(code: 'purchaseOrder.disapproved.label', default: 'Disapprove')}"/>
                                </g:if>
                            </sec:ifAnyGranted>
                            
                            <g:actionSubmit class="btn btn-info pull-right" action="newFromExisting"
                                        value="${message(code: 'purchaseOrder.newFromExisting.label', default: 'Create New Copy')}"
                                        onclick="return confirm('${message(code: 'purchaseOrder.newFromExisting.confirm.message', default: 'Are you sure you want to create a new copy of this order?')}');"/>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12" id="list-purchaseOrderLineItem" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="purchaseOrderLineItem.label"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <a id="showModal" href="javascript:" onclick="renderModal('', 'purchaseOrderLineItem')"
                       class="btn btn-primary" style="margin-bottom:10px;">Add New Line Item</a>
                    <table cellpadding="0" cellspacing="0" border="0"
                           class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>

                            <th><g:message code="purchaseOrderLineItem.itemCode.label" default="Item Code"/></th>

                            <th><g:message code="purchaseOrderLineItem.itemName.label" default="Item Name"/></th>

                            <th><g:message code="purchaseOrderLineItem.quantity.label" default="Quantity"/></th>

                            <th><g:message code="purchaseOrderLineItem.unitOfMeasure.label"
                                           default="Unit of Measure"/></th>

                            <th><g:message code="purchaseOrderLineItem.unitPrice.label" default="Unit Price"/></th>

                            <th><g:message code="purchaseOrderLineItem.amount.label" default="Amount"/></th>

                            <th><g:message code="purchaseOrderLineItem.actions.label" default="Actions"/></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div style="text-align: right;margin-top:10px">
                        <a class="jasperButton btn btn-info" title="PDF" href="${createLink(controller: 'report', params: [_format: 'PDF', _name: 'Purchase Order', PURCHASE_ORDER_ID: "${purchaseOrderInstance?.id}" , _file: 'purchaseorder'])}">
                        Print Purchase Order
                        </a>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            dataTableArray['purchaseOrderLineItem'] = $('#datatable').dataTable({
                                bProcessing:true,
                                bServerSide:true,
                                iDisplayLength:10,
                                sAjaxSource:'${createLink(controller:'purchaseOrderLineItem', action:'listJSON', id: "${purchaseOrderInstance?.id}")}',
                                "sDom":"<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType":"bootstrap",
                                "oLanguage":{
                                    "sLengthMenu":"_MENU_ records per page"
                                },
                                "aoColumnDefs":[

                                    { "sName":"item", "aTargets":[ 0 ] },

                                    { "sName":"item", "aTargets":[ 1 ] },

                                    { "sName":"quantity", "aTargets":[ 2 ] },

                                    { "sName":"unitOfMeasure", "aTargets":[ 3 ] },

                                    { "sName":"unitPrice", "aTargets":[ 4 ],
                                        "fnRender":function (o, val) {
                                            if (val == 'P 0.00') {
                                                return '<span style="color:red">' + val + '</span>';
                                            }
                                            else {
                                                return val;
                                            }
                                        }
                                    },

                                    { "sName":"amount", "aTargets":[ 5 ],
                                        "fnRender":function (o, val) {
                                            if (val == 'P 0.00') {
                                                return '<span style="color:red">' + val + '</span>';
                                            }
                                            else {
                                                return val;
                                            }
                                        }
                                    },

                                    { "bSortable":false, "aTargets":[ 6 ] }


                                ],
                                "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                                    $('td:eq(6)', nRow).append('<a class="btn btn-info mini" href="javascript:" onclick="renderModal(\'' + nRow.id + '\', \'purchaseOrderLineItem\')"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                }
                            });

                        })
                        function submitForm(elem) {
                            if (confirm("Are you sure?"))
                                $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalContainer">
    <ripple:modal className="PurchaseOrderLineItem" entity="Purchase Order Line Item" elementId="purchaseOrderLineItem"
                  parentId="${purchaseOrderInstance.id}" parentClass="PurchaseOrder" parentProperty="purchaseOrder"/>
    <ripple:modalScript parentProperty="purchaseOrder" parentId="${purchaseOrderInstance.id}"/>
</div>
</body>
</html>
