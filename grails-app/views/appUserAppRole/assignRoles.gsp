<%@ page import="com.ripple.security.AppUser; com.ripple.security.AppUserAppRole" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'appUserAppRole.label', default: 'AppUserAppRole')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <style type="text/css">
    .mini{
        padding: 3px !important;
    }
    </style>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    %{--<div class="span1 action-btn round-all">--}%
        %{--<a href="${createLink(action: 'list')}">--}%
            %{--<div><i class="icon-align-justify"></i></div>--}%

            %{--<div><strong>List</strong></div>--}%
        %{--</a>--}%
    %{--</div>--}%

    %{--<div class="span1 action-btn round-all">--}%
        %{--<a href="${createLink(action: 'create')}">--}%
            %{--<div><i class="icon-pencil"></i></div>--}%

            %{--<div><strong>Create</strong></div>--}%
        %{--</a>--}%
    %{--</div>--}%
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="list-appUserAppRole">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <g:form controller="appUserAppRole" action="updateRoles" class="form-horizontal" onsubmit="confirm('Are you sure?')">
                <div class="box-content">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>
                            <th><g:message code="appUserAppRole.appRole.username.label" default="Username" /></th>
                            <g:each in="${com.ripple.security.AppRole.list()}" var="role" status="i">
                                <th><g:message code="appUserAppRole.appRole.${role.toString()}.label" default="${role.toString()}" /></th>
                            </g:each>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        var checked = []
                        var unchecked = []

                        $(function(){
                            $('#datatable').dataTable( {
                                //"sScrollY": 200,
                                "sScrollX": "100%",
                                //"sScrollXInner": "110%",
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'appUserAppRole', action:'assignRoleJSON')}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[

                                    { "sName": "username", "aTargets": [ 0 ] },
                                <g:set var="roles" value="${com.ripple.security.AppRole.list()}"/>
                                <g:each in="${roles}" var="role" status="i">
                                    { "sName": "${role.authority}", "aTargets": [ ${i+1} ] }${i<com.ripple.security.AppRole.count()-1?',':''}
                                </g:each>
                                ],
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    <%--
                                    <g:each in="${roles}" var="role" status="i">
                                        $('td:eq(${i+1})', nRow).html( '<input type="checkbox" name="'+nRow.id+'" value="${role.id}" class="assignment" <g:if test="${role.id in AppUser.get()}">checked</g:if>/>' );
                                    </g:each>
                                    --%>
                                }
                            } );
                        })
                    </script>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </g:form>
            </div><!--/span-->

        </div>
    </div>
</div>
</body>
</html>
