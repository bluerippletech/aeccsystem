<%@ page import="com.ripple.loans.LoanPayment" %>



<div class="fieldcontain ${hasErrors(bean: loanPaymentInstance, field: 'remarks', 'error')} ">
	<label for="remarks">
		<g:message code="loanPayment.remarks.label" default="Remarks" />
		
	</label>
	<g:textField name="remarks" value="${loanPaymentInstance?.remarks}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanPaymentInstance, field: 'payrollEntry', 'error')} ">
	<label for="payrollEntry">
		<g:message code="loanPayment.payrollEntry.label" default="Payroll Entry" />
		
	</label>
	<g:select id="payrollEntry" name="payrollEntry.id" from="${com.ripple.entry.PayrollEntry.list()}" optionKey="id" value="${loanPaymentInstance?.payrollEntry?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanPaymentInstance, field: 'amount', 'error')} required">
	<label for="amount">
		<g:message code="loanPayment.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="amount" required="" value="${fieldValue(bean: loanPaymentInstance, field: 'amount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanPaymentInstance, field: 'loan', 'error')} required">
	<label for="loan">
		<g:message code="loanPayment.loan.label" default="Loan" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="loan" name="loan.id" from="${com.ripple.loans.Loan.list()}" optionKey="id" required="" value="${loanPaymentInstance?.loan?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: loanPaymentInstance, field: 'paymentDate', 'error')} required">
	<label for="paymentDate">
		<g:message code="loanPayment.paymentDate.label" default="Payment Date" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="paymentDate" value="${loanPaymentInstance?.paymentDate}" ></joda:datePicker>
</div>

