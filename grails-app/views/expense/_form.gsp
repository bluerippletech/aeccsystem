<%@ page import="com.ripple.project.Expense" %>



<div class="fieldcontain ${hasErrors(bean: expenseInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="expense.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="type" name="type.id" from="${com.ripple.project.ExpenseType.list()}" optionKey="id" required="" value="${expenseInstance?.type?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseInstance, field: 'expenseDate', 'error')} required">
	<label for="expenseDate">
		<g:message code="expense.expenseDate.label" default="Expense Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="expenseDate" precision="day"  value="${expenseInstance?.expenseDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: expenseInstance, field: 'remarks', 'error')} ">
	<label for="remarks">
		<g:message code="expense.remarks.label" default="Remarks" />
		
	</label>
	<g:textField name="remarks" value="${expenseInstance?.remarks}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="expense.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project" name="project.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" required="" value="${expenseInstance?.project?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseInstance, field: 'quantity', 'error')} required">
	<label for="quantity">
		<g:message code="expense.quantity.label" default="Quantity" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="quantity" required="" value="${fieldValue(bean: expenseInstance, field: 'quantity')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseInstance, field: 'unitPrice', 'error')} required">
	<label for="unitPrice">
		<g:message code="expense.unitPrice.label" default="Unit Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="unitPrice" required="" value="${fieldValue(bean: expenseInstance, field: 'unitPrice')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseInstance, field: 'voucherNumber', 'error')} ">
	<label for="voucherNumber">
		<g:message code="expense.voucherNumber.label" default="Voucher Number" />
		
	</label>
	<g:textField name="voucherNumber" value="${expenseInstance?.voucherNumber}"/>
</div>

