<%@ page import="com.ripple.master.Warehouse" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'warehouse.label', default: 'Warehouse')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="bootstrapjs,simplenso"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-warehouse" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="name-label" class="control-label"><g:message
                                                    code="warehouse.name.label"
                                                    default="Name"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="name-label"><g:fieldValue
                                                        bean="${warehouseInstance}" field="name"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="status-label" class="control-label"><g:message
                                                    code="warehouse.status.label"
                                                    default="Status"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                        bean="${warehouseInstance}" field="status"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="location-label" class="control-label"><g:message
                                                    code="warehouse.location.label"
                                                    default="Location"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="location-label"><g:fieldValue
                                                        bean="${warehouseInstance}" field="location"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="assignment-label" class="control-label"><g:message
                                                    code="warehouse.assignment.label"
                                                    default="Assignment"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="assignment-label">
                                                    <g:if test="${warehouseInstance?.assignment}">
                                                        <g:link controller="assignment" action="show" id="${warehouseInstance?.assignment?.id}">${warehouseInstance?.assignment?.encodeAsHTML()}</g:link></span>
                                                    </g:if>
                                                    <g:else>
                                                        None
                                                    </g:else>
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                    </fieldset>
                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${warehouseInstance?.id}"/>

                            <g:actionSubmit class="btn btn-primary" action="edit"
                                            value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                            <g:actionSubmit class="delete btn-danger" action="delete"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                        </g:form>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
