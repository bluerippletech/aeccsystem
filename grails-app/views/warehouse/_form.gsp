<%@ page import="com.ripple.master.Warehouse" %>



<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="warehouse.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${warehouseInstance.constraints.isDeleted.inList}" value="${warehouseInstance?.isDeleted}" valueMessagePrefix="warehouse.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="warehouse.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${warehouseInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="warehouse.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${warehouseInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="warehouse.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${warehouseInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="warehouse.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${warehouseInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="warehouse.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${warehouseInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'location', 'error')} required">
	<label for="location">
		<g:message code="warehouse.location.label" default="Location" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="location" required="" value="${warehouseInstance?.location}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="warehouse.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="status" from="${warehouseInstance.constraints.status.inList}" required="" value="${warehouseInstance?.status}" valueMessagePrefix="warehouse.status"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warehouseInstance, field: 'assignment', 'error')} required">
	<label for="assignment">
		<g:message code="warehouse.assignment.label" default="Assignment" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="assignment" name="assignment.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" required="" value="${warehouseInstance?.assignment?.id}" class="many-to-one"/>
</div>

