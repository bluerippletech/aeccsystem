<div class="row-fluid">

    <div class="span12" id="create-employee" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="employee.${params.action}.label" default="Edit Employee" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i
                        class="icon-cog"></i></a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">

                    <fieldset>
                        <legend>Employee Information</legend>

                        <div class="row-fluid">
                            <f:field bean="employeeInstance" property="employeeNumber"/>
                            <f:field bean="employeeInstance" property="lastName"/>
                            <f:field bean="employeeInstance" property="firstName"/>
                            <f:field bean="employeeInstance" property="middleInitials"/>
                            <f:field bean="employeeInstance" property="sex"/>
                            <f:field bean="employeeInstance" property="birthDate"/>
                            <f:field bean="employeeInstance" property="employmentDate"/>
                            <f:field bean="employeeInstance" property="taxStatus"/>
                            <f:field bean="employeeInstance" property="currentPosition"/>
                            <f:field bean="employeeInstance" property="grpNumber"/>
                            <f:field bean="employeeInstance" property="isActive"/>
                            <f:field bean="employeeInstance" property="retired"/>
                            <f:field bean="employeeInstance" property="emergencyLaborer"/>
                            <f:field bean="employeeInstance" property="isSeparated"/>
                            <f:field bean="employeeInstance" property="bank"/>
                            <f:field bean="employeeInstance" property="atmAccountNumber"/>
                            <f:field bean="employeeInstance" property="picture"/>

                        </div>

                    </fieldset>
                    <fieldset>
                        <legend>Government Issued Details</legend>

                        <div class="row-fluid">
                            <f:field bean="employeeInstance" property="sssIdNumber"/>
                            <f:field bean="employeeInstance" property="philHealthIDNumber"/>
                            <f:field bean="employeeInstance" property="hdmfIdNumber"/>
                            <f:field bean="employeeInstance" property="taxIdentificationNumber"/>
                        </div>
                    </fieldset>

                </div>
            </div>
        </div><!--/span-->

    </div>
</div>

<div class="row-fluid">

    <div class="span12" id="create-employee" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-1">
            <h4 class="box-header round-top">Personal Details
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i
                        class="icon-cog"></i></a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">

                    <fieldset>
                        <legend>Other Personal Details</legend>

                        <div class="row-fluid">
                            <f:field bean="employeeInstance" property="address"/>
                            <f:field bean="employeeInstance" property="provincialAddress"/>
                            <f:field bean="employeeInstance" property="mobileNumber"/>
                            <f:field bean="employeeInstance" property="telephoneNumber"/>
                            <f:field bean="employeeInstance" property="citizenship"/>
                            <f:field bean="employeeInstance" property="religion"/>
                            <f:field bean="employeeInstance" property="height"/>
                            <f:field bean="employeeInstance" property="weight"/>
                            <f:field bean="employeeInstance" property="fathersName"/>
                            <f:field bean="employeeInstance" property="mothersName"/>
                            <f:field bean="employeeInstance" property="spouseName"/>
                            <f:field bean="employeeInstance" property="remarks"/>
                        </div>

                    </fieldset>
                    <fieldset>
                        <legend>Emergency Details</legend>

                        <div class="row-fluid">
                            <f:field bean="employeeInstance" property="emergencyContactPerson"/>
                            <f:field bean="employeeInstance" property="emergencyContactAddress"/>
                            <f:field bean="employeeInstance" property="emergencyContactNumber"/>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Residence Certificate Details</legend>

                        <div class="row-fluid">
                            <f:field bean="employeeInstance" property="resCertNo"/>
                            <f:field bean="employeeInstance" property="resCertIssueDate"/>
                            <f:field bean="employeeInstance" property="resCertIssuePlace"/>
                        </div>
                    </fieldset>

                </div>
            </div>
        </div><!--/span-->

    </div>
</div>

<div class="row-fluid">

    <div class="span12" id="create-employee" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-2">
            <h4 class="box-header round-top">Voluntary Contributions
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i
                        class="icon-cog"></i></a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <f:field bean="employeeInstance" property="sssContribution"/>
                        <f:field bean="employeeInstance" property="philHealthContribution"/>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>

<g:if test="${action?.equals("edit")}">
    <div class="row-fluid">

        <div class="span12" id="create-employee" role="main">
            <!-- Portlet: Form Control States -->
            <div class="box" id="box-3">
                <h4 class="box-header round-top">Rates and Assignments
                    <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                    <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                    <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i
                            class="icon-cog"></i></a>
                </h4>

                <div class="box-container-toggle">
                    <div class="box-content">

                        <fieldset>
                            <legend>Assignments</legend>

                            <div class="row-fluid">
                                <f:field bean="employeeInstance" property="currentAssignment"/>
                                <div class="span6" style="margin-left:0px;">
                                    <a href="javascript:" onclick="renderModal('', 'assignmentHistory')"
                                       class="btn btn-info" rel="popover"
                                       data-content="Clicking will take you to another page to add another project assignment."
                                       data-original-title="Assign New Project">New Assignment</a>
                                </div>

                                <div class="span12" style="margin-left:0px;">
                                    <div class="control-group">
                                        <label class="control-label" for="assignmentHistory">Assignment History</label>

                                        <div class="controls">
                                            <ul id="assignmentHistoryListing" class="listing">
                                                <g:each var="a" in="${employeeInstance?.assignmentHistories ?}">
                                                    <li><a href="javascript:" onclick="renderModal('${a.id}', 'assignmentHistory')">${a?.encodeAsHTML()}</a></li>
                                                </g:each>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset>
                            <legend>Rates</legend>

                            <div class="row-fluid">
                                <div class="span6" style="margin-left:0px;">
                                    <div class="control-group">
                                        <label class="control-label" for="currentRate">Current Rate</label>

                                        <div class="controls">
                                            ${employeeInstance?.getRate()}
                                        </div>
                                    </div>
                                </div>

                                <div class="span6" style="margin-left:0px;">
                                    <a
                                       class="btn btn-info" rel="popover"
                                       data-content="Clicking will take you to another page to add new rate."
                                       data-original-title="Assign New Rate"  onclick="renderModal('', 'rate')" href="javascript:">New Rate</a>
                                    %{--<a class="btn" data-toggle="modal" href="#myModal" >Launch Modal</a>--}%
                                </div>

                                <div class="span12" style="margin-left:0px;">
                                    <div class="control-group">
                                        <label class="control-label" for="rates">Salary History</label>

                                        <div class="controls">
                                            <ul id="rateListing" class="listing">
                                                <g:set var="rates" value="${employeeInstance.rates as List}"/>
                                                <g:each var="r" in="${rates.reverse()}">
                                                    <li><a href="javascript:" onclick="renderModal('${r.id}', 'rate')">${r?.encodeAsHTML()} - ${r?.effectivityDate.toString("MMMMM dd, yyyy")} - ${r?.description}</a></li>
                                                </g:each>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div><!--/span-->

        </div>
    </div>

</g:if>