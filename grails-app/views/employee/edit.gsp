<%@ page import="com.ripple.master.Employee" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'employee.label', default: 'Employee')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <ripple:selectSubCat itemId="project" subItemId="workLocation"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>

<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<g:hasErrors bean="${employeeInstance}">
    <div class="alert alert-error">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        Validation has failed. Please correct your inputs.
        <ul>
            <g:eachError bean="${employeeInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </div>
</g:hasErrors>
<g:form class="form-horizontal" action="save" enctype="multipart/form-data">
    <g:hiddenField name="id" value="${employeeInstance?.id}"/>
    <g:hiddenField name="version" value="${employeeInstance?.version}"/>


    <g:render template="form" model="${['employeeInstance':employeeInstance,action:'edit', 'rateInstance':rateInstance]}"/>




    <div class="form-actions">
        <g:actionSubmit class="btn btn-primary" action="update"
                        value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        %{--<g:actionSubmit class="btn" action="delete"--}%
                        %{--value="${message(code: 'default.button.delete.label', default: 'Delete')}"--}%
                        %{--formnovalidate=""--}%
                        %{--onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>--}%
    </div>
</g:form>
<div id="modalContainer">
    <ripple:modal className="AssignmentHistory" entity="Assignment History" elementId="assignmentHistory" parentId="${employeeInstance.id}" parentClass="Employee" parentProperty="employee"/>
    <ripple:modal className="Rate" entity="Rate" elementId="rate" parentId="${employeeInstance.id}" parentClass="Employee" parentProperty="employee"/>
    <ripple:modalScript parentProperty="employee"  parentId="${employeeInstance.id}"/>
</div>
</body>
</html>
