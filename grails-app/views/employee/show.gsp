<%@ page import="com.ripple.master.Employee" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'employee.label', default: 'Employee')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="aecc"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
<div class="span12" id="edit-employee" role="main">
<!-- Portlet: Form Control States -->
<div class="box" id="box-0">
<h4 class="box-header round-top"><g:message code="employee.details.label" args="[entityName]"
                                            default="Employee Details"/>
    <a class="box-btn" title="close"><i class="icon-remove"></i></a>
    <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
    <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
    </a>
</h4>

<div class="box-container-toggle">
<div class="box-content">
<fieldset>
<legend><g:message code="employee.personalInformation.label" args="[entityName]"
                   default="Personal Information"/></legend>

<div class="row-fluid">
<div class="span6 column">
<div class="form-horizontal">
    <div class="control-group">
        <h5 id="employeeNumber-label" class="control-label"><g:message
                code="employee.employeeNumber.label"
                default="Employee Number"/></h5>

        <div class="controls" style="margin-top:4px">

            <span class="property-value" aria-labelledby="employeeNumber-label"><g:fieldValue
                    bean="${employeeInstance}" field="employeeNumber"/></span>

        </div>
    </div>
</div>

<div class="form-horizontal">
    <div class="control-group">
        <h5 id="lastName-label" class="control-label"><g:message
                code="employee.lastName.label"
                default="Last Name"/></h5>

        <div class="controls" style="margin-top:4px">

            <span class="property-value" aria-labelledby="lastName-label"><g:fieldValue
                    bean="${employeeInstance}" field="lastName"/></span>

        </div>
    </div>
</div>

<div class="form-horizontal">
    <div class="control-group">
        <h5 id="firstName-label" class="control-label"><g:message
                code="employee.firstName.label"
                default="First Name"/></h5>

        <div class="controls" style="margin-top:4px">

            <span class="property-value" aria-labelledby="firstName-label"><g:fieldValue
                    bean="${employeeInstance}" field="firstName"/></span>

        </div>
    </div>
</div>

<div class="form-horizontal">
    <div class="control-group">
        <h5 id="middleInitials-label" class="control-label"><g:message
                code="employee.middleInitials.label"
                default="Middle Initials"/></h5>

        <div class="controls" style="margin-top:4px">

            <span class="property-value" aria-labelledby="middleInitials-label"><g:fieldValue
                    bean="${employeeInstance}" field="middleInitials"/></span>

        </div>
    </div>
</div>

    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="bank-label" class="control-label"><g:message
                    code="employee.bank.label"
                    default="Bank"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="bank-label"><g:fieldValue
                        bean="${employeeInstance}" field="bank"/></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="atmAccountNumber-label" class="control-label"><g:message
                    code="employee.atmAccountNumber.label"
                    default="Atm Account Number"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="atmAccountNumber-label"><g:fieldValue
                        bean="${employeeInstance}" field="atmAccountNumber"/></span>

            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="emergencyLaborer-label" class="control-label"><g:message
                    code="employee.emergencyLaborer.label"
                    default="Emergency Laborer"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="emergencyLaborer-label"><g:formatBoolean
                        boolean="${employeeInstance?.emergencyLaborer}" true="Yes" false="No" /></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="isActive-label" class="control-label"><g:message
                    code="employee.isActive.label"
                    default="Is Active"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="isActive-label"><g:formatBoolean
                        boolean="${employeeInstance?.isActive}" true="Yes" false="No" /></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="isSeparated-label" class="control-label"><g:message
                    code="employee.isSeparated.label"
                    default="Is Separated"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="isSeparated-label"><g:formatBoolean
                        boolean="${employeeInstance?.isSeparated}" true="Yes" false="No" /></span>

            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="retired-label" class="control-label"><g:message
                    code="employee.retired.label"
                    default="Retired"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="retired-label"><g:formatBoolean
                        boolean="${employeeInstance?.retired}" true="Yes" false="No"/></span>

            </div>
        </div>
    </div>


</div>

<div class="span6 column">
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="picture-label" class="control-label"><g:message
                    code="employee.picture.label"
                    default="Picture"/></h5>

            <div class="controls" style="margin-top:4px">
                <g:if test="${employeeInstance.picture}">
                    <img src="${createLink(controller: 'image', action: 'show', id:employeeInstance?.id, params: [classname: 'com.ripple.master.Employee', fieldName: 'picture'])}" style="max-width: 200px;"/>
                </g:if>
                <g:else>
                    <img src="${resource(dir: "images", file: "member_ph.png")}"/>
                </g:else>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="birthDate-label" class="control-label"><g:message
                    code="employee.birthDate.label"
                    default="Birth Date"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="birthDate-label"><g:fieldValue
                        bean="${employeeInstance}" field="birthDate"/></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="sex-label" class="control-label"><g:message
                    code="employee.sex.label"
                    default="Sex"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="sex-label"><g:fieldValue
                        bean="${employeeInstance}" field="sex"/></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="employmentDate-label" class="control-label"><g:message
                    code="employee.employmentDate.label"
                    default="Employment Date"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="employmentDate-label"><g:fieldValue
                        bean="${employeeInstance}" field="employmentDate"/></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="currentPosition-label" class="control-label"><g:message
                    code="employee.currentPosition.label"
                    default="Current Position"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="currentPosition-label"><g:fieldValue
                        bean="${employeeInstance}" field="currentPosition"/></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="currentAssignment-label" class="control-label"><g:message
                    code="employee.currentAssignment.label"
                    default="Current Assignment"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="currentAssignment-label"><g:link
                        controller="assignmentHistory" action="show"
                        id="${employeeInstance?.currentAssignment?.id}">${employeeInstance?.currentAssignment?.encodeAsHTML()}</g:link></span>

            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="grpNumber-label" class="control-label"><g:message
                    code="employee.grpNumber.label"
                    default="Grp Number"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="grpNumber-label"><g:fieldValue
                        bean="${employeeInstance}" field="grpNumber"/></span>

            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="taxStatus-label" class="control-label"><g:message
                    code="employee.taxStatus.label"
                    default="Tax Status"/></h5>

            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="taxStatus-label"><g:fieldValue
                        bean="${employeeInstance}" field="taxStatus"/></span>

            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="assignmentHistories-label" class="control-label"><g:message
                    code="employee.assignmentHistories.label"
                    default="Assignment Histories"/></h5>

            <div class="controls" style="margin-top:4px">
                <ul>
                <g:each in="${employeeInstance.assignmentHistories}" var="a">
                    <li><span class="property-value" aria-labelledby="assignmentHistories-label"><g:link
                            controller="assignmentHistory" action="show"
                            id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
                    </li>
                </g:each>
                </ul>
            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="rates-label" class="control-label"><g:message
                    code="employee.rates.label"
                    default="Rates"/></h5>

            <div class="controls" style="margin-top:4px">
                <ul>
                <g:set var="rates" value="${employeeInstance.rates as List}"/>
                <g:each in="${rates?.reverse()}" var="r">
                    <li><span class="property-value" aria-labelledby="rates-label"><g:link
                            controller="rate" action="show"
                            id="${r.id}">${r?.encodeAsHTML()+' - '+r?.effectivityDate}</g:link></span>
                    </li>
                </g:each>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
</fieldset>
<fieldset>
    <legend><g:message code="employee.otherDetails.label" args="[entityName]"
                       default="Additional Details"/></legend>
    <div class="row-fluid">
        <div class="span6 column">

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="address-label" class="control-label"><g:message
                            code="employee.address.label"
                            default="Address"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="address-label"><g:fieldValue
                                bean="${employeeInstance}" field="address"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="mobileNumber-label" class="control-label"><g:message
                            code="employee.mobileNumber.label"
                            default="Mobile Number"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="mobileNumber-label"><g:fieldValue
                                bean="${employeeInstance}" field="mobileNumber"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="citizenship-label" class="control-label"><g:message
                            code="employee.citizenship.label"
                            default="Citizenship"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="citizenship-label"><g:fieldValue
                                bean="${employeeInstance}" field="citizenship"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="height-label" class="control-label"><g:message
                            code="employee.height.label"
                            default="Height"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="height-label"><g:fieldValue
                                bean="${employeeInstance}" field="height"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="fathersName-label" class="control-label"><g:message
                            code="employee.fathersName.label"
                            default="Fathers Name"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="fathersName-label"><g:fieldValue
                                bean="${employeeInstance}" field="fathersName"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="spouseName-label" class="control-label"><g:message
                            code="employee.spouseName.label"
                            default="Spouse Name"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="spouseName-label"><g:fieldValue
                                bean="${employeeInstance}" field="spouseName"/></span>

                    </div>
                </div>
            </div>

        </div>
        <div class="span6 column">

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="provincialAddress-label" class="control-label"><g:message
                            code="employee.provincialAddress.label"
                            default="Provincial Address"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="provincialAddress-label"><g:fieldValue
                                bean="${employeeInstance}" field="provincialAddress"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="telephoneNumber-label" class="control-label"><g:message
                            code="employee.telephoneNumber.label"
                            default="Telephone Number"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="telephoneNumber-label"><g:fieldValue
                                bean="${employeeInstance}" field="telephoneNumber"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="religion-label" class="control-label"><g:message
                            code="employee.religion.label"
                            default="Religion"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="religion-label"><g:fieldValue
                                bean="${employeeInstance}" field="religion"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="weight-label" class="control-label"><g:message
                            code="employee.weight.label"
                            default="Weight"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="weight-label"><g:fieldValue
                                bean="${employeeInstance}" field="weight"/></span>

                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="control-group">
                    <h5 id="mothersName-label" class="control-label"><g:message
                            code="employee.mothersName.label"
                            default="Mothers Name"/></h5>

                    <div class="controls" style="margin-top:4px">

                        <span class="property-value" aria-labelledby="mothersName-label"><g:fieldValue
                                bean="${employeeInstance}" field="mothersName"/></span>

                    </div>
                </div>
            </div>

        </div>

    </div>

</fieldset>

<fieldset>
    <legend>Emergency Contact Details</legend>
    <div class="row-fluid">
    <div class="span6 column">
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="emergencyContactPerson-label" class="control-label"><g:message
                        code="employee.emergencyContactPerson.label"
                        default="Emergency Contact Person"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="emergencyContactPerson-label"><g:fieldValue
                            bean="${employeeInstance}" field="emergencyContactPerson"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="emergencyContactNumber-label" class="control-label"><g:message
                        code="employee.emergencyContactNumber.label"
                        default="Emergency Contact Number"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="emergencyContactNumber-label"><g:fieldValue
                            bean="${employeeInstance}" field="emergencyContactNumber"/></span>

                </div>
            </div>
        </div>

    </div>
    <div class="span6 column">

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="emergencyContactAddress-label" class="control-label"><g:message
                        code="employee.emergencyContactAddress.label"
                        default="Emergency Contact Address"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="emergencyContactAddress-label"><g:fieldValue
                            bean="${employeeInstance}" field="emergencyContactAddress"/></span>

                </div>
            </div>
        </div>


    </div>
    </div>
</fieldset>
<fieldset>
    <legend>Resident Certificate Details</legend>
    <div class="row-fluid">
    <div class="span6 column">
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="resCertNo-label" class="control-label"><g:message
                        code="employee.resCertNo.label"
                        default="Res Cert No"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="resCertNo-label"><g:fieldValue
                            bean="${employeeInstance}" field="resCertNo"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="resCertIssuePlace-label" class="control-label"><g:message
                        code="employee.resCertIssuePlace.label"
                        default="Res Cert Issue Place"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="resCertIssuePlace-label"><g:fieldValue
                            bean="${employeeInstance}" field="resCertIssuePlace"/></span>

                </div>
            </div>
        </div>

    </div>
    <div class="span6 column">
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="resCertIssueDate-label" class="control-label"><g:message
                        code="employee.resCertIssueDate.label"
                        default="Res Cert Issue Date"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="resCertIssueDate-label"><g:fieldValue
                            bean="${employeeInstance}" field="resCertIssueDate"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="remarks-label" class="control-label"><g:message
                        code="employee.remarks.label"
                        default="Remarks"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="remarks-label"><g:fieldValue
                            bean="${employeeInstance}" field="remarks"/></span>

                </div>
            </div>
        </div>

    </div>
    </div>
</fieldset>
<fieldset>
    <legend>Voluntary Contributions</legend>
    <div class="row-fluid">
    <div class="span6 column">

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="sssContribution-label" class="control-label"><g:message
                        code="employee.sssContribution.label"
                        default="Sss Contribution"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="sssContribution-label"><g:fieldValue
                            bean="${employeeInstance}" field="sssContribution"/></span>

                </div>
            </div>
        </div>

    </div>
    <div class="span6 column">

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="philHealthContribution-label" class="control-label"><g:message
                        code="employee.philHealthContribution.label"
                        default="Phil Health Contribution"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="philHealthContribution-label"><g:fieldValue
                            bean="${employeeInstance}" field="philHealthContribution"/></span>

                </div>
            </div>
        </div>

    </div>
    </div>
</fieldset>
<fieldset>
    <legend>Government Issued Details</legend>
    <div class="row-fluid">
        <div class="span6 column">
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="sssIdNumber-label" class="control-label"><g:message
                        code="employee.sssIdNumber.label"
                        default="Sss Id Number"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="sssIdNumber-label"><g:fieldValue
                            bean="${employeeInstance}" field="sssIdNumber"/></span>

                </div>
            </div>
        </div>
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="hdmfIdNumber-label" class="control-label"><g:message
                        code="employee.hdmfIdNumber.label"
                        default="Hdmf Id Number"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="hdmfIdNumber-label"><g:fieldValue
                            bean="${employeeInstance}" field="hdmfIdNumber"/></span>

                </div>
            </div>
        </div>
        </div>
        <div class="span6 column">
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="philHealthIDNumber-label" class="control-label"><g:message
                        code="employee.philHealthIDNumber.label"
                        default="Phil Health IDN umber"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="philHealthIDNumber-label"><g:fieldValue
                            bean="${employeeInstance}" field="philHealthIDNumber"/></span>

                </div>
            </div>
        </div>
        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="taxIdentificationNumber-label" class="control-label"><g:message
                        code="employee.taxIdentificationNumber.label"
                        default="Tax Identification Number"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="taxIdentificationNumber-label"><g:fieldValue
                            bean="${employeeInstance}" field="taxIdentificationNumber"/></span>

                </div>
            </div>
        </div>
        </div>
    </div>
</fieldset>

<div class="form-actions form-horizontal">
    <g:form>
        <g:hiddenField name="id" value="${employeeInstance?.id}"/>
        <g:actionSubmit class="btn btn-primary" action="edit"
                        value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

        %{--<g:actionSubmit class="delete btn-danger" action="delete"--}%
                        %{--value="${message(code: 'default.button.delete.label', default: 'Delete')}"--}%
                        %{--onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>--}%
    </g:form>
</div>

</div>
</div>
</div>

</div>
</div>
</body>
</html>
