<%@ page import="com.ripple.master.Employee" %>
<!doctype html>
<html>
	<head>
        <meta charset="utf-8" name="layout" content="main"/>
		<g:set var="entityName" value="${message(code: 'employee.label', default: 'Employee')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <r:require modules="aecc"/>
	</head>
	<body>
    <div class="row-fluid">
        <div class="span1 action-btn round-all">
            <a href="${createLink(uri: '/')}">
                <div><i class="icon-home"></i></div>
                <div><strong><g:message code="default.home.label"/></strong></div>
            </a>
        </div>
        <div class="span1 action-btn round-all">
            <a href="${createLink(action: 'list')}">
                <div><i class="icon-align-justify"></i></div>
                <div><strong>List</strong></div>
            </a>
        </div>
    </div>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            ${flash.message}
        </div>
    </g:if>
    <g:hasErrors bean="${employeeInstance}">
        <div class="alert alert-error">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            Validation has failed. Please correct your inputs.
            <ul>
                <g:eachError bean="${employeeInstance}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </div>
    </g:hasErrors>
    <g:form class="form-horizontal" action="save"  enctype="multipart/form-data">
    <g:render template="form" model="${['employeeInstance':employeeInstance]}"/>
        <div class="form-actions">
            <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
            <g:submitButton name="cancel" class="btn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />
        </div>
    </g:form>
	</body>
</html>
