<%@ page import="com.ripple.master.geography.Province" %>



<div class="fieldcontain ${hasErrors(bean: provinceInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="province.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${provinceInstance.constraints.isDeleted.inList}" value="${provinceInstance?.isDeleted}" valueMessagePrefix="province.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: provinceInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="province.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${provinceInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: provinceInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="province.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${provinceInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: provinceInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="province.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${provinceInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: provinceInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="province.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${provinceInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: provinceInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="province.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${provinceInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: provinceInstance, field: 'cities', 'error')} ">
	<label for="cities">
		<g:message code="province.cities.label" default="Cities" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${provinceInstance?.cities?}" var="c">
    <li><g:link controller="city" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="city" action="create" params="['province.id': provinceInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'city.label', default: 'City')])}</g:link>
</li>
</ul>

</div>

