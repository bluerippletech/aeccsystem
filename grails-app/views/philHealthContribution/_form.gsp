<%@ page import="com.ripple.deductions.PhilHealthContribution" %>



<div class="fieldcontain ${hasErrors(bean: philHealthContributionInstance, field: 'lowRange', 'error')} required">
	<label for="lowRange">
		<g:message code="philHealthContribution.lowRange.label" default="Low Range" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="lowRange" required="" value="${fieldValue(bean: philHealthContributionInstance, field: 'lowRange')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: philHealthContributionInstance, field: 'highRange', 'error')} required">
	<label for="highRange">
		<g:message code="philHealthContribution.highRange.label" default="High Range" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="highRange" required="" value="${fieldValue(bean: philHealthContributionInstance, field: 'highRange')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: philHealthContributionInstance, field: 'salaryBase', 'error')} required">
	<label for="salaryBase">
		<g:message code="philHealthContribution.salaryBase.label" default="Salary Base" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="salaryBase" required="" value="${fieldValue(bean: philHealthContributionInstance, field: 'salaryBase')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: philHealthContributionInstance, field: 'employeeShare', 'error')} required">
	<label for="employeeShare">
		<g:message code="philHealthContribution.employeeShare.label" default="Employee Share" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="employeeShare" required="" value="${fieldValue(bean: philHealthContributionInstance, field: 'employeeShare')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: philHealthContributionInstance, field: 'employerShare', 'error')} required">
	<label for="employerShare">
		<g:message code="philHealthContribution.employerShare.label" default="Employer Share" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="employerShare" required="" value="${fieldValue(bean: philHealthContributionInstance, field: 'employerShare')}"/>
</div>

