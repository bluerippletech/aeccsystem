<%@ page import="com.ripple.receiving.ReceivingReportLineItem" %>



<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="receivingReportLineItem.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${receivingReportLineItemInstance.constraints.isDeleted.inList}" value="${receivingReportLineItemInstance?.isDeleted}" valueMessagePrefix="receivingReportLineItem.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="receivingReportLineItem.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${receivingReportLineItemInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="receivingReportLineItem.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${receivingReportLineItemInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="receivingReportLineItem.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${receivingReportLineItemInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="receivingReportLineItem.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${receivingReportLineItemInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'item', 'error')} required">
	<label for="item">
		<g:message code="receivingReportLineItem.item.label" default="Item" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="item" name="item.id" from="${com.ripple.master.item.Item.list()}" optionKey="id" required="" value="${receivingReportLineItemInstance?.item?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'unitOfMeasure', 'error')} required">
	<label for="unitOfMeasure">
		<g:message code="receivingReportLineItem.unitOfMeasure.label" default="Unit Of Measure" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="unitOfMeasure" name="unitOfMeasure.id" from="${com.ripple.master.item.LevelUnit.list()}" optionKey="id" required="" value="${receivingReportLineItemInstance?.unitOfMeasure?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'quantity', 'error')} required">
	<label for="quantity">
		<g:message code="receivingReportLineItem.quantity.label" default="Quantity" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="quantity" required="" value="${fieldValue(bean: receivingReportLineItemInstance, field: 'quantity')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'quantityRemaining', 'error')} required">
	<label for="quantityRemaining">
		<g:message code="receivingReportLineItem.quantityRemaining.label" default="Quantity Remaining" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="quantityRemaining" required="" value="${fieldValue(bean: receivingReportLineItemInstance, field: 'quantityRemaining')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'unitPrice', 'error')} required">
	<label for="unitPrice">
		<g:message code="receivingReportLineItem.unitPrice.label" default="Unit Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="unitPrice" required="" value="${fieldValue(bean: receivingReportLineItemInstance, field: 'unitPrice')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: receivingReportLineItemInstance, field: 'receivingReport', 'error')} required">
	<label for="receivingReport">
		<g:message code="receivingReportLineItem.receivingReport.label" default="Receiving Report" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="receivingReport" name="receivingReport.id" from="${com.ripple.receiving.ReceivingReport.list()}" optionKey="id" required="" value="${receivingReportLineItemInstance?.receivingReport?.id}" class="many-to-one"/>
</div>

