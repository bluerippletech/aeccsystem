<%@ page import="grails.util.GrailsUtil" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <r:require modules="all"/>
</head>
<body>
<!-- main content area -->
<div class="row-fluid">
    <!-- Portlet Set 1 -->
    <div class="span12" id="col1"> <!-- class column makes this a sortable column, id=col1 is for the remember position function -->
    <!-- Portlet: Box 1 -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top">Error
                <a class="box-btn" title="close"><i class="icon-remove"></i></a><!-- Can be removed if not wanted -->
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a><!-- Can be removed if not wanted -->
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a><!-- Can be removed if not wanted -->
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <h2 class="heading color-black">oops! our server seems to have the hiccups. we’ve already reported this error.</h2>
                    <g:if test="${('development'.equals(GrailsUtil.getEnvironment())||'test'.equals(GrailsUtil.getEnvironment()))}">
                    <%--<p><g:renderException exception="${exception}" /></p> --%>
                        <h1>Grails Runtime Exception</h1>

                        <h2>Error Details</h2>

                        <div class="message">
                            <strong>Error ${request.'javax.servlet.error.status_code'}:</strong>
                            ${request.'javax.servlet.error.message'.encodeAsHTML()}<br/>
                            <strong>Servlet:</strong> ${request.'javax.servlet.error.servlet_name'}<br/>
                            <strong>URI:</strong> ${request.'javax.servlet.error.request_uri'}<br/>
                            <g:if test="${exception}">
                                <strong>Exception Message:</strong> ${exception.message?.encodeAsHTML()} <br/>
                                <strong>Caused by:</strong> ${exception.cause?.message?.encodeAsHTML()} <br/>
                                <strong>Class:</strong> ${exception.className} <br/>
                                <strong>At Line:</strong> [${exception.lineNumber}] <br/>
                                <strong>Code Snippet:</strong><br/>

                                <div class="snippet">
                                    <g:each var="cs" in="${exception.codeSnippet}">
                                        ${cs?.encodeAsHTML()}<br/>
                                    </g:each>
                                </div>
                            </g:if>
                        </div>

                        <g:if test="${exception}">
                            <h2>Stack Trace</h2>

                            <div class="stack">
                                <pre><g:each in="${exception.stackTraceLines}">${it.encodeAsHTML()}<br/></g:each></pre>
                            </div>
                        </g:if>

                    </g:if>
                    <div class="error_404_box error_box">
                        500
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content area -->
</body>
</html>