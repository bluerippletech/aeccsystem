<%@ page import="com.ripple.master.item.Item" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'item.label', default: 'Item')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <style type="text/css">
    .mini{
        padding: 3px !important;
    }
    </style>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="list-item" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>

                            <th><g:message code="item.itemCode.label" default="Item Code" /></th>

                            <th><g:message code="item.itemName.label" default="Item Name" /></th>

                            <th><g:message code="item.desciption.label" default="Description" /></th>

                            <th><g:message code="item.status.label" default="Status" /></th>

                            <th><g:message code="item.itemClass.label" default="Item Class" /></th>

                            <th><g:message code="item.itemSubclass.label" default="Item Subclass" /></th>

                            <th><g:message code="item.actions.label" default="Actions" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            $('#datatable').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'item', action:'listJSON')}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[
                                    
                                    { "sName": "itemCode", "aTargets": [ 0 ] },
                                    
                                    { "sName": "itemName", "aTargets": [ 1 ] },
                                    
                                    { "sName": "description", "aTargets": [ 2 ] },
                                    
                                    { "sName": "status", "aTargets": [ 3 ],
                                        "fnRender": function (o,val){
                                            if(val=='Active'){
                                                return '<span class="label label-success">Active</span>';
                                            }
                                            else{
                                                return '<span class="label label-important">Inactive</span>';
                                            }
                                        }
                                    },
                                    
                                    { "sName": "itemClass", "aTargets": [ 4 ] },
                                    
                                    { "sName": "itemSubclass", "aTargets": [ 5 ] },

                                    { "bSortable": false, "aTargets": [ 6 ] }


                                ],
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    $('td:eq(6)', nRow).html( '<a class="btn btn-success mini" href="${createLink(action:'show')}/'+nRow.id+'"><i class="icon-zoom-in icon-white"></i></a>&nbsp;' );
                                $('td:eq(6)', nRow).append('<a class="btn btn-info mini" href="${createLink(action:'edit')}/'+nRow.id+'"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                $('td:eq(6)', nRow).append('<form method="POST" action="delete" name="delete" style="display:inline"><input type="hidden" name="id" value="'+nRow.id+'"><a class="btn btn-danger mini" href="javascript:" onclick="submitForm(this)"><i class="icon-trash icon-white"></i></a></form>');
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                    $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
