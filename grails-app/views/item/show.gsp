<%@ page import="com.ripple.master.item.Item" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'item.label', default: 'Item')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-item" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="itemCode-label" class="control-label"><g:message
                                                    code="item.itemCode.label"
                                                    default="Item Code"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="itemCode-label"><g:fieldValue
                                                        bean="${itemInstance}" field="itemCode"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="description-label" class="control-label"><g:message
                                                    code="item.description.label"
                                                    default="Description"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                                                        bean="${itemInstance}" field="description"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="stockMovement-label" class="control-label"><g:message
                                                    code="item.stockMovement.label"
                                                    default="Stock Movement"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="stockMovement-label"><g:fieldValue
                                                        bean="${itemInstance}" field="stockMovement"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="algonPartNumber-label" class="control-label"><g:message
                                                    code="item.algonPartNumber.label"
                                                    default="Algon Part Number"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="algonPartNumber-label"><g:fieldValue
                                                        bean="${itemInstance}" field="algonPartNumber"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="oemPartNumber-label" class="control-label"><g:message
                                                    code="item.oemPartNumber.label"
                                                    default="Oem Part Number"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="oemPartNumber-label"><g:fieldValue
                                                        bean="${itemInstance}" field="oemPartNumber"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="itemSubclass-label" class="control-label"><g:message
                                                    code="item.itemSubclass.label"
                                                    default="Item Subclass"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="itemSubclass-label"><g:link
                                                        controller="itemSubclass" action="show"
                                                        id="${itemInstance?.itemSubclass?.id}">${itemInstance?.itemSubclass?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="itemName-label" class="control-label"><g:message
                                                    code="item.itemName.label"
                                                    default="Item Name"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="itemName-label"><g:fieldValue
                                                        bean="${itemInstance}" field="itemName"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="status-label" class="control-label"><g:message
                                                    code="item.status.label"
                                                    default="Status"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                        bean="${itemInstance}" field="status"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="unitOfMeasure-label" class="control-label"><g:message
                                                    code="item.unitOfMeasure.label"
                                                    default="Unit Of Measure"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="unitOfMeasure-label"><g:fieldValue
                                                        bean="${itemInstance}" field="unitOfMeasure"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="supplierPartNumber-label" class="control-label"><g:message
                                                    code="item.supplierPartNumber.label"
                                                    default="Supplier Part Number"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="supplierPartNumber-label"><g:fieldValue
                                                        bean="${itemInstance}" field="supplierPartNumber"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="itemClass-label" class="control-label"><g:message
                                                    code="item.itemClass.label"
                                                    default="Item Class"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="itemClass-label"><g:link
                                                        controller="itemClass" action="show"
                                                        id="${itemInstance?.itemClass?.id}">${itemInstance?.itemClass?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                    </fieldset>
                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${itemInstance?.id}"/>

                            <g:actionSubmit class="btn btn-primary" action="edit"
                                            value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                            <g:actionSubmit class="delete btn-danger" action="delete"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
