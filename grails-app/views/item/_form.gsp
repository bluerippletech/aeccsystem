<%@ page import="com.ripple.master.item.Item" %>

<f:display bean="itemInstance" property="itemNumber"/>
<f:field bean="itemInstance" property="itemCode"/>
<f:field bean="itemInstance" property="itemName"/>
<f:field bean="itemInstance" property="itemClass"/>
<f:field bean="itemInstance" property="description"/>
<f:field bean="itemInstance" property="itemSubclass"/>
<f:field bean="itemInstance" property="status"/>
<f:field bean="itemInstance" property="stockMovement"/>
<f:field bean="itemInstance" property="algonPartNumber"/>
<f:field bean="itemInstance" property="oemPartNumber"/>
<f:field bean="itemInstance" property="supplierPartNumber"/>
<div class="span12" style="margin-left:0px;">
    <table class="table table-bordered">
        <tr>
            <th>UoM</th>
            %{--<g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>--}%
            <th>Label for Unit Of Measurement</th>
            <th>Conversion</th>
        </tr>
        <tr><f:field bean="itemInstance" property="baseLevel"/></tr>
        <tr><f:field bean="itemInstance" property="secondLevel"/></tr>
        <tr><f:field bean="itemInstance" property="thirdLevel"/></tr>
    </table>
</div>



