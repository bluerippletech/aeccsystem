<%@ page import="com.ripple.master.item.ItemSubclass" %>



<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="itemSubclass.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${itemSubclassInstance.constraints.isDeleted.inList}" value="${itemSubclassInstance?.isDeleted}" valueMessagePrefix="itemSubclass.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="itemSubclass.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${itemSubclassInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="itemSubclass.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${itemSubclassInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="itemSubclass.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${itemSubclassInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="itemSubclass.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${itemSubclassInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="itemSubclass.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${itemSubclassInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="itemSubclass.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${itemSubclassInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: itemSubclassInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="itemSubclass.status.label" default="Status" />
		
	</label>
	<g:select name="status" from="${itemSubclassInstance.constraints.status.inList}" value="${itemSubclassInstance?.status}" valueMessagePrefix="itemSubclass.status" noSelection="['': '']"/>
</div>

