<%@ page import="com.ripple.master.geography.Address" %>



<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="address.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${addressInstance.constraints.isDeleted.inList}" value="${addressInstance?.isDeleted}" valueMessagePrefix="address.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="address.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${addressInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="address.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${addressInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="address.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${addressInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="address.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${addressInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'address1', 'error')} required">
	<label for="address1">
		<g:message code="address.address1.label" default="Address1" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="address1" required="" value="${addressInstance?.address1}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'address2', 'error')} ">
	<label for="address2">
		<g:message code="address.address2.label" default="Address2" />
		
	</label>
	<g:textField name="address2" value="${addressInstance?.address2}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="address.city.label" default="City" />
		
	</label>
	<g:select id="city" name="city.id" from="${com.ripple.master.geography.City.list()}" optionKey="id" value="${addressInstance?.city?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'province', 'error')} ">
	<label for="province">
		<g:message code="address.province.label" default="Province" />
		
	</label>
	<g:select id="province" name="province.id" from="${com.ripple.master.geography.Province.list()}" optionKey="id" value="${addressInstance?.province?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'zipCode', 'error')} ">
	<label for="zipCode">
		<g:message code="address.zipCode.label" default="Zip Code" />
		
	</label>
	<g:textField name="zipCode" value="${addressInstance?.zipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'telephoneNo', 'error')} ">
	<label for="telephoneNo">
		<g:message code="address.telephoneNo.label" default="Telephone No" />
		
	</label>
	<g:textField name="telephoneNo" value="${addressInstance?.telephoneNo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'faxNo', 'error')} ">
	<label for="faxNo">
		<g:message code="address.faxNo.label" default="Fax No" />
		
	</label>
	<g:textField name="faxNo" value="${addressInstance?.faxNo}"/>
</div>

