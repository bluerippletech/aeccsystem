<%@ page import="com.ripple.master.Assignment" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="print"/>
    <title>Project Operation Cost Summary</title>

</head>
<body>
%{--<div class="nav">--}%
    %{--<span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}">Home</a></span>--}%
    %{--<span class="menuButton"><g:link class="list" action="list">Project List</g:link></span>--}%
    %{--<span class="menuButton"><g:link class="create" action="create">New Project</g:link></span>--}%
%{--</div>--}%
<div class="body">
    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div id="message" class="message" style="display:none"></div>
    <div id="error" class="errorsbox" style="display:none"></div>
    <fieldset>
        <legend>Cost Summary Report</legend>
        <div width="100%" align="center">Algon Engineering Construction Corporation</div>
        <div width="100%" align="center">159 JP Cabaguio Avenue Davao City</div>
        <br>
        <div width="100%" align="center">${project?.projectName}</div>
        <div width="100%" align="center">${project?.projectAddress}</div>
        <br>
        <div width="100%" align="center">Project Operation Cost Summary</div>
        <div width="100%" align="center">For the Ending Period of ${period}</div>

        <div>
            <table width="100%">
                <tr>
                    <td width="300"><b>Collections</b></td><td class="centerbold">This Month</td>
                    <td class="centerbold">To Date</td><td class="centerbold">Balance</td><td class="centerbold">Estimate</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Collections</td>
                    <td style="text-align:right;"><g:formatNumber number="${thisMonthCollectionAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${monthToDateCollectionAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${balanceToDate}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${estimateTotalAmount}" format="###,##0.00"/></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Retention
                    </td>
                    <td style="text-align:right;"><g:formatNumber number="${thisMonthCollectionRetentionAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${monthToDateCollectionRetentionAmount}" format="###,##0.00"/></td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Withholding Tax
                    </td>
                    <td style="text-align:right;"><g:formatNumber number="${thisMonthCollectionWtAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${monthToDateCollectionWtAmount}" format="###,##0.00"/></td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Forward Withholding Tax
                    </td>
                    <td style="text-align:right;"><g:formatNumber number="${thisMonthCollectionFwtAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${monthToDateCollectionFwtAmount}" format="###,##0.00"/></td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Recoupment
                    </td>
                    <td style="text-align:right;"><g:formatNumber number="${thisMonthCollectionRecoupmentAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${monthToDateCollectionRecoupmentAmount}" format="###,##0.00"/></td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Net Collections
                    </td>
                    <td style="text-align:right;"><g:formatNumber number="${thisMonthCollectionNetAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${monthToDateCollectionNetAmount}" format="###,##0.00"/></td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                </tr>
                <g:each var="i" in="${finalList}" status="a">
                    <g:each var="y" in="${i}" status="c">
                        <g:if test="${c==0}">
                            <tr>
                                <td colspan="5"><b>${y}</b></td>
                            </tr>
                        </g:if>
                        <g:else>
                        <g:set var="subtotal1" value="0.00"/>
                                <g:set var="subtotal2" value="0.00"/>
                                <g:set var="subtotal3" value="0.00"/>
                                <g:set var="subtotal4" value="0.00"/>
                            <g:each var="z" in="${y}" status="d">
                                <tr>
                                
                                    <g:each var="expense" in="${z}" status="column">
                                        <g:if test="${column==0}">
                                        
                                            <td class="${a}${column}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${expense}</td>
                                        </g:if>
                                        <g:else>
                                        <g:if test="${column==1 }">
                                        <g:set var="subtotal${column}" value='${subtotal1.toBigDecimal()+expense.toBigDecimal() }'/>
                                        </g:if>
                                        <g:if test="${column==2 }">
                                        <g:set var="subtotal${column}" value='${subtotal2.toBigDecimal()+expense.toBigDecimal() }'/>
                                        </g:if>
                                        <g:if test="${column==3 }">
                                        <g:set var="subtotal${column}" value='${subtotal3.toBigDecimal()+expense.toBigDecimal() }'/>
                                        </g:if>
                                        <g:if test="${column==4 }">
                                        <g:set var="subtotal${column}" value='${subtotal4.toBigDecimal()+expense.toBigDecimal() }'/>
                                        </g:if>
                                        
                                            <td class="${a}${column}"
                                                <g:if test="${expense<0}">
                                                    style="text-align:right;color:red;"
                                                </g:if>
                                                <g:else>
                                                    style="text-align:right;"
                                                </g:else>

                                                    value="${expense}"><g:formatNumber number="${expense}" format="###,##0.00"/></td>
                                        </g:else>
                                    </g:each>
                                </tr>
                            </g:each>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total</td>
                                <td class="total${a}1" style="text-align:right;">
                                <g:formatNumber number="${subtotal1}" format="###,##0.00"/>
                                </td>
                                <td class="total${a}2" style="text-align:right;">
                                <g:formatNumber number="${subtotal2}" format="###,##0.00"/>
                                </td>
                                <td class="total${a}3" style="text-align:right;">
                                <g:formatNumber number="${subtotal3}" format="###,##0.00"/>
                                </td>
                                <td class="total${a}4" style="text-align:right;">
                                <g:formatNumber number="${subtotal4}" format="###,##0.00"/>
                                </td>
                            </tr>
                            <script><%--
                            $(document).ready(function(){
                                var sum1 = parseFloat(0.00);
                                var sum2 = parseFloat(0.00);
                                var sum3 = parseFloat(0.00);
                                var sum4 = parseFloat(0.00);
                                $(".${a}1").each(function(index) {
                                    sum1 += parseFloat($(this).attr("value"));
                                });
                                $(".${a}2").each(function(index) {
                                    sum2 += parseFloat($(this).attr("value"));
                                });
                                $(".${a}3").each(function(index) {
                                    sum3 += parseFloat($(this).attr("value"));
                                });
                                $(".${a}4").each(function(index) {
                                    sum4 += parseFloat($(this).attr("value"));
                                });
                                $(".total${a}1").text(sum1.toFixed(2)).formatCurrency({symbol:'P'});
                                if (sum1.toFixed(2) < 0)
                                    $(".total${a}1").css("color", "red");
                                $(".total${a}2").text(sum2.toFixed(2)).formatCurrency({symbol:'P'});
                                if (sum2.toFixed(2) < 0)
                                    $(".total${a}2").css("color", "red");

                                $(".total${a}3").text(sum3.toFixed(2)).formatCurrency({symbol:'P'});
                                if (sum3.toFixed(2) < 0)
                                    $(".total${a}3").css("color", "red");
                                $(".total${a}4").text(sum4.toFixed(2)).formatCurrency({symbol:'P'});
                                if (sum4.toFixed(2) < 0)
                                    $(".total${a}4").css("color", "red");
                            });
                            --%></script>
                        </g:else>
                    </g:each>
                </g:each>
                <tr>
                    <td>
                        <b>Total Direct & Indirect Cost</b>
                    </td>
                    <td id="id1">

                    </td>
                    <td id="id2">

                    </td>
                    <td id="id3">

                    </td>
                    <td id="id4">

                    </td>
                    <script type="text/javascript">
                    $(document).ready(function(){
                        var i = 1;
                        for (i = 1; i < 5; i++) {
                            $("#id" + i).text(parseFloat($(".total0" + i).asNumber()) +
                                    parseFloat($(".total1" + i).asNumber()) +
                                    parseFloat($(".total2" + i).asNumber()) +
                                    parseFloat($(".total3" + i).asNumber()) +
                                    parseFloat($(".total4" + i).asNumber())
                                    ).formatCurrency({symbol:'P'}).css('text-align', 'right');
                        }
                        ;
                    });
                    </script>
                </tr>
                <tr>
                    <td><b>Total Collections</b></td>
                    <td style="text-align:right;"><g:formatNumber number="${thisMonthCollectionAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${monthToDateCollectionAmount}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${balanceToDate}" format="###,##0.00"/></td>
                    <td style="text-align:right;"><g:formatNumber number="${estimateTotalAmount}" format="###,##0.00"/></td>
                </tr>
            </table>
        </div>
    </fieldset>
</div>
</body>
</html>
