<%@ page import="org.joda.time.LocalDate; com.ripple.master.Assignment" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'assignment.label', default: 'Assignment')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <r:script>
        var dataTableArray = {};
    </r:script>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
<div class="span12" id="edit-assignment" role="main">
<!-- Portlet: Form Control States -->
<div class="box" id="box-0">
    <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
        <a class="box-btn" title="close"><i class="icon-remove"></i></a>
        <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
        <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
        </a>
    </h4>

    <div class="box-container-toggle">
        <div class="box-content">
            <fieldset>
                <div class="row-fluid">
                    <div class="span6 column">

                        <div class="form-horizontal">
                            <div class="control-group">
                                <h5 id="projectName-label" class="control-label"><g:message
                                        code="assignment.projectName.label"
                                        default="Project Name"/></h5>

                                <div class="controls" style="margin-top:4px">

                                    <span class="property-value" aria-labelledby="projectName-label"><g:fieldValue
                                            bean="${assignmentInstance}" field="projectName"/></span>

                                </div>
                            </div>
                        </div>


                        %{--<div class="form-horizontal">--}%
                        %{--<div class="control-group">--}%
                        %{--<h5 id="collections-label" class="control-label"><g:message--}%
                        %{--code="assignment.collections.label"--}%
                        %{--default="Collections"/></h5>--}%
                        %{--<div class="controls" style="margin-top:4px">--}%

                        %{--<g:each in="${assignmentInstance.collections}" var="c">--}%
                        %{--<span class="property-value" aria-labelledby="collections-label"><g:link--}%
                        %{--controller="collection" action="show"--}%
                        %{--id="${c.id}">${c?.encodeAsHTML()}</g:link></span>--}%
                        %{--</g:each>--}%

                        %{--</div>--}%
                        %{--</div>--}%
                        %{--</div>--}%

                        %{--<div class="form-horizontal">--}%
                        %{--<div class="control-group">--}%
                        %{--<h5 id="expenses-label" class="control-label"><g:message--}%
                        %{--code="assignment.expenses.label"--}%
                        %{--default="Expenses"/></h5>--}%
                        %{--<div class="controls" style="margin-top:4px">--}%

                        %{--<g:each in="${assignmentInstance.expenses}" var="e">--}%
                        %{--<span class="property-value" aria-labelledby="expenses-label"><g:link--}%
                        %{--controller="expense" action="show"--}%
                        %{--id="${e.id}">${e?.encodeAsHTML()}</g:link></span>--}%
                        %{--</g:each>--}%

                        %{--</div>--}%
                        %{--</div>--}%
                        %{--</div>--}%

                        <div class="form-horizontal">
                            <div class="control-group">
                                <h5 id="projectAddress-label" class="control-label"><g:message
                                        code="assignment.projectAddress.label"
                                        default="Project Address"/></h5>

                                <div class="controls" style="margin-top:4px">

                                    <span class="property-value" aria-labelledby="projectAddress-label"><g:fieldValue
                                            bean="${assignmentInstance}" field="projectAddress"/></span>

                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="control-group">
                                <h5 id="locations-label" class="control-label"><g:message
                                        code="assignment.locations.label"
                                        default="Locations"/></h5>

                                <div class="controls" style="margin-top:4px">

                                    <ul id="assignmentLocationListing" class="listing">
                                        <g:each var="l" in="${assignmentInstance.locations}">
                                            <li><a href="javascript:"
                                                   onclick="renderModal('${l?.id}', 'assignmentLocation')">${l?.encodeAsHTML()}</a>
                                            </li>
                                        </g:each>
                                    </ul>

                                    <a id="showModal4" class="btn" href="javascript:"
                                       onclick="renderModal('', 'assignmentLocation')">Add Assignment Location</a><br/><br/>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="span6 column">
                        <div class="form-horizontal">
                            <div class="control-group">
                                <h5 id="description-label" class="control-label"><g:message
                                        code="assignment.description.label"
                                        default="Description"/></h5>

                                <div class="controls" style="margin-top:4px">

                                    <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                                            bean="${assignmentInstance}" field="description"/></span>

                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="control-group">
                                <h5 id="active-label" class="control-label"><g:message
                                        code="assignment.active.label"
                                        default="Active"/></h5>

                                <div class="controls" style="margin-top:4px">

                                    <span class="property-value" aria-labelledby="active-label"><g:formatBoolean
                                            boolean="${assignmentInstance?.active}" true="Yes" false="No"/></span>

                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div class="control-group">
                                <h5 id="warehouses-label" class="control-label"><g:message
                                        code="assignment.warehouses.label"
                                        default="Warehouses"/></h5>

                                <div class="controls" style="margin-top:4px">

                                    <ul id="warehouseListing" class="listing">
                                        <g:each var="l" in="${assignmentInstance.warehouses}">
                                            <li><a href="javascript:"
                                                   onclick="renderModal('${l?.id}', 'warehouse')">${l?.encodeAsHTML()}</a>
                                            </li>
                                        </g:each>
                                    </ul>

                                    <a id="showModal5" class="btn" href="javascript:"
                                       onclick="renderModal('', 'warehouse')">Add Warehouse</a><br/><br/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-actions form-horizontal">
                    <g:form controller="assignment" action="generateCostSummary" class="form-horizontal" method="post">
                        <div class="span6" style="margin-left:0px;">
                            <div class="control-group">
                                <label class="control-label" for="csReport">Cost Summary Report as of:</label>

                                <div class="controls">
                                    <input type="hidden" name="id" id="id-hidden" value="${assignmentInstance.id}"/>
                                    <input type="text" class="input-xlarge datepicker" style="width: 210px;" name="date"
                                           id="date" value="${new LocalDate().toString("M/d/yyyy")}"/>
                                    <g:actionSubmit class="btn btn-primary" action="generateCostSummary"
                                                    value="${message(code: 'default.button.view.label', default: 'View')}"/>
                                </div>
                            </div>
                        </div>
                    </g:form>
                </div>
            </fieldset>
        </div>
    </div>
</div><!--/span-->

<div class="box" id="box-1">
    <h4 class="box-header round-top"><g:message code="default.list.label" args="['Material Estimates']"/>
        <a class="box-btn" title="close"><i class="icon-remove"></i></a>
        <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
        <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
        </a>
    </h4>

    <div class="box-container-toggle">
        <div class="box-content">
            <a id="showModal1" class="btn" href="javascript:"
               onclick="renderModal('', 'estimateItem')">Add Estimate Item</a><br/><br/>
            <table cellpadding="0" cellspacing="0" border="0"
                   class="table table-striped table-bordered bootstrap-datatable" id="datatableEstimates">
                <thead>
                <tr>

                    <th><g:message code="estimateItem.expenseCategory.label" default="Expense Category"/></th>

                    <th><g:message code="estimateItem.expenseType.label" default="Expense Type"/></th>

                    <th><g:message code="estimateItem.quantity.label" default="Quantity"/></th>

                    <th><g:message code="estimateItem.unitPrice.label" default="Unit Price"/></th>

                    <th><g:message code="estimateItem.total.label" default="Total"/></th>

                    <th><g:message code="estimateItem.actions.label" default="Actions"/></th>

                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <script type="text/javascript">
                $(function () {
                    dataTableArray['estimateItem'] = $('#datatableEstimates').dataTable({
                        bProcessing:true,
                        bServerSide:true,
                        iDisplayLength:10,
                        sAjaxSource:'${createLink(controller:'estimateItem', action:'listJSON', id:assignmentInstance.id)}',
                        "sDom":"<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                        "sPaginationType":"bootstrap",
                        "oLanguage":{
                            "sLengthMenu":"_MENU_ records per page"
                        },
                        "aoColumnDefs":[

                            { "aTargets":[ 0 ] },

                            { "aTargets":[ 1 ] },

                            { "sName":"quantity", "aTargets":[ 2 ] },

                            { "sName":"unitPrice", "aTargets":[ 3 ] },

                            { "aTargets":[ 4 ] },

                            { "bSortable":false, "aTargets":[ 5 ] }


                        ],
                        "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                            $('td:eq(5)', nRow).html('<a class="btn btn-info mini" href="javascript:" onclick="renderModal(\'' + nRow.id + '\', \'estimateItem\')"><i class="icon-edit icon-white"></i></a>&nbsp;');
                        }
                    });

                })
                function submitForm(elem) {
                    if (confirm("Are you sure?"))
                        $(elem).closest("form").submit();
                }
            </script>
        </div>
    </div><!--/span-->

</div>

<div class="box" id="box-2">
    <h4 class="box-header round-top"><g:message code="default.list.label" args="['Expenses']"/>
        <a class="box-btn" title="close"><i class="icon-remove"></i></a>
        <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
        <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
        </a>
    </h4>

    <div class="box-container-toggle">
        <div class="box-content">
            <a id="showModal2" class="btn" href="javascript:"
               onclick="renderModal('', 'expense')">Add Expense</a><br/><br/>
            <table cellpadding="0" cellspacing="0" border="0"
                   class="table table-striped table-bordered bootstrap-datatable" id="datatableExpenses">
                <thead>
                <tr>

                    <th><g:message code="expense.expenseType.label" default="Expense Type"/></th>

                    <th><g:message code="expense.expenseDate.label" default="Expense Date"/></th>

                    <th><g:message code="expense.quantity.label" default="Quantity"/></th>

                    <th><g:message code="expense.unitPrice.label" default="Unit Price"/></th>

                    <th><g:message code="expense.total.label" default="Total"/></th>

                    <th><g:message code="expense.actions.label" default="Actions"/></th>

                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <script type="text/javascript">
                $(function () {
                    dataTableArray['expense'] = $('#datatableExpenses').dataTable({
                        bProcessing:true,
                        bServerSide:true,
                        iDisplayLength:10,
                        sAjaxSource:'${createLink(controller:'expense', action:'listJSON', id:assignmentInstance.id)}',
                        "sDom":"<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                        "sPaginationType":"bootstrap",
                        "oLanguage":{
                            "sLengthMenu":"_MENU_ records per page"
                        },
                        "aoColumnDefs":[

                            { "aTargets":[ 0 ] },

                            { "sName":"expenseDate", "aTargets":[ 1 ] },

                            { "sName":"quantity", "aTargets":[ 2 ] },

                            { "sName":"unitPrice", "aTargets":[ 3 ] },

                            { "aTargets":[ 4 ] },

                            { "bSortable":false, "aTargets":[ 5 ] }


                        ],
                        "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                            $('td:eq(5)', nRow).html('<a class="btn btn-info mini" href="javascript:" onclick="renderModal(\'' + nRow.id + '\', \'expense\')"><i class="icon-edit icon-white"></i></a>&nbsp;');
                        }
                    });

                })
                function submitForm(elem) {
                    if (confirm("Are you sure?"))
                        $(elem).closest("form").submit();
                }
            </script>
        </div>
    </div><!--/span-->

</div>

<div class="box" id="box-3">
    <h4 class="box-header round-top"><g:message code="default.list.label" args="['Collections']"/>
        <a class="box-btn" title="close"><i class="icon-remove"></i></a>
        <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
        <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
        </a>
    </h4>

    <div class="box-container-toggle">
        <div class="box-content">
            <a id="showModal3" class="btn" href="javascript:"
               onclick="renderModal('', 'collection')">Add Collection</a><br/><br/>
            <table cellpadding="0" cellspacing="0" border="0"
                   class="table table-striped table-bordered bootstrap-datatable" id="datatableCollections">
                <thead>
                <tr>

                    <th><g:message code="collection.collectionDate.label" default="Collection Date"/></th>

                    <th><g:message code="collection.amount.label" default="Amount"/></th>

                    <th><g:message code="collection.remarks.label" default="Remarks"/></th>

                    <th><g:message code="collection.actions.label" default="Actions"/></th>

                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <script type="text/javascript">
                $(function () {
                    dataTableArray['collection'] = $('#datatableCollections').dataTable({
                        bProcessing:true,
                        bServerSide:true,
                        iDisplayLength:10,
                        sAjaxSource:'${createLink(controller:'collection', action:'listJSON', id:assignmentInstance.id)}',
                        "sDom":"<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                        "sPaginationType":"bootstrap",
                        "oLanguage":{
                            "sLengthMenu":"_MENU_ records per page"
                        },
                        "aoColumnDefs":[

                            { "sName":"collectionDate", "aTargets":[ 0 ] },

                            { "sName":"amount", "aTargets":[ 1 ] },

                            { "sName":"remarks", "aTargets":[ 2 ] },

                            { "bSortable":false, "aTargets":[ 3 ] }


                        ],
                        "fnRowCallback":function (nRow, aData, iDisplayIndex) {
                            $('td:eq(3)', nRow).html('<a class="btn btn-info mini" href="javascript:" onclick="renderModal(\'' + nRow.id + '\', \'collection\')"><i class="icon-edit icon-white"></i></a>&nbsp;');
                        }
                    });

                })
                function submitForm(elem) {
                    if (confirm("Are you sure?"))
                        $(elem).closest("form").submit();
                }
            </script>
        </div>
    </div><!--/span-->

</div>

</div>

<div id="modalContainer">
    <ripple:modal className="EstimateItem" entity="Estimate Item" elementId="estimateItem"
                  parentId="${assignmentInstance.id}" parentClass="Assignment" parentProperty="project"/>
    <ripple:modal className="Expense" entity="Expense" elementId="expense" parentId="${assignmentInstance.id}"
                  parentClass="Assignment" parentProperty="project"/>
    <ripple:modal className="Collection" entity="Collection" elementId="collection" parentId="${assignmentInstance.id}"
                  parentClass="Assignment" parentProperty="project"/>
    <ripple:modal className="AssignmentLocation" entity="Project Location" elementId="assignmentLocation"
                  parentId="${assignmentInstance.id}" parentClass="Assignment" parentProperty="assignment"/>
    <ripple:modal className="Warehouse" entity="Warehouse" elementId="warehouse" parentId="${assignmentInstance.id}"
                  parentClass="Assignment" parentProperty="assignment"/>
    <ripple:modalScript parentProperty="${['project', 'assignment']}" parentId="${assignmentInstance.id}"/>
</div>
</div>
</body>
</html>
