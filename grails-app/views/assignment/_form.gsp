<%@ page import="com.ripple.master.Assignment" %>



<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'projectName', 'error')} required">
	<label for="projectName">
		<g:message code="assignment.projectName.label" default="Project Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="projectName" required="" value="${assignmentInstance?.projectName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="assignment.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${assignmentInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'active', 'error')} ">
	<label for="active">
		<g:message code="assignment.active.label" default="Active" />
		
	</label>
	<g:checkBox name="active" value="${assignmentInstance?.active}" />
</div>

<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'collections', 'error')} ">
	<label for="collections">
		<g:message code="assignment.collections.label" default="Collections" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${assignmentInstance?.collections?}" var="c">
    <li><g:link controller="collection" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="collection" action="create" params="['assignment.id': assignmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'collection.label', default: 'Collection')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'estimateItems', 'error')} ">
	<label for="estimateItems">
		<g:message code="assignment.estimateItems.label" default="Estimate Items" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${assignmentInstance?.estimateItems?}" var="e">
    <li><g:link controller="estimateItem" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="estimateItem" action="create" params="['assignment.id': assignmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'estimateItem.label', default: 'EstimateItem')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'expenses', 'error')} ">
	<label for="expenses">
		<g:message code="assignment.expenses.label" default="Expenses" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${assignmentInstance?.expenses?}" var="e">
    <li><g:link controller="expense" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="expense" action="create" params="['assignment.id': assignmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'expense.label', default: 'Expense')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'locations', 'error')} ">
	<label for="locations">
		<g:message code="assignment.locations.label" default="Locations" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${assignmentInstance?.locations?}" var="l">
    <li><g:link controller="assignmentLocation" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="assignmentLocation" action="create" params="['assignment.id': assignmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'assignmentLocation.label', default: 'AssignmentLocation')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: assignmentInstance, field: 'projectAddress', 'error')} ">
	<label for="projectAddress">
		<g:message code="assignment.projectAddress.label" default="Project Address" />
		
	</label>
	<g:textField name="projectAddress" value="${assignmentInstance?.projectAddress}"/>
</div>

