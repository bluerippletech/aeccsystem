<%@ page import="com.ripple.purchasing.PurchaseOrderLineItem" %>



<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="purchaseOrderLineItem.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${purchaseOrderLineItemInstance.constraints.isDeleted.inList}" value="${purchaseOrderLineItemInstance?.isDeleted}" valueMessagePrefix="purchaseOrderLineItem.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="purchaseOrderLineItem.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${purchaseOrderLineItemInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="purchaseOrderLineItem.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${purchaseOrderLineItemInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="purchaseOrderLineItem.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${purchaseOrderLineItemInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="purchaseOrderLineItem.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${purchaseOrderLineItemInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'item', 'error')} required">
	<label for="item">
		<g:message code="purchaseOrderLineItem.item.label" default="Item" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="item" name="item.id" from="${com.ripple.master.item.Item.list()}" optionKey="id" required="" value="${purchaseOrderLineItemInstance?.item?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'unitPrice', 'error')} required">
	<label for="unitPrice">
		<g:message code="purchaseOrderLineItem.unitPrice.label" default="Unit Price" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="unitPrice" required="" value="${fieldValue(bean: purchaseOrderLineItemInstance, field: 'unitPrice')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'quantity', 'error')} required">
	<label for="quantity">
		<g:message code="purchaseOrderLineItem.quantity.label" default="Quantity" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="quantity" required="" value="${fieldValue(bean: purchaseOrderLineItemInstance, field: 'quantity')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'purchaseOrder', 'error')} required">
	<label for="purchaseOrder">
		<g:message code="purchaseOrderLineItem.purchaseOrder.label" default="Purchase Order" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="purchaseOrder" name="purchaseOrder.id" from="${com.ripple.purchasing.PurchaseOrder.list()}" optionKey="id" required="" value="${purchaseOrderLineItemInstance?.purchaseOrder?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: purchaseOrderLineItemInstance, field: 'unitOfMeasure', 'error')} required">
	<label for="unitOfMeasure">
		<g:message code="purchaseOrderLineItem.unitOfMeasure.label" default="Unit Of Measure" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="unitOfMeasure" name="unitOfMeasure.id" from="${com.ripple.master.item.LevelUnit.list()}" optionKey="id" required="" value="${purchaseOrderLineItemInstance?.unitOfMeasure?.id}" class="many-to-one"/>
</div>

