<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
<div class="control-group ${invalid ? 'error' : ''}">
	<label class="control-label" for="${property}">${label}</label>
	<div class="controls">
        <g:select value="${value}" name="${property}" from="${com.ripple.master.supplier.Supplier.constraints?.infoStatus?.inList}" noSelection="${['':'Select One...']}"></g:select>
		<g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
	</div>
</div>
</div>