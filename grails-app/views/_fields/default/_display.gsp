<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            ${fieldValue(bean:bean,field:property)}
        </div>
    </div>
</div>