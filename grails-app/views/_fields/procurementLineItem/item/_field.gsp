<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
<div class="control-group ${invalid ? 'error' : ''}">
	<label class="control-label" for="${property}">${label}</label>
	<div class="controls">
        <ripple:autoComplete property="${property}" valueId="${value?.id}" name="${property}.itemName" id="${property}" class="pull-left" value="${value}" controller="item" action="listing" onComplete="changeCategory"/>
        <ripple:itemPicker searchProperty="${property}" property="${property}" controller="item" action="listJSON" fields="itemCode,itemName,description,status,itemClass,itemSubclass" classId="procurementLineItem" pickerName="Item List" onComplete="changeCategory"/>
        <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
	</div>
</div>
</div>