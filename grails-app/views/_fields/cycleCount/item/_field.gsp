<%@ page import="com.ripple.master.item.Item" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <g:select name="${property}.id" id="${property}" from="${Item.findAll()}" value="${bean?.item?.id}" optionKey="id" optionValue="itemCode" noSelection="${['':'Select an Item']}"></g:select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>