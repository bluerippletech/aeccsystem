<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <g:select name="${property}" from="${1..4}" value="${value}"/>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>