<%@ page defaultCodec="html" %>
<g:set var="expenseType" value="${com.ripple.project.ExpenseType.get(value?.id)}"/>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="expenseCategory">Expense Category</label>
        <div class="controls">
            %{--<select name="expenseCategory.id" id="expenseCategory" value="${value?.id}"></select>--}%
            <g:select onchange="changeCategory(this)" value="${expenseType?.expenseCategory}" name="expenseCategory" from="${com.ripple.project.ExpenseType?.constraints?.expenseCategory?.inList}" noSelection="${['':'Select One...']}"></g:select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <select name="${property}.id" id="${property}.id" value="${value?.id}">
                %{--<g:if test="${expenseType}">--}%
                    <g:each in="${com.ripple.project.ExpenseType.list()}" var="expenseTypeTemp">
                        <option value="${expenseTypeTemp?.id}" ${(expenseType?.id==expenseTypeTemp?.id)?'selected=selected':''}>${expenseTypeTemp?.expenseType}</option>
                    </g:each>
                %{--</g:if>--}%
            </select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>
<script type="text/javascript">
    function changeCategory(selectObj){
        $.getJSON("${createLink(controller: 'expenseType', action: 'categorized')}",{'category': $(selectObj).val(), ajax: 'true'}, function(j){
            var options = '';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].optionKey + '">' + j[i].optionValue + '</option>';
            }
            $("select#${property}\\.id").html(options);
        })
    };
</script>