<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
<div class="control-group ${invalid ? 'error' : ''}">
	<label class="control-label" for="${property}">${label}</label>
	<div class="controls">
        <input type="text" class="input-xlarge" style="width: 210px;" name="contactPerson.${property}" id="${property}" value="${value}"/>
		<g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
	</div>
</div>
</div>