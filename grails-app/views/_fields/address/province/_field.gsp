<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
<div class="control-group ${invalid ? 'error' : ''}">
	<label class="control-label" for="${property}">${label}</label>
	<div class="controls">
        %{--<g:select value="${value}" name="address.${property}.id" optionKey="id" optionValue="name" from="${com.ripple.master.geography.City.list()}" noSelection="${['':'Select One...']}"></g:select>--}%
        <input type="text" autocomplete="off" data-provide="typeahead" class="input-xlarge" style="width: 210px;" name="address.${property}.name" id="${property}" value="${value}" />
        <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
	</div>
</div>
</div>
<script type="text/javascript">
    $(function(){
        $('#${property}').typeahead({
            source: function(query, process) {
                $.post('${createLink(controller: property, action: 'listing')}', { q: query, limit: 4 }, function(data) {
                    process(data);
                }); // end of post
            }
        });
    })
</script>