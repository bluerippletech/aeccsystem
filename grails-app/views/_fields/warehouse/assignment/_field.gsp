<%@ page import="com.ripple.master.Assignment" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <g:select name="assignment.id" id="assignment" from="${Assignment.findAllByActive(1)}" value="${bean.assignment?.id}" optionKey="id" optionValue="projectName" noSelection="${['':'Select a Project']}"></g:select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>