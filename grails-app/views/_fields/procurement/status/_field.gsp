<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <select id="${property}" name="${property}">
                <g:each in="${procurementInstance?.constraints?.status?.inList}">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <option value="${it}" <g:if test="${procurementInstance?.status==it}">selected="selected"</g:if>>${it}</option>
                    </sec:ifAnyGranted>
                    <sec:ifNotGranted roles="ROLE_ADMIN">
                        <g:if test="${it=='Draft' || it=='For Approval' || it=='Cancelled'}">
                            <option value="${it}" <g:if test="${procurementInstance?.status==it}">selected="selected"</g:if>>${it}</option>
                        </g:if>
                    </sec:ifNotGranted>
                </g:each>
            </select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>