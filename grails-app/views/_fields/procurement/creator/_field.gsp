<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <input type="hidden" class="" name="${property}.id" id="${property}.id" value="${sec.loggedInUserInfo(field: 'id')}" />
            <span class="input-large uneditable-input"><sec:loggedInUserInfo field='username'/></span>
        </div>
    </div>
</div>