<%@ page import="com.ripple.master.Employee" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <g:select name="${property}.id" id="${property}" from="${Employee.findAllByIsActive(1)}" value="${bean?.requestor?.id}" optionKey="id" noSelection="${['':'Select an Employee']}"></g:select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>