<%@ page import="com.ripple.master.AssignmentLocation" defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <g:select name="applicableSites.id" id="applicableSites" from="${AssignmentLocation.list()}" multiple="multiple" size="${AssignmentLocation.list().size()}"  optionKey="id" optionValue="projectAndLocation" class="multiSelect" value="${value?.id}"/>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#applicableSites").multiSelect({
            selectAll: true,
            noneSelected: '0 site/s',
            oneOrMoreSelected: '% site/s'
        });
    });
</script>
