<%@ page import="com.ripple.master.item.LevelUnit" defaultCodec="html" %>
<g:set var="unitOfMeasure" value="${LevelUnit.get(value?.id)}"/>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <select name="${property}.id" id="${property}.id" value="${value?.id}">
                <g:if test="${unitOfMeasure}">
                    <option value="${unitOfMeasure?.id}" ${(unitOfMeasure?.id==unitOfMeasure?.id)?'selected=selected':''}>${unitOfMeasure?.unitOfMeasure?.name}</option>
                </g:if>
            </select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>
<script type="text/javascript">
    function changeCategory(selectObj){
        $.getJSON("${createLink(controller: 'procurementLineItem', action: 'categorized')}",{'category': $(selectObj).val(), ajax: 'true'}, function(j){
            var options = '';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].optionKey + '">' + j[i].optionValue + '</option>';
            }
            $("select#${property}\\.id").html(options);
        })
    };
</script>