<%@ page defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
<div class="control-group ${invalid ? 'error' : ''}">
	<label class="control-label" for="${property}">${label}</label>
	<div class="controls">
        <g:select name="${property}.id" from="${com.ripple.purchasing.PurchaseOrder.findAllByStatusOrStatus('Approved','Partially Served')}" optionKey="id" noSelection="${['':'-Select a Purchase Order-']}"/>
        <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
	</div>
</div>
</div>