<%@ page import="com.ripple.entry.PayrollEntry" defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group ${invalid ? 'error' : ''}">
        <label class="control-label" for="${property}">${label}</label>
        <div class="controls">
            <g:select optionKey="id" from="${PayrollEntry.findAllByEmployee(bean?.loan?.employee).reverse()}" name="payrollEntry.id" value="${value?.id}"></g:select>
            <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        </div>
    </div>
</div>