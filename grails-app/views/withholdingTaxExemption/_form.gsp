<%@ page import="com.ripple.deductions.WithholdingTaxExemption" %>



<div class="fieldcontain ${hasErrors(bean: withholdingTaxExemptionInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="withholdingTaxExemption.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="status" name="status.id" from="${com.ripple.deductions.WithholdingTaxStatus.list()}" optionKey="id" required="" value="${withholdingTaxExemptionInstance?.status?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxExemptionInstance, field: 'bracket', 'error')} required">
	<label for="bracket">
		<g:message code="withholdingTaxExemption.bracket.label" default="Bracket" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="bracket" name="bracket.id" from="${com.ripple.deductions.WithholdingTaxBracket.list()}" optionKey="id" required="" value="${withholdingTaxExemptionInstance?.bracket?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxExemptionInstance, field: 'value', 'error')} required">
	<label for="value">
		<g:message code="withholdingTaxExemption.value.label" default="Value" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="value" required="" value="${fieldValue(bean: withholdingTaxExemptionInstance, field: 'value')}"/>
</div>

