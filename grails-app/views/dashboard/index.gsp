<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <title><sec:loggedInUserInfo field="username"/> - Dashboard</title>
    <r:require modules="all"/>

</head>

<body>

<div class="row-fluid">
    <!-- Portlet Set 4 -->
    <div class="span4 column" id="col1">
        <!-- Portlet: Site Activity Gauges -->
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="portlet.label.thisMonth" args="${['Birthdays']}"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <g:if test="${birthdays.size() > 0}">
                        <ul class="dashboard-member-activity">
                            <g:each in="${birthdays}" var="employee">
                                <g:if test="${employee?.isActive==true}">
                                    <li>
                                   <a href="${createLink(controller: 'employee', action: 'show', id: employee.id)}">
                                    <g:if test="${employee.picture}">
                                        <img src="${createLink(controller: 'image', action: 'show', id: employee.id, params: [classname: 'com.ripple.master.Employee', fieldName: 'picture'])}"
                                             class="dashboard-member-activity-avatar"/></a>
                                    </g:if>
                                    <g:else>
                                        <img src="images/member_ph.png" class="dashboard-member-activity-avatar"/></a>
                                    </g:else>
                                    <strong>Name:</strong> <a
                                        href="${createLink(controller: 'employee', action: 'show', id: employee.id)}">${employee.toString()}</a><br/>
                                    <strong>Date:</strong> ${employee.birthDate.toString('MMMMM dd, yyyy')}<br/>
                                    <strong>Position:</strong> <span
                                        class="label label-success">${employee.currentPosition}</span>
                                    </a>
                                    </li>
                                </g:if>
                            </g:each>
                        </ul>
                    </g:if>
                </div>
            </div>
        </div><!--/span-->
    </div>

    <div class="span4 column" id="col2">
        <!-- Portlet: Site Activity Gauges -->
        <div class="box" id="box-2">
            <h4 class="box-header round-top"><g:message code="portlet.label.thisMonth" args="${['Anniversaries']}"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <g:if test="${anniversaries.size() > 0}">
                        <ul class="dashboard-member-activity">
                            <g:each in="${anniversaries}" var="employee">
                                <g:if test="${employee?.isActive == true}">
                                    <li>
                                   <a href="${createLink(controller: 'employee', action: 'show', id: employee.id)}">
                                    <g:if test="${employee.picture}">
                                        <img src="${createLink(controller: 'image', action: 'show', id: employee.id, params: [classname: 'com.ripple.master.Employee', fieldName: 'picture'])}"
                                             class="dashboard-member-activity-avatar"/></a>
                                    </g:if>
                                    <g:else>
                                        <img src="images/member_ph.png" class="dashboard-member-activity-avatar"/></a>
                                    </g:else>
                                    <strong>Name:</strong> <a
                                        href="${createLink(controller: 'employee', action: 'show', id: employee.id)}">${employee.toString()}</a><br/>
                                    <strong>Since:</strong> ${employee.employmentDate.toString('MMMMM dd, yyyy')}<br/>
                                    <strong>Position:</strong> <span
                                        class="label label-success">${employee.currentPosition}</span>
                                    </a>
                                    </li>
                                </g:if>
                            </g:each>
                        </ul>
                    </g:if>
                </div>
            </div>
        </div><!--/span-->
    </div>

    <div class="span4 column" id="col3">
        <!-- Portlet: Site Activity Gauges -->
        <div class="box" id="box-3">
            <h4 class="box-header round-top"><g:message code="portlet.label.nextMonth" args="${['Anniversaries']}"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <g:if test="${anniversariesNxtMonth.size() > 0}">
                        <ul class="dashboard-member-activity">
                            <g:each in="${anniversariesNxtMonth}" var="employee">
                                <g:if test="${employee?.isActive == true}">

                                    <li>
                                   <a href="${createLink(controller: 'employee', action: 'show', id: employee.id)}">
                                    <g:if test="${employee.picture}">
                                        <img src="${createLink(controller: 'image', action: 'show', id: employee.id, params: [classname: 'com.ripple.master.Employee', fieldName: 'picture'])}"
                                             class="dashboard-member-activity-avatar"/></a>
                                    </g:if>
                                    <g:else>
                                        <img src="images/member_ph.png" class="dashboard-member-activity-avatar"/></a>
                                    </g:else>
                                    <strong>Name:</strong> <a
                                        href="${createLink(controller: 'employee', action: 'show', id: employee.id)}">${employee.toString()}</a><br/>
                                    <strong>Since:</strong> ${employee.employmentDate.toString('MMMMM dd, yyyy')}<br/>
                                    <strong>Position:</strong> <span
                                        class="label label-success">${employee.currentPosition}</span>
                                    </a>
                                    </li>
                                </g:if>
                            </g:each>
                        </ul>
                    </g:if>
                </div>
            </div>
        </div><!--/span-->
    </div>
</div>
</body>
</html>
