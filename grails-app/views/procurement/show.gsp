
<%@ page import="com.ripple.procurement.Procurement" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'procurement.label', default: 'Procurement')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <r:script>
        var dataTableArray = {};
        $(function(){$('#'+"procurementLineItemModal").on('show', function () {
            changeCategory($('#item\\.id'))
            $.post('${createLink(controller: 'procurementLineItem', action: 'getUnitMeasure')}', { 'id': $('#procurementLineItemModal').find('#id').val() }, function(data) {
                $("#procurementLineItemModal").find("#unitOfMeasure\\.id").ajaxComplete(function(event,request, settings){
                    $(this).val(data)
                });
            }); // end of post
//            $()
        })});

        function enableSubmit(checkbox){
            var status = $(checkbox).attr('checked');
            var submitButton = $('.checked-all').attr('disabled');
            if(status && submitButton){
                $('.checked-all').removeAttr('disabled')
            }
            else if($('input[name="selectedItemLine"]:checked').length<1){
                $('.checked-all').attr('disabled','disabled');
            }
        }

        function toggleCheckboxes(checkbox){
            var status = $(checkbox).attr('checked');
            if(status){
                $('input[name="selectedItemLine"]').attr('checked','checked');
                if($('input[name="selectedItemLine"]:checked').length>0){
                    $('.checked-all').removeAttr('disabled')
                }
            }
            else{
                $('input[name="selectedItemLine"]').removeAttr('checked');
                $('.checked-all').attr('disabled','disabled');
            }
        }
    </r:script>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-procurement" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="procurement.label"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="procurementNumber-label" class="control-label"><g:message
                                                code="procurement.procurementNumber.label"
                                                default="Procurement Number"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="procurementNumber-label"><g:fieldValue
                                                    bean="${procurementInstance}" field="procurementNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="project-label" class="control-label"><g:message
                                                code="procurement.project.label"
                                                default="Project"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="project-label"><g:link
                                                    controller="assignment" action="show"
                                                    id="${procurementInstance?.project?.id}">${procurementInstance?.project?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="requestor-label" class="control-label"><g:message
                                                code="procurement.requestor.label"
                                                default="Requestor"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="requestor-label"><g:link
                                                    controller="employee" action="show"
                                                    id="${procurementInstance?.requestor?.id}">${procurementInstance?.requestor?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="creator-label" class="control-label"><g:message
                                                code="procurement.creator.label"
                                                default="Creator"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="creator-label"><g:link
                                                    controller="employee" action="show"
                                                    id="${procurementInstance?.creator?.id}">${procurementInstance?.creator?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="warehouse-label" class="control-label"><g:message
                                                code="procurement.warehouse.label"
                                                default="Warehouse"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="warehouse-label"><g:link
                                                    controller="warehouse" action="show"
                                                    id="${procurementInstance?.warehouse?.id}">${procurementInstance?.warehouse?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="dateStarted-label" class="control-label"><g:message
                                                code="procurement.dateStarted.label"
                                                default="Date Created"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="dateStarted-label"><g:fieldValue
                                                    bean="${procurementInstance}" field="dateStarted"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="dateClosed-label" class="control-label"><g:message
                                                    code="procurement.dateClosed.label"
                                                    default="Date Closed"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="dateClosed-label"><g:fieldValue
                                                        bean="${procurementInstance}" field="dateClosed"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="status-label" class="control-label"><g:message
                                                code="procurement.status.label"
                                                default="Status"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                    bean="${procurementInstance}" field="status"/></span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </fieldset>
                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${procurementInstance?.id}"/>

                            <g:if test="${procurementInstance?.status=='Approved'}">
                                <sec:ifAnyGranted roles="ROLE_ADMIN">
                                    <g:actionSubmit class="btn btn-primary" action="edit"
                                                    value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>
                                </sec:ifAnyGranted>
                            </g:if>
                            <g:else>
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>
                             </g:else>

                            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_USER">
                                <input type="hidden" name="version" value="${procurementInstance?.version}"/>
                                <g:if test="${procurementInstance?.status=='Draft'}">
                                    <g:actionSubmit class="btn btn-success" action="formStatus"
                                                    value="${message(code: 'procurement.forApproval.label', default: 'For Approval')}"/>
                                </g:if>
                                <g:actionSubmit class="btn btn-danger" action="formStatus"
                                                value="${message(code: 'procurement.cancel.label', default: 'Cancel')}"/>
                            </sec:ifAnyGranted>

                            <sec:ifAnyGranted roles="ROLE_ADMIN">
                                <g:actionSubmit class="btn btn-primary" action="formStatus"
                                    value="${message(code: 'procurement.disapproved.label', default: 'Send back to draft')}"/>
                                <g:if test="${procurementInstance?.status=='For Approval'}">
                                    <g:actionSubmit class="btn btn-success" action="formStatus"
                                                    value="${message(code: 'procurement.approved.label', default: 'Approve')}"/>
                                    <g:actionSubmit class="btn btn-danger" action="formStatus"
                                                    value="${message(code: 'procurement.disapproved.label', default: 'Disapprove')}"/>
                                </g:if>
                            </sec:ifAnyGranted>

                            <g:actionSubmit class="btn btn-info pull-right" action="newFromExisting"
                                            value="${message(code: 'procurement.newFromExisting.label', default: 'Create New Copy')}"
                                            onclick="return confirm('${message(code: 'procurement.newFromExisting.confirm.message', default: 'Are you sure you want to create a new copy of this slip?')}');"/>

                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="list-procurementLineItem" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="procurementLineItem.label"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <g:if test="${procurementInstance?.status=='Approved'}">
                        <sec:ifAnyGranted roles="ROLE_ADMIN">
                            <a id="showModal" href="javascript:" onclick="renderModal('', 'procurementLineItem')" class="btn btn-primary" style="margin-bottom:10px;">Add New Line Item</a>
                        </sec:ifAnyGranted>
                    </g:if>
                    <g:else>
                        <a id="showModal" href="javascript:" onclick="renderModal('', 'procurementLineItem')" class="btn btn-primary" style="margin-bottom:10px;">Add New Line Item</a>
                    </g:else>
                    <g:form controller="procurement">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable-procurement-item">
                        <thead>
                        <tr>

                            <th><input type="checkbox" onclick="toggleCheckboxes(this)"/></th>

                            <th><g:message code="procurementLineItem.itemCode.label" default="Item Code" /></th>

                            <th><g:message code="procurementLineItem.itemName.label" default="Item Name" /></th>

                            <th><g:message code="procurementLineItem.quantity.label" default="Quantity" /></th>

                            <th><g:message code="procurementLineItem.available.label" default="Available in Warehouse" /></th>

                            <th><g:message code="procurementLineItem.unitOfMeasure.label" default="Unit of Measure" /></th>

                            <th><g:message code="procurementLineItem.purpose.label" default="MIS Purpose" /></th>

                            <th><g:message code="procurementLineItem.actions.label" default="Actions" /></th>

                            <th><g:message code="procurementLineItem.qtyToProcess.label" default="Qty To Process" /></th>

                            <th><g:message code="procurementLineItem.uoM.label" default="UoM" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                        <input type="hidden" name="warehouse.id" value="${procurementInstance?.warehouse?.id}"/>
                        <input type="hidden" name="procurement.id" value="${procurementInstance?.id}"/>
                        <g:if test="${procurementInstance?.status=='Approved'}">
                            <sec:ifAnyGranted roles="ROLE_ADMIN">
                                <div style="text-align: right;margin-top:10px">
                                    <g:actionSubmit action="newFromProcurementToMIS" name="newFromProcurement" class="btn btn-success checked-all" id="submitItemLine"
                                                    value="${message(code: 'procurement.newMIS.label', default: 'Create New Material Issuance Slip')}" disabled="disabled"/>
                                    <g:actionSubmit action="newFromProcurementToPurchaseOrder" name="newFromProcurement" class="btn btn-success checked-all" id="submitItemLine"
                                                    value="${message(code: 'procurement.newPurchaseOrder.label', default: 'Create New Purchase Order')}" disabled="disabled"/>

                                </div>
                            </sec:ifAnyGranted>
                        </g:if>
                        <g:else>
                            <div style="text-align: right;margin-top:10px">
                                <g:actionSubmit action="newFromProcurementToMIS" name="newFromProcurement" class="btn btn-success checked-all" id="submitItemLine"
                                                value="${message(code: 'procurement.newMIS.label', default: 'Create New Material Issuance Slip')}" disabled="disabled"/>
                                <g:actionSubmit action="newFromProcurementToPurchaseOrder" name="newFromProcurement" class="btn btn-success checked-all" id="submitItemLine"
                                                value="${message(code: 'procurement.newPurchaseOrder.label', default: 'Create New Purchase Order')}" disabled="disabled"/>

                            </div>
                        </g:else>
                    </g:form>
                    <script type="text/javascript">
                        $(function(){
                            var procurementStatus = "${procurementInstance?.status}";
                            dataTableArray['procurementLineItem'] = $('#datatable-procurement-item').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'procurementLineItem', action:'listJSON', id: "${procurementInstance?.id}")}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[
                                    { "bSortable": false, "aTargets": [0], "sWidth": "20px" },

                                    { "sName": "item", "aTargets": [ 1 ] },

                                    { "sName": "item", "aTargets": [ 2 ] },

                                    { "sName": "quantity", "aTargets": [ 3 ] },

                                    { "sName": "available", "aTargets": [ 4 ], "sWidth": "80px" },

                                    { "sName": "unitOfMeasure", "aTargets": [ 5 ] },

                                    { "sName": "purpose", "aTargets": [ 6 ] },

                                    { "bSortable": false, "aTargets": [ 7 ] },

                                    { "bSortable": false, "aTargets": [ 8 ], "sWidth": "40px"},

                                    { "bSortable": false, "aTargets": [ 9 ]}
                                ],
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    $('td:eq(0)', nRow).append('<input class="select-item" type="checkbox" name="selectedItemLine" onchange="enableSubmit(this)" value="'+nRow.id+'"/>');
                                    $('td:eq(7)', nRow).append('<a class="btn btn-info mini edit-action" href="javascript:" onclick="renderModal(\''+nRow.id+'\', \'procurementLineItem\')"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                    $('td:eq(8)', nRow).append('<input type="number" class="input-mini" id="'+nRow.id+'" name="'+nRow.id+'" step="0.01"/>');
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modalContainer">
    <ripple:modal className="ProcurementLineItem" entity="Procurement Line Item" elementId="procurementLineItem" parentId="${procurementInstance.id}" parentClass="Procurement" parentProperty="procurement"/>
    <ripple:modalScript parentProperty="procurement"  parentId="${procurementInstance.id}"/>
</div>
</body>
</html>
