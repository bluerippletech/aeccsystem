<%@ page import="com.ripple.procurement.Procurement" %>



<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="procurement.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${procurementInstance.constraints.isDeleted.inList}" value="${procurementInstance?.isDeleted}" valueMessagePrefix="procurement.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="procurement.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${procurementInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="procurement.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${procurementInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="procurement.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${procurementInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="procurement.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${procurementInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'creator', 'error')} required">
	<label for="creator">
		<g:message code="procurement.creator.label" default="Creator" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="creator" name="creator.id" from="${com.ripple.master.Employee.list()}" optionKey="id" required="" value="${procurementInstance?.creator?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'dateClosed', 'error')} required">
	<label for="dateClosed">
		<g:message code="procurement.dateClosed.label" default="Date Closed" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="dateClosed" value="${procurementInstance?.dateClosed}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'lineItems', 'error')} ">
	<label for="lineItems">
		<g:message code="procurement.lineItems.label" default="Line Items" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${procurementInstance?.lineItems?}" var="l">
    <li><g:link controller="procurementLineItem" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="procurementLineItem" action="create" params="['procurement.id': procurementInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'procurementLineItem.label', default: 'ProcurementLineItem')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'procurementNumber', 'error')} ">
	<label for="procurementNumber">
		<g:message code="procurement.procurementNumber.label" default="Procurement Number" />
		
	</label>
	<g:textField name="procurementNumber" value="${procurementInstance?.procurementNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="procurement.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project" name="project.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" required="" value="${procurementInstance?.project?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'requestor', 'error')} required">
	<label for="requestor">
		<g:message code="procurement.requestor.label" default="Requestor" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="requestor" name="requestor.id" from="${com.ripple.master.Employee.list()}" optionKey="id" required="" value="${procurementInstance?.requestor?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="procurement.status.label" default="Status" />
		
	</label>
	<g:textField name="status" value="${procurementInstance?.status}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementInstance, field: 'warehouse', 'error')} required">
	<label for="warehouse">
		<g:message code="procurement.warehouse.label" default="Warehouse" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="warehouse" name="warehouse.id" from="${com.ripple.master.Warehouse.list()}" optionKey="id" required="" value="${procurementInstance?.warehouse?.id}" class="many-to-one"/>
</div>

