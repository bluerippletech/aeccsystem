<%@ page import="com.ripple.deductions.SssContribution" %>



<div class="fieldcontain ${hasErrors(bean: sssContributionInstance, field: 'lowRange', 'error')} required">
	<label for="lowRange">
		<g:message code="sssContribution.lowRange.label" default="Low Range" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="lowRange" required="" value="${fieldValue(bean: sssContributionInstance, field: 'lowRange')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sssContributionInstance, field: 'highRange', 'error')} required">
	<label for="highRange">
		<g:message code="sssContribution.highRange.label" default="High Range" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="highRange" required="" value="${fieldValue(bean: sssContributionInstance, field: 'highRange')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sssContributionInstance, field: 'monthlySalaryCredit', 'error')} required">
	<label for="monthlySalaryCredit">
		<g:message code="sssContribution.monthlySalaryCredit.label" default="Monthly Salary Credit" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="monthlySalaryCredit" required="" value="${fieldValue(bean: sssContributionInstance, field: 'monthlySalaryCredit')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sssContributionInstance, field: 'employeeCompensation', 'error')} required">
	<label for="employeeCompensation">
		<g:message code="sssContribution.employeeCompensation.label" default="Employee Compensation" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="employeeCompensation" required="" value="${fieldValue(bean: sssContributionInstance, field: 'employeeCompensation')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sssContributionInstance, field: 'employeeContribution', 'error')} required">
	<label for="employeeContribution">
		<g:message code="sssContribution.employeeContribution.label" default="Employee Contribution" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="employeeContribution" required="" value="${fieldValue(bean: sssContributionInstance, field: 'employeeContribution')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sssContributionInstance, field: 'employerContribution', 'error')} required">
	<label for="employerContribution">
		<g:message code="sssContribution.employerContribution.label" default="Employer Contribution" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="employerContribution" required="" value="${fieldValue(bean: sssContributionInstance, field: 'employerContribution')}"/>
</div>

