
<%@ page import="com.ripple.deductions.SssContribution" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'sssContribution.label', default: 'SssContribution')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-sssContribution" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="highRange-label" class="control-label"><g:message
                                                    code="sssContribution.highRange.label"
                                                    default="High Range"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="highRange-label"><g:fieldValue
                                                        bean="${sssContributionInstance}" field="highRange"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="employeeCompensation-label" class="control-label"><g:message
                                                    code="sssContribution.employeeCompensation.label"
                                                    default="Employee Compensation"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="employeeCompensation-label"><g:fieldValue
                                                        bean="${sssContributionInstance}" field="employeeCompensation"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="employerContribution-label" class="control-label"><g:message
                                                    code="sssContribution.employerContribution.label"
                                                    default="Employer Contribution"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="employerContribution-label"><g:fieldValue
                                                        bean="${sssContributionInstance}" field="employerContribution"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="lowRange-label" class="control-label"><g:message
                                                    code="sssContribution.lowRange.label"
                                                    default="Low Range"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="lowRange-label"><g:fieldValue
                                                        bean="${sssContributionInstance}" field="lowRange"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="monthlySalaryCredit-label" class="control-label"><g:message
                                                    code="sssContribution.monthlySalaryCredit.label"
                                                    default="Monthly Salary Credit"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="monthlySalaryCredit-label"><g:fieldValue
                                                        bean="${sssContributionInstance}" field="monthlySalaryCredit"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="employeeContribution-label" class="control-label"><g:message
                                                    code="sssContribution.employeeContribution.label"
                                                    default="Employee Contribution"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="employeeContribution-label"><g:fieldValue
                                                        bean="${sssContributionInstance}" field="employeeContribution"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>
                    </fieldset>
                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${sssContributionInstance?.id}"/>
                        %{--<g:link class="edit" action="edit" id="${sssContributionInstance?.id}"><g:message--}%
                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                            <g:actionSubmit class="btn btn-primary" action="edit"
                                            value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                            <g:actionSubmit class="delete btn-danger" action="delete"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
