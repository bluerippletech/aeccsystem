<%@ page import="com.ripple.deductions.WithholdingTaxStatus" %>



<div class="fieldcontain ${hasErrors(bean: withholdingTaxStatusInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="withholdingTaxStatus.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${withholdingTaxStatusInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxStatusInstance, field: 'dependents', 'error')} required">
	<label for="dependents">
		<g:message code="withholdingTaxStatus.dependents.label" default="Dependents" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="dependents" from="${0..4}" class="range" required="" value="${fieldValue(bean: withholdingTaxStatusInstance, field: 'dependents')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxStatusInstance, field: 'brackets', 'error')} ">
	<label for="brackets">
		<g:message code="withholdingTaxStatus.brackets.label" default="Brackets" />
		
	</label>
	<g:select name="brackets" from="${com.ripple.deductions.WithholdingTaxBracket.list()}" multiple="multiple" optionKey="id" size="5" value="${withholdingTaxStatusInstance?.brackets*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxStatusInstance, field: 'exemptions', 'error')} ">
	<label for="exemptions">
		<g:message code="withholdingTaxStatus.exemptions.label" default="Exemptions" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${withholdingTaxStatusInstance?.exemptions?}" var="e">
    <li><g:link controller="withholdingTaxExemption" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="withholdingTaxExemption" action="create" params="['withholdingTaxStatus.id': withholdingTaxStatusInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'withholdingTaxExemption.label', default: 'WithholdingTaxExemption')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxStatusInstance, field: 'period', 'error')} required">
	<label for="period">
		<g:message code="withholdingTaxStatus.period.label" default="Period" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="period" name="period.id" from="${com.ripple.deductions.WithholdingTaxPeriod.list()}" optionKey="id" required="" value="${withholdingTaxStatusInstance?.period?.id}" class="many-to-one"/>
</div>

