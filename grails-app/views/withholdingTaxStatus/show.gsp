
<%@ page import="com.ripple.deductions.WithholdingTaxStatus" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-withholdingTaxStatus" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="dependents-label" class="control-label"><g:message
                                                    code="withholdingTaxStatus.dependents.label"
                                                    default="Dependents"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="dependents-label"><g:fieldValue
                                                        bean="${withholdingTaxStatusInstance}" field="dependents"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="exemptions-label" class="control-label"><g:message
                                                    code="withholdingTaxStatus.exemptions.label"
                                                    default="Exemptions"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${withholdingTaxStatusInstance.exemptions}" var="e">
                                                    <span class="property-value" aria-labelledby="exemptions-label"><g:link
                                                            controller="withholdingTaxExemption" action="show"
                                                            id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="name-label" class="control-label"><g:message
                                                    code="withholdingTaxStatus.name.label"
                                                    default="Name"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="name-label"><g:fieldValue
                                                        bean="${withholdingTaxStatusInstance}" field="name"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="brackets-label" class="control-label"><g:message
                                                    code="withholdingTaxStatus.brackets.label"
                                                    default="Brackets"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${withholdingTaxStatusInstance.brackets}" var="b">
                                                    <span class="property-value" aria-labelledby="brackets-label"><g:link
                                                            controller="withholdingTaxBracket" action="show"
                                                            id="${b.id}">${b?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="period-label" class="control-label"><g:message
                                                    code="withholdingTaxStatus.period.label"
                                                    default="Period"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="period-label"><g:link
                                                        controller="withholdingTaxPeriod" action="show"
                                                        id="${withholdingTaxStatusInstance?.period?.id}">${withholdingTaxStatusInstance?.period?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${withholdingTaxStatusInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${withholdingTaxStatusInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
