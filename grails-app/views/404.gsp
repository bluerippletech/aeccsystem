<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <r:require modules="all"/>
</head>
<body>
<!-- main content area -->
<div class="row-fluid">
    <!-- Portlet Set 1 -->
    <div class="span12 column" id="col1"> <!-- class column makes this a sortable column, id=col1 is for the remember position function -->
    <!-- Portlet: Box 1 -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top">Error
                <a class="box-btn" title="close"><i class="icon-remove"></i></a><!-- Can be removed if not wanted -->
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a><!-- Can be removed if not wanted -->
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a><!-- Can be removed if not wanted -->
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <h2 class="heading color-black">oops!</h2>
                    <p>The content your’re looking for may have been moved or is no longer available. Sorry about that.</p>

                    <div class="four columns error_404_box error_box">
                        404
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content area -->
</body>
</html>
