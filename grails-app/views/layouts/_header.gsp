<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="${createLink(uri: '/')}"><img src="${resource(dir: 'images', file: 'algon-small-logo.png')}" style="height:30px;"></a>
            <div class="btn-group pull-right">
                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="icon-user"></i> <sec:loggedInUserInfo field="username"/>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="${createLink(controller: 'appUser', action: 'showRedirect')}?username=<sec:loggedInUserInfo field="username"/>">Profile</a></li>
                    <li><a href="${createLink(controller: 'appUser', action: 'editRedirect')}?username=<sec:loggedInUserInfo field="username"/>">Settings</a></li>
                    <li class="divider"></li>
                    <li><g:link controller="logout" action="index">Logout</g:link></li>
                </ul>
            </div>
            <div class="nav-collapse">
                &nbsp;
            </div>
        </div>
    </div>
</div>
