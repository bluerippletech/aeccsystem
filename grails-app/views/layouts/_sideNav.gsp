<!-- Side Navigation -->
<div class="span2">
    <div class="member-box round-all">
        %{--<a><img src="images/member_ph.png" class="member-box-avatar" /></a>--}%
        <span>
            %{--<strong><ripple:getSecurityRole/></strong><br/>--}%
            <a><sec:loggedInUserInfo field="username"/></a><br/>
            <span class="member-box-links"><a href="${createLink(controller: 'appUser', action: 'editRedirect')}?username=<sec:loggedInUserInfo field="username"/>">Settings</a> | <g:link controller="logout" action="index">Logout</g:link></span>
        </span>
    </div>
    <div class="sidebar-nav">
        <div class="well" style="padding: 8px 0;">
            <ul class="nav nav-list">
                <li class="nav-header">Master</li>
                <li class="${params.controller=='employee'?'active':''}"><a href="${createLink(controller:'employee',action:'index')}"><i class="icon-user"></i> Employee</a></li>
                <li class="${params.controller=='rate'?'active':''}"><a href="${createLink(controller:'rate',action:'index')}"><i class="icon-gift"></i> Salary</a></li>
                <li class="${params.controller=='holiday'?'active':''}"><a href="${createLink(controller:'holiday',action:'index')}" class="dropdown-toggle"><i class="icon-calendar"></i> Holiday</a></li>
                <li class="nav-header">Payroll</li>
                <li class="${params.controller == 'workingHours' || params.controller == 'payPeriod' || params.controller == 'payrollEntry'?'active':''} dropdown"><a class="dropdown-toggle" data-toggle="collapse" data-target="#payroll-dropdown" href="#"><i class="icon-list"></i> Entry</a>
                    <ul class="${params.controller == 'workingHours' || params.controller == 'payPeriod' || params.controller == 'payrollEntry'?'':'collapse'} no-list-style custom-nav" id="payroll-dropdown">
                        <li class="${params.controller=='workingHours'?'active':''}"><a href="${createLink(controller:'workingHours',action:'index')}" ><i class="icon-arrow-right"></i> Working Hours</a></li>
                        <li class="${params.controller=='payrollEntry'?'active':''}"><a href="${createLink(controller:'payrollEntry',action:'filter')}" ><i class="icon-arrow-right"></i> Payroll Entries</a></li>
                        <li class="${params.controller=='payPeriod'?'active':''}"><a href="${createLink(controller:'payPeriod',action:'index')}" ><i class="icon-arrow-right"></i> Payroll Period</a></li>
                    </ul>
                </li>
                <li class="${params.controller == 'pagIbigContribution' || params.controller == 'philHealthContribution' || params.controller == 'sssContribution' || params.controller == 'withholdingTaxStatus' || params.controller == 'withholdingTaxPeriod' || params.controller == 'withholdingTaxBracket' || params.controller == 'withholdingTaxExemption'?'active':''} dropdown"><a class="dropdown-toggle" data-toggle="collapse" data-target="#deduc-dropdown" href="#"><i class="icon-list"></i> Deductions</a>
                    <ul class="${params.controller == 'pagIbigContribution' || params.controller == 'philHealthContribution' || params.controller == 'sssContribution' || params.controller == 'withholdingTaxStatus' || params.controller == 'withholdingTaxPeriod' || params.controller == 'withholdingTaxBracket' || params.controller == 'withholdingTaxExemption'?'':'collapse'} no-list-style custom-nav" id="deduc-dropdown">
                        <li class="${params.controller =='sssContribution'?'active':''}"><a href="${createLink(controller:'sssContribution',action:'index')}" ><i class="icon-arrow-right"></i> SSS</a></li>
                        <li class="${params.controller == 'pagIbigContribution' ?'active':''}"><a href="${createLink(controller:'pagIbigContribution',action:'index')}" ><i class="icon-arrow-right"></i> Pag-Ibig</a></li>
                        <li class="${params.controller == 'philHealthContribution'?'active':''}"><a href="${createLink(controller:'philHealthContribution',action:'index')}" ><i class="icon-arrow-right"></i> PhilHealth</a></li>
                        <li class="${params.controller == 'withholdingTaxPeriod'?'active':''}"><a href="${createLink(controller:'withholdingTaxPeriod',action:'table', id:com.ripple.deductions.WithholdingTaxPeriod?.list()?com.ripple.deductions.WithholdingTaxPeriod?.list()?.first()?.id:'')}" ><i class="icon-arrow-right"></i> WithHolding Tax</a></li>
                    </ul>
                </li>
                <li class="${params.controller?.contains('loan')?'active':''} dropdown"><a class="dropdown-toggle" data-toggle="collapse" data-target="#loan-dropdown" href="javascript:"><i class="icon-list"></i> Loans</a>
                    <ul class="${params.controller?.contains('loan')?'':'collapse'} no-list-style custom-nav" id="loan-dropdown">
                        <li class="${params.controller=='loan'?'active':''}"><a href="${createLink(controller:'loan',action:'index')}" ><i class="icon-arrow-right"></i> Employee Loans</a></li>
                        <li class="${params.controller=='loanType'?'active':''}"><a href="${createLink(controller:'loanType',action:'index')}" ><i class="icon-arrow-right"></i> Loan Types</a></li>
                    </ul>
                </li>
                %{--<li class="nav-header">Inventory Management</li>--}%
                <li class="nav-header">Asset Management</li>
                <li class="${params.controller=='equipment'?'active':''}"><a href="${createLink(controller:'equipment',action:'index')}"><i class="icon-shopping-cart"></i> Equipment</a></li>
                <li class="${params.controller=='equipmentType'?'active':''}"><a href="${createLink(controller:'equipmentType',action:'index')}"><i class="icon-bookmark"></i> Equipment Type</a></li>
                <li class="nav-header">Project Management</li>
                <li class="${params.controller?.contains('assignment')?'active':''}"><a href="${createLink(controller:'assignment',action:'index')}"><i class="icon-cog"></i> Projects</a></li>
                <li class="${params.controller?.contains('expenseType')?'active':''}"><a href="${createLink(controller:'expenseType',action:'index')}"><i class="icon-barcode"></i> Expense Type</a></li>
                %{--<li class="${params.controller?.contains('assignment')?'active':''} dropdown"><a class="dropdown-toggle" data-toggle="collapse" data-target="#assignment-dropdown" href="#"><i class="icon-th-large"></i> Projects</a>--}%
                    %{--<ul class="${params.controller?.contains('assignment')?'':'collapse'} no-list-style custom-nav" id="assignment-dropdown">--}%
                        %{--<li class="${params.controller=='assignment'?'active':''}"><a href="${createLink(controller:'assignment',action:'index')}" ><i class="icon-arrow-right"></i> Project</a></li>--}%
                        %{--<li class="${params.controller=='assignmentHistory'?'active':''}"><a href="${createLink(controller:'assignmentHistory',action:'index')}" ><i class="icon-arrow-right"></i> History</a></li>--}%
                        %{--<li class="${params.controller=='assignmentLocation'?'active':''}"><a href="${createLink(controller:'assignmentLocation',action:'index')}" ><i class="icon-arrow-right"></i> Location</a></li>--}%
                    %{--</ul>--}%
                %{--</li>--}%
                <g:if test="${grails.util.GrailsUtil.getEnvironment()!='production'}">
                <li class="nav-header">Inventory & Purchasing</li>
                <li class="${params.controller=='procurement'?'active':''}"><a href="${createLink(controller:'procurement',action:'index')}"><i class="icon-briefcase"></i> Procurement Slip</a></li>
                <li class="${params.controller=='materialIssuance'?'active':''}"><a href="${createLink(controller:'materialIssuance',action:'index')}"><i class="icon-briefcase"></i> MIS</a></li>
                <li class="${params.controller=='purchaseOrder'?'active':''}"><a href="${createLink(controller:'purchaseOrder',action:'index')}"><i class="icon-briefcase"></i> Purchase Order</a></li>
                <li class="${params.controller=='receivingReport'?'active':''}"><a href="${createLink(controller:'receivingReport',action:'index')}"><i class="icon-briefcase"></i> Receiving Report</a></li>
                </g:if>
                <li class="nav-header">Report</li>
                <li class="${params.controller=='report'?'active':''}"><a href="${createLink(controller:'report',action:'view')}"><i class="icon-book"></i> Generate Report</a></li>
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                <li class="nav-header">User Management</li>
                <li class="${params.controller=='appUser'?'active':''}"><a href="${createLink(controller:'appUser',action:'index')}"><i class="icon-user"></i> Users</a></li>
                <li class="${params.controller=='appRole'?'active':''}"><a href="${createLink(controller:'appRole',action:'index')}"><i class="icon-user"></i> Roles</a></li>
                <li class="${params.controller=='appUserAppRole'?'active':''}"><a href="${createLink(controller:'appUserAppRole',action:'assignRoles')}"><i class="icon-user"></i> Assign Roles</a></li>
                <li class="${params.controller=='requestmap'?'active':''}"><a href="${createLink(controller:'requestmap',action:'index')}"><i class="icon-user"></i> Requestmaps</a></li>
                </sec:ifAnyGranted>
                <g:if test="${grails.util.GrailsUtil.getEnvironment()!='production'}">
                <li class="nav-header">Maintenance</li>
                <li class="${params.controller == 'item' || params.controller == 'itemClass' || params.controller == 'itemSubclass'?'active':''} dropdown"><a class="dropdown-toggle" data-toggle="collapse" data-target="#setupItem-dropdown" href="javascript:"><i class="icon-list"></i> Inventory Items</a>
                    <ul class="${params.controller == 'item' || params.controller == 'itemClass' || params.controller == 'itemSubclass'?'':'collapse'} no-list-style custom-nav" id="setupItem-dropdown">
                        <li class="${params.controller =='item'?'active':''}"><a href="${createLink(controller:'item',action:'index')}" ><i class="icon-arrow-right"></i> Inventory Master</a></li>
                        <li class="${params.controller =='itemClass'?'active':''}"><a href="${createLink(controller:'itemClass',action:'index')}" ><i class="icon-arrow-right"></i> Item Class</a></li>
                        <li class="${params.controller =='itemSubclass'?'active':''}"><a href="${createLink(controller:'itemSubclass',action:'index')}" ><i class="icon-arrow-right"></i> Item Subclass</a></li>
                    </ul>
                </li>
                <li class="${params.controller=='warehouse'?'active':''}"><a href="${createLink(controller:'warehouse',action:'index')}"><i class="icon-book"></i> Warehouse</a></li>
                %{--<li class="${params.controller == 'supplier'?'active':''} dropdown"><a class="dropdown-toggle" data-toggle="collapse" data-target="#setupInv-dropdown" href="javascript:"><i class="icon-list"></i> Setup</a>--}%
                    %{--<ul class="${params.controller == 'supplier'?'':'collapse'} no-list-style custom-nav" id="setupInv-dropdown">--}%
                        %{--<li class="${params.controller =='supplier'?'active':''}"><a href="${createLink(controller:'supplier',action:'index')}" ><i class="icon-arrow-right"></i> Supplier</a></li>--}%
                    %{--</ul>--}%
                %{--</li>--}%
                <li class="${params.controller =='supplier'?'active':''}"><a href="${createLink(controller:'supplier',action:'index')}" ><i class="icon-book"></i> Supplier</a></li>
                <li class="${params.controller=='materialIssuanceClass'?'active':''}"><a href="${createLink(controller:'materialIssuanceClass',action:'index')}"><i class="icon-book"></i> MIS Class</a></li>
                </g:if>
                <li class="nav-header">Settings</li>
                %{--<li><a class="cookie-delete" href="#"><i class="icon-wrench"></i> Delete Cookies</a></li>--}%
                <li><a class="sidenav-style-1" href="#"><i class="icon-align-left"></i> Side Menu Style 1</a></li>
                <li><a class="sidenav-style-2" href="#"><i class="icon-align-right"></i> Side Menu Style 2</a></li>
                <li><g:link controller="logout" action="index"><i class="icon-off"></i> Logout</g:link></li>
            </ul>
        </div>
    </div><!--/.well -->
</div><!--/span-->
