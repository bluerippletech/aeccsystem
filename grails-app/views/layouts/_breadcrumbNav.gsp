<!-- Bread Crumb Navigation -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="${createLink(uri: '/')}">Home</a>
        </li>
        <g:if test="${params?.controller!='dashboard'}">
            <li><span class="divider">/</span></li>
            <li style="text-transform: capitalize">
                <a href="${createLink(controller: params?.controller ,action:'index')}">${message(code: "${params?.controller}.label", default: "${params?.controller?:'Error'}")}</a>
            </li>
            <li><span class="divider">/</span></li>
            <li class="active" style="text-transform: capitalize">${params?.action?:''}</li>
        </g:if>
    </ul>
</div>
