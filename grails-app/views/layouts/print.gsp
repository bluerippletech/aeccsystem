<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><g:layoutTitle default="Grails"/></title>
    <r:require modules="application,jquery"/>
    <g:layoutHead/>
    <r:layoutResources/>
</head>
<body style="padding: 0;" onload="window.print()">
<g:layoutBody/>
<g:render template="/layouts/footer"/>
<r:layoutResources/>
</body>
</html>