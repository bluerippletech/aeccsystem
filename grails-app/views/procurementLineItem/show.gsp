
<%@ page import="com.ripple.procurement.ProcurementLineItem" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'procurementLineItem.label', default: 'ProcurementLineItem')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-procurementLineItem" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="createdDate-label" class="control-label"><g:message
                                                    code="procurementLineItem.createdDate.label"
                                                    default="Created Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="createdDate-label"><g:formatDate
                                                        date="${procurementLineItemInstance?.createdDate}" format="MMMMM dd, yyyy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="editedBy-label" class="control-label"><g:message
                                                    code="procurementLineItem.editedBy.label"
                                                    default="Edited By"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="editedBy-label"><g:fieldValue
                                                        bean="${procurementLineItemInstance}" field="editedBy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="item-label" class="control-label"><g:message
                                                    code="procurementLineItem.item.label"
                                                    default="Item"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="item-label"><g:link
                                                        controller="item" action="show"
                                                        id="${procurementLineItemInstance?.item?.id}">${procurementLineItemInstance?.item?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="purpose-label" class="control-label"><g:message
                                                    code="procurementLineItem.purpose.label"
                                                    default="Purpose"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="purpose-label"><g:link
                                                        controller="materialIssuanceClass" action="show"
                                                        id="${procurementLineItemInstance?.purpose?.id}">${procurementLineItemInstance?.purpose?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="isDeleted-label" class="control-label"><g:message
                                                    code="procurementLineItem.isDeleted.label"
                                                    default="Is Deleted"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="isDeleted-label"><g:fieldValue
                                                        bean="${procurementLineItemInstance}" field="isDeleted"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="editedDate-label" class="control-label"><g:message
                                                    code="procurementLineItem.editedDate.label"
                                                    default="Edited Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="editedDate-label"><g:formatDate
                                                        date="${procurementLineItemInstance?.editedDate}" format="MMMMM dd, yyyy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="createdBy-label" class="control-label"><g:message
                                                    code="procurementLineItem.createdBy.label"
                                                    default="Created By"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="createdBy-label"><g:fieldValue
                                                        bean="${procurementLineItemInstance}" field="createdBy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="procurement-label" class="control-label"><g:message
                                                    code="procurementLineItem.procurement.label"
                                                    default="Procurement"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="procurement-label"><g:link
                                                        controller="procurement" action="show"
                                                        id="${procurementLineItemInstance?.procurement?.id}">${procurementLineItemInstance?.procurement?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="quantity-label" class="control-label"><g:message
                                                    code="procurementLineItem.quantity.label"
                                                    default="Quantity"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="quantity-label"><g:fieldValue
                                                        bean="${procurementLineItemInstance}" field="quantity"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${procurementLineItemInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${procurementLineItemInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
