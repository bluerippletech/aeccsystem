<%@ page import="com.ripple.procurement.ProcurementLineItem" %>



<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="procurementLineItem.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${procurementLineItemInstance.constraints.isDeleted.inList}" value="${procurementLineItemInstance?.isDeleted}" valueMessagePrefix="procurementLineItem.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="procurementLineItem.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${procurementLineItemInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="procurementLineItem.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${procurementLineItemInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="procurementLineItem.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${procurementLineItemInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="procurementLineItem.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${procurementLineItemInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'item', 'error')} required">
	<label for="item">
		<g:message code="procurementLineItem.item.label" default="Item" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="item" name="item.id" from="${com.ripple.master.item.Item.list()}" optionKey="id" required="" value="${procurementLineItemInstance?.item?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'procurement', 'error')} required">
	<label for="procurement">
		<g:message code="procurementLineItem.procurement.label" default="Procurement" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="procurement" name="procurement.id" from="${com.ripple.procurement.Procurement.list()}" optionKey="id" required="" value="${procurementLineItemInstance?.procurement?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'purpose', 'error')} required">
	<label for="purpose">
		<g:message code="procurementLineItem.purpose.label" default="Purpose" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="purpose" name="purpose.id" from="${com.ripple.procurement.MaterialIssuanceClass.list()}" optionKey="id" required="" value="${procurementLineItemInstance?.purpose?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: procurementLineItemInstance, field: 'quantity', 'error')} required">
	<label for="quantity">
		<g:message code="procurementLineItem.quantity.label" default="Quantity" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="quantity" required="" value="${fieldValue(bean: procurementLineItemInstance, field: 'quantity')}"/>
</div>

