<%@ page import="com.ripple.equipment.EquipmentAssignment" %>



<div class="fieldcontain ${hasErrors(bean: equipmentAssignmentInstance, field: 'operator', 'error')} ">
	<label for="operator">
		<g:message code="equipmentAssignment.operator.label" default="Operator" />
		
	</label>
	<g:select id="operator" name="operator.id" from="${com.ripple.master.Employee.list()}" optionKey="id" value="${equipmentAssignmentInstance?.operator?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentAssignmentInstance, field: 'toDate', 'error')} ">
	<label for="toDate">
		<g:message code="equipmentAssignment.toDate.label" default="To Date" />
		
	</label>
	<joda:datePicker name="toDate" value="${equipmentAssignmentInstance?.toDate}" default="none" noSelection="['': '']"></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentAssignmentInstance, field: 'project', 'error')} ">
	<label for="project">
		<g:message code="equipmentAssignment.project.label" default="Project" />
		
	</label>
	<g:select id="project" name="project.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" value="${equipmentAssignmentInstance?.project?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentAssignmentInstance, field: 'projectLocation', 'error')} ">
	<label for="projectLocation">
		<g:message code="equipmentAssignment.projectLocation.label" default="Project Location" />
		
	</label>
	<g:select id="projectLocation" name="projectLocation.id" from="${com.ripple.master.AssignmentLocation.list()}" optionKey="id" value="${equipmentAssignmentInstance?.projectLocation?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentAssignmentInstance, field: 'comments', 'error')} ">
	<label for="comments">
		<g:message code="equipmentAssignment.comments.label" default="Comments" />
		
	</label>
	<g:textField name="comments" value="${equipmentAssignmentInstance?.comments}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentAssignmentInstance, field: 'equipment', 'error')} required">
	<label for="equipment">
		<g:message code="equipmentAssignment.equipment.label" default="Equipment" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="equipment" name="equipment.id" from="${com.ripple.equipment.Equipment.list()}" optionKey="id" required="" value="${equipmentAssignmentInstance?.equipment?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: equipmentAssignmentInstance, field: 'fromDate', 'error')} required">
	<label for="fromDate">
		<g:message code="equipmentAssignment.fromDate.label" default="From Date" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="fromDate" value="${equipmentAssignmentInstance?.fromDate}" ></joda:datePicker>
</div>

