
<%@ page import="com.ripple.deductions.WithholdingTaxPeriod" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-withholdingTaxPeriod" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="brackets-label" class="control-label"><g:message
                                                    code="withholdingTaxPeriod.brackets.label"
                                                    default="Brackets"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${withholdingTaxPeriodInstance.brackets}" var="b">
                                                    <span class="property-value" aria-labelledby="brackets-label"><g:link
                                                            controller="withholdingTaxBracket" action="show"
                                                            id="${b.id}">${b?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="name-label" class="control-label"><g:message
                                                    code="withholdingTaxPeriod.name.label"
                                                    default="Name"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="name-label"><g:fieldValue
                                                        bean="${withholdingTaxPeriodInstance}" field="name"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="statuses-label" class="control-label"><g:message
                                                    code="withholdingTaxPeriod.statuses.label"
                                                    default="Statuses"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${withholdingTaxPeriodInstance.statuses}" var="s">
                                                    <span class="property-value" aria-labelledby="statuses-label"><g:link
                                                            controller="withholdingTaxStatus" action="show"
                                                            id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${withholdingTaxPeriodInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${withholdingTaxPeriodInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
