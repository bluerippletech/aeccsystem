
<%@ page import="com.ripple.deductions.WithholdingTaxExemption; com.ripple.deductions.WithholdingTaxPeriod" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'withholdingTaxPeriod.label', default: 'WithholdingTaxPeriod')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <style type="text/css">
    .mini{
        padding: 3px !important;
    }
    </style>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="list-withholdingTaxPeriod" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <g:each in="${com.ripple.deductions.WithholdingTaxPeriod.list()}" var="period" status="i">
                        <a class="btn" href="${createLink(controller: 'withholdingTaxPeriod', action: 'table', id: period.id)}">${period.toString()}</a>
                    </g:each>
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>
                            <th><g:link action="edit" id="${withholdingTaxPeriodInstance?.id}">${fieldValue(bean:withholdingTaxPeriodInstance, field:'name')}</g:link></th>
                            <th></th>
                            <g:each in="${withholdingTaxPeriodInstance?.brackets}" status="j" var="withholdingTaxBracketInstance">
                                <th><g:link controller="withholdingTaxBracket" action="edit" id="${withholdingTaxBracketInstance?.id}">${withholdingTaxBracketInstance?.toString()}</g:link></th>
                            </g:each>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Exemption</th>
                            <g:each in="${withholdingTaxPeriodInstance?.brackets}" status="j" var="withholdingTaxBracketInstance">
                                <th>${fieldValue(bean:withholdingTaxBracketInstance, field:'baseTax')}</th>
                            </g:each>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <th>('000P)</th>
                            <g:each in="${withholdingTaxPeriodInstance?.brackets}" status="j" var="withholdingTaxBracketInstance">
                                <th>+${fieldValue(bean:withholdingTaxBracketInstance, field:'percentOver')}% over</th>
                            </g:each>
                        </tr>
                        </thead>
                        <tbody>
                            <g:each in="${withholdingTaxPeriodInstance?.statuses}" status="j" var="withholdingTaxStatusInstance">
                                <tr class="${(j % 2) == 0 ? 'odd' : 'even'}">
                                    <td><g:link controller="withholdingTaxStatus" action="edit" id="${withholdingTaxStatusInstance?.id}">${withholdingTaxStatusInstance?.toString()}</g:link></td>
                                    <td><g:link controller="withholdingTaxStatus" action="edit" id="${withholdingTaxStatusInstance?.id}">${formatNumber(number:deductionsService?.getWithholdingTaxStatusExemption(withholdingTaxStatusInstance), format:"Php ###,##0.00")}</g:link></td>
                                    <g:each in="${withholdingTaxPeriodInstance.brackets}" status="k" var="withholdingTaxBracketInstance">
                                        <g:set var="withholdingTaxExemptionInstance" value="${WithholdingTaxExemption?.findByStatusAndBracket(withholdingTaxStatusInstance, withholdingTaxBracketInstance)}"></g:set>
                                        <td><g:link controller="withholdingTaxExemption" action="edit" id="${withholdingTaxExemptionInstance?.id}">${formatNumber(number:withholdingTaxExemptionInstance.value, format:"Php ###,##0.00")}</g:link></td>
                                    </g:each>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
