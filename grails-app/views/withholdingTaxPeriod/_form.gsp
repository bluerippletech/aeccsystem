<%@ page import="com.ripple.deductions.WithholdingTaxPeriod" %>



<div class="fieldcontain ${hasErrors(bean: withholdingTaxPeriodInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="withholdingTaxPeriod.name.label" default="Name" />
		
	</label>
	<g:select name="name" from="${withholdingTaxPeriodInstance.constraints.name.inList}" value="${withholdingTaxPeriodInstance?.name}" valueMessagePrefix="withholdingTaxPeriod.name" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxPeriodInstance, field: 'brackets', 'error')} ">
	<label for="brackets">
		<g:message code="withholdingTaxPeriod.brackets.label" default="Brackets" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${withholdingTaxPeriodInstance?.brackets?}" var="b">
    <li><g:link controller="withholdingTaxBracket" action="show" id="${b.id}">${b?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="withholdingTaxBracket" action="create" params="['withholdingTaxPeriod.id': withholdingTaxPeriodInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'withholdingTaxBracket.label', default: 'WithholdingTaxBracket')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: withholdingTaxPeriodInstance, field: 'statuses', 'error')} ">
	<label for="statuses">
		<g:message code="withholdingTaxPeriod.statuses.label" default="Statuses" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${withholdingTaxPeriodInstance?.statuses?}" var="s">
    <li><g:link controller="withholdingTaxStatus" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="withholdingTaxStatus" action="create" params="['withholdingTaxPeriod.id': withholdingTaxPeriodInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'withholdingTaxStatus.label', default: 'WithholdingTaxStatus')])}</g:link>
</li>
</ul>

</div>

