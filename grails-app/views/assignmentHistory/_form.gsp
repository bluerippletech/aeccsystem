<%@ page import="com.ripple.master.AssignmentHistory" %>



<div class="fieldcontain ${hasErrors(bean: assignmentHistoryInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="assignmentHistory.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${com.ripple.master.Employee.list()}" optionKey="id" required="" value="${assignmentHistoryInstance?.employee?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: assignmentHistoryInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="assignmentHistory.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project" name="project.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" required="" value="${assignmentHistoryInstance?.project?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: assignmentHistoryInstance, field: 'startDate', 'error')} required">
	<label for="startDate">
		<g:message code="assignmentHistory.startDate.label" default="Start Date" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="startDate" value="${assignmentHistoryInstance?.startDate}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: assignmentHistoryInstance, field: 'workLocation', 'error')} required">
	<label for="workLocation">
		<g:message code="assignmentHistory.workLocation.label" default="Work Location" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="workLocation" name="workLocation.id" from="${com.ripple.master.AssignmentLocation.list()}" optionKey="id" required="" value="${assignmentHistoryInstance?.workLocation?.id}" class="many-to-one"/>
</div>

