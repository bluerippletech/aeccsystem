<%@ page import="com.ripple.stock.CycleCount" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'cycleCount.label', default: 'CycleCount')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-cycleCount" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="cycleCountNumber-label" class="control-label"><g:message
                                                code="cycleCount.cycleCountNumber.label"
                                                default="Cycle Count Number"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="cycleCountNumber-label"><g:fieldValue
                                                    bean="${cycleCountInstance}" field="cycleCountNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="includeInactiveItems-label" class="control-label"><g:message
                                                code="cycleCount.includeInactiveItems.label"
                                                default="Include Inactive Items"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="includeInactiveItems-label"><g:fieldValue
                                                    bean="${cycleCountInstance}" field="includeInactiveItems"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="status-label" class="control-label"><g:message
                                                code="cycleCount.status.label"
                                                default="Status"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                                                    bean="${cycleCountInstance}" field="status"/></span>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="warehouse-label" class="control-label"><g:message
                                                code="cycleCount.warehouse.label"
                                                default="Warehouse"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="warehouse-label"><g:link
                                                    controller="warehouse" action="show"
                                                    id="${cycleCountInstance?.warehouse?.id}">${cycleCountInstance?.warehouse?.encodeAsHTML()}</g:link></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="itemCode-label" class="control-label"><g:message
                                                code="cycleCount.itemCode.label"
                                                default="Item Code"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="itemCode-label">
                                                <g:if test="${cycleCountInstance?.item}">
                                                    <g:link controller="item" action="show"
                                                            id="${cycleCountInstance?.item?.id}">${cycleCountInstance?.item?.itemCode?.encodeAsHTML()}</g:link>
                                                </g:if>
                                                <g:else>
                                                    None
                                                </g:else>
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="itemClass-label" class="control-label"><g:message
                                                code="cycleCount.itemClass.label"
                                                default="Item Class"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="warehouse-label">
                                                <g:if test="${cycleCountInstance?.itemClass}">
                                                    <g:link controller="itemClass" action="show"
                                                            id="${cycleCountInstance?.itemClass?.id}">${cycleCountInstance?.itemClass?.encodeAsHTML()}</g:link>
                                                </g:if>
                                                <g:else>
                                                    None
                                                </g:else>
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="itemSubclass-label" class="control-label"><g:message
                                                code="cycleCount.itemSubclass.label"
                                                default="Item Subclass"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="itemSubclass-label">
                                                <g:if test="${cycleCountInstance?.itemSubclass}">
                                                    <g:link controller="itemSubclass" action="show"
                                                            id="${cycleCountInstance?.itemSubclass?.id}">${cycleCountInstance?.itemSubclass?.encodeAsHTML()}</g:link>
                                                </g:if>
                                                <g:else>
                                                    None
                                                </g:else>
                                            </span>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </fieldset>

                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${cycleCountInstance?.id}"/>
                            <g:actionSubmit class="btn btn-primary" action="edit"
                                            value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                            <g:actionSubmit class="delete btn-danger" action="delete"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="list-cycleCountItems" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>

                            <th rowspan="2"><g:message code="cycleCountItems.itemCode.label" default="Item Code" /></th>

                            <th rowspan="2"><g:message code="cycleCountItems.itemName.label" default="Item Name" /></th>

                            <th colspan="3" rowspan="1"><g:message code="cycleCountItems.onHand.label" default="On-Hand" /></th>

                            <th colspan="3" rowspan="1"><g:message code="cycleCountItems.actualCount.label" default="Actual Count" /></th>

                        </tr>
                        <tr>
                            <th><g:message code="cycleCountItems.onHandBaseLevel.label" default="Base Level" /></th>

                            <th><g:message code="cycleCountItems.onHandSecondLevel.label" default="Second Level" /></th>

                            <th><g:message code="cycleCountItems.onHandThirdLevel.label" default="Third Level" /></th>

                            <th><g:message code="cycleCountItems.actualBaseLevel.label" default="Base Level" /></th>

                            <th><g:message code="cycleCountItems.actualSecondLevel.label" default="Second Level" /></th>

                            <th><g:message code="cycleCountItems.actualThirdLevel.label" default="Third Level" /></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            $('#datatable').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'cycleCountItems', action:'listJSON')}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[

                                    { "sName": "isDeleted", "aTargets": [ 0 ] },

                                    { "sName": "createdDate", "aTargets": [ 1 ] },

                                    { "sName": "editedDate", "aTargets": [ 2 ] },

                                    { "sName": "editedBy", "aTargets": [ 3 ] },

                                    { "sName": "createdBy", "aTargets": [ 4 ] },

                                    { "sName": "item", "aTargets": [ 5 ] }


                                ]
                            } );

                        });
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
