<%@ page import="com.ripple.stock.CycleCount" %>



<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="cycleCount.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${cycleCountInstance.constraints.isDeleted.inList}" value="${cycleCountInstance?.isDeleted}" valueMessagePrefix="cycleCount.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="cycleCount.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${cycleCountInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="cycleCount.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${cycleCountInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="cycleCount.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${cycleCountInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="cycleCount.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${cycleCountInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'cycleCountNumber', 'error')} required">
	<label for="cycleCountNumber">
		<g:message code="cycleCount.cycleCountNumber.label" default="Cycle Count Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="cycleCountNumber" required="" value="${cycleCountInstance?.cycleCountNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'warehouse', 'error')} required">
	<label for="warehouse">
		<g:message code="cycleCount.warehouse.label" default="Warehouse" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="warehouse" name="warehouse.id" from="${com.ripple.master.Warehouse.list()}" optionKey="id" required="" value="${cycleCountInstance?.warehouse?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'status', 'error')} required">
	<label for="status">
		<g:message code="cycleCount.status.label" default="Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="status" from="${cycleCountInstance.constraints.status.inList}" required="" value="${cycleCountInstance?.status}" valueMessagePrefix="cycleCount.status"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cycleCountInstance, field: 'items', 'error')} ">
	<label for="items">
		<g:message code="cycleCount.items.label" default="Items" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${cycleCountInstance?.items?}" var="i">
    <li><g:link controller="cycleCountItems" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="cycleCountItems" action="create" params="['cycleCount.id': cycleCountInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'cycleCountItems.label', default: 'CycleCountItems')])}</g:link>
</li>
</ul>

</div>

