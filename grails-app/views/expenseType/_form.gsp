<%@ page import="com.ripple.project.ExpenseType" %>



<div class="fieldcontain ${hasErrors(bean: expenseTypeInstance, field: 'code', 'error')} required">
	<label for="code">
		<g:message code="expenseType.code.label" default="Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="code" required="" value="${expenseTypeInstance?.code}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseTypeInstance, field: 'expenseCategory', 'error')} ">
	<label for="expenseCategory">
		<g:message code="expenseType.expenseCategory.label" default="Expense Category" />
		
	</label>
	<g:select name="expenseCategory" from="${expenseTypeInstance.constraints.expenseCategory.inList}" value="${expenseTypeInstance?.expenseCategory}" valueMessagePrefix="expenseType.expenseCategory" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseTypeInstance, field: 'expenseType', 'error')} required">
	<label for="expenseType">
		<g:message code="expenseType.expenseType.label" default="Expense Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="expenseType" required="" value="${expenseTypeInstance?.expenseType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: expenseTypeInstance, field: 'units', 'error')} ">
	<label for="units">
		<g:message code="expenseType.units.label" default="Units" />
		
	</label>
	<g:textField name="units" value="${expenseTypeInstance?.units}"/>
</div>

