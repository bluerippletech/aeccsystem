<%@ page import="com.ripple.project.Collection" %>



<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'amount', 'error')} required">
	<label for="amount">
		<g:message code="collection.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="amount" required="" value="${fieldValue(bean: collectionInstance, field: 'amount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'collectionDate', 'error')} required">
	<label for="collectionDate">
		<g:message code="collection.collectionDate.label" default="Collection Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="collectionDate" precision="day"  value="${collectionInstance?.collectionDate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'fwt', 'error')} required">
	<label for="fwt">
		<g:message code="collection.fwt.label" default="Fwt" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="fwt" required="" value="${fieldValue(bean: collectionInstance, field: 'fwt')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="collection.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project" name="project.id" from="${com.ripple.master.Assignment.list()}" optionKey="id" required="" value="${collectionInstance?.project?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'recoupment', 'error')} required">
	<label for="recoupment">
		<g:message code="collection.recoupment.label" default="Recoupment" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="recoupment" required="" value="${fieldValue(bean: collectionInstance, field: 'recoupment')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'remarks', 'error')} ">
	<label for="remarks">
		<g:message code="collection.remarks.label" default="Remarks" />
		
	</label>
	<g:textField name="remarks" value="${collectionInstance?.remarks}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'retention', 'error')} required">
	<label for="retention">
		<g:message code="collection.retention.label" default="Retention" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="retention" required="" value="${fieldValue(bean: collectionInstance, field: 'retention')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'wt', 'error')} required">
	<label for="wt">
		<g:message code="collection.wt.label" default="Wt" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="wt" required="" value="${fieldValue(bean: collectionInstance, field: 'wt')}"/>
</div>

