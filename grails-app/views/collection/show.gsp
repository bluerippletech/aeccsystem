
<%@ page import="com.ripple.project.Collection" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'collection.label', default: 'Collection')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-collection" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="collectionDate-label" class="control-label"><g:message
                                                    code="collection.collectionDate.label"
                                                    default="Collection Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="collectionDate-label"><g:formatDate
                                                        date="${collectionInstance?.collectionDate}" format="MMMMM dd, yyyy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="project-label" class="control-label"><g:message
                                                    code="collection.project.label"
                                                    default="Project"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="project-label"><g:link
                                                        controller="assignment" action="show"
                                                        id="${collectionInstance?.project?.id}">${collectionInstance?.project?.encodeAsHTML()}</g:link></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="remarks-label" class="control-label"><g:message
                                                    code="collection.remarks.label"
                                                    default="Remarks"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="remarks-label"><g:fieldValue
                                                        bean="${collectionInstance}" field="remarks"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="wt-label" class="control-label"><g:message
                                                    code="collection.wt.label"
                                                    default="Wt"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="wt-label"><g:fieldValue
                                                        bean="${collectionInstance}" field="wt"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="amount-label" class="control-label"><g:message
                                                    code="collection.amount.label"
                                                    default="Amount"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="amount-label"><g:fieldValue
                                                        bean="${collectionInstance}" field="amount"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="fwt-label" class="control-label"><g:message
                                                    code="collection.fwt.label"
                                                    default="Fwt"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="fwt-label"><g:fieldValue
                                                        bean="${collectionInstance}" field="fwt"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="recoupment-label" class="control-label"><g:message
                                                    code="collection.recoupment.label"
                                                    default="Recoupment"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="recoupment-label"><g:fieldValue
                                                        bean="${collectionInstance}" field="recoupment"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="retention-label" class="control-label"><g:message
                                                    code="collection.retention.label"
                                                    default="Retention"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="retention-label"><g:fieldValue
                                                        bean="${collectionInstance}" field="retention"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${collectionInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${collectionInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
