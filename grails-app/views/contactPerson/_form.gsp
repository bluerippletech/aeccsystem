<%@ page import="com.ripple.master.sociology.ContactPerson" %>



<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="contactPerson.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${contactPersonInstance.constraints.isDeleted.inList}" value="${contactPersonInstance?.isDeleted}" valueMessagePrefix="contactPerson.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="contactPerson.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${contactPersonInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="contactPerson.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${contactPersonInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="contactPerson.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${contactPersonInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="contactPerson.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${contactPersonInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'contactName', 'error')} ">
	<label for="contactName">
		<g:message code="contactPerson.contactName.label" default="Contact Name" />
		
	</label>
	<g:textField name="contactName" value="${contactPersonInstance?.contactName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'position', 'error')} ">
	<label for="position">
		<g:message code="contactPerson.position.label" default="Position" />
		
	</label>
	<g:textField name="position" value="${contactPersonInstance?.position}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'department', 'error')} ">
	<label for="department">
		<g:message code="contactPerson.department.label" default="Department" />
		
	</label>
	<g:textField name="department" value="${contactPersonInstance?.department}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'directLine', 'error')} ">
	<label for="directLine">
		<g:message code="contactPerson.directLine.label" default="Direct Line" />
		
	</label>
	<g:textField name="directLine" value="${contactPersonInstance?.directLine}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'otherPhone', 'error')} ">
	<label for="otherPhone">
		<g:message code="contactPerson.otherPhone.label" default="Other Phone" />
		
	</label>
	<g:textField name="otherPhone" value="${contactPersonInstance?.otherPhone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'mobileNo', 'error')} ">
	<label for="mobileNo">
		<g:message code="contactPerson.mobileNo.label" default="Mobile No" />
		
	</label>
	<g:textField name="mobileNo" value="${contactPersonInstance?.mobileNo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'emailAddress', 'error')} ">
	<label for="emailAddress">
		<g:message code="contactPerson.emailAddress.label" default="Email Address" />
		
	</label>
	<g:field type="email" name="emailAddress" value="${contactPersonInstance?.emailAddress}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactPersonInstance, field: 'faxNo', 'error')} ">
	<label for="faxNo">
		<g:message code="contactPerson.faxNo.label" default="Fax No" />
		
	</label>
	<g:textField name="faxNo" value="${contactPersonInstance?.faxNo}"/>
</div>

