
<%@ page import="com.ripple.master.sociology.ContactPerson" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'contactPerson.label', default: 'ContactPerson')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-contactPerson" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="createdDate-label" class="control-label"><g:message
                                                    code="contactPerson.createdDate.label"
                                                    default="Created Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="createdDate-label"><g:formatDate
                                                        date="${contactPersonInstance?.createdDate}" format="MMMMM dd, yyyy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="editedBy-label" class="control-label"><g:message
                                                    code="contactPerson.editedBy.label"
                                                    default="Edited By"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="editedBy-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="editedBy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="contactName-label" class="control-label"><g:message
                                                    code="contactPerson.contactName.label"
                                                    default="Contact Name"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="contactName-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="contactName"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="department-label" class="control-label"><g:message
                                                    code="contactPerson.department.label"
                                                    default="Department"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="department-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="department"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="otherPhone-label" class="control-label"><g:message
                                                    code="contactPerson.otherPhone.label"
                                                    default="Other Phone"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="otherPhone-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="otherPhone"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="emailAddress-label" class="control-label"><g:message
                                                    code="contactPerson.emailAddress.label"
                                                    default="Email Address"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="emailAddress-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="emailAddress"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6 column">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="isDeleted-label" class="control-label"><g:message
                                                    code="contactPerson.isDeleted.label"
                                                    default="Is Deleted"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="isDeleted-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="isDeleted"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="editedDate-label" class="control-label"><g:message
                                                    code="contactPerson.editedDate.label"
                                                    default="Edited Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="editedDate-label"><g:formatDate
                                                        date="${contactPersonInstance?.editedDate}" format="MMMMM dd, yyyy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="createdBy-label" class="control-label"><g:message
                                                    code="contactPerson.createdBy.label"
                                                    default="Created By"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="createdBy-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="createdBy"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="position-label" class="control-label"><g:message
                                                    code="contactPerson.position.label"
                                                    default="Position"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="position-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="position"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="directLine-label" class="control-label"><g:message
                                                    code="contactPerson.directLine.label"
                                                    default="Direct Line"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="directLine-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="directLine"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="mobileNo-label" class="control-label"><g:message
                                                    code="contactPerson.mobileNo.label"
                                                    default="Mobile No"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="mobileNo-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="mobileNo"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="faxNo-label" class="control-label"><g:message
                                                    code="contactPerson.faxNo.label"
                                                    default="Fax No"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="faxNo-label"><g:fieldValue
                                                        bean="${contactPersonInstance}" field="faxNo"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${contactPersonInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${contactPersonInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
