<g:select name="assignmentLocation.id"
          id="assignmentLocation"
          from="${projectLocations}"
          optionKey="id"
          optionValue="location"
          noSelection="['':'-Please select a Project Location-']"
          value="${assignmentHistoryInstance?.workLocation?.id}"/>
