
<%@ page import="com.ripple.entry.PayrollEntry" %>
<%@ page import="com.ripple.util.Formatter as FM" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'payrollEntry.label', default: 'PayrollEntry')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>

%{--<div class="form-horizontal">--}%
    %{--<div class="control-group">--}%
        %{--<h5 id="payPeriodLabel-label" class="control-label"><g:message--}%
                %{--code="payrollEntry.payPeriodLabel.label"--}%
                %{--default="Pay Period Label"/></h5>--}%
        %{--<div class="controls" style="margin-top:4px">--}%

            %{--<span class="property-value" aria-labelledby="payPeriodLabel-label"><g:fieldValue--}%
                    %{--bean="${payrollEntryInstance}" field="payPeriodLabel"/></span>--}%

        %{--</div>--}%
    %{--</div>--}%
%{--</div>--}%

<div class="form-horizontal">
    <div class="control-group">
        <h5 id="payPeriod-label" class="control-label"><g:message
                code="payrollEntry.payPeriod.label"
                default="Pay Period"/></h5>
        <div class="controls" style="margin-top:4px">

            <span class="property-value" aria-labelledby="payPeriod-label"><g:link
                    controller="payPeriod" action="show"
                    id="${payrollEntryInstance?.payPeriod?.id}">${payrollEntryInstance?.payPeriod?.encodeAsHTML()}</g:link></span>

        </div>
    </div>
</div>

<div class="row-fluid">
<div class="span6">
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="employee-label" class="control-label"><g:message
                    code="payrollEntry.employee.label"
                    default="Employee"/></h5>
            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="employee-label"><g:link
                        controller="employee" action="show"
                        id="${payrollEntryInstance?.employee?.id}">${payrollEntryInstance?.employee?.encodeAsHTML()}</g:link></span>

            </div>
        </div>
    </div>
</div>
<div class="span6">
    <div class="form-horizontal">
        <div class="control-group">
            <h5 id="netPay-label" class="control-label"><g:message
                    code="payrollEntry.netPay.label"
                    default="Net Pay"/></h5>
            <div class="controls" style="margin-top:4px">

                <span class="property-value" aria-labelledby="netPay-label">
                    ${FM?.formatCurrency(payrollEntryInstance?.netPay)}
                </span>

            </div>
        </div>
    </div>
</div>
</div>

<div class="row-fluid">
    <div class="span12" id="edit-payrollEntry" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.details.label" default="Working Hours Details"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6">

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="totalDaysWorked-label" class="control-label"><g:message
                                                    code="payrollEntry.totalDaysWorked.label"
                                                    default="Total Days Worked"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="totalDaysWorked-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="totalDaysWorked"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="totalRegularHours-label" class="control-label"><g:message
                                                    code="payrollEntry.totalRegularHours.label"
                                                    default="Total Hours for the Pay Period"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="totalRegularHours-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="totalRegularHours"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="lateHours-label" class="control-label"><g:message
                                                    code="payrollEntry.lateHours.label"
                                                    default="Total Hours Late"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="lateHours-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="lateHours"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="sundayHoursWorked-label" class="control-label"><g:message
                                                    code="payrollEntry.sundayHoursWorked.label"
                                                    default="Total Sunday Hours Worked"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="sundayHoursWorked-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="sundayHoursWorked"/></span>

                                            </div>
                                        </div>
                                    </div>


                            </div>
                            <div class="span6">

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="specialHolidayHours-label" class="control-label"><g:message
                                                    code="payrollEntry.specialHolidayHours.label"
                                                    default="Special Holiday Hours"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="specialHolidayHours-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="specialHolidayHours"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="regularHoursWorked-label" class="control-label"><g:message
                                                    code="payrollEntry.regularHoursWorked.label"
                                                    default="Total Regular Hours Worked"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="regularHoursWorked-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="regularHoursWorked"/></span>

                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="regularOvertimeHours-label" class="control-label"><g:message
                                                    code="payrollEntry.regularOvertimeHours.label"
                                                    default="Total Regular Overtime"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="regularOvertimeHours-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="regularOvertimeHours"/></span>

                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="legalHolidayHours-label" class="control-label"><g:message
                                                    code="payrollEntry.legalHolidayHours.label"
                                                    default="Legal Holiday Hours"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="legalHolidayHours-label"><g:fieldValue
                                                        bean="${payrollEntryInstance}" field="legalHolidayHours"/></span>

                                            </div>
                                        </div>
                                    </div>

                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row-fluid">
    <div class="span12" id="edit-payInformation" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="default.details.label" default="Pay Information"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="basicPay-label" class="control-label"><g:message
                                                code="payrollEntry.basicPay.label"
                                                default="Basic Pay"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="basicPay-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.basicPay)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="lateValue-label" class="control-label"><g:message
                                                code="payrollEntry.lateValue.label"
                                                default="Late Deduction"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="lateValue-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.lateValue)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="sundayPay-label" class="control-label"><g:message
                                                code="payrollEntry.sundayPay.label"
                                                default="Sunday Pay"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="sundayPay-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.sundayPay)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="regularHoursPay-label" class="control-label"><g:message
                                                code="payrollEntry.regularHoursPay.label"
                                                default="Regular Hours Pay"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="regularHoursPay-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.regularHoursPay)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="specialHolidayPay-label" class="control-label"><g:message
                                                code="payrollEntry.specialHolidayPay.label"
                                                default="Special Holiday Pay"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="specialHolidayPay-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.specialHolidayPay)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="bonuses-label" class="control-label"><g:message
                                                code="payrollEntry.bonuses.label"
                                                default="13th Month Pay / Bonuses"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="bonuses-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.bonuses)}
                                            </span>

                                        </div>
                                    </div>
                                </div>




                            </div>
                            <div class="span6">



                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="cola-label" class="control-label"><g:message
                                                code="payrollEntry.cola.label"
                                                default="Cola"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="cola-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.cola)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="regularOvertimePay-label" class="control-label"><g:message
                                                code="payrollEntry.regularOvertimePay.label"
                                                default="Regular Overtime Pay"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="regularOvertimePay-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.regularOvertimePay)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="legalHolidayPay-label" class="control-label"><g:message
                                                code="payrollEntry.legalHolidayPay.label"
                                                default="Legal Holiday Pay"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="legalHolidayPay-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.legalHolidayPay)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="annualLeaveCredits-label" class="control-label"><g:message
                                                code="payrollEntry.annualLeaveCredits.label"
                                                default="5 Days Per Year Inclusive Leave"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="annualLeaveCredits-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.annualLeaveCredits)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="grossPay-label" class="control-label"><g:message
                                                code="payrollEntry.grossPay.label"
                                                default="Gross Pay"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="grossPay-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.grossPay)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
 </div>
<div class="row-fluid">
    <div class="span12" id="edit-deduction" role="main">
        <div class="box" id="box-2">
            <h4 class="box-header round-top"><g:message code="default.details.label" default="Deductions"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="sssContribution-label" class="control-label"><g:message
                                                code="payrollEntry.sssContribution.label"
                                                default="Employee SSS Contribution"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="sssContribution-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.sssContribution)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="sssEmployeeCompensation-label" class="control-label"><g:message
                                                code="payrollEntry.sssEmployeeCompensation.label"
                                                default="Employer Compensation"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="sssEmployeeCompensation-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.sssEmployeeCompensation)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="philHealthContributionEmployer-label" class="control-label"><g:message
                                                code="payrollEntry.philHealthContributionEmployer.label"
                                                default="Employer Phil-Health Contribution"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="philHealthContributionEmployer-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.philHealthContributionEmployer)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="pagIbigContributionEmployer-label" class="control-label"><g:message
                                                code="payrollEntry.pagIbigContributionEmployer.label"
                                                default="Employer Pag-Ibig Contribution"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="pagIbigContributionEmployer-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.pagIbigContributionEmployer)}
                                            </span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="advances-label" class="control-label"><g:message
                                                code="payrollEntry.advances.label"
                                                default="Advances"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="advances-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.advances)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="span6">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="sssContributionEmployer-label" class="control-label"><g:message
                                                code="payrollEntry.sssContributionEmployer.label"
                                                default="Employer SSS Contribution"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="sssContributionEmployer-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.sssContributionEmployer)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="philHealthContribution-label" class="control-label"><g:message
                                                code="payrollEntry.philHealthContribution.label"
                                                default="Employee Phil-Health Contribution"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="philHealthContribution-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.philHealthContribution)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="pagIbigContribution-label" class="control-label"><g:message
                                                code="payrollEntry.pagIbigContribution.label"
                                                default="Employee Pag-Ibig Contribution"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="pagIbigContribution-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.pagIbigContribution)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="withholdingTax-label" class="control-label"><g:message
                                                code="payrollEntry.withholdingTax.label"
                                                default="Withholding Tax"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="withholdingTax-label">
                                                ${FM?.formatCurrency(payrollEntryInstance?.withholdingTax)}
                                            </span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="edit-loanPayment" role="main">
        <div class="box" id="box-3">
            <h4 class="box-header round-top"><g:message code="default.details.label" default="Loan Payments"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="payments-label" class="control-label"><g:message
                                                code="payrollEntry.payments.label"
                                                default="Payments"/></h5>
                                        <div class="controls" style="margin-top:4px">

                                            <g:each in="${payrollEntryInstance.payments}" var="p">
                                                <span class="property-value" aria-labelledby="payments-label"><g:link
                                                        controller="loanPayment" action="show"
                                                        id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
                                            </g:each>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>


    %{--<div class="form-horizontal">--}%
        %{--<div class="control-group">--}%
            %{--<h5 id="cashAdvance-label" class="control-label"><g:message--}%
                    %{--code="payrollEntry.cashAdvance.label"--}%
                    %{--default="Cash Advance"/></h5>--}%
            %{--<div class="controls" style="margin-top:4px">--}%

                %{--<span class="property-value" aria-labelledby="cashAdvance-label"><g:fieldValue--}%
                        %{--bean="${payrollEntryInstance}" field="cashAdvance"/></span>--}%

            %{--</div>--}%
        %{--</div>--}%
    %{--</div>--}%

    %{--<div class="form-horizontal">--}%
        %{--<div class="control-group">--}%
            %{--<h5 id="hdmfLoan-label" class="control-label"><g:message--}%
                    %{--code="payrollEntry.hdmfLoan.label"--}%
                    %{--default="Hdmf Loan"/></h5>--}%
            %{--<div class="controls" style="margin-top:4px">--}%

                %{--<span class="property-value" aria-labelledby="hdmfLoan-label"><g:fieldValue--}%
                        %{--bean="${payrollEntryInstance}" field="hdmfLoan"/></span>--}%

            %{--</div>--}%
        %{--</div>--}%
    %{--</div>--}%

    %{--<div class="form-horizontal">--}%
        %{--<div class="control-group">--}%
            %{--<h5 id="pagIbigLoan-label" class="control-label"><g:message--}%
                    %{--code="payrollEntry.pagIbigLoan.label"--}%
                    %{--default="Pag Ibig Loan"/></h5>--}%
            %{--<div class="controls" style="margin-top:4px">--}%

                %{--<span class="property-value" aria-labelledby="pagIbigLoan-label"><g:fieldValue--}%
                        %{--bean="${payrollEntryInstance}" field="pagIbigLoan"/></span>--}%

            %{--</div>--}%
        %{--</div>--}%
    %{--</div>--}%

    %{--<div class="form-horizontal">--}%
        %{--<div class="control-group">--}%
            %{--<h5 id="sssLoan-label" class="control-label"><g:message--}%
                    %{--code="payrollEntry.sssLoan.label"--}%
                    %{--default="Sss Loan"/></h5>--}%
            %{--<div class="controls" style="margin-top:4px">--}%

                %{--<span class="property-value" aria-labelledby="sssLoan-label"><g:fieldValue--}%
                        %{--bean="${payrollEntryInstance}" field="sssLoan"/></span>--}%

            %{--</div>--}%
        %{--</div>--}%
    %{--</div>--}%


