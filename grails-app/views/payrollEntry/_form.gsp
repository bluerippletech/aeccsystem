<%@ page import="com.ripple.entry.PayrollEntry" %>



<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="payrollEntry.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${com.ripple.master.Employee.list()}" optionKey="id" required="" value="${payrollEntryInstance?.employee?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'regularHoursWorked', 'error')} ">
	<label for="regularHoursWorked">
		<g:message code="payrollEntry.regularHoursWorked.label" default="Regular Hours Worked" />

	</label>
	<g:field type="number" name="regularHoursWorked" value="${fieldValue(bean: payrollEntryInstance, field: 'regularHoursWorked')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'lateHours', 'error')} ">
	<label for="lateHours">
		<g:message code="payrollEntry.lateHours.label" default="Late Hours" />

	</label>
	<g:field type="number" name="lateHours" value="${fieldValue(bean: payrollEntryInstance, field: 'lateHours')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'totalRegularHours', 'error')} ">
	<label for="totalRegularHours">
		<g:message code="payrollEntry.totalRegularHours.label" default="Total Regular Hours" />

	</label>
	<g:field type="number" name="totalRegularHours" value="${fieldValue(bean: payrollEntryInstance, field: 'totalRegularHours')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'regularOvertimeHours', 'error')} ">
	<label for="regularOvertimeHours">
		<g:message code="payrollEntry.regularOvertimeHours.label" default="Regular Overtime Hours" />

	</label>
	<g:field type="number" name="regularOvertimeHours" value="${fieldValue(bean: payrollEntryInstance, field: 'regularOvertimeHours')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'sundayHoursWorked', 'error')} ">
	<label for="sundayHoursWorked">
		<g:message code="payrollEntry.sundayHoursWorked.label" default="Sunday Hours Worked" />

	</label>
	<g:field type="number" name="sundayHoursWorked" value="${fieldValue(bean: payrollEntryInstance, field: 'sundayHoursWorked')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'legalHolidayHours', 'error')} ">
	<label for="legalHolidayHours">
		<g:message code="payrollEntry.legalHolidayHours.label" default="Legal Holiday Hours" />

	</label>
	<g:field type="number" name="legalHolidayHours" value="${fieldValue(bean: payrollEntryInstance, field: 'legalHolidayHours')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'totalDaysWorked', 'error')} ">
	<label for="totalDaysWorked">
		<g:message code="payrollEntry.totalDaysWorked.label" default="Total Days Worked" />

	</label>
	<g:field type="number" name="totalDaysWorked" value="${fieldValue(bean: payrollEntryInstance, field: 'totalDaysWorked')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'specialHolidayHours', 'error')} ">
	<label for="specialHolidayHours">
		<g:message code="payrollEntry.specialHolidayHours.label" default="Special Holiday Hours" />

	</label>
	<g:field type="number" name="specialHolidayHours" value="${fieldValue(bean: payrollEntryInstance, field: 'specialHolidayHours')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'regularHoursPay', 'error')} ">
	<label for="regularHoursPay">
		<g:message code="payrollEntry.regularHoursPay.label" default="Regular Hours Pay" />

	</label>
	<g:field type="number" name="regularHoursPay" value="${fieldValue(bean: payrollEntryInstance, field: 'regularHoursPay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'lateValue', 'error')} ">
	<label for="lateValue">
		<g:message code="payrollEntry.lateValue.label" default="Late Value" />

	</label>
	<g:field type="number" name="lateValue" value="${fieldValue(bean: payrollEntryInstance, field: 'lateValue')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'regularOvertimePay', 'error')} ">
	<label for="regularOvertimePay">
		<g:message code="payrollEntry.regularOvertimePay.label" default="Regular Overtime Pay" />

	</label>
	<g:field type="number" name="regularOvertimePay" value="${fieldValue(bean: payrollEntryInstance, field: 'regularOvertimePay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'sundayPay', 'error')} ">
	<label for="sundayPay">
		<g:message code="payrollEntry.sundayPay.label" default="Sunday Pay" />

	</label>
	<g:field type="number" name="sundayPay" value="${fieldValue(bean: payrollEntryInstance, field: 'sundayPay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'legalHolidayPay', 'error')} ">
	<label for="legalHolidayPay">
		<g:message code="payrollEntry.legalHolidayPay.label" default="Legal Holiday Pay" />

	</label>
	<g:field type="number" name="legalHolidayPay" value="${fieldValue(bean: payrollEntryInstance, field: 'legalHolidayPay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'specialHolidayPay', 'error')} ">
	<label for="specialHolidayPay">
		<g:message code="payrollEntry.specialHolidayPay.label" default="Special Holiday Pay" />

	</label>
	<g:field type="number" name="specialHolidayPay" value="${fieldValue(bean: payrollEntryInstance, field: 'specialHolidayPay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'basicPay', 'error')} ">
	<label for="basicPay">
		<g:message code="payrollEntry.basicPay.label" default="Basic Pay" />

	</label>
	<g:field type="number" name="basicPay" value="${fieldValue(bean: payrollEntryInstance, field: 'basicPay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'annualLeaveCredits', 'error')} ">
	<label for="annualLeaveCredits">
		<g:message code="payrollEntry.annualLeaveCredits.label" default="Annual Leave Credits" />

	</label>
	<g:field type="number" name="annualLeaveCredits" value="${fieldValue(bean: payrollEntryInstance, field: 'annualLeaveCredits')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'bonuses', 'error')} ">
	<label for="bonuses">
		<g:message code="payrollEntry.bonuses.label" default="Bonuses" />

	</label>
	<g:field type="number" name="bonuses" value="${fieldValue(bean: payrollEntryInstance, field: 'bonuses')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'grossPay', 'error')} ">
	<label for="grossPay">
		<g:message code="payrollEntry.grossPay.label" default="Gross Pay" />

	</label>
	<g:field type="number" name="grossPay" value="${fieldValue(bean: payrollEntryInstance, field: 'grossPay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'netPay', 'error')} ">
	<label for="netPay">
		<g:message code="payrollEntry.netPay.label" default="Net Pay" />

	</label>
	<g:field type="number" name="netPay" value="${fieldValue(bean: payrollEntryInstance, field: 'netPay')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'cola', 'error')} ">
	<label for="cola">
		<g:message code="payrollEntry.cola.label" default="Cola" />

	</label>
	<g:field type="number" name="cola" value="${fieldValue(bean: payrollEntryInstance, field: 'cola')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'sssContribution', 'error')} ">
	<label for="sssContribution">
		<g:message code="payrollEntry.sssContribution.label" default="Sss Contribution" />

	</label>
	<g:field type="number" name="sssContribution" value="${fieldValue(bean: payrollEntryInstance, field: 'sssContribution')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'philHealthContribution', 'error')} ">
	<label for="philHealthContribution">
		<g:message code="payrollEntry.philHealthContribution.label" default="Phil Health Contribution" />

	</label>
	<g:field type="number" name="philHealthContribution" value="${fieldValue(bean: payrollEntryInstance, field: 'philHealthContribution')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'pagIbigContribution', 'error')} ">
	<label for="pagIbigContribution">
		<g:message code="payrollEntry.pagIbigContribution.label" default="Pag Ibig Contribution" />

	</label>
	<g:field type="number" name="pagIbigContribution" value="${fieldValue(bean: payrollEntryInstance, field: 'pagIbigContribution')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'withholdingTax', 'error')} ">
	<label for="withholdingTax">
		<g:message code="payrollEntry.withholdingTax.label" default="Withholding Tax" />

	</label>
	<g:field type="number" name="withholdingTax" value="${fieldValue(bean: payrollEntryInstance, field: 'withholdingTax')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'payPeriodLabel', 'error')} ">
	<label for="payPeriodLabel">
		<g:message code="payrollEntry.payPeriodLabel.label" default="Pay Period Label" />

	</label>
	<g:textField name="payPeriodLabel" value="${payrollEntryInstance?.payPeriodLabel}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'advances', 'error')} required">
	<label for="advances">
		<g:message code="payrollEntry.advances.label" default="Advances" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="advances" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'advances')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'cashAdvance', 'error')} required">
	<label for="cashAdvance">
		<g:message code="payrollEntry.cashAdvance.label" default="Cash Advance" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="cashAdvance" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'cashAdvance')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'hdmfLoan', 'error')} required">
	<label for="hdmfLoan">
		<g:message code="payrollEntry.hdmfLoan.label" default="Hdmf Loan" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="hdmfLoan" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'hdmfLoan')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'pagIbigContributionEmployer', 'error')} required">
	<label for="pagIbigContributionEmployer">
		<g:message code="payrollEntry.pagIbigContributionEmployer.label" default="Pag Ibig Contribution Employer" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="pagIbigContributionEmployer" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'pagIbigContributionEmployer')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'pagIbigLoan', 'error')} required">
	<label for="pagIbigLoan">
		<g:message code="payrollEntry.pagIbigLoan.label" default="Pag Ibig Loan" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="pagIbigLoan" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'pagIbigLoan')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'payPeriod', 'error')} required">
	<label for="payPeriod">
		<g:message code="payrollEntry.payPeriod.label" default="Pay Period" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="payPeriod" name="payPeriod.id" from="${com.ripple.entry.PayPeriod.list()}" optionKey="id" required="" value="${payrollEntryInstance?.payPeriod?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'payments', 'error')} ">
	<label for="payments">
		<g:message code="payrollEntry.payments.label" default="Payments" />

	</label>

<ul class="one-to-many">
<g:each in="${payrollEntryInstance?.payments?}" var="p">
    <li><g:link controller="loanPayment" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="loanPayment" action="create" params="['payrollEntry.id': payrollEntryInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'loanPayment.label', default: 'LoanPayment')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'philHealthContributionEmployer', 'error')} required">
	<label for="philHealthContributionEmployer">
		<g:message code="payrollEntry.philHealthContributionEmployer.label" default="Phil Health Contribution Employer" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="philHealthContributionEmployer" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'philHealthContributionEmployer')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'sssContributionEmployer', 'error')} required">
	<label for="sssContributionEmployer">
		<g:message code="payrollEntry.sssContributionEmployer.label" default="Sss Contribution Employer" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="sssContributionEmployer" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'sssContributionEmployer')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'sssEmployeeCompensation', 'error')} required">
	<label for="sssEmployeeCompensation">
		<g:message code="payrollEntry.sssEmployeeCompensation.label" default="Sss Employee Compensation" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="sssEmployeeCompensation" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'sssEmployeeCompensation')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: payrollEntryInstance, field: 'sssLoan', 'error')} required">
	<label for="sssLoan">
		<g:message code="payrollEntry.sssLoan.label" default="Sss Loan" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="sssLoan" required="" value="${fieldValue(bean: payrollEntryInstance, field: 'sssLoan')}"/>
</div>

<div class="row-fluid">
    <div class="span12" id="edit-payrollEntry" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.edit.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">

                    <fieldset>
                        <div class="row-fluid">
                            <f:all bean="payrollEntryInstance"/>
                        </div>
                    </fieldset>

                </div>
            </div>

        </div>
    </div>
</div>

