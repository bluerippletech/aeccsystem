
<%@ page import="com.ripple.entry.PayrollEntry" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'payrollEntry.label', default: 'PayrollEntry')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
    <style type="text/css">
    .mini{
        padding: 3px !important;
    }
    </style>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="list-payrollEntry" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    Payroll Entries for ${payPeriod}<br/>
                    Project: ${project}<br/>
                    Location: ${location}<br/>
                    Total Net Pay: P <g:formatNumber number="${payrollEntryInstanceList.netPay.sum()}" format="#,##0.00"/>
                    <br/>
                    <br/>
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                        <tr>
                            
                            <th style="width:20%!important;"><g:message code="payrollEntry.employee.label" default="Employee" /></th>
                            
                            <th><g:message code="payrollEntry.grossPay.label" default="Gross Pay" /></th>
                            
                            <th><g:message code="payrollEntry.sss.label" default="SSS" /></th>
                            
                            <th style="width:40px !important;"><g:message code="payrollEntry.philHealth.label" default="Phil-Health" /></th>
                            
                            <th style="width:40px !important;"><g:message code="payrollEntry.pagIbig.label" default="Pag-Ibig" /></th>
                            
                            <th><g:message code="payrollEntry.cola.label" default="Cola" /></th>

                            <th><g:message code="payrollEntry.tax.label" default="Tax" /></th>

                            <th><g:message code="payrollEntry.netPay.label" default="Net Pay" /></th>

                            <th style="width:25px!important;"><g:message code="payrollEntry.actions.label" default="Actions" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            $('#datatable').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'payrollEntry', action:'listJSON')}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[
                                    
                                    { "sName": "employee", "aTargets": [ 0 ] },
                                    
                                    { "sName": "grossPay", "aTargets": [ 1 ] },
                                    
                                    { "sName": "sssContribution", "aTargets": [ 2 ] },
                                    
                                    { "sName": "philHealthContribution", "aTargets": [ 3 ] },
                                    
                                    { "sName": "pagIbigContribution", "aTargets": [ 4 ] },

                                    { "sName": "cola", "aTargets": [ 5 ] },

                                    { "sName": "withholdingTax", "aTargets": [ 6 ] },

                                    { "sName": "netPay", "aTargets": [ 7 ] },

                                    { "bSortable": false, "aTargets": [ 8 ] }


                                ],
                                "fnServerData": function ( sSource, aoData, fnCallback ) {

                                    aoData.push( { "name": "assignment.id", "value": "${params?.assignment?.id}" } );
                                    aoData.push( { "name": "assignmentLocation.id", "value": "${params?.assignmentLocation?.id}" } );
                                    aoData.push( { "name": "payPeriod.id", "value": "${params?.payPeriod?.id}" } );

                                    $.getJSON( sSource, aoData, function (json) {
                                        fnCallback(json);
                                    } );
                                },
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    $('td:eq(8)', nRow).html( '<a class="btn btn-success mini" href="${createLink(action:'show')}/'+nRow.id+'"><i class="icon-zoom-in icon-white"></i></a>&nbsp;' );
                                %{--$('td:eq(6)', nRow).append('<a class="btn btn-info mini" href="${createLink(action:'edit')}/'+nRow.id+'"><i class="icon-edit icon-white"></i></a>&nbsp;');--}%
                                %{--$('td:eq(6)', nRow).append('<form method="POST" action="delete" name="delete" style="display:inline"><input type="hidden" name="id" value="'+nRow.id+'"><a class="btn btn-danger mini" href="javascript:" onclick="submitForm(this)"><i class="icon-trash icon-white"></i></a></form>');--}%
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                    $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div>

        </div>
    </div>
 </div>
<div class="row-fluid">
    <div class="span12" id="list-payroll" role="main">
        <div class="box" id="box-1">
            <h4 class="box-header round-top"><g:message code="default.jasper.label" default="Generate Payroll"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <g:jasperReport jasper="payrollreport001" format="PDF" name="Generate Payslip">
                        <input type="hidden" name="payPeriod.id" value="${params?.payPeriod?.id}"/>
                        <input type="hidden" name="project.id" value="${params?.assignment?.id}"/>
                        <input type="hidden" name="projectLocation.id" value="${params?.assignmentLocation?.id}"/>
                        <table>
                            <tr>
                                <td>Employee:</td>
                                <td><g:select name="EMPLOYEE" from="${com.ripple.master.Employee.list()}" optionKey="id"
                                              noSelection="['':'-All Employees-']"/></td>
                            </tr>
                            <tr>
                                <td>Print 13th Month Pay Only?</td>
                                <td><g:checkBox name="BONUS"/></td>
                            </tr>
                        </table>
                    </g:jasperReport>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
