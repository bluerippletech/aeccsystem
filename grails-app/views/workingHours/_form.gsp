<%@ page import="com.ripple.entry.WorkingHours" %>



<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="workingHours.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${com.ripple.master.Employee.list()}" optionKey="id" required="" value="${workingHoursInstance?.employee?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'payPeriod', 'error')} required">
	<label for="payPeriod">
		<g:message code="workingHours.payPeriod.label" default="Pay Period" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="payPeriod" name="payPeriod.id" from="${com.ripple.entry.PayPeriod.list()}" optionKey="id" required="" value="${workingHoursInstance?.payPeriod?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day01', 'error')} required">
	<label for="day01">
		<g:message code="workingHours.day01.label" default="Day01" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day01" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day01')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day02', 'error')} required">
	<label for="day02">
		<g:message code="workingHours.day02.label" default="Day02" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day02" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day02')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day03', 'error')} required">
	<label for="day03">
		<g:message code="workingHours.day03.label" default="Day03" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day03" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day03')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day04', 'error')} required">
	<label for="day04">
		<g:message code="workingHours.day04.label" default="Day04" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day04" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day04')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day05', 'error')} required">
	<label for="day05">
		<g:message code="workingHours.day05.label" default="Day05" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day05" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day05')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day06', 'error')} required">
	<label for="day06">
		<g:message code="workingHours.day06.label" default="Day06" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day06" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day06')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day07', 'error')} required">
	<label for="day07">
		<g:message code="workingHours.day07.label" default="Day07" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day07" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day07')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day08', 'error')} required">
	<label for="day08">
		<g:message code="workingHours.day08.label" default="Day08" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day08" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day08')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day09', 'error')} required">
	<label for="day09">
		<g:message code="workingHours.day09.label" default="Day09" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day09" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day09')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day10', 'error')} required">
	<label for="day10">
		<g:message code="workingHours.day10.label" default="Day10" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day10" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day10')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day11', 'error')} required">
	<label for="day11">
		<g:message code="workingHours.day11.label" default="Day11" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day11" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day11')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day12', 'error')} required">
	<label for="day12">
		<g:message code="workingHours.day12.label" default="Day12" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day12" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day12')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day13', 'error')} required">
	<label for="day13">
		<g:message code="workingHours.day13.label" default="Day13" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day13" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day13')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day14', 'error')} required">
	<label for="day14">
		<g:message code="workingHours.day14.label" default="Day14" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day14" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day14')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day15', 'error')} required">
	<label for="day15">
		<g:message code="workingHours.day15.label" default="Day15" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day15" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day15')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day16', 'error')} required">
	<label for="day16">
		<g:message code="workingHours.day16.label" default="Day16" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day16" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day16')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day17', 'error')} required">
	<label for="day17">
		<g:message code="workingHours.day17.label" default="Day17" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day17" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day17')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day18', 'error')} required">
	<label for="day18">
		<g:message code="workingHours.day18.label" default="Day18" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day18" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day18')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day19', 'error')} required">
	<label for="day19">
		<g:message code="workingHours.day19.label" default="Day19" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day19" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day19')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day20', 'error')} required">
	<label for="day20">
		<g:message code="workingHours.day20.label" default="Day20" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day20" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day20')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day21', 'error')} required">
	<label for="day21">
		<g:message code="workingHours.day21.label" default="Day21" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day21" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day21')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day22', 'error')} required">
	<label for="day22">
		<g:message code="workingHours.day22.label" default="Day22" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day22" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day22')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day23', 'error')} required">
	<label for="day23">
		<g:message code="workingHours.day23.label" default="Day23" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day23" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day23')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day24', 'error')} required">
	<label for="day24">
		<g:message code="workingHours.day24.label" default="Day24" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day24" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day24')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day25', 'error')} required">
	<label for="day25">
		<g:message code="workingHours.day25.label" default="Day25" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day25" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day25')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day26', 'error')} required">
	<label for="day26">
		<g:message code="workingHours.day26.label" default="Day26" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day26" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day26')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day27', 'error')} required">
	<label for="day27">
		<g:message code="workingHours.day27.label" default="Day27" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day27" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day27')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day28', 'error')} required">
	<label for="day28">
		<g:message code="workingHours.day28.label" default="Day28" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day28" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day28')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day29', 'error')} required">
	<label for="day29">
		<g:message code="workingHours.day29.label" default="Day29" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day29" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day29')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day30', 'error')} required">
	<label for="day30">
		<g:message code="workingHours.day30.label" default="Day30" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day30" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day30')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workingHoursInstance, field: 'day31', 'error')} required">
	<label for="day31">
		<g:message code="workingHours.day31.label" default="Day31" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="day31" required="" value="${fieldValue(bean: workingHoursInstance, field: 'day31')}"/>
</div>

