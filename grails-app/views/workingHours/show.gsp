
<%@ page import="com.ripple.entry.WorkingHours" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'workingHours.label', default: 'WorkingHours')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-workingHours" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="payPeriod-label" class="control-label"><g:message
                                                    code="workingHours.payPeriod.label"
                                                    default="Pay Period"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="payPeriod-label"><g:link
                                                        controller="payPeriod" action="show"
                                                        id="${workingHoursInstance?.payPeriod?.id}">${workingHoursInstance?.payPeriod?.encodeAsHTML()}</g:link></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day02-label" class="control-label"><g:message
                                                    code="workingHours.day02.label"
                                                    default="Day02"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day02-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day02"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day04-label" class="control-label"><g:message
                                                    code="workingHours.day04.label"
                                                    default="Day04"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day04-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day04"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day06-label" class="control-label"><g:message
                                                    code="workingHours.day06.label"
                                                    default="Day06"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day06-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day06"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day08-label" class="control-label"><g:message
                                                    code="workingHours.day08.label"
                                                    default="Day08"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day08-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day08"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day10-label" class="control-label"><g:message
                                                    code="workingHours.day10.label"
                                                    default="Day10"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day10-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day10"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day12-label" class="control-label"><g:message
                                                    code="workingHours.day12.label"
                                                    default="Day12"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day12-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day12"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day14-label" class="control-label"><g:message
                                                    code="workingHours.day14.label"
                                                    default="Day14"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day14-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day14"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day16-label" class="control-label"><g:message
                                                    code="workingHours.day16.label"
                                                    default="Day16"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day16-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day16"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day18-label" class="control-label"><g:message
                                                    code="workingHours.day18.label"
                                                    default="Day18"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day18-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day18"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day20-label" class="control-label"><g:message
                                                    code="workingHours.day20.label"
                                                    default="Day20"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day20-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day20"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day22-label" class="control-label"><g:message
                                                    code="workingHours.day22.label"
                                                    default="Day22"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day22-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day22"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day24-label" class="control-label"><g:message
                                                    code="workingHours.day24.label"
                                                    default="Day24"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day24-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day24"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day26-label" class="control-label"><g:message
                                                    code="workingHours.day26.label"
                                                    default="Day26"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day26-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day26"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day28-label" class="control-label"><g:message
                                                    code="workingHours.day28.label"
                                                    default="Day28"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day28-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day28"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day30-label" class="control-label"><g:message
                                                    code="workingHours.day30.label"
                                                    default="Day30"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day30-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day30"/></span>

                                            </div>
                                        </div>
                                    </div>

                            </div>
                            <div class="span6 column">

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="employee-label" class="control-label"><g:message
                                                    code="workingHours.employee.label"
                                                    default="Employee"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="employee-label"><g:link
                                                        controller="employee" action="show"
                                                        id="${workingHoursInstance?.employee?.id}">${workingHoursInstance?.employee?.encodeAsHTML()}</g:link></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day01-label" class="control-label"><g:message
                                                    code="workingHours.day01.label"
                                                    default="Day01"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day01-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day01"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day03-label" class="control-label"><g:message
                                                    code="workingHours.day03.label"
                                                    default="Day03"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day03-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day03"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day05-label" class="control-label"><g:message
                                                    code="workingHours.day05.label"
                                                    default="Day05"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day05-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day05"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day07-label" class="control-label"><g:message
                                                    code="workingHours.day07.label"
                                                    default="Day07"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day07-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day07"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day09-label" class="control-label"><g:message
                                                    code="workingHours.day09.label"
                                                    default="Day09"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day09-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day09"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day11-label" class="control-label"><g:message
                                                    code="workingHours.day11.label"
                                                    default="Day11"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day11-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day11"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day13-label" class="control-label"><g:message
                                                    code="workingHours.day13.label"
                                                    default="Day13"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day13-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day13"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day15-label" class="control-label"><g:message
                                                    code="workingHours.day15.label"
                                                    default="Day15"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day15-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day15"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day17-label" class="control-label"><g:message
                                                    code="workingHours.day17.label"
                                                    default="Day17"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day17-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day17"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day19-label" class="control-label"><g:message
                                                    code="workingHours.day19.label"
                                                    default="Day19"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day19-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day19"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day21-label" class="control-label"><g:message
                                                    code="workingHours.day21.label"
                                                    default="Day21"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day21-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day21"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day23-label" class="control-label"><g:message
                                                    code="workingHours.day23.label"
                                                    default="Day23"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day23-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day23"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day25-label" class="control-label"><g:message
                                                    code="workingHours.day25.label"
                                                    default="Day25"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day25-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day25"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day27-label" class="control-label"><g:message
                                                    code="workingHours.day27.label"
                                                    default="Day27"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day27-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day27"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day29-label" class="control-label"><g:message
                                                    code="workingHours.day29.label"
                                                    default="Day29"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day29-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day29"/></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="day31-label" class="control-label"><g:message
                                                    code="workingHours.day31.label"
                                                    default="Day31"/></h5>
                                            <div class="controls" style="margin-top:4px">

                                                <span class="property-value" aria-labelledby="day31-label"><g:fieldValue
                                                        bean="${workingHoursInstance}" field="day31"/></span>

                                            </div>
                                        </div>
                                    </div>


                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${workingHoursInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${workingHoursInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
