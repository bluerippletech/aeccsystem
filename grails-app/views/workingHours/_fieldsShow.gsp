<%@ page defaultCodec="html" %>
%{--<f:all bean="workingHoursInstance"/>--}%
<div class="span6" style="margin-left:0px;">
    <div class="control-group">
        <label class="control-label" for="employee">Employee:</label>
        <div class="controls">
            <input type="hidden" name="payPeriod.id" id="payPeriod.id" value="${payPeriod?.id}"/>
            <input type="hidden" name="id" id="id" value="${workingHoursInstance?.id}"/>
            <input type="hidden" id="version" name="version" version="version" value="${workingHoursInstance?.version}"/>
            <input type="hidden" name="project.id" value="${project?.id}"/>
            <input type="hidden" name="projectLocation.id" value="${projectLocation?.id}"/>
            <input type="hidden" name="employee.id" id="employee.id" value="${employee?.id}"/>
            <span id="employeeName">${employee?.lastName}, ${employee?.firstName} ${employee?.middleInitials}</span>
        </div>
        <script>
            $(function(){
                $('#filter-employee').val('${employee?.id}');
                newOptions = {
                    <g:each in="${employeeList}" var='perEmployee' status="i">
                    '${perEmployee.id}':'${perEmployee.wholeName}'${i<(employeeList.size()-1)?',':''}
                    </g:each>
                };
                selectedOption = '${employee?.id}';
            })
        </script>
    </div>
</div>
<div class="span6" style="margin-left:0px;">
        <div class="control-group">
            <label>Completed entries for this pay period?:</label>
            <span id="completed">
                <g:if test="${workingHoursInstance?.id!=null}">Completed
                </g:if>
                <g:else>
                    <span style="color:red">Not Submitted</span>
                </g:else>
            </span>
        </div>
</div>
<div class="span6" style="margin-left:0px;">
        <div class="control-group">
            <label>Payroll Entry:</label>
            <span id="payrollEntryId">
                <g:if test="${payrollEntry?.id!=null}">
                    <a href="${createLink(action: 'show', controller: 'payrollEntry', id: payrollEntry?.id)}">View Payroll Entry</a>
                </g:if>
                <g:else>
                    <div style="color:red"></div>
                </g:else>
            </span>
        </div>
</div>
<g:set var="counter" value="${payPeriod.periodStartDay()}"/>
<g:set var="date" value="${payPeriod.toLocalDate(payPeriod?.periodStartDay())}"/>
<g:while test="${date.getDayOfMonth() != payPeriod.periodEndDay()+1}">
    <g:set var="label" value="${date.getDayOfMonth()}"/>
    <g:if test="${date.getDayOfMonth()<10}">
        <g:set var="label" value="0${date.getDayOfMonth()}"/>
    </g:if>
    <div class="span6" style="margin-left:0px;">
        <div class="control-group">
            <label class="control-label" for="day${label}" style="${holidayService.getStyle(date, projectLocation)}">${payPeriod.toProperLabel(date)}:</label>
            <div class="controls">
                ${fieldValue(bean: workingHoursInstance, field: 'day' + label)}
            </div>
        </div>
    </div>
    <g:set var="date" value="${date.plusDays(1)}"/>
</g:while>
