<%@ page import="com.ripple.master.AssignmentLocation; com.ripple.master.Assignment; com.ripple.entry.PayPeriod" defaultCodec="html" %>
<div class="span6" style="margin-left:0px;">
    <div class="control-group">
        <label class="control-label" for="payPeriod">Pay Period</label>
        <div class="controls">
            <g:select name="payPeriod.id" id="payPeriod" from="${PayPeriod.list(sort: 'monthYear', order: 'desc')}" optionKey="id" optionValue="dateRange" value="${payPeriod?.id}"/>
        </div>
    </div>
</div>
<div class="span6" style="margin-left:0px;">
    <div class="control-group">
        <label class="control-label" for="payPeriod">Project</label>
        <div class="controls">
            <g:select name="assignment.id" id="assignment" from="${Assignment.list(sort: 'projectName', order: 'asc')}"  optionKey="id" optionValue="projectName"  class="chzn-select" value=""  noSelection="['':'-Please select a Project-']"
                      onchange="${remoteFunction(action:'getAssignmentLocation',
                              update:[success:'assignmentLocation'], params: "'assignment.id=' + this.value")}"/>
        </div>
    </div>
</div>
<div class="span6" style="margin-left:0px;">
    <div class="control-group">
        <label class="control-label" for="payPeriod">Work Location</label>
        <div class="controls">
            <g:select name="assignmentLocation.id" id="assignmentLocation" from=""  optionKey="id" optionValue="location" noSelection="['':'-Please select a Project Location-']" value=""/>
        </div>
    </div>
</div>