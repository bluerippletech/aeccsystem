<%@ page import="com.ripple.entry.WorkingHours" %>
<!doctype html>
<html>
	<head>
        <meta charset="utf-8" name="layout" content="main"/>
		<g:set var="entityName" value="${message(code: 'workingHours.label', default: 'WorkingHours')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <r:require modules="all"/>
        <r:script>
            var newOptions = []
            var selectedOption

            function renderForm(formObj){
                return renderForm.twoParams(formObj, '');
            }

            $(function(){
                $(".chzn-select").chosen()
            })

            renderForm.twoParams = function(formObj, nav){
                $.ajax({
                    url:"${createLink(controller: 'workingHours', action: 'renderForm')}",
                    type:"POST",
                    data:$(formObj).serialize()+"&nav="+nav,
                    //dataType:"json",
                    beforeSend:function () {
                        //show modal
                        if($("#loadingModal").is(':hidden'))
                            $('#loadingModal').modal('toggle')

                        if($("#box-1").is(':visible'))
                            $("#box-1").fadeToggle('medium');

                    },
                    success:function (results) {
                        if(nav=='')
                            $("#fields-container").html(results);
                        else
                            navigate(nav, results);

                        if(newOptions || newOptions!=''){

                            var select = $('#employee-filter');
                            if(select.prop) {
                                var options = select.prop('options');
                            }
                            else {
                                var options = select.attr('options');
                            }
                            $('option', select).remove();


//                            optionHolder = _.sortBy( newOptions, function(val, key, newOptions) {
//                                return val;
//                            });

                            $.each(newOptions, function(val, text) {
                                select.append(new Option(this.value,this.id));
//                                options[options.length] = new Option(text, val);
                            });

                            if(nav=='')
                                select.val(selectedOption);
                            else if(results.employeeInstance.employee.id)
                                select.val(results.employeeInstance.employee.id);
                        }

                        $("#employee-filter").trigger("liszt:updated");

                        $("#box-1").fadeToggle('medium');

                        //hide modal
                        if($("#loadingModal").is(':visible'))
                            $('#loadingModal').modal('toggle')
                    },
                    error:function(error) {
                        //alert(error)
                        //displayErrors(error.responseText,$(obj).closest('form'));
                        //hide modal
                        if($("#loadingModal").is(':visible'))
                            $('#loadingModal').modal('toggle')
                    },
                    complete: function(data){
                    }
                })
            }

            function save(formObj){
                return save.twoParams(formObj, '');
            }

            save.twoParams = function(formObj, nav){
                 $.ajax({
                    url:"${createLink(controller: 'workingHours', action: 'save')}",
                    type:"POST",
                    data:$(formObj).serialize(),
                    //dataType:"json",
                    beforeSend:function () {
                        //show modal
                        if($("#loadingModal").is(':hidden'))
                            $('#loadingModal').modal('toggle')
                        if($("#box-1").is(':visible'))
                            $("#box-1").fadeToggle('medium');
                    },
                    success:function (results) {
                        $("#success-content-container").html("<g:message code='default.updated.message' args="['Record has been ','successfully']" default="Created"/>");
                        $(".alert-success").show();
                        $('html, body').animate({scrollTop: '0px'}, 800);
                        if(nav == 'prev'){
                            renderForm.twoParams($('#filter'), 'prev');
                        }else if(nav == 'next'){
                            renderForm.twoParams($('#filter'), 'next');
                        }
                    },
                    error:function(error) {
                        displayErrors(error.responseText, formObj);
                        $(".alert-error").show();
                        //hide modal
                        if($("#loadingModal").is(':visible'))
                            $('#loadingModal').modal('toggle')
                        if($("#box-1").is(':hidden'))
                            $("#box-1").fadeToggle('medium')
                    },
                    complete: function(data){

                    }
                })
            }

            function navigate(nav, results){
                fillFields(results.workingHoursInstance, $("#box-1"))
                if(!results.workingHoursInstance.employee)
                    fillFields(results.employeeInstance, $("#box-1"))
                fillFields(results.payPeriodInstance, $("#box-1"))
                fillFields(results.payrollEntryInstance, $("#box-1"))

                $("#filter").find("#filter-employee").val(results.employeeInstance.employee.id)
                $("#workingHoursForm").find("#employeeName").html(results.employeeInstance.employee.wholeName)

                if(results.workingHoursInstance.id){
                    $("#workingHoursForm").find("#completedId").html("<span id='completed'>Completed</span>")
                }else{
                    $("#workingHoursForm").find("#completed").html("<span style='color:red'>Not Submitted</span>")
                }
                if(results.payrollEntryInstance){
                    $("#workingHoursForm").find("#payrollEntryId").html("<a class='btn' href='${createLink(action: 'show', controller: 'payrollEntry')}/"+results.payrollEntryInstance.payrollEntry.id+"'>View Payroll Entry</a>")
                }else{
                    $("#workingHoursForm").find("#payrollEntryId").html("<span style='color:red'></span>")
                }
            }


            function fillFields(instanceJSON, container){
                _.each(instanceJSON, function(value, key){
                    if(_.isObject(value)){
                        _.each(value, function(objValue, objKey){
                            container.find('form').find("[name='"+key+"."+objKey+"']").val(objValue)
                            if(!objValue){
                                container.find('form').find("[name='"+key+"."+objKey+"']").val('')
                            }
                        });
                    }else if(!value){
                        if(value == 0)
                            container.find('form').find("[name='"+key+"']").val(value)
                        else
                            container.find('form').find("[name='"+key+"']").val('')
                    }else
                        container.find('form').find("[name='"+key+"']").val(value)
                });
                container.find('.error').find('.help-inline').remove()
                container.find('.error').removeClass('error')
            }

            function displayErrors(errorsJSON, formObj){
                var errors = $.parseJSON(errorsJSON);
                $.each( errors.errors, function(anObject){
                    if(!$(formObj).find("[name='"+this.field+"']").parent().parent().hasClass('error')){
                        $(formObj).find("[name='"+this.field+"']").parent().parent().addClass('error')
                        $(formObj).find("[name='"+this.field+"']").parent().append("<span class='help-inline'>"+this.message+"</span>");
                    }
                });
            }

            <g:if test="${params?.action == 'edit' || params?.action == 'show'}">
                $(function(){
                    renderForm($('#filter'));
                });
            </g:if>
        </r:script>
	</head>
	<body>
    <div class="row-fluid">
        <div class="span1 action-btn round-all">
            <a href="${createLink(uri: '/')}">
                <div><i class="icon-home"></i></div>
                <div><strong><g:message code="default.home.label"/></strong></div>
            </a>
        </div>
        <div class="span1 action-btn round-all">
            <a href="${createLink(action: 'list')}">
                <div><i class="icon-align-justify"></i></div>
                <div><strong>List</strong></div>
            </a>
        </div>
    </div>


        <div class="alert alert-success" role="status"  style="display: none;">
            <a class="close" href="javascript:" onclick="$('.alert-success').fadeOut();">&times;</a>
            <span id="success-content-container"></span>
        </div>


        <div class="alert alert-error" style="display: none;">
            <a class="close" href="javascript:" onclick="$('.alert-error').fadeOut();">&times;</a>
            Validation has failed. Please correct your inputs.
            %{--<ul id="error-content-container">--}%
            %{--<ul>--}%
                %{--<g:eachError bean="${workingHoursInstance}" var="error">--}%
                    %{--<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>--}%
                %{--</g:eachError>--}%
            %{--</ul>--}%
            %{--</ul>--}%
        </div>

    <div class="row-fluid">
        <div class="span12" id="filter-workingHours" role="main">
            <!-- Portlet: Form Control States -->
            <div class="box" id="box-0">
                <h4 class="box-header round-top">${entityName}
                    <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                    <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                    <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
                </h4>
                <div class="box-container-toggle">
                    <div class="box-content">
                        <g:form class="form-horizontal" name="filter" action="renderForm" onsubmit="renderForm(this); return false;" >
                            <input type="hidden" name="employee.id" id="filter-employee" value="${employee?.id}">
                            <fieldset>
                                <g:render template="hoursFilter"/>

                            </fieldset>
                            <div class="form-actions">
                                <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.enter.label', default: 'Enter')}" />
                                <button name="cancel" class="btn" onclick="$('#box-1').slideUp('medium'); return false;">${message(code: 'default.button.cancel.label', default: 'Cancel')}</button>
                            </div>
                        </g:form>
                    </div>
                </div>
            </div><!--/span-->


            <!-- Portlet: Form Control States -->
            <div class="box" id="box-1" style="display: none;">
                <h4 class="box-header round-top"><g:message code="default.input.label" default="Input" /> ${entityName}
                    <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                    <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                    <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i></a>
                </h4>
                <div class="box-container-toggle">
                    <div class="box-content">
                        <g:form class="form-horizontal" name="workingHoursForm" action="save" onsubmit="save(this); return false">
                            <div id="navigation" class="form-actions">
                                <button name="prev" class="btn btn-primary" onclick="save.twoParams($('#workingHoursForm'), 'prev'); return false;">${message(code: 'default.button.save.previous.label', default: 'Save & Previous')}</button>
                                <button name="prev" class="btn" onclick="renderForm.twoParams($('#filter'), 'prev'); return false;">${message(code: 'default.button.previous.label', default: 'Previous')}</button>
                                <g:select name="employee-filter" from="${employeeList}" class="chzn-select" optionKey="id" optionValue="lastName" onchange="\$('#filter-employee').val(\$(this).val()); \$('#filter').submit()"/>
                                <button name="next" class="btn" onclick="renderForm.twoParams($('#filter'), 'next'); return false;">${message(code: 'default.button.next.label', default: 'Next')}</button>
                                <button name="next" class="btn btn-primary" onclick="save.twoParams($('#workingHoursForm'), 'next'); return false;">${message(code: 'default.button.save.next.label', default: 'Save & Next')}</button>
                            </div>
                            <fieldset>
                                <div id="fields-container" style="margin-top: 2em"></div>
                            </fieldset>
                            <div class="form-actions">
                                <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.save.label', default: 'Save')}" />
                                <g:submitButton name="cancel" class="btn" value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />
                            </div>
                        </g:form>
                    </div>
                </div>
            </div><!--/span-->

        </div>
    </div>
    <div class="modal hide" data-backdrop="false" data-keyboard="false" id="loadingModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <h3 id="myModalLabel">Please wait...</h3>
        </div>
        <div class="modal-body">
            <p><img src="${resource(dir: 'images', file: 'loading.gif')}" height="30px" width="30px" style="margin: 0px 5px"/> Loading...</p>
        </div>
        <script>
            $(function(){
                $('#loadingModal').css({
                    width: 'auto',
                    'margin-left': function () {
                        return -($(this).width() / 2);
                    }
                });
            })
        </script>
    </div>
    </body>
</html>
