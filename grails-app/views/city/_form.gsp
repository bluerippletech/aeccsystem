<%@ page import="com.ripple.master.geography.City" %>



<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'isDeleted', 'error')} ">
	<label for="isDeleted">
		<g:message code="city.isDeleted.label" default="Is Deleted" />
		
	</label>
	<g:select name="isDeleted" from="${cityInstance.constraints.isDeleted.inList}" value="${cityInstance?.isDeleted}" valueMessagePrefix="city.isDeleted" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'createdDate', 'error')} ">
	<label for="createdDate">
		<g:message code="city.createdDate.label" default="Created Date" />
		
	</label>
	<g:datePicker name="createdDate" precision="day"  value="${cityInstance?.createdDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'editedDate', 'error')} ">
	<label for="editedDate">
		<g:message code="city.editedDate.label" default="Edited Date" />
		
	</label>
	<g:datePicker name="editedDate" precision="day"  value="${cityInstance?.editedDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'editedBy', 'error')} ">
	<label for="editedBy">
		<g:message code="city.editedBy.label" default="Edited By" />
		
	</label>
	<g:textField name="editedBy" value="${cityInstance?.editedBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="city.createdBy.label" default="Created By" />
		
	</label>
	<g:textField name="createdBy" value="${cityInstance?.createdBy}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="city.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${cityInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: cityInstance, field: 'province', 'error')} required">
	<label for="province">
		<g:message code="city.province.label" default="Province" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="province" name="province.id" from="${com.ripple.master.geography.Province.list()}" optionKey="id" required="" value="${cityInstance?.province?.id}" class="many-to-one"/>
</div>

