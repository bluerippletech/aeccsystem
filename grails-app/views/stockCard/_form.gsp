<%@ page import="com.ripple.stock.StockCard" %>



<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'transactionCode', 'error')} required">
    <label for="transactionCode">
        <g:message code="stockCard.transactionCode.label" default="Transaction Code"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="transactionCode" required="" value="${stockCardInstance?.transactionCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'transactionReferenceId', 'error')} required">
    <label for="transactionReferenceId">
        <g:message code="stockCard.transactionReferenceId.label" default="Transaction Reference Id"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="transactionReferenceId" required="" value="${stockCardInstance?.transactionReferenceId}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'rrReference', 'error')} required">
    <label for="rrReference">
        <g:message code="stockCard.rrReference.label" default="Rr Reference"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="rrReference" required="" value="${stockCardInstance?.rrReference}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'referenceStock', 'error')} ">
    <label for="referenceStock">
        <g:message code="stockCard.referenceStock.label" default="Reference Stock"/>

    </label>
    <g:select id="referenceStock" name="referenceStock.id" from="${com.ripple.stock.StockCard.list()}" optionKey="id"
              value="${stockCardInstance?.referenceStock?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'movement', 'error')} required">
    <label for="movement">
        <g:message code="stockCard.movement.label" default="Movement"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select name="movement" from="${stockCardInstance.constraints.movement.inList}" required=""
              value="${stockCardInstance?.movement}" valueMessagePrefix="stockCard.movement"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'quantity', 'error')} ">
    <label for="quantity">
        <g:message code="stockCard.quantity.label" default="Quantity"/>

    </label>
    <g:field type="number" name="quantity" value="${fieldValue(bean: stockCardInstance, field: 'quantity')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'item', 'error')} required">
    <label for="item">
        <g:message code="stockCard.item.label" default="Item"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="item" name="item.id" from="${com.ripple.master.item.Item.list()}" optionKey="id" required=""
              value="${stockCardInstance?.item?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'allocated', 'error')} ">
    <label for="allocated">
        <g:message code="stockCard.allocated.label" default="Allocated"/>

    </label>
    <g:field type="number" name="allocated" value="${fieldValue(bean: stockCardInstance, field: 'allocated')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'served', 'error')} ">
    <label for="served">
        <g:message code="stockCard.served.label" default="Served"/>

    </label>
    <g:field type="number" name="served" value="${fieldValue(bean: stockCardInstance, field: 'served')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'unitPrice', 'error')} required">
    <label for="unitPrice">
        <g:message code="stockCard.unitPrice.label" default="Unit Price"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field type="number" name="unitPrice" required=""
             value="${fieldValue(bean: stockCardInstance, field: 'unitPrice')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'transactionMovementType', 'error')} required">
    <label for="transactionMovementType">
        <g:message code="stockCard.transactionMovementType.label" default="Transaction Movement Type"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select name="transactionMovementType" from="${stockCardInstance.constraints.transactionMovementType.inList}"
              required="" value="${stockCardInstance?.transactionMovementType}"
              valueMessagePrefix="stockCard.transactionMovementType"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'warehouse', 'error')} required">
    <label for="warehouse">
        <g:message code="stockCard.warehouse.label" default="Warehouse"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="warehouse" name="warehouse.id" from="${com.ripple.master.Warehouse.list()}" optionKey="id" required=""
              value="${stockCardInstance?.warehouse?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: stockCardInstance, field: 'remainingBalance', 'error')} required">
    <label for="remainingBalance">
        <g:message code="stockCard.remainingBalance.label" default="Remaining Balance"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field type="number" name="remainingBalance" required=""
             value="${fieldValue(bean: stockCardInstance, field: 'remainingBalance')}"/>
</div>

