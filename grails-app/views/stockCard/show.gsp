<%@ page import="com.ripple.stock.StockCard" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'stockCard.label', default: 'StockCard')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
<div class="span12 column" id="edit-stockCard" role="main">
<!-- Portlet: Form Control States -->
<div class="box" id="box-0">
<h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
    <a class="box-btn" title="close"><i class="icon-remove"></i></a>
    <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
    <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
    </a>
</h4>

<div class="box-container-toggle">
<div class="box-content">
<fieldset>
<legend><g:message code="default.show.label" args="[entityName]"/></legend>

<div class="row-fluid">
    <div class="span6 column">

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="transactionReferenceId-label" class="control-label"><g:message
                        code="stockCard.transactionReferenceId.label"
                        default="Transaction Reference Id"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="transactionReferenceId-label"><g:fieldValue
                            bean="${stockCardInstance}" field="transactionReferenceId"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="referenceStock-label" class="control-label"><g:message
                        code="stockCard.referenceStock.label"
                        default="Reference Stock"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="referenceStock-label"><g:link
                            controller="stockCard" action="show"
                            id="${stockCardInstance?.referenceStock?.id}">${stockCardInstance?.referenceStock?.encodeAsHTML()}</g:link></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="quantity-label" class="control-label"><g:message
                        code="stockCard.quantity.label"
                        default="Quantity"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="quantity-label"><g:fieldValue
                            bean="${stockCardInstance}" field="quantity"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="allocated-label" class="control-label"><g:message
                        code="stockCard.allocated.label"
                        default="Allocated"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="allocated-label"><g:fieldValue
                            bean="${stockCardInstance}" field="allocated"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="unitPrice-label" class="control-label"><g:message
                        code="stockCard.unitPrice.label"
                        default="Unit Price"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="unitPrice-label"><g:fieldValue
                            bean="${stockCardInstance}" field="unitPrice"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="warehouse-label" class="control-label"><g:message
                        code="stockCard.warehouse.label"
                        default="Warehouse"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="warehouse-label"><g:link
                            controller="warehouse" action="show"
                            id="${stockCardInstance?.warehouse?.id}">${stockCardInstance?.warehouse?.encodeAsHTML()}</g:link></span>

                </div>
            </div>
        </div>

    </div>

    <div class="span6 column">

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="transactionCode-label" class="control-label"><g:message
                        code="stockCard.transactionCode.label"
                        default="Transaction Code"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="transactionCode-label"><g:fieldValue
                            bean="${stockCardInstance}" field="transactionCode"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="rrReference-label" class="control-label"><g:message
                        code="stockCard.rrReference.label"
                        default="Rr Reference"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="rrReference-label"><g:fieldValue
                            bean="${stockCardInstance}" field="rrReference"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="movement-label" class="control-label"><g:message
                        code="stockCard.movement.label"
                        default="Movement"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="movement-label"><g:fieldValue
                            bean="${stockCardInstance}" field="movement"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="item-label" class="control-label"><g:message
                        code="stockCard.item.label"
                        default="Item"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="item-label"><g:link
                            controller="item" action="show"
                            id="${stockCardInstance?.item?.id}">${stockCardInstance?.item?.encodeAsHTML()}</g:link></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="served-label" class="control-label"><g:message
                        code="stockCard.served.label"
                        default="Served"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="served-label"><g:fieldValue
                            bean="${stockCardInstance}" field="served"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="transactionMovementType-label" class="control-label"><g:message
                        code="stockCard.transactionMovementType.label"
                        default="Transaction Movement Type"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="transactionMovementType-label"><g:fieldValue
                            bean="${stockCardInstance}" field="transactionMovementType"/></span>

                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <div class="control-group">
                <h5 id="remainingBalance-label" class="control-label"><g:message
                        code="stockCard.remainingBalance.label"
                        default="Remaining Balance"/></h5>

                <div class="controls" style="margin-top:4px">

                    <span class="property-value" aria-labelledby="remainingBalance-label"><g:fieldValue
                            bean="${stockCardInstance}" field="remainingBalance"/></span>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="form-actions form-horizontal">
    <g:form>
        <g:hiddenField name="id" value="${stockCardInstance?.id}"/>
    %{--<g:link class="edit" action="edit" id="${stockCardInstance?.id}"><g:message--}%
    %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
        <g:actionSubmit class="btn btn-primary" action="edit"
                        value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

        <g:actionSubmit class="delete btn-danger" action="delete"
                        value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                        onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
    </g:form>
</div>
</fieldset>
</div>
</div>
</div><!--/span-->

</div>
</div>
</body>
</html>
