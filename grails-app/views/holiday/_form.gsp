<%@ page import="com.ripple.master.Holiday" %>



<div class="fieldcontain ${hasErrors(bean: holidayInstance, field: 'holidayDate', 'error')} required">
	<label for="holidayDate">
		<g:message code="holiday.holidayDate.label" default="Holiday Date" />
		<span class="required-indicator">*</span>
	</label>
	<joda:datePicker name="holidayDate" value="${holidayInstance?.holidayDate}" ></joda:datePicker>
</div>

<div class="fieldcontain ${hasErrors(bean: holidayInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="holiday.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${holidayInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: holidayInstance, field: 'legalHoliday', 'error')} ">
	<label for="legalHoliday">
		<g:message code="holiday.legalHoliday.label" default="Legal Holiday" />
		
	</label>
	<g:checkBox name="legalHoliday" value="${holidayInstance?.legalHoliday}" />
</div>

<div class="fieldcontain ${hasErrors(bean: holidayInstance, field: 'applicableSites', 'error')} ">
	<label for="applicableSites">
		<g:message code="holiday.applicableSites.label" default="Applicable Sites" />
		
	</label>
	<g:select name="applicableSites" from="${com.ripple.master.AssignmentLocation.list()}" multiple="multiple" optionKey="id" size="5" value="${holidayInstance?.applicableSites*.id}" class="many-to-many"/>
</div>

