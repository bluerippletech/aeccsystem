
<%@ page import="com.ripple.master.Holiday" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'holiday.label', default: 'Holiday')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="aecc,ckeditor"/>
    <style type="text/css">
    .mini{
        padding: 3px !important;
    }
    </style>

    <r:script>
        $(function(){
        $('#holiday').fullCalendar({
            eventSources:[{
                            url:'${createLink(controller:'holiday',action:'getRegularHolidayCalendar')}',
                            color:'orange',
                            textColor:'white'
                          },
                            {
                            url:'${createLink(controller:'holiday',action:'getSpecialHolidayCalendar')}',
                            color:'green',
                            textColor:'white'
                          }
            ],

            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            theme:false,
            droppable: true, // this allows things to be dropped onto the calendar !!!
            drop: function(date, allDay) { // this function is called when something is dropped
                // retrieve the dropped element's stored Event Object
                var originalEventObject = $(this).data('eventObject');
                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);
                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.allDay = allDay;
                // render the event on the calendar
                // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                $('#holiday').fullCalendar('renderEvent', copiedEventObject, true);
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
             eventClick: function(calEvent, jsEvent, view) {
                window.location.href="${createLink(controller:'holiday',action:'show')}/"+calEvent.id;
            }
        });
        });
    </r:script>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column">
        <div class="box-content">
            <h1>Calendar View of Holidays</h1>
            <hr/>
            <div class="row-fluid">
                <div class="span10"><!-- calendar column -->
                    <div id='holiday'></div><!-- calendar placeholder -->
                </div>
                <div class="span2"><!-- events column -->
                    <h2>Holiday Type</h2>
                    <hr />
                    <!-- events which can be used in the calender -->
                    <div>
                        <div class="external-event label label-success">Regular</div>
                        <div class="external-event label label-warning">Special</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row-fluid">
    <div class="span12" id="list-holiday" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.list.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>
            <div class="box-container-toggle">
                <div class="box-content">
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="holiday-datatable">
                        <thead>
                        <tr>
                            
                            <th><g:message code="holiday.holidayDate.label" default="Holiday Date" /></th>
                            
                            <th><g:message code="holiday.description.label" default="Description" /></th>
                            
                            <th><g:message code="holiday.legalHoliday.label" default="Legal Holiday" /></th>

                            <th><g:message code="holiday.actions.label" default="Actions" /></th>

                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(function(){
                            $('#holiday-datatable').dataTable( {
                                bProcessing: true,
                                bServerSide: true,
                                iDisplayLength: 10,
                                sAjaxSource: '${createLink(controller:'holiday', action:'listJSON')}',
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "sPaginationType": "bootstrap",
                                "oLanguage": {
                                    "sLengthMenu": "_MENU_ records per page"
                                },
                                "aoColumnDefs":[
                                    
                                    { "sName": "holidayDate", "aTargets": [ 0 ] },
                                    
                                    { "sName": "description", "aTargets": [ 1 ] },
                                    
                                    { "sName": "legalHoliday", "aTargets": [ 2 ] },

                                    { "bSortable":false, "aTargets": [ 3 ] }
                                    

                                ],
                                "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                                    $('td:eq(3)', nRow).html( '<a class="btn btn-success mini" href="${createLink(action:'show')}/'+nRow.id+'"><i class="icon-zoom-in icon-white"></i></a>&nbsp;' );
                                $('td:eq(3)', nRow).append('<a class="btn btn-info mini" href="${createLink(action:'edit')}/'+nRow.id+'"><i class="icon-edit icon-white"></i></a>&nbsp;');
                                $('td:eq(3)', nRow).append('<form method="POST" action="delete" name="delete" style="display:inline"><input type="hidden" name="id" value="'+nRow.id+'"><a class="btn btn-danger mini" href="javascript:" onclick="submitForm(this)"><i class="icon-trash icon-white"></i></a></form>');
                                }
                            } );

                        })
                        function submitForm(elem){
                            if(confirm("Are you sure?"))
                                    $(elem).closest("form").submit();
                        }
                    </script>
                </div>
            </div><!--/span-->

        </div>
    </div>
</body>
</html>
