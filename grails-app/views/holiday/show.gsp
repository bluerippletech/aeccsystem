
<%@ page import="com.ripple.master.Holiday" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'holiday.label', default: 'Holiday')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-holiday" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="description-label" class="control-label"><g:message
                                                    code="holiday.description.label"
                                                    default="Description"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="description-label"><g:fieldValue
                                                        bean="${holidayInstance}" field="description"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="applicableSites-label" class="control-label"><g:message
                                                    code="holiday.applicableSites.label"
                                                    default="Applicable Sites"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <g:each in="${holidayInstance.applicableSites}" var="a">
                                                    <span class="property-value" aria-labelledby="applicableSites-label"><g:link
                                                            controller="assignmentLocation" action="show"
                                                            id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                            <div class="span6">
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="holidayDate-label" class="control-label"><g:message
                                                    code="holiday.holidayDate.label"
                                                    default="Holiday Date"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="holidayDate-label"><g:fieldValue
                                                        bean="${holidayInstance}" field="holidayDate"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="legalHoliday-label" class="control-label"><g:message
                                                    code="holiday.legalHoliday.label"
                                                    default="Legal Holiday"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                
                                                <span class="property-value" aria-labelledby="legalHoliday-label"><g:formatBoolean
                                                        boolean="${holidayInstance?.legalHoliday}"/></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${holidayInstance?.id}"/>
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
