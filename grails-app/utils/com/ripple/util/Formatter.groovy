package com.ripple.util

import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

import java.text.DecimalFormat

class Formatter {

    public static String formatCurrency(value) {
        DecimalFormat myFormatter = new DecimalFormat("P #,##0.00");
        return myFormatter.format(value)
    }

    public static String formatNumber(numberFormat, value) {
        DecimalFormat myFormatter = new DecimalFormat(numberFormat)
        return myFormatter.format(value)
    }

    public static String formatLocalDate(aLocalDate, sFormat) {
        if (sFormat == null || sFormat == "")
            sFormat = "M/d/yy"
        if (aLocalDate == null)
            return ""
        DateTimeFormatter fmt = DateTimeFormat.forPattern(sFormat);
        String formattedDate = aLocalDate.toString(fmt);
        return formattedDate
    }

    public static String formatDate(aDate, sFormat) {
        if (sFormat == null || sFormat == "")
            sFormat = "M/d/yy"
        if (aDate == null)
            return ""
        return aDate.format(sFormat)
    }
}

