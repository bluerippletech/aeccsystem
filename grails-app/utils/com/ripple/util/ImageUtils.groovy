package com.ripple.util

class ImageUtils {

    def burningImageService

    public byte[] resizeImage(value) {
//        def imageTool = new org.grails.plugins.imagetools.ImageTool()

        if (value?.size()==0||value==null) {
            return null
        } else {
//            imageTool.load(value)
//            imageTool.saveOriginal()
//            imageTool.thumbnail(640)
//            return imageTool.getBytes("JPEG")
            def file = request.getFile(value)
              burningImageService.doWith(file, 'images/')
                .execute {
                  it.scaleApproximate(640, 480)
              }


              }
        }
    }