package com.ripple.util;

/**
 * Created by IntelliJ IDEA.
 * User: onats
 * Date: Feb 18, 2010
 * Time: 8:11:55 PM
 * To change this template use File | Settings | File Templates.
 */

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.beans.PropertyEditorSupport;

public class LocalDateEditor extends PropertyEditorSupport {

    private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");

    public String getAsText() {
        return ((LocalDate) getValue()).toString(formatter);
    }

    public void setAsText(String value) {
        if ("".equals(value))
            setValue(null);
        else
            setValue(new LocalDate(formatter.parseMillis(value)));
    }
}


