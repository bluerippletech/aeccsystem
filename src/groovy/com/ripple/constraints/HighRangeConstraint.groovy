package com.ripple.constraints

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors

class HighRangeConstraint extends AbstractConstraint {

    private static final String DEFAULT_MESSAGE_CODE = "default.invalid.validator.message"
    private static final String[] HIGH_RANGE_MESSAGE_CODES = ["not.high.range.error", "range.error"]

    public static final String NAME = "highRange"

    private Boolean highRange

    @Override
    public void setParameter(Object constraintParameter) {
        if (!(constraintParameter instanceof Boolean))
            throw new IllegalArgumentException("Parameter for constraint ["
                    + NAME + "] of property ["
                    + constraintPropertyName + "] of class ["
                    + constraintOwningClass + "] must be a boolean value");
        this.highRange = ((Boolean) constraintParameter)
        super.setParameter(constraintParameter);
    }

    @Override
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
        if (highRange && !validate(target, propertyValue)) {
            def args = (Object[]) [constraintPropertyName, constraintOwningClass,
                propertyValue, target.getLowRange()]

            super.rejectValue(target, errors, DEFAULT_MESSAGE_CODE,
                HIGH_RANGE_MESSAGE_CODES, args)
        }
    }

    @Override
    boolean supports(Class type) {
        return type != null && BigDecimal.class.isAssignableFrom(type);
    }

    @Override
    String getName() {
        return NAME;
    }

    @Override
    boolean validate(target, propertyValue) {
        def getLowRange = 'getLowRange'
        def getHighRange = 'getHighRange'
        if (target.metaClass.respondsTo(target, getLowRange) &&
            target.metaClass.respondsTo(target, getHighRange)) {
            return target.getLowRange() <= target.getHighRange()
        }
        else {
            throw new IllegalArgumentException("Class [" + constraintOwningClass
                    + "] must have [" + getLowRange
                    + "] and [" + getHighRange + "] MetaMethods");
        }
    }

}