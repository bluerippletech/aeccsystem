package com.ripple.constraints

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors

class OverlapConstraint extends AbstractConstraint {

    private static final String DEFAULT_MESSAGE_CODE = "default.invalid.validator.message"
    private static final String[] MUST_OVERLAP_MESSAGE_CODES = ["did.not.overlap.error", "overlap.error"]
    private static final String[] MUST_NOT_OVERLAP_MESSAGE_CODES = ["did.overlap.error", "overlap.error"]
    
    public static final String NAME = "overlap"

    private Boolean mustOverlap

    @Override
    public void setParameter(Object constraintParameter) {
        if (!(constraintParameter instanceof Boolean))
            throw new IllegalArgumentException("Parameter for constraint ["
                    + NAME + "] of property ["
                    + constraintPropertyName + "] of class ["
                    + constraintOwningClass + "] must be a boolean value");
        this.mustOverlap = ((Boolean) constraintParameter)
        super.setParameter(constraintParameter);
    }

    @Override
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
        def entryOverlap = getEntryOverlap(target, propertyValue)
        // If target is empty, ignore overlap checking...
        if (target.count() == 0)
            return
        // Truth Table
        // must not overlap / there is no overlap => false xor false = false
        // must not overlap / there is overlap    => false xor true  = true
        // must overlap / there is no overlap     => true xor false  = true
        // must overlap / there is overlap        => true xor true   = false
        if (mustOverlap.xor(entryOverlap != null))
        {
            def args = (Object[]) [constraintPropertyName, constraintOwningClass,
                propertyValue, entryOverlap.getLowRange(), entryOverlap.getHighRange()]

            super.rejectValue(target, errors, DEFAULT_MESSAGE_CODE,
                mustOverlap ? MUST_OVERLAP_MESSAGE_CODES : MUST_NOT_OVERLAP_MESSAGE_CODES,
                args)
        }
    }

    @Override
    boolean supports(Class type) {
        return type != null && BigDecimal.class.isAssignableFrom(type);
    }

    @Override
    String getName() {
        return NAME;
    }

    Object getEntryOverlap(target, propertyValue) {
        def getLowRange = 'getLowRange'
        def getHighRange = 'getHighRange'
        if (target.metaClass.respondsTo(target, getLowRange) &&
            target.metaClass.respondsTo(target, getHighRange)) {
            return target.list().find { entry ->
                entry.metaClass.overlap { value ->
                    value >= entry.getLowRange() && value <= entry.getHighRange()
                }
                entry.overlap(propertyValue)
            }
        }
        else {
            throw new IllegalArgumentException("Class [" + constraintOwningClass
                    + "] must have [" + getLowRange
                    + "] and [" + getHighRange + "] MetaMethods");
        }
    }

}