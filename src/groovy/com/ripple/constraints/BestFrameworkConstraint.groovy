package com.ripple.constraints

import org.codehaus.groovy.grails.validation.AbstractConstraint
import org.springframework.validation.Errors

class BestFrameworkConstraint extends AbstractConstraint {

    private static final String DEFAULT_MESSAGE_CODE = "default.invalid.validator.message";
    public static final String NAME = "oneCorrectResponse";

    private boolean validateConstraint

    //The parameter which the constraint is validated against:
    @Override
    public void setParameter(Object constraintParameter) {
        if (!(constraintParameter instanceof Boolean))
            throw new IllegalArgumentException("Parameter for constraint ["
                    + NAME + "] of property ["
                    + constraintPropertyName + "] of class ["
                    + constraintOwningClass + "] must be a boolean value");
        this.validateConstraint = ((Boolean) constraintParameter).booleanValue()
        super.setParameter(constraintParameter);
    }

    //Returns the default message for the given message code in the current locale:
    @Override
    protected void processValidate(Object target, Object propertyValue, Errors errors) {
        if (validateConstraint && !validate(target, propertyValue)) {
            def args = (Object[]) [constraintPropertyName, constraintOwningClass,
                    propertyValue]
            super.rejectValue(target, errors, DEFAULT_MESSAGE_CODE,
                    "not." + NAME, args);
        }
    }

    //Returns whether the constraint supports being applied against the specified type:
    @Override
    boolean supports(Class type) {
        return type != null && String.class.isAssignableFrom(type);
    }

    //The name of the constraint, which the user of the plugin will use
    //when working with your plugin.
    @Override
    String getName() {
        return NAME;
    }

    //Validate this constraint against a property value,
    //In this case, ONLY "Grails" is valid, everything else will cause an error:
    @Override
    boolean validate(target, propertyValue) {
        propertyValue ==~ /^Grails$/
    }

}