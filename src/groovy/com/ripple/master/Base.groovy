package com.ripple.master

//	AECCSystem - Grails Core Functionalities
//	03.	Logical Deletion
//	03a.	Base Class with isDeleted="No"
//	03b.	Show Records with isDeleted=�No� only

abstract class Base{

	String id
    String isDeleted="No"	//03a

    Date createdDate
    Date editedDate
    String editedBy
    String createdBy

    static auditable = [ignore:['version','createdDate','editedDate','editedBy','createdBy']]

	static mapping = {
		id generator:'uuid'
		cache true
	}
	
    static constraints = {
        isDeleted(inList:['Yes','No'])
        createdDate(nullable:true)
        editedDate(nullable:true)
        editedBy(nullable:true)
        createdBy(nullable:true)
    }
}
