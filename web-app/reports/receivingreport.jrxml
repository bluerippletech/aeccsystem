<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE jasperReport PUBLIC "-//JasperReports//DTD JasperReport//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">

<jasperReport name="receivingreport.jrxml" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" isIgnorePagination="true">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="table">
		<box>
			<pen lineWidth="1.0" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="table_TD" mode="Opaque" backcolor="#FFFFFF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<parameter name="RECEIVING_REPORT_ID" class="java.lang.String">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
     date_format(receiving_report.`date_started`,'%M %d, %Y') AS receiving_report_date_started,
     date_format(receiving_report.`date_started`,'%m/%d/%Y') AS received_date,
     date_format(receiving_report.`date_closed`,'%M %d, %Y') AS receiving_report_date_closed,
     (receiving_report_line_item.`unit_price`*receiving_report_line_item.`quantity`) AS item_amount,
     IFNULL(date_format(receiving_report.`date_checked_supplier_invoice`,'%m/%d/%Y'),'') AS receiving_report_date_checked_supplier_invoice,
     IFNULL(date_format(receiving_report.`date_checked_purchase_order`,'%m/%d/%Y'),'') AS receiving_report_date_checked_purchase_order,
     receiving_report.`receiving_report_number` AS receiving_report_receiving_report_number,
     receiving_report.`invoice_number` AS receiving_report_invoice_number,
     receiving_report_line_item.`quantity` AS receiving_report_line_item_quantity,
     receiving_report_line_item.`unit_price` AS receiving_report_line_item_unit_price,
     item.`item_code` AS item_item_code,
     unit_of_measure.`name` AS unit_of_measure_name,
     purchase_order.`purchase_order_number` AS purchase_order_purchase_order_number,
     supplier.`supplier_name` AS supplier_supplier_name,
     IFNULL(receiving_report.`created_by`,'') AS receiving_report_created_by,
     IFNULL(purchase_order_checker.username,'') AS po_checker,
     IFNULL(supplier_invoice_checker.username,'') AS invoice_checker
FROM
     `receiving_report` receiving_report INNER JOIN `receiving_report_line_item` receiving_report_line_item ON receiving_report.`id` = receiving_report_line_item.`receiving_report_id`
     INNER JOIN `item` item ON receiving_report_line_item.`item_id` = item.`id`
     INNER JOIN `level_unit` level_unit ON receiving_report_line_item.`unit_of_measure_id` = level_unit.`id`
     INNER JOIN `unit_of_measure` unit_of_measure ON level_unit.`unit_of_measure_id` = unit_of_measure.`id`
     INNER JOIN `purchase_order` purchase_order ON receiving_report.`purchase_order_id` = purchase_order.`id`
     INNER JOIN `supplier` supplier ON purchase_order.`supplier_id` = supplier.`id`
     JOIN (SELECT app_user.username FROM app_user RIGHT JOIN receiving_report ON receiving_report.purchase_order_checked_by_id=app_user.id WHERE receiving_report.id=$P{RECEIVING_REPORT_ID}) as purchase_order_checker
     JOIN (SELECT app_user.username FROM app_user RIGHT JOIN receiving_report ON receiving_report.supplier_invoice_checked_by_id=app_user.id WHERE receiving_report.id=$P{RECEIVING_REPORT_ID}) as supplier_invoice_checker
WHERE
     receiving_report.`id` = $P{RECEIVING_REPORT_ID}]]>
	</queryString>
	<field name="receiving_report_date_started" class="java.lang.String"/>
	<field name="received_date" class="java.lang.String"/>
	<field name="receiving_report_date_closed" class="java.lang.String"/>
	<field name="item_amount" class="java.math.BigDecimal"/>
	<field name="receiving_report_date_checked_supplier_invoice" class="java.lang.String"/>
	<field name="receiving_report_date_checked_purchase_order" class="java.lang.String"/>
	<field name="receiving_report_receiving_report_number" class="java.lang.String"/>
	<field name="receiving_report_invoice_number" class="java.lang.String"/>
	<field name="receiving_report_line_item_quantity" class="java.lang.Long"/>
	<field name="receiving_report_line_item_unit_price" class="java.math.BigDecimal"/>
	<field name="item_item_code" class="java.lang.String"/>
	<field name="unit_of_measure_name" class="java.lang.String"/>
	<field name="purchase_order_purchase_order_number" class="java.lang.String"/>
	<field name="supplier_supplier_name" class="java.lang.String"/>
	<field name="receiving_report_created_by" class="java.lang.String"/>
	<field name="po_checker" class="java.lang.String"/>
	<field name="invoice_checker" class="java.lang.String"/>
	<variable name="total_amount" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{item_amount}]]></variableExpression>
	</variable>
	<group name="Location Group">
		<groupHeader>
			<band height="133" isSplitAllowed="false">
				<staticText>
					<reportElement mode="Opaque" x="295" y="0" width="165" height="20" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="14" isBold="true" isUnderline="true"/>
					</textElement>
					<text><![CDATA[RECEIVING REPORT]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="631" y="12" width="36" height="20" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Right">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Date]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="0" y="42" width="57" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[Supplier:]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="0" y="75" width="188" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[Transportation Company/Mode]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="57" y="119" width="96" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[UNIT]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="0" y="119" width="57" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[QUANTITY]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="159" y="119" width="447" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[DESCRIPTION]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="606" y="119" width="76" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[UNIT COST]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="692" y="119" width="110" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[AMOUNT]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="324" y="42" width="88" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[Inv. No/Dr No.]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="478" y="42" width="84" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[Batch No./Lot No.]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="653" y="42" width="106" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[Purchase Order No.]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="324" y="75" width="109" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[Way/Freight Bill No.]]></text>
				</staticText>
				<staticText>
					<reportElement mode="Opaque" x="562" y="75" width="109" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement>
						<font size="10"/>
					</textElement>
					<text><![CDATA[Freight]]></text>
				</staticText>
				<textField hyperlinkType="None">
					<reportElement x="671" y="12" width="133" height="20"/>
					<textElement textAlignment="Right"/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{receiving_report_date_started}]]></textFieldExpression>
				</textField>
				<textField hyperlinkType="None">
					<reportElement x="28" y="55" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{supplier_supplier_name}]]></textFieldExpression>
				</textField>
				<textField hyperlinkType="None">
					<reportElement x="682" y="55" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{purchase_order_purchase_order_number}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="106">
				<staticText>
					<reportElement x="639" y="0" width="53" height="14"/>
					<textElement textAlignment="Right"/>
					<text><![CDATA[TOTAL -]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="17" y="28" width="121" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Received & Inspected]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="295" y="28" width="165" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Checked and Verified with]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="188" y="43" width="165" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Purchase Order]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="412" y="42" width="165" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Supplier Invoice]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="615" y="22" width="67" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Posted By:]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="653" y="67" width="67" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left">
						<font size="10"/>
					</textElement>
					<text><![CDATA[RR NO.]]></text>
				</staticText>
				<line>
					<reportElement x="692" y="80" width="110" height="1"/>
					<graphicElement/>
				</line>
				<textField hyperlinkType="None">
					<reportElement x="707" y="67" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{receiving_report_receiving_report_number}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="0" y="43" width="28" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[By:]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="87" y="43" width="28" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Date:]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="283" y="57" width="28" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Date:]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="166" y="57" width="28" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[By:]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="379" y="57" width="28" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[By:]]></text>
				</staticText>
				<staticText>
					<reportElement isPrintRepeatedValues="false" mode="Opaque" x="511" y="57" width="28" height="14" isPrintInFirstWholeBand="true" forecolor="#000033" backcolor="#FFFFFF"/>
					<textElement textAlignment="Center">
						<font size="10"/>
					</textElement>
					<text><![CDATA[Date:]]></text>
				</staticText>
				<textField hyperlinkType="None">
					<reportElement x="87" y="80" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{received_date}]]></textFieldExpression>
				</textField>
				<textField pattern="¤ #,##0.00" hyperlinkType="None">
					<reportElement x="692" y="0" width="110" height="20"/>
					<textElement textAlignment="Right"/>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{total_amount}]]></textFieldExpression>
				</textField>
				<textField hyperlinkType="None">
					<reportElement x="283" y="80" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{receiving_report_date_checked_purchase_order}]]></textFieldExpression>
				</textField>
				<textField hyperlinkType="None">
					<reportElement x="521" y="80" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{receiving_report_date_checked_supplier_invoice}]]></textFieldExpression>
				</textField>
				<textField hyperlinkType="None">
					<reportElement x="0" y="80" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{receiving_report_created_by}]]></textFieldExpression>
				</textField>
				<textField hyperlinkType="None">
					<reportElement x="383" y="80" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{invoice_checker}]]></textFieldExpression>
				</textField>
				<textField hyperlinkType="None">
					<reportElement x="176" y="80" width="100" height="20"/>
					<textElement/>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{po_checker}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band/>
	</background>
	<title>
		<band height="101">
			<staticText>
				<reportElement mode="Opaque" x="737" y="0" width="65" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="10"/>
				</textElement>
				<text><![CDATA[FM-PUR-07]]></text>
			</staticText>
			<staticText>
				<reportElement x="23" y="47" width="719" height="16"/>
				<textElement textAlignment="Center">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[ALGON ENGINEERING CONSTRUCTION CORPORATION]]></text>
			</staticText>
			<staticText>
				<reportElement x="23" y="63" width="719" height="16"/>
				<textElement textAlignment="Center">
					<font size="12" isBold="true"/>
				</textElement>
				<text><![CDATA[Algon Building, 159 J. P. Cabaguio Avenue, Davao City]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="632" y="0" width="105" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
				<textElement>
					<font size="10"/>
				</textElement>
				<text><![CDATA[Document No.        :]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="632" y="14" width="105" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
				<textElement>
					<font size="10"/>
				</textElement>
				<text><![CDATA[Revision No.           :]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="632" y="28" width="105" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
				<textElement>
					<font size="10"/>
				</textElement>
				<text><![CDATA[Effective Date        :]]></text>
			</staticText>
			<image hyperlinkType="None">
				<reportElement x="138" y="28" width="50" height="48"/>
				<graphicElement/>
				<imageExpression class="java.lang.String"><![CDATA["algon-logo.jpg"]]></imageExpression>
			</image>
			<staticText>
				<reportElement mode="Opaque" x="737" y="28" width="65" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="10"/>
				</textElement>
				<text><![CDATA[08/17/2009]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="737" y="14" width="65" height="14" forecolor="#000033" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left">
					<font size="10"/>
				</textElement>
				<text><![CDATA[1]]></text>
			</staticText>
			<image hyperlinkType="None">
				<reportElement x="571" y="28" width="50" height="59"/>
				<graphicElement/>
				<imageExpression class="java.lang.String"><![CDATA["tuvlogo.jpg"]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band/>
	</pageHeader>
	<columnHeader>
		<band/>
	</columnHeader>
	<detail>
		<band height="27" isSplitAllowed="false">
			<textField hyperlinkType="None">
				<reportElement x="0" y="4" width="57" height="20"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression class="java.lang.Long"><![CDATA[$F{receiving_report_line_item_quantity}]]></textFieldExpression>
			</textField>
			<textField hyperlinkType="None">
				<reportElement x="153" y="4" width="453" height="20"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{item_item_code}]]></textFieldExpression>
			</textField>
			<textField hyperlinkType="None">
				<reportElement x="57" y="4" width="96" height="20"/>
				<textElement textAlignment="Center"/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{unit_of_measure_name}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" hyperlinkType="None">
				<reportElement x="606" y="4" width="79" height="20"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{receiving_report_line_item_unit_price}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00" hyperlinkType="None">
				<reportElement x="692" y="4" width="110" height="20"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{item_amount}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band/>
	</columnFooter>
	<pageFooter>
		<band height="15"/>
	</pageFooter>
	<summary>
		<band/>
	</summary>
</jasperReport>
