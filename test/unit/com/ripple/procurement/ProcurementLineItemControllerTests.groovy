package com.ripple.procurement



import org.junit.*
import grails.test.mixin.*

@TestFor(ProcurementLineItemController)
@Mock(ProcurementLineItem)
class ProcurementLineItemControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/procurementLineItem/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.procurementLineItemInstanceList.size() == 0
        assert model.procurementLineItemInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.procurementLineItemInstance != null
    }

    void testSave() {
        controller.save()

        assert model.procurementLineItemInstance != null
        assert view == '/procurementLineItem/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/procurementLineItem/show/1'
        assert controller.flash.message != null
        assert ProcurementLineItem.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/procurementLineItem/list'


        populateValidParams(params)
        def procurementLineItem = new ProcurementLineItem(params)

        assert procurementLineItem.save() != null

        params.id = procurementLineItem.id

        def model = controller.show()

        assert model.procurementLineItemInstance == procurementLineItem
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/procurementLineItem/list'


        populateValidParams(params)
        def procurementLineItem = new ProcurementLineItem(params)

        assert procurementLineItem.save() != null

        params.id = procurementLineItem.id

        def model = controller.edit()

        assert model.procurementLineItemInstance == procurementLineItem
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/procurementLineItem/list'

        response.reset()


        populateValidParams(params)
        def procurementLineItem = new ProcurementLineItem(params)

        assert procurementLineItem.save() != null

        // test invalid parameters in update
        params.id = procurementLineItem.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/procurementLineItem/edit"
        assert model.procurementLineItemInstance != null

        procurementLineItem.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/procurementLineItem/show/$procurementLineItem.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        procurementLineItem.clearErrors()

        populateValidParams(params)
        params.id = procurementLineItem.id
        params.version = -1
        controller.update()

        assert view == "/procurementLineItem/edit"
        assert model.procurementLineItemInstance != null
        assert model.procurementLineItemInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/procurementLineItem/list'

        response.reset()

        populateValidParams(params)
        def procurementLineItem = new ProcurementLineItem(params)

        assert procurementLineItem.save() != null
        assert ProcurementLineItem.count() == 1

        params.id = procurementLineItem.id

        controller.delete()

        assert ProcurementLineItem.count() == 0
        assert ProcurementLineItem.get(procurementLineItem.id) == null
        assert response.redirectedUrl == '/procurementLineItem/list'
    }
}
