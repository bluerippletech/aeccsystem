package com.ripple.procurement



import org.junit.*
import grails.test.mixin.*

@TestFor(MaterialIssuanceClassController)
@Mock(MaterialIssuanceClass)
class MaterialIssuanceClassControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/materialIssuanceClass/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.materialIssuanceClassInstanceList.size() == 0
        assert model.materialIssuanceClassInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.materialIssuanceClassInstance != null
    }

    void testSave() {
        controller.save()

        assert model.materialIssuanceClassInstance != null
        assert view == '/materialIssuanceClass/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/materialIssuanceClass/show/1'
        assert controller.flash.message != null
        assert MaterialIssuanceClass.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/materialIssuanceClass/list'


        populateValidParams(params)
        def materialIssuanceClass = new MaterialIssuanceClass(params)

        assert materialIssuanceClass.save() != null

        params.id = materialIssuanceClass.id

        def model = controller.show()

        assert model.materialIssuanceClassInstance == materialIssuanceClass
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/materialIssuanceClass/list'


        populateValidParams(params)
        def materialIssuanceClass = new MaterialIssuanceClass(params)

        assert materialIssuanceClass.save() != null

        params.id = materialIssuanceClass.id

        def model = controller.edit()

        assert model.materialIssuanceClassInstance == materialIssuanceClass
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/materialIssuanceClass/list'

        response.reset()


        populateValidParams(params)
        def materialIssuanceClass = new MaterialIssuanceClass(params)

        assert materialIssuanceClass.save() != null

        // test invalid parameters in update
        params.id = materialIssuanceClass.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/materialIssuanceClass/edit"
        assert model.materialIssuanceClassInstance != null

        materialIssuanceClass.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/materialIssuanceClass/show/$materialIssuanceClass.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        materialIssuanceClass.clearErrors()

        populateValidParams(params)
        params.id = materialIssuanceClass.id
        params.version = -1
        controller.update()

        assert view == "/materialIssuanceClass/edit"
        assert model.materialIssuanceClassInstance != null
        assert model.materialIssuanceClassInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/materialIssuanceClass/list'

        response.reset()

        populateValidParams(params)
        def materialIssuanceClass = new MaterialIssuanceClass(params)

        assert materialIssuanceClass.save() != null
        assert MaterialIssuanceClass.count() == 1

        params.id = materialIssuanceClass.id

        controller.delete()

        assert MaterialIssuanceClass.count() == 0
        assert MaterialIssuanceClass.get(materialIssuanceClass.id) == null
        assert response.redirectedUrl == '/materialIssuanceClass/list'
    }
}
