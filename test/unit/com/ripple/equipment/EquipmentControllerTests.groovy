package com.ripple.equipment



import org.junit.*
import grails.test.mixin.*

@TestFor(EquipmentController)
@Mock(Equipment)
class EquipmentControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/equipment/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.equipmentInstanceList.size() == 0
        assert model.equipmentInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.equipmentInstance != null
    }

    void testSave() {
        controller.save()

        assert model.equipmentInstance != null
        assert view == '/equipment/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/equipment/show/1'
        assert controller.flash.message != null
        assert Equipment.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/equipment/list'


        populateValidParams(params)
        def equipment = new Equipment(params)

        assert equipment.save() != null

        params.id = equipment.id

        def model = controller.show()

        assert model.equipmentInstance == equipment
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/equipment/list'


        populateValidParams(params)
        def equipment = new Equipment(params)

        assert equipment.save() != null

        params.id = equipment.id

        def model = controller.edit()

        assert model.equipmentInstance == equipment
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/equipment/list'

        response.reset()


        populateValidParams(params)
        def equipment = new Equipment(params)

        assert equipment.save() != null

        // test invalid parameters in update
        params.id = equipment.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/equipment/edit"
        assert model.equipmentInstance != null

        equipment.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/equipment/show/$equipment.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        equipment.clearErrors()

        populateValidParams(params)
        params.id = equipment.id
        params.version = -1
        controller.update()

        assert view == "/equipment/edit"
        assert model.equipmentInstance != null
        assert model.equipmentInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/equipment/list'

        response.reset()

        populateValidParams(params)
        def equipment = new Equipment(params)

        assert equipment.save() != null
        assert Equipment.count() == 1

        params.id = equipment.id

        controller.delete()

        assert Equipment.count() == 0
        assert Equipment.get(equipment.id) == null
        assert response.redirectedUrl == '/equipment/list'
    }
}
