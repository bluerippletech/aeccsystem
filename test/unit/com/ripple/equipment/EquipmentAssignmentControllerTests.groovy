package com.ripple.equipment



import org.junit.*
import grails.test.mixin.*

@TestFor(EquipmentAssignmentController)
@Mock(EquipmentAssignment)
class EquipmentAssignmentControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/equipmentAssignment/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.equipmentAssignmentInstanceList.size() == 0
        assert model.equipmentAssignmentInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.equipmentAssignmentInstance != null
    }

    void testSave() {
        controller.save()

        assert model.equipmentAssignmentInstance != null
        assert view == '/equipmentAssignment/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/equipmentAssignment/show/1'
        assert controller.flash.message != null
        assert EquipmentAssignment.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/equipmentAssignment/list'


        populateValidParams(params)
        def equipmentAssignment = new EquipmentAssignment(params)

        assert equipmentAssignment.save() != null

        params.id = equipmentAssignment.id

        def model = controller.show()

        assert model.equipmentAssignmentInstance == equipmentAssignment
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/equipmentAssignment/list'


        populateValidParams(params)
        def equipmentAssignment = new EquipmentAssignment(params)

        assert equipmentAssignment.save() != null

        params.id = equipmentAssignment.id

        def model = controller.edit()

        assert model.equipmentAssignmentInstance == equipmentAssignment
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/equipmentAssignment/list'

        response.reset()


        populateValidParams(params)
        def equipmentAssignment = new EquipmentAssignment(params)

        assert equipmentAssignment.save() != null

        // test invalid parameters in update
        params.id = equipmentAssignment.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/equipmentAssignment/edit"
        assert model.equipmentAssignmentInstance != null

        equipmentAssignment.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/equipmentAssignment/show/$equipmentAssignment.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        equipmentAssignment.clearErrors()

        populateValidParams(params)
        params.id = equipmentAssignment.id
        params.version = -1
        controller.update()

        assert view == "/equipmentAssignment/edit"
        assert model.equipmentAssignmentInstance != null
        assert model.equipmentAssignmentInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/equipmentAssignment/list'

        response.reset()

        populateValidParams(params)
        def equipmentAssignment = new EquipmentAssignment(params)

        assert equipmentAssignment.save() != null
        assert EquipmentAssignment.count() == 1

        params.id = equipmentAssignment.id

        controller.delete()

        assert EquipmentAssignment.count() == 0
        assert EquipmentAssignment.get(equipmentAssignment.id) == null
        assert response.redirectedUrl == '/equipmentAssignment/list'
    }
}
