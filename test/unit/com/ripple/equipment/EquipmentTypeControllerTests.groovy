package com.ripple.equipment



import org.junit.*
import grails.test.mixin.*

@TestFor(EquipmentTypeController)
@Mock(EquipmentType)
class EquipmentTypeControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/equipmentType/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.equipmentTypeInstanceList.size() == 0
        assert model.equipmentTypeInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.equipmentTypeInstance != null
    }

    void testSave() {
        controller.save()

        assert model.equipmentTypeInstance != null
        assert view == '/equipmentType/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/equipmentType/show/1'
        assert controller.flash.message != null
        assert EquipmentType.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/equipmentType/list'


        populateValidParams(params)
        def equipmentType = new EquipmentType(params)

        assert equipmentType.save() != null

        params.id = equipmentType.id

        def model = controller.show()

        assert model.equipmentTypeInstance == equipmentType
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/equipmentType/list'


        populateValidParams(params)
        def equipmentType = new EquipmentType(params)

        assert equipmentType.save() != null

        params.id = equipmentType.id

        def model = controller.edit()

        assert model.equipmentTypeInstance == equipmentType
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/equipmentType/list'

        response.reset()


        populateValidParams(params)
        def equipmentType = new EquipmentType(params)

        assert equipmentType.save() != null

        // test invalid parameters in update
        params.id = equipmentType.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/equipmentType/edit"
        assert model.equipmentTypeInstance != null

        equipmentType.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/equipmentType/show/$equipmentType.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        equipmentType.clearErrors()

        populateValidParams(params)
        params.id = equipmentType.id
        params.version = -1
        controller.update()

        assert view == "/equipmentType/edit"
        assert model.equipmentTypeInstance != null
        assert model.equipmentTypeInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/equipmentType/list'

        response.reset()

        populateValidParams(params)
        def equipmentType = new EquipmentType(params)

        assert equipmentType.save() != null
        assert EquipmentType.count() == 1

        params.id = equipmentType.id

        controller.delete()

        assert EquipmentType.count() == 0
        assert EquipmentType.get(equipmentType.id) == null
        assert response.redirectedUrl == '/equipmentType/list'
    }
}
