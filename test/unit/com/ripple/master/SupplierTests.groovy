package com.ripple.master



import grails.test.mixin.*

import com.ripple.master.supplier.Supplier

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Supplier)
class SupplierTests {

    void testSomething() {
        fail "Implement me"
    }
}
