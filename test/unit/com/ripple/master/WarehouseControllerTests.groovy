package com.ripple.master



import org.junit.*
import grails.test.mixin.*

@TestFor(WarehouseController)
@Mock(Warehouse)
class WarehouseControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/warehouse/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.warehouseInstanceList.size() == 0
        assert model.warehouseInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.warehouseInstance != null
    }

    void testSave() {
        controller.save()

        assert model.warehouseInstance != null
        assert view == '/warehouse/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/warehouse/show/1'
        assert controller.flash.message != null
        assert Warehouse.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/warehouse/list'


        populateValidParams(params)
        def warehouse = new Warehouse(params)

        assert warehouse.save() != null

        params.id = warehouse.id

        def model = controller.show()

        assert model.warehouseInstance == warehouse
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/warehouse/list'


        populateValidParams(params)
        def warehouse = new Warehouse(params)

        assert warehouse.save() != null

        params.id = warehouse.id

        def model = controller.edit()

        assert model.warehouseInstance == warehouse
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/warehouse/list'

        response.reset()


        populateValidParams(params)
        def warehouse = new Warehouse(params)

        assert warehouse.save() != null

        // test invalid parameters in update
        params.id = warehouse.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/warehouse/edit"
        assert model.warehouseInstance != null

        warehouse.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/warehouse/show/$warehouse.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        warehouse.clearErrors()

        populateValidParams(params)
        params.id = warehouse.id
        params.version = -1
        controller.update()

        assert view == "/warehouse/edit"
        assert model.warehouseInstance != null
        assert model.warehouseInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/warehouse/list'

        response.reset()

        populateValidParams(params)
        def warehouse = new Warehouse(params)

        assert warehouse.save() != null
        assert Warehouse.count() == 1

        params.id = warehouse.id

        controller.delete()

        assert Warehouse.count() == 0
        assert Warehouse.get(warehouse.id) == null
        assert response.redirectedUrl == '/warehouse/list'
    }
}
