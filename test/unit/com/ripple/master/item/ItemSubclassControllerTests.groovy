package com.ripple.master.item



import org.junit.*
import grails.test.mixin.*

@TestFor(ItemSubclassController)
@Mock(ItemSubclass)
class ItemSubclassControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/itemSubclass/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.itemSubclassInstanceList.size() == 0
        assert model.itemSubclassInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.itemSubclassInstance != null
    }

    void testSave() {
        controller.save()

        assert model.itemSubclassInstance != null
        assert view == '/itemSubclass/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/itemSubclass/show/1'
        assert controller.flash.message != null
        assert ItemSubclass.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/itemSubclass/list'


        populateValidParams(params)
        def itemSubclass = new ItemSubclass(params)

        assert itemSubclass.save() != null

        params.id = itemSubclass.id

        def model = controller.show()

        assert model.itemSubclassInstance == itemSubclass
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/itemSubclass/list'


        populateValidParams(params)
        def itemSubclass = new ItemSubclass(params)

        assert itemSubclass.save() != null

        params.id = itemSubclass.id

        def model = controller.edit()

        assert model.itemSubclassInstance == itemSubclass
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/itemSubclass/list'

        response.reset()


        populateValidParams(params)
        def itemSubclass = new ItemSubclass(params)

        assert itemSubclass.save() != null

        // test invalid parameters in update
        params.id = itemSubclass.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/itemSubclass/edit"
        assert model.itemSubclassInstance != null

        itemSubclass.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/itemSubclass/show/$itemSubclass.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        itemSubclass.clearErrors()

        populateValidParams(params)
        params.id = itemSubclass.id
        params.version = -1
        controller.update()

        assert view == "/itemSubclass/edit"
        assert model.itemSubclassInstance != null
        assert model.itemSubclassInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/itemSubclass/list'

        response.reset()

        populateValidParams(params)
        def itemSubclass = new ItemSubclass(params)

        assert itemSubclass.save() != null
        assert ItemSubclass.count() == 1

        params.id = itemSubclass.id

        controller.delete()

        assert ItemSubclass.count() == 0
        assert ItemSubclass.get(itemSubclass.id) == null
        assert response.redirectedUrl == '/itemSubclass/list'
    }
}
