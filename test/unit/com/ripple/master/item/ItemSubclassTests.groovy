package com.ripple.master.item



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ItemSubclass)
class ItemSubclassTests {

    void testSomething() {
        fail "Implement me"
    }
}
