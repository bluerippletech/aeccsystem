package com.ripple.master.item



import org.junit.*
import grails.test.mixin.*

@TestFor(ItemClassController)
@Mock(ItemClass)
class ItemClassControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/itemClass/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.itemClassInstanceList.size() == 0
        assert model.itemClassInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.itemClassInstance != null
    }

    void testSave() {
        controller.save()

        assert model.itemClassInstance != null
        assert view == '/itemClass/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/itemClass/show/1'
        assert controller.flash.message != null
        assert ItemClass.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/itemClass/list'


        populateValidParams(params)
        def itemClass = new ItemClass(params)

        assert itemClass.save() != null

        params.id = itemClass.id

        def model = controller.show()

        assert model.itemClassInstance == itemClass
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/itemClass/list'


        populateValidParams(params)
        def itemClass = new ItemClass(params)

        assert itemClass.save() != null

        params.id = itemClass.id

        def model = controller.edit()

        assert model.itemClassInstance == itemClass
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/itemClass/list'

        response.reset()


        populateValidParams(params)
        def itemClass = new ItemClass(params)

        assert itemClass.save() != null

        // test invalid parameters in update
        params.id = itemClass.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/itemClass/edit"
        assert model.itemClassInstance != null

        itemClass.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/itemClass/show/$itemClass.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        itemClass.clearErrors()

        populateValidParams(params)
        params.id = itemClass.id
        params.version = -1
        controller.update()

        assert view == "/itemClass/edit"
        assert model.itemClassInstance != null
        assert model.itemClassInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/itemClass/list'

        response.reset()

        populateValidParams(params)
        def itemClass = new ItemClass(params)

        assert itemClass.save() != null
        assert ItemClass.count() == 1

        params.id = itemClass.id

        controller.delete()

        assert ItemClass.count() == 0
        assert ItemClass.get(itemClass.id) == null
        assert response.redirectedUrl == '/itemClass/list'
    }
}
