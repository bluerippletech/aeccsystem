package com.ripple.master



import org.junit.*
import grails.test.mixin.*

@TestFor(AssignmentHistoryController)
@Mock(AssignmentHistory)
class AssignmentHistoryControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/assignmentHistory/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.assignmentHistoryInstanceList.size() == 0
        assert model.assignmentHistoryInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.assignmentHistoryInstance != null
    }

    void testSave() {
        controller.save()

        assert model.assignmentHistoryInstance != null
        assert view == '/assignmentHistory/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/assignmentHistory/show/1'
        assert controller.flash.message != null
        assert AssignmentHistory.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/assignmentHistory/list'


        populateValidParams(params)
        def assignmentHistory = new AssignmentHistory(params)

        assert assignmentHistory.save() != null

        params.id = assignmentHistory.id

        def model = controller.show()

        assert model.assignmentHistoryInstance == assignmentHistory
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/assignmentHistory/list'


        populateValidParams(params)
        def assignmentHistory = new AssignmentHistory(params)

        assert assignmentHistory.save() != null

        params.id = assignmentHistory.id

        def model = controller.edit()

        assert model.assignmentHistoryInstance == assignmentHistory
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/assignmentHistory/list'

        response.reset()


        populateValidParams(params)
        def assignmentHistory = new AssignmentHistory(params)

        assert assignmentHistory.save() != null

        // test invalid parameters in update
        params.id = assignmentHistory.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/assignmentHistory/edit"
        assert model.assignmentHistoryInstance != null

        assignmentHistory.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/assignmentHistory/show/$assignmentHistory.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        assignmentHistory.clearErrors()

        populateValidParams(params)
        params.id = assignmentHistory.id
        params.version = -1
        controller.update()

        assert view == "/assignmentHistory/edit"
        assert model.assignmentHistoryInstance != null
        assert model.assignmentHistoryInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/assignmentHistory/list'

        response.reset()

        populateValidParams(params)
        def assignmentHistory = new AssignmentHistory(params)

        assert assignmentHistory.save() != null
        assert AssignmentHistory.count() == 1

        params.id = assignmentHistory.id

        controller.delete()

        assert AssignmentHistory.count() == 0
        assert AssignmentHistory.get(assignmentHistory.id) == null
        assert response.redirectedUrl == '/assignmentHistory/list'
    }
}
