package com.ripple.master



import org.junit.*
import grails.test.mixin.*

@TestFor(AssignmentLocationController)
@Mock(AssignmentLocation)
class AssignmentLocationControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/assignmentLocation/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.assignmentLocationInstanceList.size() == 0
        assert model.assignmentLocationInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.assignmentLocationInstance != null
    }

    void testSave() {
        controller.save()

        assert model.assignmentLocationInstance != null
        assert view == '/assignmentLocation/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/assignmentLocation/show/1'
        assert controller.flash.message != null
        assert AssignmentLocation.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/assignmentLocation/list'


        populateValidParams(params)
        def assignmentLocation = new AssignmentLocation(params)

        assert assignmentLocation.save() != null

        params.id = assignmentLocation.id

        def model = controller.show()

        assert model.assignmentLocationInstance == assignmentLocation
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/assignmentLocation/list'


        populateValidParams(params)
        def assignmentLocation = new AssignmentLocation(params)

        assert assignmentLocation.save() != null

        params.id = assignmentLocation.id

        def model = controller.edit()

        assert model.assignmentLocationInstance == assignmentLocation
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/assignmentLocation/list'

        response.reset()


        populateValidParams(params)
        def assignmentLocation = new AssignmentLocation(params)

        assert assignmentLocation.save() != null

        // test invalid parameters in update
        params.id = assignmentLocation.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/assignmentLocation/edit"
        assert model.assignmentLocationInstance != null

        assignmentLocation.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/assignmentLocation/show/$assignmentLocation.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        assignmentLocation.clearErrors()

        populateValidParams(params)
        params.id = assignmentLocation.id
        params.version = -1
        controller.update()

        assert view == "/assignmentLocation/edit"
        assert model.assignmentLocationInstance != null
        assert model.assignmentLocationInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/assignmentLocation/list'

        response.reset()

        populateValidParams(params)
        def assignmentLocation = new AssignmentLocation(params)

        assert assignmentLocation.save() != null
        assert AssignmentLocation.count() == 1

        params.id = assignmentLocation.id

        controller.delete()

        assert AssignmentLocation.count() == 0
        assert AssignmentLocation.get(assignmentLocation.id) == null
        assert response.redirectedUrl == '/assignmentLocation/list'
    }
}
