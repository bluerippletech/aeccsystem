package com.ripple.receiving



import org.junit.*
import grails.test.mixin.*

@TestFor(ReceivingReportLineItemController)
@Mock(ReceivingReportLineItem)
class ReceivingReportLineItemControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/receivingReportLineItem/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.receivingReportLineItemInstanceList.size() == 0
        assert model.receivingReportLineItemInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.receivingReportLineItemInstance != null
    }

    void testSave() {
        controller.save()

        assert model.receivingReportLineItemInstance != null
        assert view == '/receivingReportLineItem/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/receivingReportLineItem/show/1'
        assert controller.flash.message != null
        assert ReceivingReportLineItem.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/receivingReportLineItem/list'


        populateValidParams(params)
        def receivingReportLineItem = new ReceivingReportLineItem(params)

        assert receivingReportLineItem.save() != null

        params.id = receivingReportLineItem.id

        def model = controller.show()

        assert model.receivingReportLineItemInstance == receivingReportLineItem
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/receivingReportLineItem/list'


        populateValidParams(params)
        def receivingReportLineItem = new ReceivingReportLineItem(params)

        assert receivingReportLineItem.save() != null

        params.id = receivingReportLineItem.id

        def model = controller.edit()

        assert model.receivingReportLineItemInstance == receivingReportLineItem
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/receivingReportLineItem/list'

        response.reset()


        populateValidParams(params)
        def receivingReportLineItem = new ReceivingReportLineItem(params)

        assert receivingReportLineItem.save() != null

        // test invalid parameters in update
        params.id = receivingReportLineItem.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/receivingReportLineItem/edit"
        assert model.receivingReportLineItemInstance != null

        receivingReportLineItem.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/receivingReportLineItem/show/$receivingReportLineItem.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        receivingReportLineItem.clearErrors()

        populateValidParams(params)
        params.id = receivingReportLineItem.id
        params.version = -1
        controller.update()

        assert view == "/receivingReportLineItem/edit"
        assert model.receivingReportLineItemInstance != null
        assert model.receivingReportLineItemInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/receivingReportLineItem/list'

        response.reset()

        populateValidParams(params)
        def receivingReportLineItem = new ReceivingReportLineItem(params)

        assert receivingReportLineItem.save() != null
        assert ReceivingReportLineItem.count() == 1

        params.id = receivingReportLineItem.id

        controller.delete()

        assert ReceivingReportLineItem.count() == 0
        assert ReceivingReportLineItem.get(receivingReportLineItem.id) == null
        assert response.redirectedUrl == '/receivingReportLineItem/list'
    }
}
