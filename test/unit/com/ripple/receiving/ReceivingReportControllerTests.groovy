package com.ripple.receiving



import org.junit.*
import grails.test.mixin.*

@TestFor(ReceivingReportController)
@Mock(ReceivingReport)
class ReceivingReportControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/receivingReport/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.receivingReportInstanceList.size() == 0
        assert model.receivingReportInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.receivingReportInstance != null
    }

    void testSave() {
        controller.save()

        assert model.receivingReportInstance != null
        assert view == '/receivingReport/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/receivingReport/show/1'
        assert controller.flash.message != null
        assert ReceivingReport.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/receivingReport/list'


        populateValidParams(params)
        def receivingReport = new ReceivingReport(params)

        assert receivingReport.save() != null

        params.id = receivingReport.id

        def model = controller.show()

        assert model.receivingReportInstance == receivingReport
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/receivingReport/list'


        populateValidParams(params)
        def receivingReport = new ReceivingReport(params)

        assert receivingReport.save() != null

        params.id = receivingReport.id

        def model = controller.edit()

        assert model.receivingReportInstance == receivingReport
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/receivingReport/list'

        response.reset()


        populateValidParams(params)
        def receivingReport = new ReceivingReport(params)

        assert receivingReport.save() != null

        // test invalid parameters in update
        params.id = receivingReport.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/receivingReport/edit"
        assert model.receivingReportInstance != null

        receivingReport.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/receivingReport/show/$receivingReport.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        receivingReport.clearErrors()

        populateValidParams(params)
        params.id = receivingReport.id
        params.version = -1
        controller.update()

        assert view == "/receivingReport/edit"
        assert model.receivingReportInstance != null
        assert model.receivingReportInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/receivingReport/list'

        response.reset()

        populateValidParams(params)
        def receivingReport = new ReceivingReport(params)

        assert receivingReport.save() != null
        assert ReceivingReport.count() == 1

        params.id = receivingReport.id

        controller.delete()

        assert ReceivingReport.count() == 0
        assert ReceivingReport.get(receivingReport.id) == null
        assert response.redirectedUrl == '/receivingReport/list'
    }
}
