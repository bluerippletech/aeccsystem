package com.ripple.purchasing



import org.junit.*
import grails.test.mixin.*

@TestFor(PurchaseOrderLineItemController)
@Mock(PurchaseOrderLineItem)
class PurchaseOrderLineItemControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/purchaseOrderLineItem/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.purchaseOrderLineItemInstanceList.size() == 0
        assert model.purchaseOrderLineItemInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.purchaseOrderLineItemInstance != null
    }

    void testSave() {
        controller.save()

        assert model.purchaseOrderLineItemInstance != null
        assert view == '/purchaseOrderLineItem/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/purchaseOrderLineItem/show/1'
        assert controller.flash.message != null
        assert PurchaseOrderLineItem.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/purchaseOrderLineItem/list'


        populateValidParams(params)
        def purchaseOrderLineItem = new PurchaseOrderLineItem(params)

        assert purchaseOrderLineItem.save() != null

        params.id = purchaseOrderLineItem.id

        def model = controller.show()

        assert model.purchaseOrderLineItemInstance == purchaseOrderLineItem
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/purchaseOrderLineItem/list'


        populateValidParams(params)
        def purchaseOrderLineItem = new PurchaseOrderLineItem(params)

        assert purchaseOrderLineItem.save() != null

        params.id = purchaseOrderLineItem.id

        def model = controller.edit()

        assert model.purchaseOrderLineItemInstance == purchaseOrderLineItem
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/purchaseOrderLineItem/list'

        response.reset()


        populateValidParams(params)
        def purchaseOrderLineItem = new PurchaseOrderLineItem(params)

        assert purchaseOrderLineItem.save() != null

        // test invalid parameters in update
        params.id = purchaseOrderLineItem.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/purchaseOrderLineItem/edit"
        assert model.purchaseOrderLineItemInstance != null

        purchaseOrderLineItem.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/purchaseOrderLineItem/show/$purchaseOrderLineItem.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        purchaseOrderLineItem.clearErrors()

        populateValidParams(params)
        params.id = purchaseOrderLineItem.id
        params.version = -1
        controller.update()

        assert view == "/purchaseOrderLineItem/edit"
        assert model.purchaseOrderLineItemInstance != null
        assert model.purchaseOrderLineItemInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/purchaseOrderLineItem/list'

        response.reset()

        populateValidParams(params)
        def purchaseOrderLineItem = new PurchaseOrderLineItem(params)

        assert purchaseOrderLineItem.save() != null
        assert PurchaseOrderLineItem.count() == 1

        params.id = purchaseOrderLineItem.id

        controller.delete()

        assert PurchaseOrderLineItem.count() == 0
        assert PurchaseOrderLineItem.get(purchaseOrderLineItem.id) == null
        assert response.redirectedUrl == '/purchaseOrderLineItem/list'
    }
}
