package com.ripple.purchasing



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(PurchaseOrderLineItem)
class PurchaseOrderLineItemTests {

    void testSomething() {
        fail "Implement me"
    }
}
