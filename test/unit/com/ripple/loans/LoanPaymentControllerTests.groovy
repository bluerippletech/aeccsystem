package com.ripple.loans



import org.junit.*
import grails.test.mixin.*

@TestFor(LoanPaymentController)
@Mock(LoanPayment)
class LoanPaymentControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/loanPayment/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.loanPaymentInstanceList.size() == 0
        assert model.loanPaymentInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.loanPaymentInstance != null
    }

    void testSave() {
        controller.save()

        assert model.loanPaymentInstance != null
        assert view == '/loanPayment/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/loanPayment/show/1'
        assert controller.flash.message != null
        assert LoanPayment.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/loanPayment/list'


        populateValidParams(params)
        def loanPayment = new LoanPayment(params)

        assert loanPayment.save() != null

        params.id = loanPayment.id

        def model = controller.show()

        assert model.loanPaymentInstance == loanPayment
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/loanPayment/list'


        populateValidParams(params)
        def loanPayment = new LoanPayment(params)

        assert loanPayment.save() != null

        params.id = loanPayment.id

        def model = controller.edit()

        assert model.loanPaymentInstance == loanPayment
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/loanPayment/list'

        response.reset()


        populateValidParams(params)
        def loanPayment = new LoanPayment(params)

        assert loanPayment.save() != null

        // test invalid parameters in update
        params.id = loanPayment.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/loanPayment/edit"
        assert model.loanPaymentInstance != null

        loanPayment.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/loanPayment/show/$loanPayment.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        loanPayment.clearErrors()

        populateValidParams(params)
        params.id = loanPayment.id
        params.version = -1
        controller.update()

        assert view == "/loanPayment/edit"
        assert model.loanPaymentInstance != null
        assert model.loanPaymentInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/loanPayment/list'

        response.reset()

        populateValidParams(params)
        def loanPayment = new LoanPayment(params)

        assert loanPayment.save() != null
        assert LoanPayment.count() == 1

        params.id = loanPayment.id

        controller.delete()

        assert LoanPayment.count() == 0
        assert LoanPayment.get(loanPayment.id) == null
        assert response.redirectedUrl == '/loanPayment/list'
    }
}
