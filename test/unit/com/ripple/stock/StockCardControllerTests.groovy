package com.ripple.stock



import org.junit.*
import grails.test.mixin.*

@TestFor(StockCardController)
@Mock(StockCard)
class StockCardControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/stockCard/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.stockCardInstanceList.size() == 0
        assert model.stockCardInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.stockCardInstance != null
    }

    void testSave() {
        controller.save()

        assert model.stockCardInstance != null
        assert view == '/stockCard/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/stockCard/show/1'
        assert controller.flash.message != null
        assert StockCard.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/stockCard/list'


        populateValidParams(params)
        def stockCard = new StockCard(params)

        assert stockCard.save() != null

        params.id = stockCard.id

        def model = controller.show()

        assert model.stockCardInstance == stockCard
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/stockCard/list'


        populateValidParams(params)
        def stockCard = new StockCard(params)

        assert stockCard.save() != null

        params.id = stockCard.id

        def model = controller.edit()

        assert model.stockCardInstance == stockCard
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/stockCard/list'

        response.reset()


        populateValidParams(params)
        def stockCard = new StockCard(params)

        assert stockCard.save() != null

        // test invalid parameters in update
        params.id = stockCard.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/stockCard/edit"
        assert model.stockCardInstance != null

        stockCard.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/stockCard/show/$stockCard.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        stockCard.clearErrors()

        populateValidParams(params)
        params.id = stockCard.id
        params.version = -1
        controller.update()

        assert view == "/stockCard/edit"
        assert model.stockCardInstance != null
        assert model.stockCardInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/stockCard/list'

        response.reset()

        populateValidParams(params)
        def stockCard = new StockCard(params)

        assert stockCard.save() != null
        assert StockCard.count() == 1

        params.id = stockCard.id

        controller.delete()

        assert StockCard.count() == 0
        assert StockCard.get(stockCard.id) == null
        assert response.redirectedUrl == '/stockCard/list'
    }
}
