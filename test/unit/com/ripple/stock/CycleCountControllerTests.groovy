package com.ripple.stock



import org.junit.*
import grails.test.mixin.*

@TestFor(CycleCountController)
@Mock(CycleCount)
class CycleCountControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/cycleCount/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.cycleCountInstanceList.size() == 0
        assert model.cycleCountInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.cycleCountInstance != null
    }

    void testSave() {
        controller.save()

        assert model.cycleCountInstance != null
        assert view == '/cycleCount/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/cycleCount/show/1'
        assert controller.flash.message != null
        assert CycleCount.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/cycleCount/list'


        populateValidParams(params)
        def cycleCount = new CycleCount(params)

        assert cycleCount.save() != null

        params.id = cycleCount.id

        def model = controller.show()

        assert model.cycleCountInstance == cycleCount
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/cycleCount/list'


        populateValidParams(params)
        def cycleCount = new CycleCount(params)

        assert cycleCount.save() != null

        params.id = cycleCount.id

        def model = controller.edit()

        assert model.cycleCountInstance == cycleCount
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/cycleCount/list'

        response.reset()


        populateValidParams(params)
        def cycleCount = new CycleCount(params)

        assert cycleCount.save() != null

        // test invalid parameters in update
        params.id = cycleCount.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/cycleCount/edit"
        assert model.cycleCountInstance != null

        cycleCount.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/cycleCount/show/$cycleCount.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        cycleCount.clearErrors()

        populateValidParams(params)
        params.id = cycleCount.id
        params.version = -1
        controller.update()

        assert view == "/cycleCount/edit"
        assert model.cycleCountInstance != null
        assert model.cycleCountInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/cycleCount/list'

        response.reset()

        populateValidParams(params)
        def cycleCount = new CycleCount(params)

        assert cycleCount.save() != null
        assert CycleCount.count() == 1

        params.id = cycleCount.id

        controller.delete()

        assert CycleCount.count() == 0
        assert CycleCount.get(cycleCount.id) == null
        assert response.redirectedUrl == '/cycleCount/list'
    }
}
