package com.ripple.stock



import org.junit.*
import grails.test.mixin.*

@TestFor(CycleCountItemsController)
@Mock(CycleCountItems)
class CycleCountItemsControllerTests {


    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/cycleCountItems/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.cycleCountItemsInstanceList.size() == 0
        assert model.cycleCountItemsInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.cycleCountItemsInstance != null
    }

    void testSave() {
        controller.save()

        assert model.cycleCountItemsInstance != null
        assert view == '/cycleCountItems/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/cycleCountItems/show/1'
        assert controller.flash.message != null
        assert CycleCountItems.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/cycleCountItems/list'


        populateValidParams(params)
        def cycleCountItems = new CycleCountItems(params)

        assert cycleCountItems.save() != null

        params.id = cycleCountItems.id

        def model = controller.show()

        assert model.cycleCountItemsInstance == cycleCountItems
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/cycleCountItems/list'


        populateValidParams(params)
        def cycleCountItems = new CycleCountItems(params)

        assert cycleCountItems.save() != null

        params.id = cycleCountItems.id

        def model = controller.edit()

        assert model.cycleCountItemsInstance == cycleCountItems
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/cycleCountItems/list'

        response.reset()


        populateValidParams(params)
        def cycleCountItems = new CycleCountItems(params)

        assert cycleCountItems.save() != null

        // test invalid parameters in update
        params.id = cycleCountItems.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/cycleCountItems/edit"
        assert model.cycleCountItemsInstance != null

        cycleCountItems.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/cycleCountItems/show/$cycleCountItems.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        cycleCountItems.clearErrors()

        populateValidParams(params)
        params.id = cycleCountItems.id
        params.version = -1
        controller.update()

        assert view == "/cycleCountItems/edit"
        assert model.cycleCountItemsInstance != null
        assert model.cycleCountItemsInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/cycleCountItems/list'

        response.reset()

        populateValidParams(params)
        def cycleCountItems = new CycleCountItems(params)

        assert cycleCountItems.save() != null
        assert CycleCountItems.count() == 1

        params.id = cycleCountItems.id

        controller.delete()

        assert CycleCountItems.count() == 0
        assert CycleCountItems.get(cycleCountItems.id) == null
        assert response.redirectedUrl == '/cycleCountItems/list'
    }
}
